<?php

    if (!isset($_SESSION))
    {
    session_start();
    }

?>
<?php include "config/header.php" ?>

<body class="header_sticky">   
    <!-- Preloader -->
<!--    <section class="loading-overlay">-->
<!--        <div class="Loading-Page">-->
<!--            <h2 class="loader">Loading</h2>-->
<!--        </div>-->
<!--    </section> -->

    <!-- Boxed -->
    <div class="boxed">

<?php

include 'config/logged_in_user.php';
include 'config/menu.php';
?>
    

<?php
if (isset($_SESSION['email']))
    {
    $email=$_SESSION['email'];
    $sql=mysqli_query($connection,"SELECT * FROM user_details where email='$email'");
    while ($row=mysqli_fetch_array($sql))
    {
        $user_id=$row['user_id'];
        $user_role=$row['user_role'];
    }
    
 
 ?>




    <section class="flat-row page-addlisting">
        <div class="container">
            <div class="add-filter">
                <div class="row">
                    <div class="col-md-12 wrap-accadion">
                        <form method="post" action="page-addlisting.php" class="filter-form form-addlist" enctype="multipart/form-data">
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Business Title</label>
                                    <input type="text" class="form-control" name="title" id="title" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Postcode,Suburb,State</label>
                                    <input type="text" name="postcode" id="title" required>
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Image</label>
                                    <input type="file" name="image[]" multiple="multiple" required>
                                </div>


                                <?php
                                if ($user_role=='gold')
                                {
                                ?>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Video</label>
                                    <input type="text" name="video" id="title" placeholder="Enter last 11 digit of video link" required>
                                </div>

                                <?php } ?>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Facebook</label>
                                    <input type="text" name="facebook" id="title" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Twitter</label>
                                    <input type="text" name="twitter" id="title" >
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Linkedin</label>
                                    <input type="text" name="linkedin" id="title" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Youtube</label>
                                    <input type="text" name="youtube" id="title" >
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Android App</label>
                                    <input type="text" name="android_app" id="title" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">IOS App</label>
                                    <input type="text" name="ios_app" id="title" >
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <div class="col-md-6">
                                        <label class="nhan">Map</label>
                                        <input type="text" name="map" id="title" >
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Address</label>
                                    <input type="text" name="address" id="title" required>
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Telephone</label>
                                    <input type="text" name="telephone" id="title" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Mobile</label>
                                    <input type="text" name="mobile" id="title" required>
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Email</label>
                                    <input type="text" name="email" id="title" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Website</label>
                                    <input type="text" name="website" id="title" required>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="nhan">About Us</label>
                                <textarea cols="4" rows="4" name="about_us" required></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="nhan">Services</label>
                                <textarea cols="4" rows="4" name="services" required></textarea>
                            </div>

                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Status</label>
                                    <select name="status">
                                        <option value="Published">Choose...</option>
                                        <option value="Published">Publish</option>
                                        <option value="Draft">Draft</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="nhan">Category</label>
                                    <select name="category">
                                        <option value="">Select category</option>
                                        <?php
                                        $q=mysqli_query($connection,"SELECT * FROM business_directory_category");
                                        while($res=mysqli_fetch_array($q)) {
                                            $bdc_id = $res['bdc_id'];
                                            $bdc_title = $res['bdc_title'];
                                            ?>

                                            <option value="<?php echo $bdc_id ?>"><?php echo $bdc_title;?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="button-addlisting">
                                <button type="submit" name="submit" class="flat-button"">Add Listing</button>
                            </div>

                        </form>

                        <!--From action-->
                        <?php
                        if (isset($_POST['submit']))
                        {
                            //$image=$_FILES['image']['name'];
                            //$image_tmp=$_FILES['image']['tmp_name'];
                            $video=$_POST['video'];
                            $title=$_POST['title'];
                            $postcode=$_POST['postcode'];
                            $facebook=$_POST['facebook'];
                            $twitter=$_POST['twitter'];
                            $linkedin=$_POST['linkedin'];
                            $youtube=$_POST['youtube'];
                            $android_app=$_POST['android_app'];
                            $ios_app=$_POST['ios_app'];
                            $map=$_POST['map'];
                            $address=$_POST['address'];
                            $telephone=$_POST['telephone'];
                            $mobile=$_POST['mobile'];
                            $email=$_POST['email'];
                            $website=$_POST['website'];
                            $about_us=$_POST['about_us'];
                            $services=$_POST['services'];
                            $status=$_POST['status'];

                            $category=$_POST['category'];
                            $unique_id=mt_rand(50,9999999);

                            $insert_query="INSERT INTO business_directory_details(bd_user_id,
                                            bd_title,bd_postcode_suburb_state,bd_facebook,
                                            bd_twitter,bd_linkedin,bd_youtube,bd_android_app,
                                            bd_ios_app,bd_map,bd_contact_address,bd_contact_telephone,
                                            bd_contact_mobile,bd_contact_email,bd_contact_website,bd_about_us,
                                            bd_services,bd_unique_id,bd_status,bd_category) values ('$user_id','$title','$postcode','$facebook',
                                            '$twitter','$linkedin','$youtube','$android_app','$ios_app',
                                            '$map','$address','$telephone','$mobile','$email','$website',
                                            '$about_us','$services','$unique_id','$status','$category')";
                            if (mysqli_query($connection,$insert_query))
                            {
                                $id_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details 
                                                                                where bd_user_id='$user_id' and bd_unique_id='$unique_id'");
                                if ($result=mysqli_fetch_array($id_search_query))
                                {
                                    $bd_id=$result['bd_id'];

                                    $counter = 0;
                                    $uploadedFiles = array();
                                    $errors = array();
                                    $extension = array("jpeg","jpg","png","gif");

                                    if ($user_role=="regular")
                                    {
                                        foreach($_FILES["image"]["tmp_name"] as $key=>$tmp_name)
                                        {
                                            $temp = $_FILES["image"]["tmp_name"][$key];
                                            $name = $_FILES["image"]["name"][$key];

                                            if (empty($temp)) {
                                                break;
                                            }

                                            $counter++;
                                            if ($counter>5)
                                            {

                                                break;
                                            }
                                            $ext = pathinfo($name, PATHINFO_EXTENSION);
                                            if(in_array($ext, $extension) == false){
                                                $UploadOk = false;
                                                array_push($errors, $name." is invalid file type.");
                                            }
                                            move_uploaded_file($temp,"image/$name");
                                            $image_insert_query="INSERT INTO business_directory_image(bdi_user_id,bdi_bd_id,bdi_image)
                                                              VALUES ('$user_id','$bd_id','$name')";
                                            mysqli_query($connection,$image_insert_query);
                                        }
                                        $video_insert_query="INSERT INTO business_directory_video(bdv_user_id,bdv_bd_id,bdv_video)
                                                              VALUES ('$user_id','$bd_id','$video')";
                                        if (mysqli_query($connection,$video_insert_query))
                                        {
                                            echo "<script> alert('Your upload limit is 5.images uploaded.') </script>";
                                        }
                                    }
                                    else
                                    {
                                        foreach($_FILES["image"]["tmp_name"] as $key=>$tmp_name)
                                        {
                                            $temp = $_FILES["image"]["tmp_name"][$key];
                                            $name = $_FILES["image"]["name"][$key];

                                            if (empty($temp)) {
                                                break;
                                            }

                                            $counter++;
                                            $ext = pathinfo($name, PATHINFO_EXTENSION);
                                            if(in_array($ext, $extension) == false){
                                                $UploadOk = false;
                                                array_push($errors, $name." is invalid file type.");
                                            }
                                            move_uploaded_file($temp,"image/$name");
                                            $image_insert_query="INSERT INTO business_directory_image(bdi_user_id,bdi_bd_id,bdi_image)
                                                              VALUES ('$user_id','$bd_id','$name')";
                                            mysqli_query($connection,$image_insert_query);
                                        }
                                        $video_insert_query="INSERT INTO business_directory_video(bdv_user_id,bdv_bd_id,bdv_video)
                                                              VALUES ('$user_id','$bd_id','$video')";
                                        if (mysqli_query($connection,$video_insert_query))
                                        {
                                            echo "<script> alert('Successful.') </script>";
                                        }
                                    }

                                }
                            }
                        }
                        ?>



                    </div>
                </div>
            </div>


        </div>
    </section>  

<?php }


 ?>
        <?php

        if (!isset($_SESSION['email']))
        { echo "<h1 class='text-center'>Please login first.</h1>"; }
        ?>


<?php include "config/footer.php" ?>



</body>
</html>