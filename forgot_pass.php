<?php include "config/header.php" ?>

<body class="header_sticky">

<div class="boxed">

    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    ?>
    <div class="text-center" style="min-height: 410px;">
        <h2>Reset Password</h2>

        <form class="form-group text-center" action="" method="post">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <form action="" class="form-group">
                        <label style="color: #0a0a0b; font-size: 15px; margin-left: -36rem;">Enter Email</label>
                        <input type="email" class="form-control" style="border-color: #0a0a0b" name="email" placeholder="Enter your Email" id="title" required>

                        <button type="submit" name="submit" style="background-color: #e8280b;"  class="btn btn-danger">Submit</button>

                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>


        </form>

    </div>


    <?php
    if(isset($_POST['submit'])) {
        //email verify
        $email = $_POST['email'];

        $sql = mysqli_query($connection, "SELECT * FROM user_details where email='$email'");
        if (mysqli_num_rows($sql) >0) {

            $res = mysqli_fetch_array($sql);
            $given_name = $res['given_name'];
            $email = $res['email'];
            $password = $res['password'];

        } else {

            echo "<h5 style='color:darkred; text-align:center;'><b>Email is not registered.</b></h5> <br> <br>";
        }

        // EDIT THE 2 LINES BELOW AS REQUIRED
        $email_to = $email;
        $email_subject = "Forgot Password";

        $email_from = "ashiqtechsolutions@gmail.com";
        $error_message = "";
        $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

        if(!preg_match($email_exp,$email)) {
            $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
        }


        $email_message = "Your Password:\n\n";


        function clean_string($string) {
            $bad = array("content-type","bcc:","to:","cc:","href");
            return str_replace($bad,"",$string);
        }


        global $given_name;
        global $password;

        $email_message .= "Name: ".clean_string($given_name)."\n";
       // $email_message .= "Phone Number: ".clean_string($number)."\n";
        $email_message .= "Email: ".clean_string($email_from)."\n";
        $email_message .= "Subject: ".clean_string($email_subject)."\n";
        $email_message .= "Message: ".clean_string($password)."\n";

// create email headers
        $headers = 'From: '.$email_from."\r\n".
            'Reply-To: '.$email_from."\r\n" .
            'X-Mailer: PHP/' . phpversion();
        if (@mail($email_to, $email_subject, $email_message, $headers))
        {
            echo "<h5 style='color:#077647; text-align:center;'><b>Thanks so much for your message. We check e-mail frequently and will try our best to respond to your inquiry.</b></h5> <br> <br>";
        }
        else
        {
            echo "<h5 style='color:darkred; text-align:center;'><b>Email couldn't sent.</b></h5> <br> <br>";
        }

    }
    ?>


    <?php include "config/footer.php" ?>

</body>
</html>

