-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2018 at 12:03 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chamber_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `about_us_id` int(11) NOT NULL,
  `about_us_title` varchar(255) NOT NULL,
  `about_us_content` text NOT NULL,
  `about_us_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`about_us_id`, `about_us_title`, `about_us_content`, `about_us_image`) VALUES
(1, 'About Us', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'we.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_governance`
--

CREATE TABLE `about_us_governance` (
  `governance_id` int(11) NOT NULL,
  `governance_title` varchar(255) NOT NULL,
  `governance_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us_governance`
--

INSERT INTO `about_us_governance` (`governance_id`, `governance_title`, `governance_content`) VALUES
(1, 'Governance', '<h3><a href=\"http://chamber.techsolutions-global.com/board-of-management\">Board M</a>anagement</h3><p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_history`
--

CREATE TABLE `about_us_history` (
  `history_id` int(11) NOT NULL,
  `history_title` varchar(255) NOT NULL,
  `history_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us_history`
--

INSERT INTO `about_us_history` (`history_id`, `history_title`, `history_content`) VALUES
(1, 'About Us', '<p>The Bayside Chamber of Commerce consists of a group of professionals &amp; business owners, who are committed to connect, assist and support each other in the interest of improving the Bayside Business community.</p><p>The Bayside area is one of the most interesting and diverse areas to visit in Sydney. Especially our beaches such as Brighton Le Sands,&nbsp; Ramsgate and Dolls Point, Shopping is plentiful&nbsp; such as the big Westfield complex at&nbsp; Eastgarden as well as a choice of many ethnic eating places in Rockdale, Arncliffee, Brighton Le Sands, Eastgardens and Mascot. We are conveniently located a short distance from Sydney domestic and international Airport and the Sydney CBD.</p><p>What we do for our Members:</p><ul>	<li>To meets monthly to advance its work program.</li>	<li>To promote the business interests of members;</li>	<li>To promote and facilitate communication between busineses through our website and regular meeting;</li>	<li>To encourag residents to support local business;</li>	<li>To ensure transport, parking and pedestrian facilities meet the needs of the community and businesses</li>	<li>To promote the benefits of shopping in Bayside Area;</li>	<li>To increase commercial activity for local businesses;</li>	<li>To provide networking, learning and development for members;</li>	<li>To act as a single voice for local businesses to represent their collective views to all levels of government, industry and the community;</li>	<li>Support a vibrant and flourishing business community that contributes positively to the broader local community;</li>	<li>	<p>To represent the interests of the business community to government, local government and statutory authorities</p>	</li></ul>');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_members_forum`
--

CREATE TABLE `about_us_members_forum` (
  `members_forum_id` int(11) NOT NULL,
  `members_forum_name` varchar(255) NOT NULL,
  `members_forum_designation` varchar(255) NOT NULL,
  `members_forum_info` text NOT NULL,
  `members_forum_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us_members_forum`
--

INSERT INTO `about_us_members_forum` (`members_forum_id`, `members_forum_name`, `members_forum_designation`, `members_forum_info`, `members_forum_date`) VALUES
(3, 'Arifur Rahman', 'Programmer', 'Demo text', '2017-10-10 09:33:05'),
(4, 'Farid Ahmed', 'CEO', 'Gess Australia\r\nRockdale, Australia. ', '2017-10-22 11:16:06'),
(5, 'Faisal', 'Membership ', '', '2017-11-06 02:39:53');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_our_mission`
--

CREATE TABLE `about_us_our_mission` (
  `our_mission_id` int(11) NOT NULL,
  `our_mission_title` varchar(255) NOT NULL,
  `our_mission_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us_our_mission`
--

INSERT INTO `about_us_our_mission` (`our_mission_id`, `our_mission_title`, `our_mission_content`) VALUES
(1, 'Our Mission', '<p>Our mission is to<strong> </strong>promote and meet the needs of business and industry and to create the best community in which to live, work and do business. The mission&quot; of the Bayside Chamber of Commerce is to provide leadership for the advancement of economic vitality and equality of life for the total community.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_our_staff`
--

CREATE TABLE `about_us_our_staff` (
  `our_staff_id` int(11) NOT NULL,
  `our_staff_name` varchar(255) NOT NULL,
  `our_staff_designation` varchar(255) NOT NULL,
  `our_staff_image` text NOT NULL,
  `our_staff_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `about_us_our_staff`
--

INSERT INTO `about_us_our_staff` (`our_staff_id`, `our_staff_name`, `our_staff_designation`, `our_staff_image`, `our_staff_date`) VALUES
(1, 'Farid Ahmed', 'CEO', '1508672479221729_1028077860527_1350_n.jpg', '2017-10-22 11:41:19');

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `admin_id` int(11) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`admin_id`, `admin_username`, `admin_password`) VALUES
(1, 'admin@chamberofcommerce.com', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
(2, 'techsolutions', 'd3d6459872ab6ca5930b88c86afba57021b44910');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `album_id` int(11) NOT NULL,
  `album_title` varchar(255) NOT NULL,
  `album_image` text NOT NULL,
  `album_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`album_id`, `album_title`, `album_image`, `album_date`) VALUES
(1, 'Christmas Party', '1512972059christmas-party.jpg', '2017-12-11 06:00:59');

-- --------------------------------------------------------

--
-- Table structure for table `album_photo`
--

CREATE TABLE `album_photo` (
  `album_photo_id` int(11) NOT NULL,
  `album_photo_c_id` int(11) NOT NULL,
  `album_photo_name` varchar(255) NOT NULL,
  `album_photo_image` text NOT NULL,
  `album_photo_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album_photo`
--

INSERT INTO `album_photo` (`album_photo_id`, `album_photo_c_id`, `album_photo_name`, `album_photo_image`, `album_photo_date`) VALUES
(1, 1, 'Christmas Party Annoucement', '151297281324232162_198533880706221_4171814665627039536_n.jpg', '2017-12-11 06:13:33'),
(2, 1, 'Perticipents', '151297283624775044_198811940678415_1626973651592704269_n.jpg', '2017-12-11 06:13:56'),
(3, 1, 'Board Members', '151297289324296844_198811864011756_1538390141103383509_n.jpg', '2017-12-11 06:14:53'),
(4, 1, 'Christmas Party ', '151297291524796394_198811844011758_3505371604339754559_n.jpg', '2017-12-11 06:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL,
  `banner_image` text NOT NULL,
  `banner_image_title` varchar(255) NOT NULL,
  `banner_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_image`, `banner_image_title`, `banner_date`) VALUES
(1, '1508661497s1.jpg', '1', '2017-10-22 08:38:17'),
(5, '1509929237King Street.jpg', 'rockdale', '2017-11-06 00:45:24'),
(6, '1509929266Rockdale Council.jpg', 'rockdale', '2017-11-06 00:45:52'),
(8, '1510033477Sydney Airport.jpg', 'Sydney Airport', '2017-11-07 05:42:43'),
(9, '1510033716Novotel-Brighton-2400x600.jpg', 'Brighton Le Sands', '2017-11-07 05:46:42'),
(10, '1510035366Port Botany.jpg', 'Port Botany', '2017-11-07 06:14:12'),
(11, '1510036661Sydney Airport1.jpg', 'Sydney Airport', '2017-11-07 06:35:48');

-- --------------------------------------------------------

--
-- Table structure for table `bottom_left_ad`
--

CREATE TABLE `bottom_left_ad` (
  `bottom_left_ad_id` int(11) NOT NULL,
  `bottom_left_ad_image` text NOT NULL,
  `bottom_left_ad_image_title` varchar(255) NOT NULL,
  `bottom_left_ad_link` varchar(255) NOT NULL,
  `bottom_left_ad_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bottom_left_ad`
--

INSERT INTO `bottom_left_ad` (`bottom_left_ad_id`, `bottom_left_ad_image`, `bottom_left_ad_image_title`, `bottom_left_ad_link`, `bottom_left_ad_date`) VALUES
(1, 'ad222.jpg', '1', '#', '2017-11-08 07:55:32'),
(2, 'ad222.jpg', '2', '#', '2017-11-08 07:55:40'),
(3, 'ad222.jpg', '3', '#', '2017-11-08 07:56:16'),
(4, 'ad222.jpg', '4', '#', '2017-11-08 07:56:23'),
(5, 'ad222.jpg', '5', '#', '2017-11-08 07:56:34'),
(6, 'ad222.jpg', '6', '#', '2017-11-08 07:56:47'),
(7, 'ad222.jpg', '7', '#', '2017-11-08 07:56:55'),
(8, 'ad222.jpg', '8', '#', '2017-11-08 07:57:08'),
(9, 'ad222.jpg', '9', '#', '2017-11-08 07:57:18'),
(10, 'ad222.jpg', '10', '#', '2017-11-08 07:57:26'),
(11, 'ad222.jpg', '11', '#', '2017-11-08 07:57:40'),
(12, 'ad222.jpg', '12', '#', '2017-11-08 07:57:56'),
(13, 'ad222.jpg', '13', '#', '2017-11-08 07:58:10'),
(14, 'ad222.jpg', '14', '#', '2017-11-08 07:58:24'),
(15, 'ad222.jpg', '15', '#', '2017-11-08 07:58:56'),
(16, 'ad222.jpg', '16', '#', '2017-11-08 07:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `bottom_right_ad`
--

CREATE TABLE `bottom_right_ad` (
  `bottom_right_ad_id` int(11) NOT NULL,
  `bottom_right_ad_image` text NOT NULL,
  `bottom_right_ad_image_title` varchar(255) NOT NULL,
  `bottom_right_ad_link` varchar(255) NOT NULL,
  `bottom_right_ad_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bottom_right_ad`
--

INSERT INTO `bottom_right_ad` (`bottom_right_ad_id`, `bottom_right_ad_image`, `bottom_right_ad_image_title`, `bottom_right_ad_link`, `bottom_right_ad_date`) VALUES
(1, 'ad222.jpg', '1', '#', '2017-11-08 08:16:32'),
(2, 'ad222.jpg', '2', '#', '2017-11-08 08:16:40'),
(3, 'ad222.jpg', '3', '#', '2017-11-08 08:16:47'),
(4, 'ad222.jpg', '4', '#', '2017-11-08 08:16:55'),
(5, 'ad222.jpg', '5', '#', '2017-11-08 08:17:03'),
(6, 'ad222.jpg', '6', '#', '2017-11-08 08:17:11'),
(7, 'ad222.jpg', '7', '#', '2017-11-08 08:17:20'),
(8, 'ad222.jpg', '8', '#', '2017-11-08 08:17:28'),
(9, 'ad222.jpg', '9', '#', '2017-11-08 08:17:37'),
(10, 'ad222.jpg', '10', '#', '2017-11-08 08:17:47'),
(11, 'ad222.jpg', '11', '#', '2017-11-08 08:17:56'),
(12, 'ad222.jpg', '12', '#', '2017-11-08 08:18:59'),
(13, 'ad222.jpg', '13', '#', '2017-11-08 08:18:48'),
(14, 'ad222.jpg', '14', '#', '2017-11-08 08:18:30'),
(15, 'ad222.jpg', '15', '#', '2017-11-08 08:18:17'),
(16, 'ad222.jpg', '16', '#', '2017-11-08 08:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `business_category`
--

CREATE TABLE `business_category` (
  `category_id` int(11) NOT NULL,
  `category_title` varchar(255) NOT NULL,
  `category_image` text NOT NULL,
  `category_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_category`
--

INSERT INTO `business_category` (`category_id`, `category_title`, `category_image`, `category_date`) VALUES
(3, 'A TO Z', '15088217321.jpg', '2017-10-24 05:38:52'),
(4, 'Hotel & Accomodation', '15088218302.jpg', '2017-11-22 08:57:22'),
(5, 'Priting & Design', '1508821848artist-2.jpg', '2017-11-22 08:58:12'),
(6, 'BANKS', '1508821865banks.jpg', '2017-10-24 05:11:05'),
(7, 'Restaurant and Cafee', 'Restaurant.jpg', '2017-11-22 09:08:33'),
(8, 'Function Centres', 'Function photo.jpg', '2017-11-22 09:08:55'),
(9, 'Migration and Student Admission', 'Student Admissions and Migration.jpg', '2017-11-22 09:57:01'),
(10, 'Travel & Ticketing', 'Travel and Ticketing.jpg', '2017-11-22 10:01:33'),
(11, 'Accounting and Taxation', 'Accounting and Taxation.jpg', '2017-11-22 09:04:49'),
(12, 'Real Estate and Strata', '1511342234Real Estate Logo.jpg', '2017-11-22 09:15:21'),
(13, 'Cleaning Services', 'Cleaning Logo.jpg', '2017-11-23 03:14:55');

-- --------------------------------------------------------

--
-- Table structure for table `business_organization`
--

CREATE TABLE `business_organization` (
  `b_o_id` int(11) NOT NULL,
  `b_o_c_id` varchar(255) NOT NULL,
  `b_o_name` varchar(255) NOT NULL,
  `b_o_short_info` text NOT NULL,
  `b_o_contact` text NOT NULL,
  `b_o_link` text NOT NULL,
  `b_o_image` text NOT NULL,
  `b_o_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_organization`
--

INSERT INTO `business_organization` (`b_o_id`, `b_o_c_id`, `b_o_name`, `b_o_short_info`, `b_o_contact`, `b_o_link`, `b_o_image`, `b_o_date`) VALUES
(1, '9', 'GESS AUSTRALIA', 'Australia is still a young country and offers many opportunities for further education, employment & Permanent Residency.  The Australian education system is of a high standard and tertiary qualifications are recognized world-wide.  Job opportunities are many and varied.  To gain permanent employment in Australia will be of great benefit to you & your family that provides even greater opportunities and security for the future of your children. GESS Australia will assist you with every step of the visa application process using cost effective way. We focus on providing high-quality service and customer satisfaction. We offer a range of services in relation to Visa Application, Review Tribunal and Citizenship matter.With a variety of offerings to choose from, we\\\'re sure you\\\'ll be happy to deal with us. Look around our website and if you have any questions , please feel free to contact us. We hope to see you again! Check back later for new updates to our website. There\\\'s much more to come! GESS Australia is a trusted firm that helps people with all types overseas student admissions, visa applications to DIBP and other immigration & citizenship related matters.', ' Street Address:\r\nRockdale Gardens, \r\nIris Tower \r\nShop 2, 5 Keats Ave\r\nRockdale, NSW-2216\r\n\r\nPhone: 02 8003 8006, 02 8355 5514, 02 8387 7701\r\n              02 8355 5517, 02 8355 5518\r\n\r\nMobile: 0402 22 22 11\r\nwww.gessaustralia.com.au', 'www.gessaustralia.com.au', 'Gess Logo.jpg', '2017-11-22 10:06:49'),
(2, '10', 'Subcontinental Travel', 'Subcontinental Travel offers international travel service to our valued clients with cheaper flights to most countries in the world.  Our Firm offers a full range of travel products such as international and domestic Travel Tickets, Hotel and car Booking and Travel insurance etc.  We have the expertise of travel staff who can provide all our clients with a personalized, professional service, from initial enquiries through to final arrangements, with the best possible value for money.  Our experienced travel service team specialises in providing travel products at discounted prices for passenger. We can also issue ticket from our office regardless of where you travel from, For example, if someone from Bangladesh would like to travel from Bangladesh to Australia, we can issue cheaper ticket for your relatives from Sydney. We deliver a friendly, reliable, quality service with a focus on the individual travellerâ€™s specific needs and expectations.', ' Street Address:\r\nRockdale Gardens, \r\nIris Tower \r\nShop 2, 5 Keats Ave\r\nRockdale, NSW-2216\r\n\r\nPhone: 02 8003 8006, 02 8355 5514, 02 8387 7701\r\n              02 8355 5517, 02 8355 5518\r\n\r\nMobile: 0402 22 22 11\r\nwww.subcontinentaltravel.com.au', 'www.subcontinentaltravel.com.au', 'Travel Logo.jpg', '2017-11-22 10:07:26'),
(3, '11', 'Voyager Accounting Services', 'Voyager Accounting Services is an accounting, business advisory and consulting firm, offering clients exceptional quality services with maintaining high professionalism. We are dedicated to providing our clients with excellent r professional, personalized services in the wide range of tax, accounting, financial, and business worlds. Tax and accounting done with the utmost knowledge, integrity, trust, passion, and security â€“ thatâ€™s what we are all about. We are also committed to forming close relationship with our clients, enabling us to understand your unique situation and customise the assistance we provide to suit your requirements. ', '\r\nVoyager Accounting Services\r\n\r\nRoom 2; Suite 2\r\n5 Keats Avenue; ROCKDALE\r\nNSW 2216\r\nTel: 02 -8355 5514\r\nM : 0401 359 427\r\nEmail : quamrulkhan@voyageraccounting.com or info@voyageraccouting.com', 'www.voyageraccounting.com', '1511179057Voyager.png', '2017-11-20 11:55:44'),
(4, '9', 'Internation Migration and Education Services', 'Introduction:\r\n\r\nInternational Migration & Education Services (IMES) Pty Ltd is a leading education consulting firm working with fame since 2009. We are recognized as a trustworthy partner to help our client. Having qualified education counselor team, IMES has been cooperating professionals and students to achieve their goals. We are dedicated to help onshore and offshore students to enroll different college and universities in Australia. Not only in studentship, but we also have the expertise to advise you in your career as an advantage. By proving such service with the efficiency we have enrolled more than 2900 students in different colleges and university in Australia since its insertion in the year of 2009. We have the client testimony with satisfaction.\r\n\r\n\r\nPreface:\r\n\r\nIMES attributes its success to people who constitute IMES and the ever-growing client base largely because of the referrals and strict adherence to ethical policies. It provides step-by-step guidance to its entire client base, not through standard steps rather defining each clientâ€™s individual requirements with pre and post departure arrangements all less than one roof. The center is dedicated to providing strategic advice and support in a professional way. IMES has the experience as well as the expertise to help you realize your dream and advice the client according to their career.\r\n\r\n\r\nBackground of IMES:\r\n\r\nInternational Migration & Education Services (IMES) was founded on May 24, 2015, by Mr. Soyab Ahmed. Previously, Mr. Soyab worked in student consultancy firm and he joined the firm in 2009. Since then, he has personally enrolled more than 2900 students in different colleges and universities. Since IMES establishes\r\n\r\nA. More than 700+ students have been enrolled in different colleges and universities in Australia.\r\n\r\nB. Migration assistance served more than 100+ clients.\r\n\r\nC. Enrolled Professional year courses are 60+\r\n\r\nD. RPL qualification arranged 150+\r\n\r\nE. Internship arranged 5+\r\n\r\nF. Other services 100+', 'http://imesaustralia.com.au/contactus', 'http://imesaustralia.com.au/', '1511321614IMES Logo.png', '2017-11-22 03:31:41'),
(5, '8', 'Red Rose Function Centre', 'We are a Function Venue of Hut-Bazaar Restaurant, which you can have for your almost any type of parties, starting from personal/ friends & family programs to official conference. We offer a full package including food. Depending on your requirements, we also offer Balloon decorations, Cards, Posters, as well as full wedding stage setup.\r\n\r\nA dedicated, professional and friendly team of floor staff and function coordinators will ensure your amazing experience for your program in Red Rose Function Centre, giving you an absolute peace of mind.\r\n\r\nJust beside Red Rose, located our Restaurant, Hut-Bazaar, the famous and authentic place for Bangladeshi and sub-continental cuisine. Being established in 2007, Alhamdu-Lillah, We feel proud of being the first Bangladeshi restaurant in Sydney.\r\n\r\nAccordingly we ourselves provide the catering in the functions in this venue; so it becomes easier for us to make sure quality food is served. We would like to say- we, Hut-Bazaar, are the first to be a Gold Licence Caterer in the community and maintaining it since several years!\r\n\r\nHowever, while we mostly offer Bangladeshi, Indian, Nepalese or Pakistani food in our venue, at the same time we have done programs with Lebanese, Thai or Chinese food. So, whatever you have in your mind about food, we are open to discuss!\r\n\r\nBut we are not only doing programs in our venue. If you got your function booked in some other venues but prefer catering from us; we have that options as well. Depending on your requirements, we can offer both Delivery and On Site Service!', 'Contact Us At ::\r\n\r\n96A Railway St, Rockdale, NSW 2216.    \r\nAziz: 0424 714 934\r\nSharif: 0434 197 785\r\nOffice: 02 9567 1984\r\n\r\nTo visit the venue and discuss, pls call us first to have an appointment.', 'www.redrosefunctioncentre.com.au', '1511342064Red Rose Logo.png', '2017-11-22 09:12:31'),
(6, '12', 'W Herrmann Real Estate', 'W. Herrmann Real Estate first opened their doors in 1969. From its inception, the directors were determined that the office would provide complete real estate service to the community.As a family run business, emphasis has always been on friendly, efficient service backed by professional expertise and industry experience. ', 'Address: 20 King St, Rockdale NSW 2216, Australia\r\nContact: (02) 9597 3655\r\nOpening Hours:\r\nMon  9:00 am â€“ 5:00 pm\r\nTue   9:00 am â€“ 5:00 pm\r\nWed  9:00 am â€“ 5:00 pm\r\nThu   9:00 am â€“ 5:00 pm\r\nFri    9:00 am â€“ 5:00 pm\r\nSat   Closed\r\nSun  Closed', 'http://www.wherrmann.com.au', 'W hermann logo.jpg', '2017-11-22 09:45:03'),
(7, '5', 'AOZ Prints and Graphics', 'Bar + Grill is a WordPress theme designed to the exacting standards of modern restaurants & â€œlocalâ€ businesses. The theme comes bundled with several premium features, including:\r\n\r\n    Theme Customizer: Unlimited Font & Color Options\r\n    Design options for for white tablecloth restaurants, small pubs & everything in between\r\n    The Revolution Slider\r\n    Drag & Drop Content via Visual Composer\r\n    Open Table Integration\r\n    Foursquare Integration\r\n    Social Media Integration\r\n    Menu Templates\r\n    Responsive Layouts (great for mobile devices)\r\n    WooCommerce Integration (great for Gift Cards & Merchandise)\r\n    Frontend Content Builder\r\n    Frontend Theme Customizer\r\n    AJAX Live Search (see results instantly, lowers your bounce rate!)\r\n    SEO Supercharged\r\n    Fast Page Loadtimes\r\n    The Mythology Framework features pack\r\n', 'AOZ PRINT & GRAPHICS\r\n\r\nWe are conveniently located at Mascot shops, only metres away from Mascot Post Office\r\n\r\n1215 Botany Road, Mascot NSW 2020\r\nTel: 02 9317 5263\r\nFax: 02 9317 5264\r\nMob: 0401 086 391\r\ninfo@aozprint.com.au\r\n\r\nHOURS OF OPERATION\r\nMon-Fri 9am - 6pm | Sat 10am - 4pm\r\nSunday & Public Holidays CLOSED\r\n', 'http://www.aozprint.com.au/', '1511344348AOz Prints.jpg', '2017-11-22 09:50:35'),
(8, '13', 'Awesome JK Cleaning Services', 'We provide all types cleaning services.', 'Contact: 0415 409 009', 'www.awesomejkcleaning services.com.au', '1511407259Cleaning Logo.jpg', '2017-11-23 03:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `business_resource`
--

CREATE TABLE `business_resource` (
  `b_r_id` int(11) NOT NULL,
  `b_r_title` varchar(255) NOT NULL,
  `b_r_content` text NOT NULL,
  `b_r_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_resource`
--

INSERT INTO `business_resource` (`b_r_id`, `b_r_title`, `b_r_content`, `b_r_date`) VALUES
(1, 'Starting a business', '<h2><strong>Are you planning to start a business?</strong></h2><p>For free advice contact business advisor from Bayside Chamber of Commerce 0402 22 22 11 &nbsp; <a href=\"\\\">http://www.baysidechamberofcommerce.com.au</a></p><p>Need an ABN (Australian Business Number)? Contact&nbsp;<a href=\"\\\">www.abr.gov.au</a></p><p>Need a TFN Tax File Number?&nbsp;<a href=\"\\\">www.ato.gov.au</a></p><p>Australian Securities &amp; Investment Commission (ASIC) &ndash; if you wish to form a company 1300 300 630&nbsp;<a href=\"\\\">www.asic.gov.au</a></p>', '2017-11-14 07:32:20'),
(2, 'Running your business', '<p><strong>Below are a few resources to help with running your business in theBayside business areas;</strong></p><p>ABN Lookup Link<br /><a href=\"http://abr.business.gov.au/\" target=\"_blank\">http://abr.business.gov.au/</a></p><p>Australian Consumer Laws<br /><a href=\"http://www.consumerlaw.gov.au/\" target=\"_blank\">http://www.consumerlaw.gov.au</a></p><p>Bayside Council Requirements<br />Ph: 02 9562 1666</p><p>Department of Fair Trading<br /><a href=\"http://www.fairtrading.nsw.gov.au/\" target=\"_blank\">http://www.fairtrading.nsw.gov.au</a></p><p>Employment Obligations<br />FWA/FWO Fair Work Australia (FWA)<br /><a href=\"http://www.fwa.gov.au/\" target=\"_blank\">http://www.fwa.gov.au</a>&nbsp;</p><p>Fair Work Information Statement<br /><a href=\"http://www.fairwork.gov.au/employment/employees/pages/fair-work-information-statement.aspx\">http://www.fairwork.gov.au/employment/employees/pages/fair-work-information-statement.aspx</a></p><p>Work, Health &amp; Safety (WHS) and Workers Compensation<br />(systems and accident notification, hazard identification)<br />Ph: 13 10 50<br /><a href=\"http://www.workcover.nsw.gov.au/\" target=\"_blank\">http://www.workcover.nsw.gov.au</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</p><p>Tax Tables links to the ATO&nbsp;<br /><a href=\"http://www.ato.gov.au/businesses/content/33283.htm?headline=Newwithholdingtaxtablesavailablehere&amp;segment=businesses\" target=\"_blank\">http://www.ato.gov.au/businesses/content/33283.htm?headline=Newwithholdingtaxtablesavailablehere&amp;segment=businesses</a></p><p>Small Business Support Line<br /><strong>1800 77 7275</strong><br />Monday - Friday | 8am - 8pm<br /><a href=\"http://www.ausindustry.gov.au/programs/small-business/sbsl/Pages/default.aspx\" target=\"_blank\">http://www.ausindustry.gov.au/programs/small-business/sbsl/Pages/default.aspx</a></p><p>Superannuation Information<br /><a href=\"http://www.ato.gov.au/individuals/pathway.aspx?pc=001/002/064&amp;alias=super\" target=\"_blank\">http://www.ato.gov.au/individuals/pathway.aspx?pc=001/002/064&amp;alias=super</a></p>', '2017-11-14 07:35:08'),
(3, 'Useful links', '<h3>BUSINESS LINKS</h3><p>Australian Government Business:&nbsp;<a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">http://www.business.gov.au/business-topics/Pages/default.aspx</a></p><p>Business Enterprise Centres: <a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">https://www.becaustralia.org.au</a></p><p>Australian Tax Office &ndash; Small Business:&nbsp;<a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">https://www.ato.gov.au/Business</a></p><p>Australian Tax Office &ndash; Super:&nbsp;<a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">https://www.ato.gov.au/super</a></p><p>NSW Business Chamber:&nbsp;<a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">http://www.nswbusinesschamber.com.au</a></p><p>Free information about Fair Work, health and safety compliance and workplace relations issues in Australia.</p><p><a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">http://www.workplaceassured.com.au/</a></p>', '2017-11-14 07:39:58'),
(5, 'NSW Business Chamber', '<p>Bayside Chamber of Commerce has an Alliance Partner Membership with NSW Business Chamber and all&nbsp;our members can choose to take up the free associate membership package on offer worth over $1,000.&nbsp;<a href=\"http://www.nswbusinesschamber.com.au/Home\">http://www.nswbusinesschamber.com.au</a></p><p>Benefits for Regional Chamber Members are:</p><ul>	<li>Unlimited phone calls to Business Hotline 13 26 96 (information, support and advice for all your business questions)</li>	<li>3 phone calls to Workplace Advice Line 13 29 59 (workplace experts to help with industrial relations decisions)</li>	<li>Online access to &quot;Ask Us How&quot; business guides such as sales, marketing, business planning, WHS, people management and finance</li>	<li>One online business health check per year</li>	<li>Discount on networking and educational events at NSWBC</li>	<li>Quarterly Business Magazine and monthly e-news</li>	<li>Annual listing on the online business directory on NSWBC</li>	<li>Limited access to Business Legals Toolkit</li>	<li>Discount on business support tools and products</li>	<li>Free trial of Work Health Safety</li>	<li>Free trial of Workplaceinfo</li></ul>', '2017-11-14 07:36:25');

-- --------------------------------------------------------

--
-- Table structure for table `client_feedback`
--

CREATE TABLE `client_feedback` (
  `client_feedback_id` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_feedback_date` varchar(255) NOT NULL,
  `client_feedback_image` text NOT NULL,
  `client_feedback` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_feedback`
--

INSERT INTO `client_feedback` (`client_feedback_id`, `client_name`, `client_feedback_date`, `client_feedback_image`, `client_feedback`) VALUES
(2, 'Farid AHMED', '2017-11-27', '622EB8FE-9770-4B1B-A87B-6754542E5DEB.jpeg', 'Few years ago i joined Rockdale Chamber of Commerce as a member. I attended most of the meetings and functions organised by Chamber. i served previous Rockdale chamber of Commerce as a Vice President. This year our chamber name has been changed to Bayside Chamber of Commerce in line with council amalgamations. i am current elected Secretary of the chamber. It is a great opportunity and my previlege to represent such a fantastic business organization as being a long time local business person. urge all the business people in the bayside area should join the chamber and make it active.'),
(4, 'Ron ROYSTON OAM', '2017-11-29', '15118750259A631ECF-2A47-4A56-9637-EBDEE336493F.jpeg', 'Life Member, Canterbury District soccer and Football Association(CDSFA)\r\nLife Member, Former Secretary and President, Marrickville Red Davills Football Club.'),
(6, 'Michael NAGI Councillor', '2017-11-29', '151187553542C8660F-0C64-4A15-B032-5DAB37AE5075.jpeg', 'I am Michael NAGI, a current councillor of Bayside Council and former Councillor and Deputy Mayor of Rockdale Council. I am a local Business man and running my business in Arncliffee for many years.I am proud to be part of the Bayside Chamber of commerce that is representing the business people of the bayside communty in thr interst of the their business. I urge the local business people in Bayside area to join the chamber as i did.'),
(7, 'Dr. Anthony PUN OAM', '2017-11-29', '1511875619DE04F7C5-8F08-4398-8805-86BE15ADB13E.jpeg', 'Chairman, Multicultural Communities council of NSW'),
(8, 'Mahabub CHOWDHURY', '2017-11-29', '1511875948EBF194FF-C91A-4740-83EA-87FA7CD34CC9.jpeg', 'My name is Mahabub Chowdhury and i am current president of Bangladesh association of NSW. Our community organization has been serving the people of Bangladesh cummunity more than 32 years in Australia. I am very proud to support Bayside Chamber of Commerce that is running fantastically in the Bayside area and i am very proud to support the chamber.'),
(9, 'Tim CAHILL', '2017-11-29', '1511878025F3C8FA70-F5F8-415A-90B5-695192D16E4F.jpeg', 'Australian National Team Socceroos.'),
(10, 'Armando GARDIMAN', '2017-11-29', '151187920803EBEC5D-87A2-464C-B440-D3C8A373DC12.jpeg', 'Managing Partner, Turner Freeman Lawyer'),
(11, 'William WH SEUNG', '2017-11-29', '151196018452F89466-9F5B-4C0E-A136-4CD67C91786F.jpeg', 'My name is William W.H. Seung and I am currently Chairman of Korean Australian Community Support Inc., Vive Chair of Multicultural Communities Council of NSW, Honorary Member of National Unification Advisory Council Korea. I also served community since 1979  many ways as President of Korean Society of Sydney Australia, President Korean Chamber of Commerce & Industry in Australia, President of Korean Language Schools Association in Australia and Statutory Board Member of Anti-Discrimination Board NSW.\r\n\r\nI am very proud to support Bayside Chamber of Commerce and to encourage all members do their business successfully so that they could make this area is one of the best community in NSW.\r\n\r\nI am very much appreciate that you give me an opportunity to congratulate and support for your Chamber. \"\r\n\r\nWilliam Won-Hong Seung JP\r\n\r\nChairperson, Korean Australian Community Support Inc. \r\nVice Chairperson, Multicultural Communities Council, NSW Inc. \r\nHonorary Member, National Unification Advisory Council Korea \r\nFormer Statutory Board Member, Anti Discrimination Board NSW,\r\nFormer President, Korean Society of Sydney, Australia Inc. (å‰ í˜¸ì£¼ì‹œë“œë‹ˆí•œì¸íšŒ/íšŒìž¥)'),
(12, 'Dr. Thang HA', '2017-11-29', '1511960349B7E250DD-E73C-4632-8F38-A4F4A353C3F6.jpeg', 'President Vietnamese community Australia\r\nDirector, Multicultural communties council of NSW'),
(13, 'John MARCUS', '2017-11-30', '1511960629975EF748-2B2F-46E8-A955-3AC8A0780DA3.jpeg', 'Principal \r\nMARCUS LEGAL'),
(14, 'Nick ILOSKI', '2017-11-30', '1511960973AE56D714-B669-4AD0-9507-F89838DE7E61.png', 'Managing Director\r\nGlad GROUP');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `contact_us_id` int(11) NOT NULL,
  `contact_us_address` text NOT NULL,
  `contact_us_phone` varchar(255) NOT NULL,
  `contact_us_email` varchar(255) NOT NULL,
  `contact_us_website` varchar(255) NOT NULL,
  `contact_us_info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`contact_us_id`, `contact_us_address`, `contact_us_phone`, `contact_us_email`, `contact_us_website`, `contact_us_info`) VALUES
(1, 'Rockdale Gardens Iris Tower, \r\nShop 2, 5 Keats ave, \r\nRockdale, NSW-2216 \r\n\r\nAustralia Postal Address: \r\nPO Box 140, Rockdale, NSW-2216  ', 'Office: 02-8003 8006, 02-83555514, 02-83555517, 02-83555518 Mobile: 0402 22 22 11', 'secretary@baysidechamberofcommerce.com.au', 'http://chamber.techsolutions-global.com/', 'We are about 2 minutes walk from Rockdale railway station and 2 minutes walk from the bus stop at the corner of Princes HWY and Bay Street. ');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_type` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `event_summary` text NOT NULL,
  `event_details` text NOT NULL,
  `event_image` text NOT NULL,
  `event_location` varchar(255) NOT NULL,
  `event_creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_title`, `event_type`, `start_date`, `end_date`, `event_summary`, `event_details`, `event_image`, `event_location`, `event_creation_date`) VALUES
(2, 'Test2 22', 'Regular 22', '2017-10-25', '2017-10-28', 'Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2  22', '<p>Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp; 22</p>', 'event3.jpg', 'Barisal 22', '2017-10-03 09:14:23');

-- --------------------------------------------------------

--
-- Table structure for table `interview`
--

CREATE TABLE `interview` (
  `in_id` int(11) NOT NULL,
  `in_title` varchar(255) NOT NULL,
  `in_person` varchar(255) NOT NULL,
  `in_details` text NOT NULL,
  `in_image` text NOT NULL,
  `in_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interview`
--

INSERT INTO `interview` (`in_id`, `in_title`, `in_person`, `in_details`, `in_image`, `in_date`) VALUES
(3, 'Interview Demo 1', 'One Specific Person Name', '<p><strong>Question 1: Why Do You Want To Work For Us?</strong><br /><br />It&rsquo;s rare for an interview not to include this question.<br />The good news is that it&rsquo;s an easy one to prepare for.<br />Most companies want to recruit people who are enthusiastic about the company and its products. They don&rsquo;t want people on the team who &ldquo;ended up there by accident&rdquo;. So this is your chance to show why working for the company is important to you and why you think you will fit in.<br /><br /><strong>Question 2: What interests you about this job?</strong><br /><br />When you&#39;re asked what interests you about the position you are interviewing for, the best way to respond is to describe the qualifications listed in the job posting, then connect them to your skills and experience. That way, the employer will see that you know about the job you&#39;re interviewing for (not everyone does) and that you have the qualifications necessary to do the job.&nbsp;<br /><br /><strong>Question 3: What do you know about Our Company?</strong><br /><br />A typical job interview question, asked to find out how much company research you have conducted, is &quot;What do you know about this company?&quot;&nbsp;<br />Prepare in advance, and in a word, research, so, you can provide relevant and current information about your prospective employer to the interviewer. Start by researching the company online. Review the &quot;About Us&quot; section of the company web site. Google the company, read blogs that mention it, and check.<br /><br /><strong>Question 4: What challenges are you looking for in this position?</strong><br /><br />A typical interview question to determine what you are looking for your in next job, and whether you would be a good fit for the position being hired for, is &quot;What challenges are you looking for in a position?&quot;&nbsp;<br /><br /><strong>Question 5: Who was your best boss and who was the worst?</strong><br /><br />I&#39;ve learned from each boss I&#39;ve had. From the good ones I learnt what to do, from the challenging ones - what not to do.&nbsp;<br />Early in my career, I had a mentor who helped me a great deal, we still stay in touch. I&#39;ve honestly learned something from each boss I&#39;ve had.&nbsp;<br /><br /><br /><strong>Question 6: What have you been doing since your last job?</strong><br /><br />If you have an employment gap on your resume, the interviewer will probably ask you what you have been doing while you were out of work.&nbsp;<br />The best way to answer this question is to be honest, but do have an answer prepared. You will want to let the interviewer know that you were busy and active, regardless of whether you were out of work by choice, or otherwise.&nbsp;<br /><br /><strong>Question 7: Why did you choose this particular career path?</strong><br /><br />Sometimes in interviews, you will be asked questions that lend themselves to be answered vaguely or with lengthy explanations. Take this opportunity to direct your answer in a way that connects you with the position and company, be succinct and support your answer with appropriate specific examples.<br /><br /><strong>Question 8: What are your aspirations beyond this job?</strong><br /><br />Again, don&#39;t fall into the trap of specifying job titles. Stick to a natural progression you see as plausible. How should this job grow for the good of the organization? Then turn your attention once again to the job at hand. If you seem too interested in what lies beyond this job, the interviewer will fear that you won&#39;t stick around for long.<br /><br /><strong>Question 9: Why do you think this industry would sustain your interest in the long haul?</strong><br /><br />What expectations or projects do you have for the business that would enable you to grow without necessarily advancing? What excites you about the business? What proof can you offer that your interest has already come from a deep curiosity-perhaps going back at least a few years-rather than a current whim you&#39;ll outgrow?<br /><br /><strong>Question 10: Tell me about yourself?</strong><br /><br />This is not an invitation to ramble on. If the context isn&#39;t clear, you need to know more about the question before giving an answer. In such a situation, you could ask, &quot;Is there a particular aspect of my background that you would like more information on?&quot; This will enable the interviewer to help you find the appropriate focus and avoid discussing irrelevancies.</p>', '1509342076information-interview.png', '2017-10-30 08:20:21'),
(4, 'Interview Demo 2', 'One Specific Person Name', '<p><strong>Question 1: Why Do You Want To Work For Us?</strong><br /><br />It&rsquo;s rare for an interview not to include this question.<br />The good news is that it&rsquo;s an easy one to prepare for.<br />Most companies want to recruit people who are enthusiastic about the company and its products. They don&rsquo;t want people on the team who &ldquo;ended up there by accident&rdquo;. So this is your chance to show why working for the company is important to you and why you think you will fit in.<br /><br /><strong>Question 2: What interests you about this job?</strong><br /><br />When you&#39;re asked what interests you about the position you are interviewing for, the best way to respond is to describe the qualifications listed in the job posting, then connect them to your skills and experience. That way, the employer will see that you know about the job you&#39;re interviewing for (not everyone does) and that you have the qualifications necessary to do the job.&nbsp;<br /><br /><strong>Question 3: What do you know about Our Company?</strong><br /><br />A typical job interview question, asked to find out how much company research you have conducted, is &quot;What do you know about this company?&quot;&nbsp;<br />Prepare in advance, and in a word, research, so, you can provide relevant and current information about your prospective employer to the interviewer. Start by researching the company online. Review the &quot;About Us&quot; section of the company web site. Google the company, read blogs that mention it, and check.<br /><br /><strong>Question 4: What challenges are you looking for in this position?</strong><br /><br />A typical interview question to determine what you are looking for your in next job, and whether you would be a good fit for the position being hired for, is &quot;What challenges are you looking for in a position?&quot;&nbsp;<br /><br /><strong>Question 5: Who was your best boss and who was the worst?</strong><br /><br />I&#39;ve learned from each boss I&#39;ve had. From the good ones I learnt what to do, from the challenging ones - what not to do.&nbsp;<br />Early in my career, I had a mentor who helped me a great deal, we still stay in touch. I&#39;ve honestly learned something from each boss I&#39;ve had.&nbsp;<br /><br /><br /><strong>Question 6: What have you been doing since your last job?</strong><br /><br />If you have an employment gap on your resume, the interviewer will probably ask you what you have been doing while you were out of work.&nbsp;<br />The best way to answer this question is to be honest, but do have an answer prepared. You will want to let the interviewer know that you were busy and active, regardless of whether you were out of work by choice, or otherwise.&nbsp;<br /><br /><strong>Question 7: Why did you choose this particular career path?</strong><br /><br />Sometimes in interviews, you will be asked questions that lend themselves to be answered vaguely or with lengthy explanations. Take this opportunity to direct your answer in a way that connects you with the position and company, be succinct and support your answer with appropriate specific examples.<br /><br /><strong>Question 8: What are your aspirations beyond this job?</strong><br /><br />Again, don&#39;t fall into the trap of specifying job titles. Stick to a natural progression you see as plausible. How should this job grow for the good of the organization? Then turn your attention once again to the job at hand. If you seem too interested in what lies beyond this job, the interviewer will fear that you won&#39;t stick around for long.<br /><br /><strong>Question 9: Why do you think this industry would sustain your interest in the long haul?</strong><br /><br />What expectations or projects do you have for the business that would enable you to grow without necessarily advancing? What excites you about the business? What proof can you offer that your interest has already come from a deep curiosity-perhaps going back at least a few years-rather than a current whim you&#39;ll outgrow?<br /><br /><strong>Question 10: Tell me about yourself?</strong><br /><br />This is not an invitation to ramble on. If the context isn&#39;t clear, you need to know more about the question before giving an answer. In such a situation, you could ask, &quot;Is there a particular aspect of my background that you would like more information on?&quot; This will enable the interviewer to help you find the appropriate focus and avoid discussing irrelevancies.</p>', '1509351831iStock_64702199_MEDIUM-778x518.jpg', '2017-10-30 08:22:40'),
(5, 'Interview Demo 3', 'One Specific Person Name', '<p><strong>Question 1: Why Do You Want To Work For Us?</strong><br />\r\n<br />\r\nIt&rsquo;s rare for an interview not to include this question.<br />\r\nThe good news is that it&rsquo;s an easy one to prepare for.<br />\r\nMost companies want to recruit people who are enthusiastic about the company and its products. They don&rsquo;t want people on the team who &ldquo;ended up there by accident&rdquo;. So this is your chance to show why working for the company is important to you and why you think you will fit in.<br />\r\n<br />\r\n<strong>Question 2: What interests you about this job?</strong><br />\r\n<br />\r\nWhen you&#39;re asked what interests you about the position you are interviewing for, the best way to respond is to describe the qualifications listed in the job posting, then connect them to your skills and experience. That way, the employer will see that you know about the job you&#39;re interviewing for (not everyone does) and that you have the qualifications necessary to do the job.&nbsp;<br />\r\n<br />\r\n<strong>Question 3: What do you know about Our Company?</strong><br />\r\n<br />\r\nA typical job interview question, asked to find out how much company research you have conducted, is &quot;What do you know about this company?&quot;&nbsp;<br />\r\nPrepare in advance, and in a word, research, so, you can provide relevant and current information about your prospective employer to the interviewer. Start by researching the company online. Review the &quot;About Us&quot; section of the company web site. Google the company, read blogs that mention it, and check.<br />\r\n<br />\r\n<strong>Question 4: What challenges are you looking for in this position?</strong><br />\r\n<br />\r\nA typical interview question to determine what you are looking for your in next job, and whether you would be a good fit for the position being hired for, is &quot;What challenges are you looking for in a position?&quot;&nbsp;<br />\r\n<br />\r\n<strong>Question 5: Who was your best boss and who was the worst?</strong><br />\r\n<br />\r\nI&#39;ve learned from each boss I&#39;ve had. From the good ones I learnt what to do, from the challenging ones - what not to do.&nbsp;<br />\r\nEarly in my career, I had a mentor who helped me a great deal, we still stay in touch. I&#39;ve honestly learned something from each boss I&#39;ve had.&nbsp;<br />\r\n<br />\r\n<br />\r\n<strong>Question 6: What have you been doing since your last job?</strong><br />\r\n<br />\r\nIf you have an employment gap on your resume, the interviewer will probably ask you what you have been doing while you were out of work.&nbsp;<br />\r\nThe best way to answer this question is to be honest, but do have an answer prepared. You will want to let the interviewer know that you were busy and active, regardless of whether you were out of work by choice, or otherwise.&nbsp;<br />\r\n<br />\r\n<strong>Question 7: Why did you choose this particular career path?</strong><br />\r\n<br />\r\nSometimes in interviews, you will be asked questions that lend themselves to be answered vaguely or with lengthy explanations. Take this opportunity to direct your answer in a way that connects you with the position and company, be succinct and support your answer with appropriate specific examples.<br />\r\n<br />\r\n<strong>Question 8: What are your aspirations beyond this job?</strong><br />\r\n<br />\r\nAgain, don&#39;t fall into the trap of specifying job titles. Stick to a natural progression you see as plausible. How should this job grow for the good of the organization? Then turn your attention once again to the job at hand. If you seem too interested in what lies beyond this job, the interviewer will fear that you won&#39;t stick around for long.<br />\r\n<br />\r\n<strong>Question 9: Why do you think this industry would sustain your interest in the long haul?</strong><br />\r\n<br />\r\nWhat expectations or projects do you have for the business that would enable you to grow without necessarily advancing? What excites you about the business? What proof can you offer that your interest has already come from a deep curiosity-perhaps going back at least a few years-rather than a current whim you&#39;ll outgrow?<br />\r\n<br />\r\n<strong>Question 10: Tell me about yourself?</strong><br />\r\n<br />\r\nThis is not an invitation to ramble on. If the context isn&#39;t clear, you need to know more about the question before giving an answer. In such a situation, you could ask, &quot;Is there a particular aspect of my background that you would like more information on?&quot; This will enable the interviewer to help you find the appropriate focus and avoid discussing irrelevancies.</p>\r\n', '1509351865Credible-Coaching.jpg', '2017-10-30 08:22:31');

-- --------------------------------------------------------

--
-- Table structure for table `join_now_content`
--

CREATE TABLE `join_now_content` (
  `join_now_content_id` int(11) NOT NULL,
  `join_now_title` varchar(255) NOT NULL,
  `join_now_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `join_now_content`
--

INSERT INTO `join_now_content` (`join_now_content_id`, `join_now_title`, `join_now_content`) VALUES
(1, 'Why join the Bayside Chamber of Commerce?', '<ol>	<li><strong>Business&nbsp;Promotion.</strong><br />	Chamber membership offers you many opportunities to promote your business including our monthly newsletter, social media, event attendance and sponsorship, online advertising and SO much more. &nbsp;</li>	<li><strong>Networking Opportunities.</strong><br />	Connect to the Bayside&#39;s foremost business network of a number of members working together to improve the business and economic prosperity of the region. Meet local business owners and decision-makers, and develop valuable new business relationships.</li>	<li><strong>Advocacy opportunities.</strong><br />	The Chamber serves as a strong voice in both local and state government in association with NSW Business Chamber, collectively representing members on issues that impact businesses. &nbsp;The Chamber focuses on policy areas that are critical to the economic vitality of the Bayside area. <strong>&nbsp;</strong></li>	<li><strong>Community</strong>.Heighten your knowledge of how the community is moving ahead by reading the Chamber Business News and our website blogs. Business &amp; Community workshop allow you to connect and network with members from the community!</li>	<li><strong>Referrals</strong><br />	&nbsp;As the Bayside chamber of Commerce, we advocate for the success and advancement of all member businesses; therefore, it is our policy to primarily provide referrals to members. &nbsp;Your involvement in Chamber membership demonstrates&nbsp; to other businesses and to potential customers that you are committed to supporting the local economy. &nbsp;</li>	<li><strong>Members-Only Discounts.</strong><br />	Members can take advantage of many cost-saving promotional opportunities and discounts offered by other members.</li>	<li><strong>Seminar and Workshop.</strong><br />	Our Chamber offers a wide range of professional development and informational seminars in association with Local Council and NSW chamber of Commerce throughout the year that is beneficial to our members. The programs are designed to help your business needs. &nbsp;</li>	<li><strong>Build Credibility and Growth.</strong>&nbsp;<br />	Demonstrate your commitment to the community by becoming a part of the vision and strategies of the most influential business organization in the Bayside area. . As a member of Bayside Chamber of commerce, you have the opportunity to join various committees in your Local Council so you may have a voice, make a difference and make connections.Your investment in the Chamber helps us to promote and strengthen Bayside business community. When the Chamber assists Businesses that are moving here or expanding their businesses, it means more jobs and more customers for you.</li></ol><p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9. Leadership Opportunities.</strong><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Members determine the goals and objectives of the Chamber by participating meeting that support their business and professional goals. These involvements enable members to take active leadership roles in the Chamber and in the business</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; community.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `join_now_info`
--

CREATE TABLE `join_now_info` (
  `join_now_info_id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `m_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `abn_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `business_address` text NOT NULL,
  `postal_address` text NOT NULL,
  `office_number` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `join_now_info`
--

INSERT INTO `join_now_info` (`join_now_info_id`, `f_name`, `m_name`, `l_name`, `business_name`, `abn_number`, `email`, `business_address`, `postal_address`, `office_number`, `mobile_number`, `website`, `profession`, `join_date`) VALUES
(1, 'Arifur', 'Rahman', 'Raju', 'TechSolutions', '112233', 'ariftechsolutions@gmail.com', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton, Dhaka', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton, Dhaka', '01926677534', '01715799134', 'http://www.techsolutionsbd.com', 'Programmer', '2017-10-23 07:56:07'),
(3, 'Tanvir ', 'Ahmed', 'Shaikat', 'TechSolutions', '110022', 'ta.shaikat@gmail.com', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton, Dhaka.', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton, Dhaka.', '01911111', '01811111', 'http://www.techsolutionsbd.com', 'Programmer', '2017-10-23 11:51:18'),
(4, 'Nuruzzaman ', '', 'Lucky', 'Stark Mobile', '009988', 'nuruzzamanlucky@gmail.com', 'Panthopath, Dhaka.', 'Shyamoli, Dhaka.', '0191111111', '017155555', 'http://lucky.com', 'Designer', '2017-10-25 11:59:14'),
(5, 'Nuruzzaman ', 'Ahmed', 'Lucky', 'Stark Mobile', '11223344', 'lucky@gmail.com', 'Rockdale Gardens Iris Tower, Shop 2, 5 Keats ave, ', 'Rockdale Gardens Iris Tower, Shop 2, 5 Keats ave, 2', '112200', '112299', 'http://lucky.com', 'Designer', '2017-11-09 12:00:41'),
(6, 'Nuruzzaman ', 'Ahmed', 'Lucky', 'Stark Mobile', '11223344', 'lucky@gmail.com', 'Rockdale Gardens Iris Tower, Shop 2, 5 Keats ave, ', 'Rockdale Gardens Iris Tower, Shop 2, 5 Keats ave, 2', '112200', '112299', 'http://lucky.com', 'Designer', '2017-11-09 12:02:48'),
(7, 'Farid', '', 'Ahmed', 'Gess', '17449700286', 'farid_farida@hotmail.com', '2 keats ave ', 'As above', '', '0402222211', '', '', '2017-11-12 10:51:39');

-- --------------------------------------------------------

--
-- Table structure for table `members_benifit`
--

CREATE TABLE `members_benifit` (
  `members_benifit_id` int(11) NOT NULL,
  `members_benifit_title` varchar(255) NOT NULL,
  `members_benifit_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `members_benifit`
--

INSERT INTO `members_benifit` (`members_benifit_id`, `members_benifit_title`, `members_benifit_content`) VALUES
(1, 'Membership & Benefits', '<h3><strong>Why Join the Bayside Chamber of Commerce ? </strong></h3><ul>	<li><strong>Business&nbsp;Promotion.</strong><br />	Chamber membership offers you many opportunities to promote your business including our monthly newsletter, social media, event attendance and sponsorship, online advertising and SO much more. &nbsp;</li>	<li><strong>Networking Opportunities.</strong><br />	Connect to the Bayside&#39;s foremost business network of a number of members working together to improve the business and economic prosperity of the region. Meet local business owners and decision-makers, and develop valuable new business relationships.</li>	<li><strong>Advocacy opportunities.</strong><br />	The Chamber serves as a strong voice in both local and state government in association with NSW Business Chamber, collectively representing members on issues that impact businesses. &nbsp;The Chamber focuses on policy areas that are critical to the economic vitality of the Bayside area. <strong>&nbsp;</strong></li>	<li><strong>Community</strong>.Heighten your knowledge of how the community is moving ahead by reading the Chamber Business News and our website blogs. Business &amp; Community workshop allow you to connect and network with members from the community!</li>	<li><strong>Referrals</strong><br />	&nbsp;As the Bayside chamber of Commerce, we advocate for the success and advancement of all member businesses; therefore, it is our policy to primarily provide referrals to members. &nbsp;Your involvement in Chamber membership demonstrates&nbsp; to other businesses and to potential customers that you are committed to supporting the local economy. &nbsp;</li>	<li><strong>Members-Only Discounts.</strong><br />	Members can take advantage of many cost-saving promotional opportunities and discounts offered by other members.</li>	<li><strong>Seminar and Workshop.</strong><br />	Our Chamber offers a wide range of professional development and informational seminars in association with Local Council and NSW chamber of Commerce throughout the year that is beneficial to our members. The programs are designed to help your business needs. &nbsp;</li>	<li><strong>Build Credibility and Growth.</strong>&nbsp;<br />	Demonstrate your commitment to the community by becoming a part of the vision and strategies of the most influential business organization in the Bayside area. . As a member of Bayside Chamber of commerce, you have the opportunity to join various committees in your Local Council so you may have a voice, make a difference and make connections.Your investment in the Chamber helps us to promote and strengthen Bayside business community. When the Chamber assists Businesses that are moving here or expanding their businesses, it means more jobs and more customers for you.</li>	<li><strong>9. Leadership Opportunities.</strong> Members determine the goals and objectives of the Chamber by participating meeting that support their business and professional goals. These involvements enable members to take active leadership roles in the Chamber and in the busines community.</li></ul><h3><strong>Additional Benefits</strong></h3><ul>	<li><a href=\"http://businesschamber.com.au/Business-Savers-Program\">NSW Business Chamber Benefits</a></li></ul>');

-- --------------------------------------------------------

--
-- Table structure for table `member_to_member`
--

CREATE TABLE `member_to_member` (
  `member_to_member_id` int(11) NOT NULL,
  `member_to_member_category_id` varchar(255) NOT NULL,
  `member_to_member_info` text NOT NULL,
  `member_to_member_contact` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_to_member`
--

INSERT INTO `member_to_member` (`member_to_member_id`, `member_to_member_category_id`, `member_to_member_info`, `member_to_member_contact`, `date`) VALUES
(2, '1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '+61 111222333', '2017-10-24 07:04:28'),
(4, '2', '<p>20% Discount for members for business and organisational development strategies and projects,</p>\r\n', '0428104482', '2017-10-24 07:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_title` varchar(255) NOT NULL,
  `news_author` varchar(255) NOT NULL,
  `news_content` text NOT NULL,
  `news_image` text NOT NULL,
  `news_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_title`, `news_author`, `news_content`, `news_image`, `news_date`) VALUES
(3, 'Campaign.', '', '', '1508925904pexels-photo-341858.jpeg', '2017-10-25'),
(4, 'Seminar', 'Farid AHMED', '', 'konf.jpg', '2017-10-30'),
(5, 'Presentation ', 'Bayside Chamber of Commerce', '', 'VCM_events_live_stream_hero.png', '2017-10-29'),
(6, 'Christmas Party 2017', 'Bayside Chamber of Commerce', '<p>Our Christmas Party will be held at Red Rose Function Centre,96A Railway Street, Rockdale,NSW-2216 at 6.30PM.</p><p>&nbsp;</p>', '1512397176B0FBB119-6A22-43C3-AC64-522459F22ED5.jpeg', '2017-12-05');

-- --------------------------------------------------------

--
-- Table structure for table `our_board`
--

CREATE TABLE `our_board` (
  `member_id` int(11) NOT NULL,
  `member_name` varchar(255) NOT NULL,
  `member_designation` varchar(255) NOT NULL,
  `member_image` text NOT NULL,
  `member_details` text NOT NULL,
  `member_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `our_board`
--

INSERT INTO `our_board` (`member_id`, `member_name`, `member_designation`, `member_image`, `member_details`, `member_date`) VALUES
(16, 'Quamrul Islam KHAN', 'President', 'Quamrul.jpeg', '<p>My Name is Quamrul Islam KHAN and I am a current President of Bayside Chamber of commerce. Firstly I would like to thank all members who supported me to be elected as a President.</p><p>Currently I have been running my own business in the Bayside area as a Tax Consultant with Australian Taxation Office. I have also worked as a Manager Accounts with H&amp;R Blocks and Tax Accountant at Nextzen Accounting Services as a Tax Consultant. In addition to working as a consultant in the Taxation area, I have also worked as an accounting Lecturer in a Vocational educational institute in Sydney.</p><p>I migrated to Australia from Bangladesh as a skilled migrant with my family. Before coming to Australia, I worked in Beximco group as a senior corporate manager, Yearly turnover of the company is equivalent to AUD $10 Billion.</p><p>I have completed Master of Commerce and Master of Business Administration from Bangladesh and after migrated to Australia, I completed Advanced Diploma in Accounting from St. George TAFE.</p><p>&nbsp;</p><p>My professional membership includes the following: IPA Australia: MIPA Membership</p><p>CPA Australia: An Associate Member of CPA Australia</p><p>IFA (Institute of Financial Accountants) , UK : AFA and</p><p>also i am a JP (Justice of the peace).</p><p>As being a business professional many years, I will try my best to help other business professionals in the bayside area in my capacity as you know that we are a not for profit organization and everyone is very busy in their own profession to run the family.</p><p>&nbsp;</p>', '2017-12-06 02:29:50'),
(17, 'Patrick MEDWAY', 'Vice President', 'Patrick.JPG', '', '2017-11-14 07:54:09'),
(18, 'Farid AHMED', 'Secretary', '1510644207Farid AHmed.jpg', '<p>My name is Farid AHMED and I am the owner of GESS Australia- advising clients in the area of Australian Immigration &amp; Citizenship Law and Sub continental Travel- Selling domestic &amp; International Air Tickets and All types of Travel Insurance. I have been working in the Bayside Area for more than a decade.I have been serving Australian communities in the areas of Business, Social, Sports, Cultural and other areas in Australia, in various capacities for more 15 years.&nbsp;</p>\r\n\r\n<p>My aim is to support and assist the diverse business community in the Bayside council area and work closely with business owners regardless of the size of the business. I believe that an active involvement in the leadership of the business activities of the Bayside Business community is one of the key ways for revitalizing businesses in the LGA and the close connection to the people of the community.</p>\r\n\r\n<p>I completed Bachelor &amp; Master Degree, (MBA) from Bangladesh, I came to Australia to undertake another post graduate degree &amp; completed Master of Business Administration (MBA) specializing in information system from Australia. After completing my MBA degree, I studied Professional Doctorate Program. I also completed a few other courses from Australia in the area of &quot; Australian Immigration and Citizenship Law&quot; from University of Technology Sydney (UTS), &quot;Foundation of Tax in Australian Taxation Law&quot; from Taxation Institute of Australia and &quot;International Travel and Ticketing Operations&quot; from AFTA.&nbsp;</p>\r\n\r\n<p>Current &amp; Previous community Involvements:</p>\r\n\r\n<p>1.) &nbsp;Rockdale Chamber of Commerce.</p>\r\n\r\n<p>== Secretary</p>\r\n\r\n<p>== Former Vice President</p>\r\n\r\n<p>2.) &nbsp;Multicultural Communities&rsquo; Council of NSW Inc.</p>\r\n\r\n<p>== Director</p>\r\n\r\n<p>3.) &nbsp;The Ethnic Communities Council of NSW Inc.</p>\r\n\r\n<p>== Former Vice Chair</p>\r\n\r\n<p>== Member of the Management committee</p>\r\n\r\n<p>&nbsp;4.) Marrickville Football Club</p>\r\n\r\n<p>== Member of the Management committee</p>\r\n\r\n<p>5.) Community Welfare First Inc.</p>\r\n\r\n<p>== Former President</p>\r\n\r\n<p>6.) Villawood Immigration Detention Centre</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;(Department of Immigration and Citizenship)</p>\r\n\r\n<p>== Committee member of DIAC&#39;s consultative Group&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; Committee</p>\r\n\r\n<p>7.) Migration Review Tribunal and Refugee&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; Review Tribunal</p>\r\n\r\n<p>==Member of the Community Liaison Meetings Committee</p>\r\n\r\n<p>8.) Department of Families &amp; community Services</p>\r\n\r\n<p>==Member of Multicultural Affairs advisory Group &nbsp;</p>\r\n\r\n<p>9.) Red Cross Australia</p>\r\n\r\n<p>==Member and Contributor</p>\r\n\r\n<p>10.) Amnesty International Australia</p>\r\n\r\n<p>= &nbsp; Member and Contributor</p>\r\n\r\n<p>11) Canterbury and District Soccer&nbsp;Football Association (CDSFA)</p>\r\n\r\n<p>&nbsp;= &nbsp; Former Grading Officer</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>My involvement in various community activities in numerous roles has given me a unique opportunity to mix with people from different backgrounds and understand their needs and concerns. I have a special interest in the Business community in terms of the Small to Large organizations in the Bayside Area and wish to work together to share our concerns &amp; other issues.</p>\r\n\r\n<p>With a wealth of local business experience, knowledge and community involvement, I have much to contribute to the members of the Bayside business community. &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-11-14 07:21:34'),
(19, 'Nazmy SALIB', 'Treasurer', 'Flower.jpg', '', '2017-11-14 07:54:47'),
(20, 'Soyeb AHMED', 'Board Member', 'IMAG0120.jpg', '', '2017-11-18 11:05:13'),
(21, 'Alam SMITH', 'Board Member', 'IMG_4928.JPG', '', '2017-11-15 05:43:13'),
(22, 'Khondoker Jalil ur Rahman KHAN', 'Board Member', 'IMG_4877.JPG', '<p>My Name is Jalilur Rahman KHAN. I am proud to get elected as a&nbsp; Board Member of Bayside Chamber of commerce and also would like to thank all members who supported me to be elected as a Board Member.</p><p>I am running a cleaning business in the Bayside area and as a local business person, I am always passionate to help other business people in the area and also support local community by providing my services.</p><p>I came to Australia with my family as an international student and hence we naturalised in the country. Before coming to Australia, I run clothing business in Garments industry.</p><p>I completed Bachelor of Commerce in Bangladesh before coming to Australia.</p><p>As being a business professional in the area, it is my privilege and great opportunity to help other business professionals in the bayside area in my capacity and I will try my best to do so by involving in the Chamber.&nbsp;&nbsp;&nbsp;</p>', '2017-11-15 05:43:25'),
(23, 'Reg SOARES', 'Board Member', 'Reg 1.jpg', '', '2017-11-28 09:25:14'),
(24, 'Saleh MOSTAFA', 'Board Member', 'F06E6AFB-866F-4A7C-AE3F-FAEB520F98BC.jpeg', '', '2017-11-28 13:44:27'),
(25, 'Abdur RAHIM', 'Board Member', 'Rahim.jpeg', '', '2017-11-29 03:10:37');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `photo_id` int(11) NOT NULL,
  `photo_name` text NOT NULL,
  `photo_title` varchar(255) NOT NULL,
  `photo_upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`photo_id`, `photo_name`, `photo_title`, `photo_upload_date`) VALUES
(1, 'DBEBC1A3-58E2-4E6B-83A8-179F2E584B0A.jpeg', 'Business peesentation 1', '2017-11-15 13:50:25'),
(2, '322C0617-A648-415D-8D59-7182A18D282F.jpeg', 'Business presentation 2 ', '2017-11-15 13:52:36'),
(3, 'D939F0DA-1F71-4A13-9209-25AF986B86C0.jpeg', 'Business presentation 3', '2017-12-05 13:43:44'),
(4, '590683AE-97F4-4AB9-A57F-D03C8FE95372.jpeg', 'Event 4', '2017-11-15 13:53:49'),
(5, '1508843226eid.jpg', 'Inauguration Ceremony', '2017-10-25 06:04:01'),
(6, '74515BA6-7C8C-4834-843A-7C3BE2C8F78A.jpeg', 'We are', '2017-12-05 13:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `slogan`
--

CREATE TABLE `slogan` (
  `slogan_id` int(11) NOT NULL,
  `slogan_title` varchar(255) NOT NULL,
  `slogan_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slogan`
--

INSERT INTO `slogan` (`slogan_id`, `slogan_title`, `slogan_date`) VALUES
(1, 'Your Membership renewal is due. Please pay now.', '2017-11-20 14:45:31'),
(2, 'Our Christmas party is on 5th December 2017 at Red Rose Function Centre, Rockdale.', '2017-11-20 14:46:58'),
(3, 'Please Book for your advertisement. First come first serve basis!!!', '2017-11-20 14:48:55'),
(4, 'Our Next Meeting is on 05 December 2017 at Advanced Diversity Services', '2017-11-20 14:47:46');

-- --------------------------------------------------------

--
-- Table structure for table `social_link`
--

CREATE TABLE `social_link` (
  `social_link_id` int(11) NOT NULL,
  `social_link` varchar(255) NOT NULL,
  `social_link_icon` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_link`
--

INSERT INTO `social_link` (`social_link_id`, `social_link`, `social_link_icon`) VALUES
(1, 'Bayside Chamber of Commerce', '<i class=\"fa fa-facebook\" aria-hidden=\"true\" style=\" color: #3a589e;\"></i> '),
(2, 'https://twitter.com/', '<i class=\"fa fa-twitter\" aria-hidden=\"true\" style=\"color: #00aced;\"></i>'),
(3, 'https://www.linkedin.com/', '<i class=\"fa fa-linkedin\" aria-hidden=\"true\" style=\"color: #225982;\"></i>'),
(4, 'https://www.youtube.com/', '<i class=\"fa fa-youtube\" aria-hidden=\"true\" style=\"color: #f00;\"></i> ');

-- --------------------------------------------------------

--
-- Table structure for table `top_left_ad`
--

CREATE TABLE `top_left_ad` (
  `top_left_ad_id` int(11) NOT NULL,
  `top_left_ad_image` text NOT NULL,
  `top_left_ad_image_title` varchar(255) NOT NULL,
  `top_left_ad_link` varchar(255) NOT NULL,
  `top_left_ad_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `top_left_ad`
--

INSERT INTO `top_left_ad` (`top_left_ad_id`, `top_left_ad_image`, `top_left_ad_image_title`, `top_left_ad_link`, `top_left_ad_date`) VALUES
(1, 'ad222.jpg', '1', '#', '2017-11-08 07:51:17'),
(2, 'ad222.jpg', '2', '#', '2017-11-08 07:51:41'),
(3, 'ad222.jpg', '3', '#', '2017-11-08 07:51:49'),
(4, 'ad222.jpg', '4', '#', '2017-11-08 07:51:57'),
(5, 'ad222.jpg', '5', '#', '2017-11-08 07:52:05'),
(6, 'ad222.jpg', '6', '#', '2017-11-08 07:52:15');

-- --------------------------------------------------------

--
-- Table structure for table `top_right_ad`
--

CREATE TABLE `top_right_ad` (
  `top_right_ad_id` int(11) NOT NULL,
  `top_right_ad_image` text NOT NULL,
  `top_right_ad_image_title` varchar(255) NOT NULL,
  `top_right_ad_link` varchar(255) NOT NULL,
  `top_right_ad_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `top_right_ad`
--

INSERT INTO `top_right_ad` (`top_right_ad_id`, `top_right_ad_image`, `top_right_ad_image_title`, `top_right_ad_link`, `top_right_ad_date`) VALUES
(1, 'ad222.jpg', '1', '#', '2017-11-08 07:52:45'),
(2, 'ad222.jpg', '2', '#', '2017-11-08 07:52:53'),
(3, 'ad222.jpg', '3', '#', '2017-11-08 07:54:34'),
(4, 'ad222.jpg', '4', '#', '2017-11-08 07:54:41'),
(5, 'ad222.jpg', '5', '#', '2017-11-08 07:54:49'),
(6, 'ad222.jpg', '6', '#', '2017-11-08 07:54:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `user_id` int(11) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `given_name` varchar(255) NOT NULL,
  `date_of_birth` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_role` varchar(255) NOT NULL,
  `date_of_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`user_id`, `last_name`, `given_name`, `date_of_birth`, `email`, `password`, `user_role`, `date_of_creation`, `active_status`) VALUES
(1, 'Ashiq', 'Fardus', '2017-07-06', 'ashiq@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'regular', '2018-08-16 08:58:19', 0),
(2, 'BD', 'Techsolutions', '2018-08-12', 'ts.hosting2017@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'gold', '2018-08-16 09:01:36', 0),
(3, 'Tech', 'Solutions', '2018-08-01', 'trech@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'gold', '2018-08-16 09:02:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `video_id` int(11) NOT NULL,
  `video_title` varchar(255) NOT NULL,
  `youtube_video_id` varchar(255) NOT NULL,
  `video_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`video_id`, `video_title`, `youtube_video_id`, `video_date`) VALUES
(3, 'Test1', 'nFmXyjG2lwA', '2017-10-30 07:40:36'),
(4, 'Test2', 'bfgcFJFE3mE', '2017-10-30 07:40:09'),
(5, 'Test3', 'jO58Zk826Tc', '2017-10-30 07:39:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us_governance`
--
ALTER TABLE `about_us_governance`
  ADD PRIMARY KEY (`governance_id`);

--
-- Indexes for table `about_us_history`
--
ALTER TABLE `about_us_history`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `about_us_members_forum`
--
ALTER TABLE `about_us_members_forum`
  ADD PRIMARY KEY (`members_forum_id`);

--
-- Indexes for table `about_us_our_mission`
--
ALTER TABLE `about_us_our_mission`
  ADD PRIMARY KEY (`our_mission_id`);

--
-- Indexes for table `about_us_our_staff`
--
ALTER TABLE `about_us_our_staff`
  ADD PRIMARY KEY (`our_staff_id`);

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`album_id`);

--
-- Indexes for table `album_photo`
--
ALTER TABLE `album_photo`
  ADD PRIMARY KEY (`album_photo_id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `bottom_left_ad`
--
ALTER TABLE `bottom_left_ad`
  ADD PRIMARY KEY (`bottom_left_ad_id`);

--
-- Indexes for table `bottom_right_ad`
--
ALTER TABLE `bottom_right_ad`
  ADD PRIMARY KEY (`bottom_right_ad_id`);

--
-- Indexes for table `business_category`
--
ALTER TABLE `business_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `business_organization`
--
ALTER TABLE `business_organization`
  ADD PRIMARY KEY (`b_o_id`);

--
-- Indexes for table `business_resource`
--
ALTER TABLE `business_resource`
  ADD PRIMARY KEY (`b_r_id`);

--
-- Indexes for table `client_feedback`
--
ALTER TABLE `client_feedback`
  ADD PRIMARY KEY (`client_feedback_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`contact_us_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `interview`
--
ALTER TABLE `interview`
  ADD PRIMARY KEY (`in_id`);

--
-- Indexes for table `join_now_content`
--
ALTER TABLE `join_now_content`
  ADD PRIMARY KEY (`join_now_content_id`);

--
-- Indexes for table `join_now_info`
--
ALTER TABLE `join_now_info`
  ADD PRIMARY KEY (`join_now_info_id`);

--
-- Indexes for table `members_benifit`
--
ALTER TABLE `members_benifit`
  ADD PRIMARY KEY (`members_benifit_id`);

--
-- Indexes for table `member_to_member`
--
ALTER TABLE `member_to_member`
  ADD PRIMARY KEY (`member_to_member_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `our_board`
--
ALTER TABLE `our_board`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `slogan`
--
ALTER TABLE `slogan`
  ADD PRIMARY KEY (`slogan_id`);

--
-- Indexes for table `social_link`
--
ALTER TABLE `social_link`
  ADD PRIMARY KEY (`social_link_id`);

--
-- Indexes for table `top_left_ad`
--
ALTER TABLE `top_left_ad`
  ADD PRIMARY KEY (`top_left_ad_id`);

--
-- Indexes for table `top_right_ad`
--
ALTER TABLE `top_right_ad`
  ADD PRIMARY KEY (`top_right_ad_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us_governance`
--
ALTER TABLE `about_us_governance`
  MODIFY `governance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `about_us_history`
--
ALTER TABLE `about_us_history`
  MODIFY `history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `about_us_members_forum`
--
ALTER TABLE `about_us_members_forum`
  MODIFY `members_forum_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `about_us_our_mission`
--
ALTER TABLE `about_us_our_mission`
  MODIFY `our_mission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `about_us_our_staff`
--
ALTER TABLE `about_us_our_staff`
  MODIFY `our_staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `album_photo`
--
ALTER TABLE `album_photo`
  MODIFY `album_photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `bottom_left_ad`
--
ALTER TABLE `bottom_left_ad`
  MODIFY `bottom_left_ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `bottom_right_ad`
--
ALTER TABLE `bottom_right_ad`
  MODIFY `bottom_right_ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `business_category`
--
ALTER TABLE `business_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `business_organization`
--
ALTER TABLE `business_organization`
  MODIFY `b_o_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `business_resource`
--
ALTER TABLE `business_resource`
  MODIFY `b_r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `client_feedback`
--
ALTER TABLE `client_feedback`
  MODIFY `client_feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `contact_us_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `interview`
--
ALTER TABLE `interview`
  MODIFY `in_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `join_now_content`
--
ALTER TABLE `join_now_content`
  MODIFY `join_now_content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `join_now_info`
--
ALTER TABLE `join_now_info`
  MODIFY `join_now_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `members_benifit`
--
ALTER TABLE `members_benifit`
  MODIFY `members_benifit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member_to_member`
--
ALTER TABLE `member_to_member`
  MODIFY `member_to_member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `our_board`
--
ALTER TABLE `our_board`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `slogan`
--
ALTER TABLE `slogan`
  MODIFY `slogan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `social_link`
--
ALTER TABLE `social_link`
  MODIFY `social_link_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `top_left_ad`
--
ALTER TABLE `top_left_ad`
  MODIFY `top_left_ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `top_right_ad`
--
ALTER TABLE `top_right_ad`
  MODIFY `top_right_ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
