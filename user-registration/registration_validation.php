<?php
/**
 * Created by PhpStorm.
 * User: i7
 * Date: 8/16/2018
 * Time: 2:41 PM
 */
require '../config/database.php';

function user_registration()
{
    global $connection;

    $result=array(
        'result'=>1,
        'message'=>'undefined error'
    );

    if (isset($_POST['submit']))
    {
        $last_name=$_POST['last_name'];
        $given_name=$_POST['given_name'];
        $date_of_birth=$_POST['date_of_birth'];
        $email=$_POST['email'];
        $user_role=$_POST['user_role'];
        $password=$_POST['password'];

        //Email check if exists or not
        $emailcheck=mysqli_query($connection,"Select email from user_details WHERE email='$email'");
        $echeck=mysqli_num_rows($emailcheck);
        if ($echeck!=0)
        {
            $result['message']='Email already exists';
            return $result;
        }

        //Id no check if exists or not
        $lastnamechk=mysqli_query($connection,"Select last_name from user_details WHERE last_name='$last_name'");
        $id_check=mysqli_num_rows($lastnamechk);
        if ($id_check!=0)
        {
            $result['message']='Last name already exists';
            return $result;
        }

        //Password length check
        if (strlen($password)>9 || strlen($password)<6)
        {
            $result['message']='Password length must be between 6-9 character';
            return $result;
        }


        $password=sha1($password);
        $query="INSERT INTO user_details(last_name,given_name,date_of_birth,email,password,user_role) 
                VALUES ('$last_name','$given_name','$date_of_birth','$email','$password','$user_role')";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Registration completed. Please Login Now.';
            return $result;
        }
    }
}
