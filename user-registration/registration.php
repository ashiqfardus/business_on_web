<html>
    <head>
        <title>
            Registration
        </title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="col-md-6">
                <form action="registration.php" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="text" class="form-control"  placeholder="Enter last name" name="last_name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Given Name</label>
                        <input type="text" class="form-control"  placeholder="Enter given name" name="given_name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Date of Birth</label>
                        <input type="date" class="form-control" name="date_of_birth">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control"  placeholder="Enter email" name="email">
                    </div>
                    <div class="form-group">
                        <select class="custom-select" name="user_role">
                            <option selected>Select User Role</option>
                            <option value="regular">Regular</option>
                            <option value="gold">Gold</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1"> Create Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </body>
</html>

<!--        Sign-up action-->
<?php
require_once 'registration_validation.php';
$result=user_registration();

//        Showing messages

if ($result)
{
    if ($result['result']==0)
    {
        ?>
        <div class="col-md-4">
            <div class="alert alert-success" role="alert">
                <strong>Success!</strong> <?php echo $result['message']; ?>
                <script>window.location.href='login.php';</script>
            </div>
        </div>
        <?php
    }
    else
    {
        ?>
        <div class="col-md-4">
            <div class="alert alert-danger" role="alert">
                <strong>Error!</strong> <?php echo $result['message']; ?>
            </div>
        </div>
        <?php
    }
}
?>

