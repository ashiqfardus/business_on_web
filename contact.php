<?php include "config/header.php" ?>

<body class="header_sticky">
<!-- Preloader -->
<!--    <section class="loading-overlay">-->
<!--        <div class="Loading-Page">-->
<!--            <h2 class="loader">Loading</h2>-->
<!--        </div>-->
<!--    </section> -->

<!-- Boxed -->
<div class="boxed">



    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    ?>
    <?php
    if (!isset($_SESSION))
    {
        session_start();
    }
    ?>


    <style>

        .listing-grid .flat-product .featured-product .rate-product {
            padding: 20px 12px 102px 6px;
        }
        .listing-grid .flat-product {
            padding: 5px;
            margin-bottom: 30px;
            height: 10em;
            overflow: hidden;
        }
        .flat-product .rate-product .flat-button:before {
            background: rgba(107, 107, 107, 0.28);
        }
        .widget-form .flat-button {
            color: #777;
            border: 1px solid #f2f2f2;
            padding: 15px 0px 13px 0px;
            background: #fff;
            box-shadow: 1px 2px 5px 0px rgba(0, 0, 0, 0.1);
        }
        .card-header
        {
            padding: 0.25rem 0.45rem;
            margin-bottom: 0;
            background-color: #8a171a !important;
            color: #ffffff;
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
            height: 3em;
            overflow: hidden;
        }
        .bg-dark
        {
            font-family: sans-serif;
            font-size: 100%;
            font-weight: inherit;
            font-style: inherit;
            vertical-align: baseline;
            margin: 0;
            line-height: 1.3;
            border: 0;
            outline: 0;
            background: transparent;
        }
    </style>

    <div class="row">
        <div class="col-md-3">
            <div class=" widget widget-form style2" style="text-align: center;">

                <?php
                $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='contact_ad_case_left' limit 22");
                while ($result=mysqli_fetch_array($sql))
                {
                    $bd_ad_id=$result['bd_ad_id'];
                    $bd_ad_title=$result['bd_ad_title'];
                    $bd_ad_image=$result['bd_ad_image'];
                    $bd_ad_url=$result['bd_ad_url'];
                    $bd_ad_category=$result['bd_ad_category'];
                    $bd_ad_date=$result['bd_ad_date'];
                    ?>
                    <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 10em; margin-top:5px;"></a>
                    <?php
                }
                ?>
            </div>

        </div>
        <div class="col-md-6">
            <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                <div class="form-group col-md-12">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Email</label>
                    <input type="text" class="form-control" name="email" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Subject</label>
                    <input type="text" name="subject" class="form-control">
                </div>
                <div class="form-group col-md-12">
                    <label>Details</label>
                    <textarea type="text" name="subject" class="form-control"></textarea>
                </div>
                <br>
                <div class="form-group col-md-12">
                    <center><input style="color: white;" type="submit" name="submit" class="btn btn-success" value="Submit"></center>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <div class=" widget widget-form style2" style="text-align: center;">

                <?php
                $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='contact_ad_case_right' limit 22");
                while ($result=mysqli_fetch_array($sql))
                {
                    $bd_ad_id=$result['bd_ad_id'];
                    $bd_ad_title=$result['bd_ad_title'];
                    $bd_ad_image=$result['bd_ad_image'];
                    $bd_ad_url=$result['bd_ad_url'];
                    $bd_ad_category=$result['bd_ad_category'];
                    $bd_ad_date=$result['bd_ad_date'];
                    ?>
                    <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 10em; margin-top:5px;"></a>
                    <?php
                }
                ?>
            </div>

        </div>
    </div>





    <?php
    if (isset($_POST['submit']))
    {
        $name=$_POST['name'];
        $email=$_POST['email'];
        $subject=$_POST['subject'];
        $details=$_POST['details'];

        $sql="INSERT INTO messages(name,email,subject,details) VALUES ('$name','$email','$subject','$details')";
        if (mysqli_query($connection,$sql))
        {
            echo "<script>alert('Messages Sent. We will response as soon as possible.')</script>";
        }


    }
    ?>


    <?php include "config/footer.php" ?>


</body>
</html>