<?php include "config/header.php" ?>

<body class="header_sticky">
<!-- Preloader -->
<!--    <section class="loading-overlay">-->
<!--        <div class="Loading-Page">-->
<!--            <h2 class="loader">Loading</h2>-->
<!--        </div>-->
<!--    </section> -->

<!-- Boxed -->
<div class="boxed">

    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    if (!isset($_SESSION))
    {
        session_start();
    }
    ?>


    <section class="main-content page-listing-grid">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <div class=" clearfix">
                        <div class="float-left clearfix">

                            <form novalidate="" class="filter-form clearfix form-inline" id="filter-form" method="post" action="index.php">
                                <p class="book-notes">
                                    <input type="text" placeholder="What are you looking for?" name="question" required="" style="height: 34px;">
                                </p>
                                <p class="book-notes" style="    margin-left: 25px; margin-right: 25px; margin-bottom: 14px;">
                                    IN
                                </p>

                                <p class="book-form-address icon">
                                    <input type="text" placeholder="Postcode/Suburb/State" name="address" required="" style="height: 34px;">
                                </p>

                                <button style="height: 34px; margin-left: 50px; margin-bottom: 19px; padding: 10px; width: 145px;" class="" value="search" type="submit" name="search">Search <i class="ion-ios-search-strong"></i></button>

                            </form>
                        </div>

                        <div class="float-right">
                            <div class="flat-sort">
                                <?php
                                if (isset($_GET['view'])) {
                                    $cat_id = $_GET['view'];
                                }
                                ?>
                                <a href="listing_list_directory.php?view=<?php echo $cat_id ?>" class="course-list-view active"><i class="fa fa-list"></i></a>
                                <a href="all_cat_directory.php?id=<?php echo $cat_id ?>" class="course-grid-view "><i class="fa fa-th"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <h5 class="text-center">Categories</h5>
                            <a href="listing-grid.php"><p><b>All Directory</b></p></a>
                            <hr>
                            <?php

                            //Category search
                            $c_sql=mysqli_query($connection, "SELECT * FROM business_directory_category order by bdc_title");
                            while ($c_res=mysqli_fetch_array($c_sql))
                            {
                                $cat=$c_res['bdc_title'];
                                $cat_id=$c_res['bdc_id'];
                                ?>

                                <li><a href="listing_list_directory.php?view=<?php echo $cat_id;?>"><?php echo $cat; ?></a></li>
                                <?php
                            }

                            ?>
                        </div>
                        <div class="col-md-10">
                            <div class="row">

                                <?php
                                if (isset($_GET['view'])) {
                                    $cat_id = $_GET['view'];
                                    ?>
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>A</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'A%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>B</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'B%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>C</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'C%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>D</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'D%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>E</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'E%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>F</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'F%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>G</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'G%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>H</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'H%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>I</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'I%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>J</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'J%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>K</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'K%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>L</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'L%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <h6 class="text-center"><b>M</b></h6>

                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'M%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>N</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'N%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <h6 class="text-center"><b>O</b></h6>

                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'O%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>P</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'P%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where  bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>Q</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where  bd_title LIKE 'Q%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>R</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where  bd_title LIKE 'R%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <h6 class="text-center"><b>S</b></h6>

                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where  bd_title LIKE 'S%' and bd_category='$cat_id'and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where  bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>T</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'T%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>U</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'U%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>V</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'V%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>W</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'W%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>X</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'X%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>


                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>Y</b></h6>

                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where  bd_title LIKE 'Y%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where  bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>
                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>

                                                <?php }
                                            } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>Z</b></h6>


                                            <?php

                                            $business_search_query = mysqli_query($connection, "SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'Z%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result = mysqli_fetch_array($business_search_query)) {
                                                $bd_id = $result['bd_id'];
                                                $title = $result['bd_title'];
                                                $postcode = $result['bd_postcode_suburb_state'];
                                                $facebook = $result['bd_facebook'];
                                                $twitter = $result['bd_twitter'];
                                                $linkedin = $result['bd_linkedin'];
                                                $android_app = $result['bd_android_app'];
                                                $ios_app = $result['bd_ios_app'];
                                                //$map = $result['bd_map'];
                                                $contact_address = $result['bd_contact_address'];
                                                $contact_telephone = $result['bd_contact_telephone'];
                                                $contact_mobile = $result['bd_contact_mobile'];
                                                $contact_email = $result['bd_contact_email'];
                                                $contact_website = $result['bd_contact_website'];
                                                $about_us = $result['bd_about_us'];
                                                $services = $result['bd_services'];
                                                $status = $result['bd_status'];


                                                //image for business
                                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image = mysqli_fetch_array($image_query)) {
                                                    $image_name = $image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id ?>">
                                                        <p class="text-center"><?php echo $title; ?></p></a>

                                                <?php }
                                            } ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div>




                </div><!-- /.col-lg-9 -->
                <div class="col-lg-4">
                    <div class="sidebar">

                        <div class="widget widget-map" style="text-align: center;">
                            <?php
                            $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='all_directory_ad' limit 18");
                            while ($result=mysqli_fetch_array($sql))
                            {
                                $bd_ad_id=$result['bd_ad_id'];
                                $bd_ad_title=$result['bd_ad_title'];
                                $bd_ad_image=$result['bd_ad_image'];
                                $bd_ad_url=$result['bd_ad_url'];
                                $bd_ad_category=$result['bd_ad_category'];
                                $bd_ad_date=$result['bd_ad_date'];
                                ?>
                                <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                                <?php
                            }
                            ?>
                        </div>
                    </div><!-- /.sidebar -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <!--Search action-->

    <?php include "config/footer.php" ?>




</body>
</html>