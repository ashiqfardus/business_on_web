<?php include "config/header.php" ?>

<body class="header_sticky">
<!-- Preloader -->
<!--    <section class="loading-overlay">-->
<!--        <div class="Loading-Page">-->
<!--            <h2 class="loader">Loading</h2>-->
<!--        </div>-->
<!--    </section> -->

<!-- Boxed -->
<div class="boxed">

    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    if (!isset($_SESSION))
    {
        session_start();
    }
    ?>


    <section class="main-content page-listing-grid">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class=" clearfix">
                        <div class="float-left clearfix">

                            <form novalidate="" class="filter-form clearfix form-inline" id="filter-form" method="post" action="index.php">
                                <p class="book-notes">
                                    <input type="text" placeholder="What are you looking for?" name="question" required="" style="height: 34px;">
                                </p>
                                <p class="book-notes" style="    margin-left: 25px; margin-right: 25px; margin-bottom: 14px;">
                                    IN
                                </p>

                                <p class="book-form-address icon">
                                    <input type="text" placeholder="Postcode/Suburb/State" name="address" required="" style="height: 34px;">
                                </p>

                                <button style="height: 34px; margin-left: 50px; margin-bottom: 19px; padding: 10px; width: 145px;" class="" value="search" type="submit" name="search">Search <i class="ion-ios-search-strong"></i></button>

                            </form>
                        </div>


                        <div class="float-right">
                            <div class="flat-sort">
                                <a href="all_directory_list.php" class="course-list-view active"><i class="fa fa-list"></i></a>
                                <a href="all_directory.php" class="course-grid-view "><i class="fa fa-th"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>A</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'A%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php } ?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>B</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'B%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php } ?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>C</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'C%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php } ?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>D</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'D%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>E</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'E%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>F</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'F%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php } ?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>G</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'G%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>H</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'H%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>I</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'I%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>J</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'J%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>K</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'K%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>L</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'L%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>M</b></h6>

                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'M%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>N</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'N%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>O</b></h6>

                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'O%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>P</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'P%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>Q</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'Q%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>R</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'R%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>S</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'S%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>T</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'T%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>

                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>U</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'U%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>V</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'V%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>W</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'W%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>X</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'X%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>

                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>Y</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'Y%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>
                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                <?php }?>
                            </div>
                            <div class="content-inner active listing-list">

                                <h6 class="text-center"><b>Z</b></h6>


                                <?php

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'Z%' 
                                                                                       ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];

                                    $image_name=$result['bdc_image'];

                                    ?>

                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                <?php }?>
                            </div>
                        </div>
                    </div>

                </div><!-- /.col-lg-9 -->
                <div class="col-lg-4">
                    <div class="sidebar">

                        <div class="widget widget-map" style="text-align: center;">
                            <?php
                            $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='all_directory_ad' limit 18");
                            while ($result=mysqli_fetch_array($sql))
                            {
                                $bd_ad_id=$result['bd_ad_id'];
                                $bd_ad_title=$result['bd_ad_title'];
                                $bd_ad_image=$result['bd_ad_image'];
                                $bd_ad_url=$result['bd_ad_url'];
                                $bd_ad_category=$result['bd_ad_category'];
                                $bd_ad_date=$result['bd_ad_date'];
                                ?>
                                <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                                <?php
                            }
                            ?>
                        </div>
                    </div><!-- /.sidebar -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

        <!--Search action-->

    <?php include "config/footer.php" ?>




</body>
</html>