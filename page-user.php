<?php include "config/header.php" ?>

<body class="header_sticky">
<!-- Preloader -->
<!--<section class="loading-overlay">-->
<!--    <div class="Loading-Page">-->
<!--        <h2 class="loader">Loading</h2>-->
<!--    </div>-->
<!--</section>-->

<!-- Boxed -->
<div class="boxed">

    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    if (!isset($_SESSION))
    {
        session_start();
    }
    if (isset($_SESSION['email']))
    {
        $email=$_SESSION['email'];
        $sql=mysqli_query($connection,"SELECT * FROM user_details where email='$email'");
        while ($row=mysqli_fetch_array($sql))
        {
            $user_id=$row['user_id'];
            $user_role=$row['user_role'];
            $given_name=$row['given_name'];
            $date_of_birth=$row['date_of_birth'];
        }

    ?>
    <section class="flat-row page-user bg-theme">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="flat-user">
                        <a href="#" class="edit" title="">Profile <i class="ion-person"></i></a>

                        <h3 class="name"><?php echo $given_name;?></h3>
                        <ul class="info">
                            <li class="email"><i class="fa fa-envelope"></i><a href="#" title=""><?php echo $email;?></a></li>
                            <li class="face"><i class="fa fa-user-plus"></i><a href="#" title=""><?php echo $user_role;?></a></li>
                            <li class="twiter"><i class="fa fa-calendar"></i><a href="#" title=""><?php echo $date_of_birth;?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="flat-tabs style2" data-effect="fadeIn">
                        <ul class="menu-tab clearfix">
                            <li class="active"><a href="#"><i class="ion-navicon-round"></i> Directory</a></li>
                        </ul><!-- /.menu-tab -->
                    
                    <div class="content-tab listing-user">
                        <div class="content-inner active listing-list">
                            <?php
                                $results_per_page = 6;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_details
                                       where bd_user_id='$user_id'";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                 $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;

                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' 
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    $map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where 	bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                            ?>
                            <div class="flat-product clearfix" style="height:9.5em;">
                                        <div class="featured-product">
                                            <img src="image/<?php echo "$image_name"; ?>" alt="image" style="width:9em; height: 9em; border-radius: 4px;">
                                        </div>
                                        <div class="rate-product">
                                            <div class="link-review clearfix">
                                                <div class="button-product float-left" style="margin-top: -2em;">
                                                    <?php
                                                    if ($status=="Published")
                                                    { ?>
                                                        <button type="button" style="height: 28px; margin-top: 15px;" class="flat-button bg-green" onclick="location.href='#'"><?php echo $status;?></button>
                                                    <?php }
                                                    else{
                                                        ?>
                                                        <button type="button" style="height: 28px; margin-top: 15px;" class="flat-button" onclick="location.href='#'"><?php echo $status;?></button>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="info-product" style=" margin-top: -25px;">
                                                <h6 class="title"><a href="listing-single.php?view=<?php echo $bd_id?>"><?php echo $title?></a></h6>
                                            </div>
                                        </div>
                                        <ul class="wrap-button float-right" style="padding: 15px 18px 0px 0px; !important">
                                          
                                            <li>
                                                <a href="edit_directory.php?edit=<?php echo $bd_id?>"><button type="button" class="button" onclick="location.href='#'"><i class="ion-edit"></i><span>Edit</span></button></a>
                                            </li>
                                            <li>
                                                <a onclick='return ask_for_delete()' href="page-user.php?delete=<?php echo $bd_id ?>"><button type="button" class="button"><i class="ion-trash-a"></i><span>Delete</span></button></a>
                                            </li>
                                        </ul>
                                    </div>

                            <?php } }?>

                            <div class="blog-pagination style2 text-center">
                                <ul class="flat-pagination clearfix">
                                    <?php
                                        for ($i=1;$i<=$number_of_pages;$i++) {
                                            echo '<li class="active"><a href="page-user.php?page=' . $i . '">'.$i.'</a></li>';
                                        }
                                    ?>
                                    <?php
                                    if ($page==$number_of_pages || $number_of_pages==0)
                                    {

                                    }
                                    else {
                                        $page_final=$page+1;
                                        echo '<li class="next">
                                            <a href="page-user.php?page='.$page_final.'">Next</a>
                                        </li>';
                                    }
                                    ?>


                                </ul><!-- /.flat-pagination -->
                            </div>
                        </div>
                    </div> 
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="widget widget-map" style="text-align: center;">
                        <?php
                        $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='bottom_right_ad' limit 8");
                        while ($result=mysqli_fetch_array($sql))
                        {
                            $bd_ad_id=$result['bd_ad_id'];
                            $bd_ad_title=$result['bd_ad_title'];
                            $bd_ad_image=$result['bd_ad_image'];
                            $bd_ad_url=$result['bd_ad_url'];
                            $bd_ad_category=$result['bd_ad_category'];
                            $bd_ad_date=$result['bd_ad_date'];
                            ?>
                            <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php } ?>



    <?php include "config/footer.php" ?>

    <?php

    if (!isset($_SESSION['email']))
    { echo "
    <script type='text/javascript'>
$(window).load(function()
{
    $('#popup_login').modal('show');
});
</script>"; }
    ?>

    <?php
if (isset($_GET['delete']))
{
    $delete_id=$_GET['delete'];
    $img= "SELECT * FROM business_directory_image WHERE bdi_bd_id = '$delete_id'";
    $res=mysqli_query($connection,$img);
    while ($data=mysqli_fetch_array($res))
    {
        $image_id=$data['id'];
        $image_details=$data['bdi_image'];
        $file = "image/$image_details";
        if (!unlink($file))
        {
            echo ("Error deleting $file");
        }
        else
        {
            echo ("Deleted $file");
        }

        $details_delete="DELETE FROM business_directory_details where bd_id='$delete_id'";
        $image_delete="DELETE FROM business_directory_image where bdi_bd_id='$delete_id'";
        $video_delete="DELETE FROM business_directory_video where bdv_bd_id='$delete_id'";


        mysqli_query($connection,$image_delete);
        mysqli_query($connection,$video_delete);
        mysqli_query($connection,$details_delete);
        echo "<script> window.location='page-user.php'; </script>";

    }


}?>

</body>
</html>

<!--Delete script-->

