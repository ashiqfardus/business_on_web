<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
    </div>
    <div class="header-title">
        <h1>All privacy </h1>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="privacy_case.php?source=add_new">
                            <i class="fa fa-plus"></i> Add</a>
                    </div>
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr class="info">


                                <th>ID</th>
                                <th>Title</th>
                                <th>Details</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $ad_search_query=mysqli_query($connection,"SELECT * FROM privacy");
                            while ($result=mysqli_fetch_array($ad_search_query)) {
                                $id=$result['id'];
                                $title=$result['title'];
                                $details=$result['details'];

                                echo "<tr>";
                                echo "<td> $id </td>";
                                echo "<td> $title </td>";
                                echo "<td> $details </td>";

                                echo "<td>
                                                    <a href='privacy_case.php?source=edit&edit_id={$id}' class='btn btn-info btn-sm'>Edit</a>
                                                    <a onclick='return ask_for_delete()' href='privacy_case.php?delete_id={$id}' class='btn btn-danger btn-sm'>Delete</a>
                                                  </td>";
                                echo "</tr>";

                            } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($_GET['delete_id']))
    {
        $delete_id=$_GET['delete_id'];


            $details_delete="DELETE FROM privacy where id='$id'";
            mysqli_query($connection,$details_delete);
            echo "<script> window.location='privacy_case.php'; </script>";



    }?>

</section>
<!-- /.content -->