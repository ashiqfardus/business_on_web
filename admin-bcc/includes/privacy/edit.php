<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-plus"></i>
    </div>
    <div class="header-title">
        <h1>Edit</h1>
        <!-- <small></small> -->
        <!--  <br> -->
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Form controls -->
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="privacy_case.php">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> All</a>
                    </div>
                </div>
                <div class="panel-body">


                    <?php
                    if (isset($_GET['edit_id']))
                    {
                        $bd_ad_id=$_GET['edit_id'];
                        $query=mysqli_query($connection,"SELECT * FROM privacy where id='$bd_ad_id'");

                        while ($result=mysqli_fetch_array($query))
                        {
                            $id=$result['id'];
                            $title=$result['title'];
                            $details=$result['details'];
                        }
                    }
                    ?>



                    <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group col-md-12">
                            <label>AD Title</label>
                            <input type="text" class="form-control" name="title" required value="<?php echo $title;?>">
                        </div>

                        <div class="form-group col-md-12">
                            <label>Details</label>
                            <textarea id="editor1" class="form-control" rows="3" col="50" name="details"> <?php echo $details;?> </textarea>
                        </div>
                        <br>
                        <div class="form-group col-md-12">
                            <center><input type="submit" name="submit" class="btn btn-success" value="Submit"></center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
if (isset($_POST['submit']))
{
    $title=$_POST['title'];
    $details=$_POST['details'];

    $update_query="UPDATE privacy SET title='$title', details='$details' where id='$id'";
    if (mysqli_query($connection,$update_query))
    {
        echo "Success";
    }
}
?>