            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-pencil" aria-hidden="true"></i>
               </div>
               <div class="header-title">
                  <h1>Edit Top Right Ad</h1>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">







<!-- Upload Top Right Ad Image -->






                  <div class="col-md-12">
                    <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                             <center> <a class="btn btn-add " href="top_right_ad_case.php"> 
                              <i class="fa fa-picture-o"></i> All Top Right Ad</a> </center> 
                           </div>
                        </div>
                        <div class="panel-body">





<?php 

  if (isset($_GET['b_id'])) {

      $the_top_right_ad_id = $_GET['b_id'];
    }

    $query = "SELECT * FROM top_right_ad WHERE top_right_ad_id= $the_top_right_ad_id "; //query for showing post.
    $select_top_right_ad_info = mysqli_query($connection, $query);

    while ($row = mysqli_fetch_assoc($select_top_right_ad_info)) {

    $top_right_ad_id = $row['top_right_ad_id'];

    $top_right_ad_image_title = $row['top_right_ad_image_title'];

    $top_right_ad_link = $row['top_right_ad_link'];

    $top_right_ad_image = $row['top_right_ad_image'];


}

  if (isset($_POST['update_top_right_ad'])) {
    
    $top_right_ad_image_title = mysqli_real_escape_string($connection, $_POST['top_right_ad_image_title']);

    $top_right_ad_link = mysqli_real_escape_string($connection, $_POST['top_right_ad_link']);
    
    $top_right_ad_image = $_FILES['top_right_ad_image']['name'];
    $top_right_ad_image_temp = $_FILES['top_right_ad_image']['tmp_name'];
    

    move_uploaded_file($top_right_ad_image_temp, "../admin_images/top_right_ad/$top_right_ad_image");

    if (empty($top_right_ad_image)) {
      
      $query = "SELECT * FROM top_right_ad WHERE top_right_ad_id = $the_top_right_ad_id ";
      $select_image = mysqli_query($connection, $query);

      while ($row = mysqli_fetch_array($select_image)) {
        
        $top_right_ad_image = $row['top_right_ad_image'];
      }
    }

    $query = "UPDATE top_right_ad SET ";
    $query.= "top_right_ad_image_title = '{$top_right_ad_image_title}', ";
    $query.= "top_right_ad_link = '{$top_right_ad_link}', ";
    $query.= "top_right_ad_image = '{$top_right_ad_image}' ";

    $query.= "WHERE top_right_ad_id = {$the_top_right_ad_id} ";

    $update_top_right_ad = mysqli_query($connection,$query);

    if (! $update_top_right_ad) {
      die("QUERY FAILED" . mysqli_error($connection));
    } else {
      echo "<center><h4 style='color:green;font-weight:600;'>Your Top Right Ad Has Been Updated!</h4></center>";
    }
    
  }
    

 ?>

                          



                          <form class="col-md-12 col-sm-12" method="post" action="" enctype="multipart/form-data">

                              <div class="col-md-12">
                                  <center><img src="../admin_images/top_right_ad/<?php echo $top_right_ad_image; ?>" width="25%"></center>
                                  <hr>
                              </div>

                              <div class="form-group col-md-4">
                                 <label>Company Name</label>
                                 <input type="text" class="form-control" name="top_right_ad_image_title" value="<?php echo $top_right_ad_image_title; ?>">
                              </div>

                              <div class="form-group col-md-4">
                                 <label> URL/Website Address</label>
                                 <input type="text" class="form-control" name="top_right_ad_link" value="<?php echo $top_right_ad_link; ?>">
                              </div>                       

                              <div class="form-group col-md-4">
                                 <label>Picture upload</label> <span style="color: red;">(Image Size : 165*125 Pixel)</span>
                                 <input type="file" class="form-control" name="top_right_ad_image">
                                 
                              </div>
                            
                              <br>
                              <div class="form-group col-md-12 text-center">
                                 <input type="submit" name="update_top_right_ad" value="Update" class="btn btn-success">
                                 <br>
                                 <br>
                              </div>
                           </form>
                         </div>
                        </div>
                  </div>





<!-- Upload Top Right Ad Image End -->





               </div>
               
            </section>
            <!-- /.content -->