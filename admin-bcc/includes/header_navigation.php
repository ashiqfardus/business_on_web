         <header class="main-header">
            <a href="index" class="logo">
               <!-- Logo -->
               <span class="logo-mini">
               <p style="color: #fff;"><i class="fa fa-th-list" aria-hidden="true"></i></p>
               </span>
               <span class="logo-lg">
               <img src="assets/images/logo.png" alt="">
               </span>
            </a>
            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top">
               <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                  <!-- Sidebar toggle button-->
                  <span class="sr-only">Toggle navigation</span>
                  <span class="pe-7s-angle-left-circle"></span>
               </a>
               
             <div class="navbar-custom-menu">

                  <ul class="nav navbar-nav">
                     <li class="dropdown dropdown-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="assets/images/avatar.png" class="img-circle" width="45" height="45" alt="user"></a>
                        <ul class="dropdown-menu" >
                           
                           <li><a href="change_password.php"><i class="fa fa-key"></i> Change Your Password</a></li>
                           <li><a href="../admin-login/logout.php">
                              <i class="fa fa-sign-out"></i> Signout</a>
                           </li>
                        </ul>
                     </li>
                  </ul>

                  
               </div>
            </nav>
         </header>