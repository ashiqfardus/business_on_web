<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
    </div>
    <div class="header-title">
        <h1>All Directory </h1>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="business_directory_case.php?source=add_new_directory">
                            <i class="fa fa-plus"></i> Add Directory</a>
                    </div>
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr class="info">


                                <th>ID</th>
                                <th>Video</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details");
                            while ($result=mysqli_fetch_array($business_search_query)) {
                                $bd_id = $result['bd_id'];
                                $bd_user_id = $result['bd_user_id'];
                                $title = $result['bd_title'];
                                $postcode = $result['bd_postcode_suburb_state'];
                                $facebook = $result['bd_facebook'];
                                $twitter = $result['bd_twitter'];
                                $linkedin = $result['bd_linkedin'];
                                $youtube = $result['bd_youtube'];
                                $android_app = $result['bd_android_app'];
                                $ios_app = $result['bd_ios_app'];
                                $map = $result['bd_map'];
                                $contact_address = $result['bd_contact_address'];
                                $contact_telephone = $result['bd_contact_telephone'];
                                $contact_mobile = $result['bd_contact_mobile'];
                                $contact_email = $result['bd_contact_email'];
                                $contact_website = $result['bd_contact_website'];
                                $about_us = $result['bd_about_us'];
                                $services = $result['bd_services'];
                                $status = $result['bd_status'];
                                $category=$result['bd_category'];

                                //image for business
                                $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image
                                                                        where 	bdi_user_id='$bd_user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                while ($image = mysqli_fetch_array($image_query)) {
                                    $image_name = $image['bdi_image'];


                                    $q = mysqli_query($connection, "SELECT * FROM business_directory_video where bdv_bd_id='$bd_id'");
                                    while ($row = mysqli_fetch_array($q)) {
                                        $video = $row['bdv_video'];

                                        echo "<tr>";
                                        echo "<td> $bd_id </td>";
                                        //echo "<td> $given_name </td>";
                                        echo "<td> $video </td>";

                                        echo "<td> $title </td>";
                                        echo "<td><div style='text-align: center;'> <img width='auto' height='100px' src='../image/$image_name' > </div> </td>";
                                        echo "<td>
                                                    <a href='#' class='btn btn-success btn-sm'  data-id='{$bd_id}'  data-toggle='modal' data-target='#$bd_id' data-whatever='mdo'>View</a>
                                                    <a href='business_directory_case.php?source=edit_directory&edit_id={$bd_id}' class='btn btn-info btn-sm'>Edit</a>
                                                    <a onclick='return ask_for_delete()' href='business_directory_case.php?delete_id={$bd_id}' class='btn btn-danger btn-sm'>Delete</a>
                                                  </td>";
                                        echo "</tr>";
                                        ?>


                                        <div class="modal fade bs-example-modal-lg" id="<?php echo $bd_id ?>"
                                             tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span></button>
                                                        <h2 class="modal-title"
                                                            id="gridSystemModalLabel"><?php echo $title ?></h2>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div>
                                                                <img src="../directory/image/<?php echo $image_name ?>"
                                                                     style="width: 100%; height: 500px;">
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 20px;">
                                                            <ul>
                                                                <p style="font-size: 18px;">
                                                                    <b>Username:</b> <?php //echo $given_name ?></p>
                                                                <p style="font-size: 18px;">
                                                                    <b>Category:</b> <?php $q=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_id='$category'");
                                                                    $r=mysqli_fetch_array($q);
                                                                    echo $category_title=$r['bdc_title'];
                                                                    ?></p>
                                                                <p style="font-size: 18px;">
                                                                    <b>Video:</b> <?php echo $video?></p>
                                                                <p style="font-size: 18px;"><b>Postcode/Suburb/State4:</b> <?php echo $postcode ?>
                                                                </p>
                                                                <p style="font-size: 18px;">
                                                                    Facebook:</b> <?php echo $facebook ?></p>
                                                                <p style="font-size: 18px;">
                                                                    Twitter:</b> <?php echo $twitter ?></p>
                                                                <p style="font-size: 18px;">
                                                                    Linkedin:</b> <?php echo $linkedin ?></p>
                                                                <p style="font-size: 18px;">
                                                                    Youtube:</b> <?php echo $youtube ?></p>
                                                                <p style="font-size: 18px;">Android
                                                                    App:</b> <?php echo $android_app ?></p>
                                                                <p style="font-size: 18px;">IOS
                                                                    App:</b> <?php echo $ios_app ?></p>
                                                                <p style="font-size: 18px;">
                                                                    <b>Map:</b> <?php echo $map ?></p>
                                                                <p style="font-size: 18px;">
                                                                    <b>Address:</b> <?php echo $contact_address ?>
                                                                </p>
                                                                <p style="font-size: 18px;">
                                                                    <b>Telephone:</b> <?php echo $contact_telephone ?>
                                                                </p>
                                                                <p style="font-size: 18px;">
                                                                    <b>Mobile:</b> <?php echo $contact_mobile ?></p>
                                                                <p style="font-size: 18px;">
                                                                    <b>Email:</b> <?php echo $contact_email ?></p>
                                                                <p style="font-size: 18px;">
                                                                    <b>Website:</b> <?php echo $contact_website ?>
                                                                </p>
                                                                <p style="font-size: 18px;"><b>About
                                                                        Us:</b> <?php echo $about_us ?></p>
                                                                <p style="font-size: 18px;">
                                                                    <b>Services:</b> <?php echo $services ?></p>
                                                                <p style="font-size: 18px;">
                                                                    <b>Status:</b> <?php echo $status ?></p>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                        <button type="button" class="btn btn-primary">Save changes
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                        <?php
                                    }

                                }
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($_GET['delete_id']))
    {
        $delete_id=$_GET['delete_id'];
        $img= "SELECT * FROM business_directory_image WHERE bdi_bd_id = '$delete_id'";
        $res=mysqli_query($connection,$img);
        while ($data=mysqli_fetch_array($res))
        {
            $image_id=$data['id'];
            $image_details=$data['bdi_image'];
            $file = "../image/$image_details";
            if (!unlink($file))
            {
                echo ("Error deleting $file");
            }
            else
            {
                echo ("Deleted $file");
            }

            $details_delete="DELETE FROM business_directory_details where bd_id='$delete_id'";
            $image_delete="DELETE FROM business_directory_image where bdi_bd_id='$delete_id'";
            $video_delete="DELETE FROM business_directory_video where bdv_bd_id='$delete_id'";


            mysqli_query($connection,$image_delete);
            mysqli_query($connection,$video_delete);
            mysqli_query($connection,$details_delete);
            echo "<script> window.location='business_directory_case.php'; </script>";

        }


    }?>

</section>
<!-- /.content -->