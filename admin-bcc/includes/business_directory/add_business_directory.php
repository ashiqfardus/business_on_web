<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-plus"></i>
    </div>
    <div class="header-title">
        <h1>Add Directory</h1>
        <!-- <small></small> -->
        <!--  <br> -->
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Form controls -->
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="business_directory_case.php">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> All Directory</a>
                    </div>
                </div>
                <div class="panel-body">


                    <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group col-md-6">
                            <label>Business Title</label>
                            <input type="text" class="form-control" name="title" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Postcode/Suburb/State</label>
                            <input type="text" class="form-control" name="postcode" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="nhan">Username</label>
                            <select name="username" class="form-control">
                                <option value="">Select username</option>
                                <?php
                                $q=mysqli_query($connection,"SELECT * FROM user_details where active_status=1");
                                while($res=mysqli_fetch_array($q)) {
                                    $user_id = $res['user_id'];
                                    $given_name = $res['given_name'];
                                    ?>

                                    <option value="<?php echo $user_id ?>"><?php echo $given_name;?></option>

                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="nhan">Status</label>
                            <select name="status" class="form-control">
                                <option value="Published">Choose...</option>
                                <option value="Published">Publish</option>
                                <option value="Draft">Draft</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label class="nhan">Category</label>
                            <select name="category" class="form-control">
                                <option value="">Select category</option>
                                <?php
                                $q=mysqli_query($connection,"SELECT * FROM business_directory_category");
                                while($res=mysqli_fetch_array($q)) {
                                    $bdc_id = $res['bdc_id'];
                                    $bdc_title = $res['bdc_title'];
                                    ?>

                                    <option value="<?php echo $bdc_id ?>"><?php echo $bdc_title;?></option>

                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Image</label> <span style="color: red;">(Size 650*400 Pixel)</span>
                            <input type="file" name="image[]" multiple="multiple" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Video</label>
                            <input type="text" class="form-control" name="video" placeholder="Enter last 11 digit of video link" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Facebook</label>
                            <input type="text" class="form-control" name="facebook" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Twitter</label>
                            <input type="text" class="form-control" name="twitter" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Linkedin</label>
                            <input type="text" class="form-control" name="linkedin" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Youtube</label>
                            <input type="text" class="form-control" name="youtube" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Android App</label>
                            <input type="text" class="form-control" name="android_app" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>IOS App</label>
                            <input type="text" class="form-control" name="ios_app" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Map</label>
                            <input type="text" class="form-control" name="map" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Address</label>
                            <input type="text" class="form-control" name="address" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Telephone</label>
                            <input type="text" class="form-control" name="telephone" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Mobile</label>
                            <input type="text" class="form-control" name="mobile" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Website</label>
                            <input type="text" class="form-control" name="website" required>
                        </div>


                        <div class="form-group col-md-12">
                            <label>About Us</label>
                            <textarea id="editor1" class="form-control" rows="3" col="50" name="about_us" required></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Services</label>
                            <textarea id="editor1" class="form-control" rows="3" col="50" name="services" required></textarea>
                        </div>


                        <br>

                        <div class="form-group col-md-12">
                            <center><input type="submit" name="submit" class="btn btn-success" value="Submit"></center>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</section>


<?php
if (isset($_POST['submit']))
{
    $user_id=$_POST['username'];
    //$image=$_FILES['image']['name'];
    //$image_tmp=$_FILES['image']['tmp_name'];
    $video=$_POST['video'];
    $title=$_POST['title'];
    $postcode=$_POST['postcode'];
    $facebook=$_POST['facebook'];
    $twitter=$_POST['twitter'];
    $linkedin=$_POST['linkedin'];
    $youtube=$_POST['youtube'];
    $android_app=$_POST['android_app'];
    $ios_app=$_POST['ios_app'];
    $map=$_POST['map'];
    $address=$_POST['address'];
    $telephone=$_POST['telephone'];
    $mobile=$_POST['mobile'];
    $email=$_POST['email'];
    $website=$_POST['website'];
    $about_us=$_POST['about_us'];
    $services=$_POST['services'];
    $status=$_POST['status'];
    $category=$_POST['category'];
    $unique_id=mt_rand(50,9999999);




    $insert_query="INSERT INTO business_directory_details(bd_user_id,
                                            bd_title,bd_postcode_suburb_state,bd_facebook,
                                            bd_twitter,bd_linkedin,bd_youtube,bd_android_app,
                                            bd_ios_app,bd_map,bd_contact_address,bd_contact_telephone,
                                            bd_contact_mobile,bd_contact_email,bd_contact_website,bd_about_us,
                                            bd_services,bd_unique_id,bd_status,bd_category) values ('$user_id','$title','$postcode','$facebook',
                                            '$twitter','$linkedin','$youtube','$android_app','$ios_app',
                                            '$map','$address','$telephone','$mobile','$email','$website',
                                            '$about_us','$services','$unique_id','$status','$category')";
    if (mysqli_query($connection,$insert_query))
    {
        $id_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details 
                                                                                where bd_user_id='$user_id' and bd_unique_id='$unique_id'");
        if ($result=mysqli_fetch_array($id_search_query))
        {
            $bd_id=$result['bd_id'];

            $counter = 0;
            $uploadedFiles = array();
            $errors = array();
            $extension = array("jpeg","jpg","png","gif");

            foreach($_FILES["image"]["tmp_name"] as $key=>$tmp_name)
            {
                $temp = $_FILES["image"]["tmp_name"][$key];
                $name = $_FILES["image"]["name"][$key];

                if (empty($temp)) {
                    break;
                }

                $counter++;
                $ext = pathinfo($name, PATHINFO_EXTENSION);
                if(in_array($ext, $extension) == false){
                    $UploadOk = false;
                    array_push($errors, $name." is invalid file type.");
                }
                move_uploaded_file($temp,"../image/$name");
                $image_insert_query="INSERT INTO business_directory_image(bdi_user_id,bdi_bd_id,bdi_image)
                                                              VALUES ('$user_id','$bd_id','$name')";
                mysqli_query($connection,$image_insert_query);
            }
            $video_insert_query="INSERT INTO business_directory_video(bdv_user_id,bdv_bd_id,bdv_video)
                                                              VALUES ('$user_id','$bd_id','$video')";
            if (mysqli_query($connection,$video_insert_query))
            {
                echo "Success";
            }
        }
    }
}
?>