<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-plus"></i>
    </div>
    <div class="header-title">
        <h1>Edit Directory</h1>
        <!-- <small></small> -->
        <!--  <br> -->
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Form controls -->
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="business_directory_case.php">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> All Directory</a>
                    </div>
                </div>
                <div class="panel-body">


                    <?php
                    if (isset($_GET['edit_id']))
                    {
                        $bd_id=$_GET['edit_id'];
                        $query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_id='$bd_id'");
                        $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image where bdi_bd_id='$bd_id'");
                        $video_query=mysqli_query($connection,"SELECT * FROM business_directory_video where bdv_bd_id='$bd_id'");

                        while ($result=mysqli_fetch_array($query))
                        {
                            $user_id=$result['bd_user_id'];
                            $title=$result['bd_title'];
                            $postcode=$result['bd_postcode_suburb_state'];
                            $facebook=$result['bd_facebook'];
                            $twitter=$result['bd_twitter'];
                            $linkedin=$result['bd_linkedin'];
                            $android_app=$result['bd_android_app'];
                            $ios_app=$result['bd_ios_app'];
                            $map=$result['bd_map'];
                            $youtube=$result['bd_youtube'];
                            $contact_address=$result['bd_contact_address'];
                            $contact_telephone=$result['bd_contact_telephone'];
                            $contact_mobile=$result['bd_contact_mobile'];
                            $contact_email=$result['bd_contact_email'];
                            $contact_website=$result['bd_contact_website'];
                            $about_us=$result['bd_about_us'];
                            $services=$result['bd_services'];
                            $status=$result['bd_status'];
                            $category=$result['bd_category'];
                        }
                        while ($image=mysqli_fetch_array($image_query))
                        {
                            $image_name=$image['bdi_image'];
                        }
                        while ($video_result=mysqli_fetch_array($video_query))
                        {

                            $video_name=$video_result['bdv_video'];
                        }

                        $user_search=mysqli_query($connection,"SELECT * FROM user_details where user_id='$user_id'");
                        while ($user=mysqli_fetch_array($user_search))
                        {
                            $given_name=$user['given_name'];
                        }

                    }
                    ?>



                    <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group col-md-6">
                            <label>Business Title</label>
                            <input type="text" class="form-control" name="title" required value="<?php echo $title;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Postcode/Suburb/State</label>
                            <input type="text" class="form-control" name="postcode" required value="<?php echo $postcode;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="nhan">Username</label>
                            <select name="username" class="form-control">
                                <option value="<?php echo $user_id?>"><?php echo $given_name?></option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="nhan">Status</label>

                            <select name="status" class="form-control">
                                <option value="<?php echo $status?>"><?php echo $status?></option>
                                <option value="Published">Publish</option>
                                <option value="Draft">Draft</option>
                            </select>

                        </div>

                        <div class="form-group col-md-12">
                            <label class="nhan">Category</label>
                            <select name="category" class="form-control">
                                <option value="<?php echo $category?>"><?php $q=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_id='$category'");
                                    $r=mysqli_fetch_array($q);
                                    echo $category_title=$r['bdc_title'];
                                    ?></option>
                                <?php
                                $q=mysqli_query($connection,"SELECT * FROM business_directory_category");
                                while($res=mysqli_fetch_array($q)) {
                                    $bdc_id = $res['bdc_id'];
                                    $bdc_title = $res['bdc_title'];
                                    ?>

                                    <option value="<?php echo $bdc_id ?>"><?php echo $bdc_title;?></option>

                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Image</label> <span style="color: red;">(Size 650*400 Pixel)</span>
                            <input type="file" name="image[]" multiple="multiple" class="form-control">
                            <img src="../image/<?php echo "$image_name"; ?>" alt="" width="100">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Video</label>
                            <input type="text" class="form-control" name="video" required value="<?php echo $video_name;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Facebook</label>
                            <input type="text" class="form-control" name="facebook" required value="<?php echo $facebook;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Twitter</label>
                            <input type="text" class="form-control" name="twitter" required value="<?php echo $twitter;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Linkedin</label>
                            <input type="text" class="form-control" name="linkedin" required value="<?php echo $linkedin;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Youtube</label>
                            <input type="text" class="form-control" name="youtube" required value="<?php echo $youtube;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Android App</label>
                            <input type="text" class="form-control" name="android_app" required value="<?php echo $android_app;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>IOS App</label>
                            <input type="text" class="form-control" name="ios_app" required value="<?php echo $ios_app;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Map</label>
                            <input type="text" class="form-control" name="map" required value="<?php echo $map;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Address</label>
                            <input type="text" class="form-control" name="address" required value="<?php echo $contact_address;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Telephone</label>
                            <input type="text" class="form-control" name="telephone" required value="<?php echo $contact_telephone;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Mobile</label>
                            <input type="text" class="form-control" name="mobile" required value="<?php echo $contact_mobile;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" required value="<?php echo $contact_email;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Website</label>
                            <input type="text" class="form-control" name="website" required value="<?php echo $contact_website;?>">
                        </div>


                        <div class="form-group col-md-12">
                            <label>About Us</label>
                            <textarea id="editor1" class="form-control" rows="3" col="50" name="about_us" required><?php echo $about_us;?></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Services</label>
                            <textarea id="editor1" class="form-control" rows="3" col="50" name="services" required><?php echo $services;?></textarea>
                        </div>


                        <br>

                        <div class="form-group col-md-12">
                            <center><input type="submit" name="submit" class="btn btn-success" value="Submit"></center>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</section>
<?php
if (isset($_POST['submit']))
{
//    $image=$_FILES['image']['name'];
//    $image_tmp=$_FILES['image']['tmp_name'];
    $video=$_POST['video'];
    $title=$_POST['title'];
    $postcode=$_POST['postcode'];
    $facebook=$_POST['facebook'];
    $twitter=$_POST['twitter'];
    $linkedin=$_POST['linkedin'];
    $youtube=$_POST['youtube'];
    $android_app=$_POST['android_app'];
    $ios_app=$_POST['ios_app'];
    $map=$_POST['map'];
    $address=$_POST['address'];
    $telephone=$_POST['telephone'];
    $mobile=$_POST['mobile'];
    $email=$_POST['email'];
    $website=$_POST['website'];
    $about_us=$_POST['about_us'];
    $services=$_POST['services'];
    $d_status=$_POST['status'];
    $category=$_POST['category'];

    $insert_query="UPDATE business_directory_details SET 
                                          bd_title='$title', bd_postcode_suburb_state='$postcode',
                                          bd_facebook='$facebook', bd_twitter='$twitter',
                                          bd_linkedin='$linkedin', bd_youtube='$youtube',
                                          bd_android_app='$android_app',  bd_ios_app='$ios_app',
                                          bd_map='$map',bd_contact_address='$address',
                                          bd_contact_telephone='$telephone',bd_contact_mobile='$mobile',
                                          bd_contact_email='$email',bd_contact_website='$website',
                                          bd_about_us='$about_us',bd_services='$services',bd_status='$d_status',bd_category='$category' where bd_id='$bd_id' ";
    if (mysqli_query($connection,$insert_query))
    {
        $counter = 0;
        $uploadedFiles = array();
        $errors = array();
        $extension = array("jpeg","jpg","png","gif");

        foreach($_FILES["image"]["tmp_name"] as $key=>$tmp_name)
        {
            $temp = $_FILES["image"]["tmp_name"][$key];
            $image = $_FILES["image"]["name"][$key];

            if (empty($temp)) {
                break;
            }

            $counter++;
            $ext = pathinfo($image, PATHINFO_EXTENSION);
            if(in_array($ext, $extension) == false){
                $UploadOk = false;
                array_push($errors, $image." is invalid file type.");
            }
            move_uploaded_file($temp,"../image/$image");
            $image_search=mysqli_query($connection,"SELECT * FROM business_directory_image where bdi_bd_id='$bd_id'");
            while ($row=mysqli_fetch_array($image_search))
            {
                $id=$row['id'];

                $image_insert_query="UPDATE business_directory_image SET
                                  bdi_image='$image' where id='$id'";
                mysqli_query($connection,$image_insert_query);
            }


        }
        $video_insert_query="UPDATE business_directory_video SET
                              bdv_video='$video' where bdv_bd_id='$bd_id'";
        if (mysqli_query($connection,$video_insert_query))
        {
            echo "Success";
        }
    }
}
?>