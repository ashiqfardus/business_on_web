<section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-plus"></i>
               </div>
               <div class="header-title">
                  <h1>Add Content</h1>
                  <!-- <small></small> -->
                 <!--  <br> -->
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="content_case"> 
                              <i class="fa fa-list"></i> View All Content</a>  
                           </div>
                        </div>
                        <div class="panel-body">
                           <form class="col-md-12 col-sm-12">
                              <div class="form-group col-md-6">
                                 <label>First Name</label>
                                 <input type="text" class="form-control" placeholder="Enter First Name" required>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Last Name</label>
                                 <input type="text" class="form-control" placeholder="Enter last Name" required>
                              </div>
                             
                              
                              <div class="form-group col-md-6">
                                 <label>Facebook Id</label>
                                 <input type="text" class="form-control" placeholder="Enter Facebook details" required>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Date of Birth</label>
                                 <input id='minMaxExample' type="text" class="form-control" placeholder="Enter Date...">
                              </div>
                              <div class="form-group col-md-12">
                                 <label>Address</label>
                                 <textarea id="editor1" class="form-control" rows="3" col="50" required></textarea>
                              </div>
                              <div class="form-group col-md-4">
                                 <label>Content type</label>
                                 <select class="form-control">
                                    <option selected hidden>Choose</option>
                                    <option>vendor</option>
                                    <option>vip</option>
                                    <option>regular</option>
                                 </select>
                              </div>

                               <div class="form-group col-md-4">
                                 <label>Content type</label>
                                 <select class="form-control">
                                    <option selected hidden>Choose</option>
                                    <option>vendor</option>
                                    <option>vip</option>
                                    <option>regular</option>
                                 </select>
                              </div>

                              <div class="form-group col-md-4">
                                 <label>Picture upload</label>
                                 <input type="file" name="picture">
                                 <input type="hidden" name="old_picture">
                              </div>
                              
                              <!-- <div class="form-check col-md-6">
                                 <label>Status</label><br>
                                 <label class="radio-inline">
                                 <input type="radio" name="status" value="1" checked="checked">Active</label>
                                 <label class="radio-inline"><input type="radio" name="status" value="0" >Inctive</label>
                              </div> -->
                                <br>
                              <div class="reset-button col-md-12 text-center">
                                 <!-- <a href="#" class="btn btn-warning">Reset</a> -->
                                 <a href="#" class="btn btn-success">Save</a>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>