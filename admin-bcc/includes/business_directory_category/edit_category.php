<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-plus"></i>
    </div>
    <div class="header-title">
        <h1>Edit Category</h1>
        <!-- <small></small> -->
        <!--  <br> -->
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Form controls -->
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="business_directory_category_case.php">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> All Category</a>
                    </div>
                </div>
                <div class="panel-body">


                    <?php
                    if (isset($_GET['edit_id']))
                    {
                        $bdc_id=$_GET['edit_id'];
                        $query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_id='$bdc_id'");

                        while ($result=mysqli_fetch_array($query))
                        {
                            $bdc_id=$result['bdc_id'];
                            $bdc_title=$result['bdc_title'];
                            $bdc_image=$result['bdc_image'];
                            $bd_ad_date=$result['bdc_date'];
                        }
                    }
                    ?>



                    <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">

                        <div class="form-group col-md-6">
                            <label>Title</label>
                            <input type="text" class="form-control" name="title" value="<?php echo $bdc_title; ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Image</label> <span style="color: red;">(Size 125*125 Pixel)</span>
                            <input type="file" name="cat_image" class="form-control">
                            <img style="margin-top: 5px;" src="../image/cat_image/<?php echo $bdc_image; ?>" alt="" width="100">
                        </div>
                        <br>
                        <div class="form-group col-md-12">
                            <center><input type="submit" name="submit" class="btn btn-success" value="Submit"></center>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>

<?php
if (isset($_POST['submit']))
{
    $title=$_POST['title'];
    $image=$_FILES['cat_image']['name'];
    $image_tmp=$_FILES['cat_image']['tmp_name'];
    if (empty($image))
    {
        $ad_image=$bdc_image;
    }

    move_uploaded_file($image_tmp,"../image/cat_image/$image");
    $update_query="UPDATE business_directory_category SET bdc_title='$title',bdc_image='$image'
                                                        where bdc_id='$bdc_id'";
    if (mysqli_query($connection,$update_query))
    {
        //echo "<script> window.location='business_directory_category_case.php'; </script>";
    }
}
?>