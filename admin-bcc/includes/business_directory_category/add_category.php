<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-plus"></i>
    </div>
    <div class="header-title">
        <h1>Add Category</h1>
        <!-- <small></small> -->
        <!--  <br> -->
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Form controls -->
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="business_directory_category_case.php">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> All Category</a>
                    </div>
                </div>
                <div class="panel-body">


                    <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group col-md-6">
                            <label>Title</label>
                            <input type="text" class="form-control" name="title" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Image</label> <span style="color: red;">(Size 125*125 Pixel)</span>
                            <input type="file" name="cat_image" class="form-control">
                        </div>
                        <br>
                        <div class="form-group col-md-12">
                            <center><input type="submit" name="submit" class="btn btn-success" value="Submit"></center>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</section>

<?php
if (isset($_POST['submit']))
{
    $title=$_POST['title'];
    $image=$_FILES['cat_image']['name'];
    $image_tmp=$_FILES['cat_image']['tmp_name'];
    move_uploaded_file($image_tmp,"../image/cat_image/$image");

    $insert_query="INSERT INTO business_directory_category(bdc_title,bdc_image)
                        values ('$title','$image')";
    if (mysqli_query($connection,$insert_query))
    {
        echo "Success";
    }
}
?>