<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
    </div>
    <div class="header-title">
        <h1>All Category </h1>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="business_directory_category_case.php?source=add_new">
                            <i class="fa fa-plus"></i> Add New</a>
                    </div>
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr class="info">


                                <th>ID</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Date</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $ad_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category");
                            while ($result=mysqli_fetch_array($ad_search_query)) {
                                $bdc_id=$result['bdc_id'];
                                $bdc_title=$result['bdc_title'];
                                $image=$result['bdc_image'];
                                $bdc_date=$result['bdc_date'];

                                echo "<tr>";
                                echo "<td> $bdc_id </td>";
                                echo "<td> $bdc_title </td>";
                                echo "<td> <div style='text-align: center;'> <img width='auto' height='80px' src='../image/cat_image/$image' > </div> </td>";
                                echo "<td> $bdc_date </td>";
                                echo "<td>
                                                    <a href='business_directory_category_case.php?source=edit&edit_id={$bdc_id}' class='btn btn-info btn-sm'>Edit</a>
                                                    <a onclick='return ask_for_delete()' href='business_directory_category_case.php?delete_id={$bdc_id}' class='btn btn-danger btn-sm'>Delete</a>
                                                  </td>";
                                echo "</tr>";

                            } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($_GET['delete_id']))
    {
        $delete_id=$_GET['delete_id'];
            $details_delete="DELETE FROM business_directory_category where bdc_id='$delete_id'";
            mysqli_query($connection,$details_delete);
            echo "<script> window.location='business_directory_category_case.php'; </script>";



    }?>

</section>
<!-- /.content -->