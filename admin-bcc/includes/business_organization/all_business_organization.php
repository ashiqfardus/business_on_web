            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-list" aria-hidden="true"></i>
               </div>
               <div class="header-title">
                  <h1>All Business Organization</h1>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="business_organization_case.php?source=add_business_organization"> 
                              <i class="fa fa-plus"></i> Add Business Organization</a>  
                           </div>
                        </div>
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                                 <thead>
                                    <tr class="info">
                                       <th>ID</th>
                                       <th>Organization Name</th>
                                       <th>Business Category</th>
                                       <th>Organization Web Address</th>
                                       <th>Organization Short Info</th>
                                       <th>Organization Contact Info</th>
                                       <th>Image</th>
                                       <th>Date of Creation</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>



<?php

$query = "SELECT * FROM business_organization";
$select_business_organization_info = mysqli_query($connection, $query);

while ($row = mysqli_fetch_assoc($select_business_organization_info)) {
   
   $b_o_id = $row['b_o_id'];
   $b_o_c_id = $row['b_o_c_id'];
   $b_o_name = $row['b_o_name'];
   $b_o_link = $row['b_o_link'];
   $b_o_short_info = $row['b_o_short_info'];
   $b_o_contact = $row['b_o_contact'];
   $b_o_image = $row['b_o_image'];
   $b_o_date = $row['b_o_date'];


   echo "<tr>";

   echo "<td> $b_o_id </td>";

   echo "<td> $b_o_name </td>";

    $query = "SELECT * FROM business_category WHERE category_id= {$b_o_c_id} "; //edit categories id.
    $select_categories_id = mysqli_query($connection, $query);

    while ($row = mysqli_fetch_assoc($select_categories_id)) {
    $category_id = $row['category_id'];
    $category_title = $row['category_title'];

    echo "<td>{$category_title}</td>";
    }

   echo "<td> $b_o_link </td>";

   echo "<td> $b_o_short_info </td>";

   echo "<td> $b_o_contact </td>";

   echo "<td> <img width='100%' src='../admin_images/business_organization/$b_o_image' > </td>";

   echo "<td> $b_o_date </td>";

   echo "<td>
               <a style='text-decoration:none; color:green;font-size: 30px;' href='business_organization_case.php?source=edit_business_organization&b_id={$b_o_id}'><i class='fa fa-pencil-square' aria-hidden='true'></i></a>
               <a onclick='return ask_for_delete()' style='text-decoration:none; color:red;font-size: 30px;' href='business_organization_case.php?delete={$b_o_id}'><i class='fa fa-trash' aria-hidden='true'></i></a>
         </td>";

   echo "</tr>";
}

 ?>


                                    
                                 </tbody>
                              </table>




<?php 

    if (isset($_GET['delete'])) {
        $the_b_o_id = $_GET['delete'];
        $query = "SELECT * FROM business_organization WHERE b_o_id = $the_b_o_id";
        $result = mysqli_query($connection, $query);

        while ($row = mysqli_fetch_assoc($result)) {
          $b_o_id = $row['b_o_id'];
          $b_o_image = $row['b_o_image'];
    
        $file = "../admin_images/business_organization/$b_o_image";
            if (!unlink($file))
            {
            echo ("Error deleting $file");
            }
            else
            {
            echo ("Deleted $file");
            }

        $query = "DELETE FROM business_organization WHERE b_o_id = $the_b_o_id ";
        $delete_query = mysqli_query($connection, $query);
        
        echo "<script> window.location.href='business_organization_case.php'; </script>";
    }
}

 ?>





                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </section>
            <!-- /.content -->