<section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-pencil"></i>
               </div>
               <div class="header-title">
                  <h1>Edit Business Organization</h1>
                  <!-- <small></small> -->
                 <!--  <br> -->
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="business_organization_case.php"> 
                              <i class="fa fa-list"></i> All Business Organization</a>  
                           </div>
                        </div>
                        <div class="panel-body">



<?php 

if (isset($_GET['b_id'])) {
   $the_b_o_id = $_GET['b_id'];
}

$query = "SELECT * FROM business_organization WHERE b_o_id =$the_b_o_id";

$select_business_organization_info = mysqli_query($connection, $query);

while ($row = mysqli_fetch_assoc($select_business_organization_info)) {

   $b_o_id = $row['b_o_id'];

   $b_o_c_id = $row['b_o_c_id'];

   $b_o_name = $row['b_o_name'];

   $b_o_link = $row['b_o_link'];

   $b_o_short_info = $row['b_o_short_info'];

   $b_o_contact = $row['b_o_contact'];

   $b_o_image = $row['b_o_image'];


}


if (isset($_POST['update_business_organization'])) {

   $b_o_c_id = mysqli_real_escape_string($connection, $_POST['b_o_c_id']);
   
   $b_o_name = mysqli_real_escape_string($connection, $_POST['b_o_name']);

   $b_o_link = mysqli_real_escape_string($connection, $_POST['b_o_link']);

   $b_o_short_info = mysqli_real_escape_string($connection, $_POST['b_o_short_info']);

   $b_o_contact = mysqli_real_escape_string($connection, $_POST['b_o_contact']);


   $b_o_image = $_FILES['b_o_image']['name'];
   $b_o_image_temp = $_FILES['b_o_image']['tmp_name'];

   move_uploaded_file($b_o_image_temp, "../admin_images/business_organization/$b_o_image");


   if (empty($b_o_image)) {
      
      $query = "SELECT * FROM business_organization WHERE b_o_id = $the_b_o_id ";
      $select_image = mysqli_query($connection, $query);

      while ($row = mysqli_fetch_array($select_image)) {
        
        $b_o_image = $row['b_o_image'];
      }
    }


   $query = " UPDATE business_organization SET ";

   $query .= " b_o_c_id = '{$b_o_c_id}', ";

   $query .= " b_o_name = '{$b_o_name}', ";

   $query .= " b_o_link = '{$b_o_link}', ";

   $query .= " b_o_short_info = '{$b_o_short_info}', ";

   $query .= " b_o_contact = '{$b_o_contact}', ";

   $query .= " b_o_image = '{$b_o_image}' ";

   $query .= " WHERE b_o_id = {$the_b_o_id} ";

   $update_business_organization = mysqli_query($connection, $query);

   if (! $update_business_organization) {
       die("QUERY FAILED" . mysqli_error($connection));
   }
   else {
      echo "<center><h4 style='color:green;font-weight:600;'>Your Event Has Been Updated!</h4></center>";
    }

}


 ?>



                           <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                              <div class="form-group col-md-6">
                                 <label>Organization Name</label>
                                 <input type="text" class="form-control" name="b_o_name" value="<?php echo $b_o_name; ?>">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Organization Web Address</label>
                                 <input type="text" class="form-control" name="b_o_link" value="<?php echo $b_o_link; ?>">
                              </div>
                             
                              
                              <div class="form-group col-md-6">
                                 <label>Organization Logo/Picture</label>
                                 <input type="file" class="form-control" name="b_o_image">
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Business Category</label>
                                 <select class="form-control" id="sel1" name="b_o_c_id">


<?php 


         $query = "SELECT * FROM business_category WHERE category_id= {$b_o_c_id} "; //edit categories id.
         $select_categories_id = mysqli_query($connection, $query);

         while ($row = mysqli_fetch_assoc($select_categories_id)) {
         $category_id = $row['category_id'];
         $category_title = $row['category_title'];

         echo "<option value='$category_id'>{$category_title}</option>";
         }

          ?>

            
         <?php             

         $query = "SELECT * FROM business_category ORDER BY category_id ASC LIMIT 50 OFFSET 1 ";
         $select_categories = mysqli_query($connection, $query);

         while ($row = mysqli_fetch_assoc($select_categories)) {
         $category_id = $row['category_id'];
         $category_title = $row['category_title'];

         echo "<option value='$category_id'>{$category_title}</option>";


         }

         ?>

                                  </select>
                              </div>

                              <div class="col-md-12">
                                 <center>
                                    <img src="../admin_images/<?php echo $b_o_image; ?>" >
                                 </center>
                              </div>
                              
                              <div class="form-group col-md-6">
                                 <label>Organization Short Info</label>
                                 <textarea class="form-control" rows="8" col="50" name="b_o_short_info"><?php echo $b_o_short_info; ?></textarea>
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Organization Contact Info</label>
                                 <textarea class="form-control" rows="8" col="50" name="b_o_contact"><?php echo $b_o_contact; ?></textarea>
                              </div>

                              
                             
                               <div class="form-group col-md-12">
                                 <center><input type="submit" name="update_business_organization" class="btn btn-success" value="Update"></center>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>