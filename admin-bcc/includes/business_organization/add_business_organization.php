<section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-plus"></i>
               </div>
               <div class="header-title">
                  <h1>Add Business Organization</h1>
                  <!-- <small></small> -->
                 <!--  <br> -->
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="business_organization_case.php"> 
                              <i class="fa fa-list" aria-hidden="true"></i> All Business Organization</a>  
                           </div>
                        </div>
                        <div class="panel-body">




<?php 

if (isset($_POST['add_business_organization'])) {

   $b_o_c_id = mysqli_real_escape_string($connection, $_POST['b_o_c_id']);
   
   $b_o_name = mysqli_real_escape_string($connection, $_POST['b_o_name']);

   $b_o_link = mysqli_real_escape_string($connection, $_POST['b_o_link']);

   $b_o_short_info = mysqli_real_escape_string($connection, $_POST['b_o_short_info']);

   $b_o_contact = mysqli_real_escape_string($connection, $_POST['b_o_contact']);


   $b_o_image = $_FILES['b_o_image']['name'];
   $b_o_image = time().$b_o_image;
   $b_o_image_temp = $_FILES['b_o_image']['tmp_name'];

   move_uploaded_file($b_o_image_temp, "../admin_images/business_organization/$b_o_image");

   $query = "INSERT INTO business_organization(b_o_c_id, b_o_name, b_o_link, b_o_short_info, b_o_contact, b_o_image )";
   $query .= "VALUES('{$b_o_c_id}', '{$b_o_name}', '{$b_o_link}', '{$b_o_short_info}', '{$b_o_contact}', '{$b_o_image}' )";

   $create_business_organization_query = mysqli_query($connection, $query);

   if (!$create_business_organization_query) {
      die("Query FAILED" . mysqli_error($connection));
   } else {
      echo "<script> window.location='business_organization_case.php'; </script>";
   }





}






 ?>





                           <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                              <div class="form-group col-md-6">
                                 <label>Organization Name</label>
                                 <input type="text" class="form-control" name="b_o_name" required>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Organization Web Address</label>
                                 <input type="text" class="form-control" name="b_o_link">
                              </div>
                             
                              
                              <div class="form-group col-md-6">
                                 <label>Organization Logo/Picture</label>
                                 <input type="file" class="form-control" name="b_o_image" required>
                              </div>

                               <div class="form-group col-md-6">
                                 <label>Business Category</label>
                                 <select class="form-control" id="sel1" name="b_o_c_id">

<?php 

$query = "SELECT * FROM business_category ORDER BY category_id LIMIT 50 OFFSET 1";
$select_info = mysqli_query($connection, $query);

while($row = mysqli_fetch_assoc($select_info)){

$category_id = $row['category_id'];
$category_title = $row['category_title'];

?>


                              <option value="<?php echo $category_id; ?>"><?php echo $category_title; ?></option> 
<?php } ?>
                                  </select>
                              </div>
                              
                              
                              <div class="form-group col-md-6">
                                 <label>Organization Short Info</label>
                                 <textarea class="form-control" rows="8" col="50" name="b_o_short_info" required></textarea>
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Organization Contact Info</label>
                                 <textarea class="form-control" rows="8" col="50" name="b_o_contact" required></textarea>
                              </div>

                              
                             
                               <div class="form-group col-md-12">
                                 <center><input type="submit" name="add_business_organization" class="btn btn-success" value="Add"></center>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>