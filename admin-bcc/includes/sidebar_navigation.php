<div class="sidebar">
               <!-- sidebar menu -->
               <ul class="sidebar-menu">


<!-- Start 1 -->
                  <li class="active">
                     <a href="index"><i class="fa fa-tachometer"></i><span>Dashboard</span>
                     <span class="pull-right-container">
                     </span>
                     </a>
                  </li>
<!-- End 1 -->
<!-- Start 3 -->
                   <li class="treeview">

                     <a href="#">
                     <i class="fa fa-archive" aria-hidden="true"></i></i><span>About</span>
                     <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                     </a>

                     <ul class="treeview-menu">
                        <li><a href="about_us_case.php?source=history"><i class="fa fa-info" aria-hidden="true"></i>About Us</a></li>
<!--                        <li><a href="about_us_case.php?source=our_mission"><i class="fa fa-info" aria-hidden="true"></i>Our Mission</a></li>-->
<!--                        <li><a href="about_us_case.php?source=governance"><i class="fa fa-info" aria-hidden="true"></i>Governance</a></li>-->
<!--                       -->
<!--                        <li><a href="about_us_case.php?source=members_forum"><i class="fa fa-user" aria-hidden="true"></i>Members</a></li>-->
<!--                        <li><a href="about_us_case.php?source=our_staff"><i class="fa fa-user" aria-hidden="true"></i>Our Staff</a></li>-->
                     </ul>
                  </li>
<!-- End 3 -->

                   <li class="treeview">

                       <a href="service_case.php">
                           <i class="fa fa-archive" aria-hidden="true"></i></i><span>Services</span>
                       </a>
                   </li>


                   <!-- Start 3 -->
                   <li class="treeview">

                       <a href="#">
                           <i class="fa fa-user-circle" aria-hidden="true"></i></i><span>User</span>
                           <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                       </a>

                       <ul class="treeview-menu">
                           <li><a href="user_case.php?source=add_new_user"><i class="fa fa-user-plus" aria-hidden="true"></i>Add New User</a></li>
                           <li><a href="user_case.php?source=view_all_user"><i class="fa fa-list-ol" aria-hidden="true"></i>View All User</a></li>
                       </ul>
                   </li>
                   <!-- End 3 -->

                   <li class="treeview">
                       <a href="#">
                           <i class="fa fa-th"></i><span>Directory category</span>
                           <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                       </a>
                       <ul class="treeview-menu">
                           <li><a href="business_directory_category_case.php?source=add_new">
                                   <i class="fa fa-plus" aria-hidden="true"></i><span>Add New</span>
                                   <span class="pull-right-container">

                     </span>
                               </a></li>


                           <li> <a href="business_directory_category_case.php">
                                   <i class="fa fa-bars" aria-hidden="true"></i></i><span>All Category</span>
                                   <span class="pull-right-container">

                     </span>
                               </a></li>

                       </ul>
                   </li>



                   <!-- New Business directory -->
                   <li class="treeview">

                       <a href="#">
                           <i class="fa fa-diamond" aria-hidden="true"></i></i><span>Business Directory</span>
                           <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                       </a>

                       <ul class="treeview-menu">
                           <li><a href="business_directory_case.php?source=add_new_directory"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New Directory</a></li>
                           <li><a href="business_directory_case.php?source=view_all_directory"><i class="fa fa-list-alt" aria-hidden="true"></i>View All Directory</a></li>
                       </ul>
                   </li>



                   <!--Business directory add-->
                   <li class="treeview">

                       <a href="#">
                           <i class="fa fa-area-chart" aria-hidden="true"></i></i><span>Business Directory AD</span>
                           <span class="pull-right-container">
                               <i class="fa fa-angle-left pull-right"></i>
                           </span>
                       </a>

                       <ul class="treeview-menu">
                           <li class="treeview">
                               <a href="#">
                                   <i class="fa fa-lightbulb-o"></i><span>Top AD</span>
                                   <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                   </span>
                               </a>
                               <ul class="treeview-menu">
                                   <li>
                                       <a href="">
                                           <i class="fa fa-picture-o" aria-hidden="true"></i><span>Top Left Ad</span>
                                           <span class="pull-right-container">
                                               <i class="fa fa-angle-left pull-right"></i>
                                           </span>
                                       </a>

                                       <ul class="treeview-menu">
                                           <li><a href="business_directory_top_left_ad_case.php?source=add_new"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New </a></li>
                                           <li><a href="business_directory_top_left_ad_case.php?source=view_all"><i class="fa fa-list-alt" aria-hidden="true"></i>View All</a></li>
                                       </ul>
                                   </li>
                                   <li>
                                       <a href="">
                                           <i class="fa fa-picture-o" aria-hidden="true"></i><span>Top Right Ad</span>
                                           <span class="pull-right-container">
                                               <i class="fa fa-angle-left pull-right"></i>
                                           </span>
                                       </a>
                                       <ul class="treeview-menu">
                                           <li><a href="business_directory_top_right_ad_case.php?source=add_new"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New </a></li>
                                           <li><a href="business_directory_top_right_ad_case.php?source=view_all"><i class="fa fa-list-alt" aria-hidden="true"></i>View All</a></li>
                                       </ul>
                                   </li>

                               </ul>
                           </li>

                           <li class="treeview">
                               <a href="#">
                                   <i class="fa fa-lightbulb-o"></i><span>Bottom AD</span>
                                   <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                   </span>
                               </a>
                               <ul class="treeview-menu">
                                   <li>
                                       <a href="">
                                           <i class="fa fa-picture-o" aria-hidden="true"></i><span>Bottom Left Ad</span>
                                           <span class="pull-right-container">
                                               <i class="fa fa-angle-left pull-right"></i>
                                           </span>
                                       </a>
                                       <ul class="treeview-menu">
                                           <li><a href="business_directory_bottom_left_ad_case.php?source=add_new"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New </a></li>
                                           <li><a href="business_directory_bottom_left_ad_case.php?source=view_all"><i class="fa fa-list-alt" aria-hidden="true"></i>View All</a></li>
                                       </ul>
                                   </li>
                                   <li>
                                       <a href="">
                                           <i class="fa fa-picture-o" aria-hidden="true"></i><span>Bottom Right Ad</span>
                                           <span class="pull-right-container">
                                               <i class="fa fa-angle-left pull-right"></i>
                                           </span>
                                       </a>
                                       <ul class="treeview-menu">
                                           <li><a href="business_directory_bottom_right_ad_case.php?source=add_new"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New </a></li>
                                           <li><a href="business_directory_bottom_right_ad_case.php?source=view_all"><i class="fa fa-list-alt" aria-hidden="true"></i>View All</a></li>
                                       </ul>
                                   </li>

                               </ul>
                           </li>
                           <li class="treeview">
                               <a href="#">
                                   <i class="fa fa-lightbulb-o"></i><span>Single Page AD</span>
                                   <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                   </span>
                               </a>
                               <ul class="treeview-menu">
                                   <li><a href="business_directory_single_page_ad_case.php?source=add_new"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New </a></li>
                                   <li><a href="business_directory_single_page_ad_case.php?source=view_all"><i class="fa fa-list-alt" aria-hidden="true"></i>View All</a></li>
                               </ul>
                           </li>
                           <li class="treeview">
                               <a href="#">
                                   <i class="fa fa-lightbulb-o"></i><span>All Directory AD</span>
                                   <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                   </span>
                               </a>
                               <ul class="treeview-menu">
                                   <li><a href="business_directory_all_ad_case.php?source=add_new"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New </a></li>
                                   <li><a href="business_directory_all_ad_case.php?source=view_all"><i class="fa fa-list-alt" aria-hidden="true"></i>View All</a></li>
                               </ul>
                           </li>

                           <li class="treeview">
                               <a href="#">
                                   <i class="fa fa-lightbulb-o"></i><span>Business Directory AD </span>
                                   <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                   </span>
                               </a>
                               <ul class="treeview-menu">
                                   <li><a href="business_directory_ad_case.php?source=add_new"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New </a></li>
                                   <li><a href="business_directory_ad_case.php?source=view_all"><i class="fa fa-list-alt" aria-hidden="true"></i>View All</a></li>
                               </ul>
                           </li>

                           <li class="treeview">
                               <a href="#">
                                   <i class="fa fa-lightbulb-o"></i><span>About us AD </span>
                                   <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                   </span>
                               </a>
                               <ul class="treeview-menu">
                                   <li><a href="about_us_ad_case.php?source=add_new"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New </a></li>
                                   <li><a href="about_us_ad_case.php?source=view_all"><i class="fa fa-list-alt" aria-hidden="true"></i>View All</a></li>
                               </ul>
                           </li>

                           <li class="treeview">
                               <a href="#">
                                   <i class="fa fa-lightbulb-o"></i><span>Contact AD </span>
                                   <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                   </span>
                               </a>
                               <ul class="treeview-menu">
                                   <li><a href="contact_ad_case.php?source=add_new"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New </a></li>
                                   <li><a href="contact_ad_case.php?source=view_all"><i class="fa fa-list-alt" aria-hidden="true"></i>View All</a></li>
                               </ul>
                           </li>

                           <li class="treeview">
                               <a href="#">
                                   <i class="fa fa-lightbulb-o"></i><span>Services AD </span>
                                   <span class="pull-right-container">
                                       <i class="fa fa-angle-left pull-right"></i>
                                   </span>
                               </a>
                               <ul class="treeview-menu">
                                   <li><a href="service_ad_case.php?source=add_new"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Add New </a></li>
                                   <li><a href="service_ad_case.php?source=view_all"><i class="fa fa-list-alt" aria-hidden="true"></i>View All</a></li>
                               </ul>
                           </li>

                       </ul>
                   </li>



                   <li class="treeview">

                       <a href="#">
                           <i class="fa fa-user-circle" aria-hidden="true"></i></i><span>Privacy</span>
                           <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                       </a>

                       <ul class="treeview-menu">
                           <li><a href="privacy_case.php?source=add_new"><i class="fa fa-user-plus" aria-hidden="true"></i>Add New </a></li>
                           <li><a href="privacy_case.php?source=view_all"><i class="fa fa-list-ol" aria-hidden="true"></i>View All </a></li>
                       </ul>
                   </li>

                   <li class="treeview">
                       <a href="#">
                           <i class="fa fa-user-circle" aria-hidden="true"></i></i><span>Condition Of Use</span>
                           <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                       </a>

                       <ul class="treeview-menu">
                           <li><a href="condition_of_use_case.php?source=add_new"><i class="fa fa-user-plus" aria-hidden="true"></i>Add New </a></li>
                           <li><a href="condition_of_use_case.php?source=view_all"><i class="fa fa-list-ol" aria-hidden="true"></i>View All </a></li>
                       </ul>
                   </li>

                   <li class="treeview">
                       <a href="#">
                           <i class="fa fa-user-circle" aria-hidden="true"></i></i><span>Terms & Condition</span>
                           <span class="pull-right-container">
                     <i class="fa fa-angle-left pull-right"></i>
                     </span>
                       </a>

                       <ul class="treeview-menu">
                           <li><a href="terms_case.php?source=add_new"><i class="fa fa-user-plus" aria-hidden="true"></i>Add New </a></li>
                           <li><a href="terms_case.php?source=view_all"><i class="fa fa-list-ol" aria-hidden="true"></i>View All </a></li>
                       </ul>
                   </li>


<!-- Start 10 -->
                  <li class="treeview">

                     <a href="messages.php">
                    <i class="fa fa-envelope" aria-hidden="true"></i><span>Messages</span>
                     <span class="pull-right-container">
                     
                     </span>
                     </a>

                  </li>
<!-- End 10 -->
   
                

               </ul>
            </div>