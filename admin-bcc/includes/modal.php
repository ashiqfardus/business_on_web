<?php

$query = "SELECT * FROM join_now_info WHERE join_now_info_id = join_now_info_id";
$select_join_now_info = mysqli_query($connection, $query);

while ($row = mysqli_fetch_assoc($select_join_now_info)) {
   
   $join_now_info_id = $row['join_now_info_id'];
   $f_name = $row['f_name'];   
   $m_name = $row['m_name'];
   $l_name = $row['l_name'];
   $business_name = $row['business_name'];
   $abn_number = $row['abn_number'];
   $email = $row['email'];
   $business_address = $row['business_address'];
   $postal_address = $row['postal_address'];
   $office_number = $row['office_number'];
   $mobile_number = $row['mobile_number'];
   $website = $row['website'];
   $profession = $row['profession'];
   $join_date = $row['join_date'];


?>


<style>


   .modal-dialog{
      width: 60%;
   }

   .modal-header{
      border-radius: 4px;
      background-color: #009688;
   }
   .modal-content {
    border-radius: 4px;
   }

   .modal-header h2{
      color: #fff;
      text-transform: uppercase;
      font-weight: 600;

   }

   .modal-body h5{
      
   }

   .modal-body span{
      background-color: #eee;
    padding: 5px 10px;
    font-size: 15px;
    font-weight: 600;
    color: #000;
    border-radius: 2px;
   }

   .modal-body p{
    font-weight: 600;
    color: #000;
    padding-top: 6px;
   }


</style>









 <div class="modal fade" id="join_now<?php echo $join_now_info_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header modal-header-primary">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <center><h2>INFO OF <?php echo $f_name . " " .$m_name . " " .$l_name; ?></h2></center>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">

                                 <div class="col-md-3">
                                    <h5><span>First Name</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $f_name; ?></p>
                                 </div>
                                 <div class="clearfix"></div>


                                 <div class="col-md-3">
                                    <h5><span>Middle Name</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $m_name; ?></p>
                                 </div>
                                 <div class="clearfix"></div>

                                 <div class="col-md-3">
                                    <h5><span>Last Name</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $l_name; ?></p>
                                 </div>
                                 <div class="clearfix"></div>


                                 <div class="col-md-3">
                                    <h5><span>Business Name</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $business_name; ?></p>
                                 </div>
                                 <div class="clearfix"></div>


                                 <div class="col-md-3">
                                    <h5><span>ABN Number</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $abn_number; ?></p>
                                 </div>
                                 <div class="clearfix"></div>


                                 <div class="col-md-3">
                                    <h5><span>Email</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $email; ?></p>
                                 </div>
                                 <div class="clearfix"></div>


                                 <div class="col-md-3">
                                    <h5><span>Business Address</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $business_address; ?></p>
                                 </div>
                                 <div class="clearfix"></div>


                                 <div class="col-md-3">
                                    <h5><span>Postal Address</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $postal_address; ?></p>
                                 </div>
                                 <div class="clearfix"></div>



                                 <div class="col-md-3">
                                    <h5><span>Office Number</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $office_number; ?></p>
                                 </div>
                                 <div class="clearfix"></div>


                                 <div class="col-md-3">
                                    <h5><span>Mobile Number</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $mobile_number; ?></p>
                                 </div>
                                 <div class="clearfix"></div>



                                 <div class="col-md-3">
                                    <h5><span>Website</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $website; ?></p>
                                 </div>
                                 <div class="clearfix"></div>


                                 <div class="col-md-3">
                                    <h5><span>Profession</span></h5>
                                 </div>

                                 <div class="col-md-9">
                                     <p>: <?php echo $profession; ?></p>
                                 </div>
                                 <div class="clearfix"></div>

                                


                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <center><button type="button" class="btn btn-danger" data-dismiss="modal">Close</button></center>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>

<?php } ?>