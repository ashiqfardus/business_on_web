            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-plus" aria-hidden="true"></i>
               </div>
               <div class="header-title">
                  <h1>Add Staff</h1>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">







<!-- Upload our_board Image -->






                  <div class="col-md-12">

<?php 
if (isset($_POST['add_our_staff'])) {


   $our_staff_name = mysqli_real_escape_string($connection,$_POST['our_staff_name']);
   $our_staff_designation = mysqli_real_escape_string($connection,$_POST['our_staff_designation']);
   
   $our_staff_image = $_FILES['our_staff_image']['name'];
   $our_staff_image = time().$our_staff_image;
   $our_staff_image_temp = $_FILES['our_staff_image']['tmp_name'];

   move_uploaded_file($our_staff_image_temp, "../admin_images/our_staff/$our_staff_image");

   $query = "INSERT INTO about_us_our_staff(our_staff_name, our_staff_designation, our_staff_image) ";

   $query .="VALUES('{$our_staff_name}', '{$our_staff_designation}', '{$our_staff_image}') ";

   $create_our_staff_query = mysqli_query($connection, $query);

   if (!$create_our_staff_query) {
      die("Query FAILED" . mysqli_error($connection));
   } else {
      echo "<script> window.location='about_us_case.php?source=our_staff'; </script>";
   }
   
   
}

 ?>



                     <form class="col-md-12 col-sm-12" method="post" action="" enctype="multipart/form-data">
                              <div class="form-group col-md-4">
                                 <label>Name</label>
                                 <input type="text" class="form-control" name="our_staff_name" required>
                              </div> 
                               <div class="form-group col-md-4">
                                 <label>Designation</label>
                                 <input type="text" class="form-control" name="our_staff_designation" required>
                              </div>                       

                              <div class="form-group col-md-4">
                                 <label>Picture upload</label>
                                 <input type="file" class="form-control" name="our_staff_image" required>
                                 
                              </div>
                            
                              <br>
                              <div class="form-group col-md-12 text-center">
                                 <input type="submit" name="add_our_staff" value="Add staff" class="btn btn-success">
                                 <br>
                                 <br>
                              </div>
                           </form>
                  </div>





<!-- Upload our_board Image End -->







<!-- All our_board Table -->

                  <div class="col-md-12">
                     <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                             <center> <a class="btn btn-add "> 
                              <i class="fa fa-users"></i> All Staff</a> </center> 
                           </div>
                        </div>
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                                 <thead>
                                    <tr class="info">


                                       <th>ID</th>
                                       <th>Staff Name</th>
                                       <th>Staff Designation </th>
                                       <th> Staff Picture </th>
                                       <th>Action</th>
                                       
                                    </tr>
                                 </thead>
                                 <tbody>


<?php

$query = "SELECT * FROM about_us_our_staff";
$select_our_staff = mysqli_query($connection, $query);

while ($row = mysqli_fetch_assoc($select_our_staff)) {
   
   $our_staff_id = $row['our_staff_id'];
   $our_staff_name = $row['our_staff_name'];
   
   $our_staff_designation = $row['our_staff_designation'];
   $our_staff_image = $row['our_staff_image'];


   echo "<tr>";

   echo "<td> $our_staff_id </td>";

   echo "<td> $our_staff_name </td>";

   

   echo "<td> $our_staff_designation </td>";

   echo "<td> <center> <img src='../admin_images/our_staff/$our_staff_image' width='35%'> </center> </td>";

   echo "<td>
               <a style='text-decoration:none; color:green;font-size: 30px;' href='about_us_case.php?source=edit_our_staff&s_id={$our_staff_id}'><i class='fa fa-pencil-square' aria-hidden='true'></i></a>
               <a onclick='return ask_for_delete()' style='text-decoration:none; color:red;font-size: 30px;' href='about_us_case.php?source=our_staff&delete={$our_staff_id}'><i class='fa fa-trash' aria-hidden='true'></i></a>
         </td>";

   echo "</tr>";
}

 ?>


                                    
                                 </tbody>
                              </table>




<?php 

    if (isset($_GET['delete'])) {
        $the_our_staff_id = $_GET['delete'];
        $query = "SELECT * FROM about_us_our_staff WHERE our_staff_id = $the_our_staff_id";
        $result = mysqli_query($connection, $query);

        while ($row = mysqli_fetch_assoc($result)) {
          $our_staff_id = $row['our_staff_id'];
          $our_staff_image = $row['our_staff_image'];
    
        $file = "../admin_images/our_staff/$our_staff_image";
            if (!unlink($file))
            {
            echo ("Error deleting $file");
            }
            else
            {
            echo ("Deleted $file");
            }

        $query = "DELETE FROM about_us_our_staff WHERE our_staff_id = $the_our_staff_id ";
        $delete_query = mysqli_query($connection, $query);
        
        echo "<script> window.location.href='about_us_case.php?source=our_staff'; </script>";
    }
}

 ?>






                           </div>
                        </div>
                     </div>
                  </div>

<!-- All our_board Table End -->






               </div>
               
            </section>
            <!-- /.content -->