            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-plus" aria-hidden="true"></i>
               </div>
               <div class="header-title">
                  <h1>Add Board Member</h1>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">







<!-- Upload our_board Image -->






                  <div class="col-md-12">

<?php 
if (isset($_POST['add_members_forum'])) {


   $members_forum_name = mysqli_real_escape_string($connection,$_POST['members_forum_name']);
   $members_forum_designation = mysqli_real_escape_string($connection,$_POST['members_forum_designation']);
   $members_forum_info = mysqli_real_escape_string($connection,$_POST['members_forum_info']);
   

   $query = "INSERT INTO about_us_members_forum(members_forum_name, members_forum_designation, members_forum_info) ";

   $query .="VALUES('{$members_forum_name}', '{$members_forum_designation}', '{$members_forum_info}') ";

   $create_members_forum = mysqli_query($connection, $query);

   if (!$create_members_forum) {
      die("Query FAILED" . mysqli_error($connection));
   } else {
      echo "<script> window.location='about_us_case.php?source=members_forum'; </script>";
   }
   
   
}

 ?>



                     <form class="col-md-12 col-sm-12" method="post" action="" enctype="multipart/form-data">
                              <div class="form-group col-md-6">
                                 <label>Name</label>
                                 <input type="text" class="form-control" name="members_forum_name" required>
                              </div> 
                               <div class="form-group col-md-6">
                                 <label>Designation</label>
                                 <input type="text" class="form-control" name="members_forum_designation" required>
                              </div>                       

                              <div class="form-group col-md-12">
                                 <label>Info</label>
                                 <textarea class="form-control" cols="5" rows="3" name="members_forum_info"></textarea>
                                 
                              </div>
                            
                              <br>
                              <div class="form-group col-md-12 text-center">
                                 <input type="submit" name="add_members_forum" value="Add Member" class="btn btn-success">
                                 <br>
                                 <br>
                              </div>
                           </form>
                  </div>





<!-- Upload our_board Image End -->







<!-- All our_board Table -->

                  <div class="col-md-12">
                     <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                             <center> <a class="btn btn-add "> 
                              <i class="fa fa-users"></i> All Member</a> </center> 
                           </div>
                        </div>
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                                 <thead>
                                    <tr class="info">


                                       <th>ID</th>
                                       <th>Member Name</th>
                                       <th>Member Designation </th>
                                       <th>Member Info</th>
                                       <th>Action</th>
                                       
                                    </tr>
                                 </thead>
                                 <tbody>


<?php

$query = "SELECT * FROM about_us_members_forum";
$select_about_us_members_forum_info = mysqli_query($connection, $query);

while ($row = mysqli_fetch_assoc($select_about_us_members_forum_info)) {
   
   $members_forum_id = $row['members_forum_id'];
   $members_forum_name = $row['members_forum_name'];   
   $members_forum_designation = $row['members_forum_designation'];
   $members_forum_info = $row['members_forum_info'];
   


   echo "<tr>";

   echo "<td> $members_forum_id </td>";

   echo "<td> $members_forum_name </td>";

   echo "<td> $members_forum_designation </td>";

   echo "<td> $members_forum_info </td>";

   echo "<td>
               <a style='text-decoration:none; color:green;font-size: 30px;' href='about_us_case.php?source=edit_members_forum&f_id={$members_forum_id}'><i class='fa fa-pencil-square' aria-hidden='true'></i></a>
               <a onclick='return ask_for_delete()' style='text-decoration:none; color:red;font-size: 30px;' href='about_us_case.php?source=members_forum&delete={$members_forum_id}'><i class='fa fa-trash' aria-hidden='true'></i></a>
         </td>";

   echo "</tr>";
}

 ?>


                                    
                                 </tbody>
                              </table>




<?php 

    if (isset($_GET['delete'])) {
        $the_members_forum_id = $_GET['delete'];

        $query = "DELETE FROM about_us_members_forum WHERE members_forum_id = $the_members_forum_id ";
        $delete_query = mysqli_query($connection, $query);
        
        echo "<script> window.location='about_us_case.php?source=members_forum'; </script>";

}

 ?>






                           </div>
                        </div>
                     </div>
                  </div>

<!-- All our_board Table End -->






               </div>
               
            </section>
            <!-- /.content -->