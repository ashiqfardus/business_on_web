            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-pencil" aria-hidden="true"></i>
               </div>
               <div class="header-title">
                  <h1>Edit Staff</h1>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">







<!-- Upload our_board Image -->






                  <div class="col-md-12">
                    <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                             <center> <a class="btn btn-add " href="about_us_case.php?source=our_staff"> 
                              <i class="fa fa-users"></i> All Staff</a> </center> 
                           </div>
                        </div>
                        <div class="panel-body">





<?php 

  if (isset($_GET['s_id'])) {

      $the_our_staff_id = $_GET['s_id'];
    }

    $query = "SELECT * FROM about_us_our_staff WHERE our_staff_id= $the_our_staff_id "; //query for showing post.
    $select_our_staff_info = mysqli_query($connection, $query);

    while ($row = mysqli_fetch_assoc($select_our_staff_info)) {

    $our_staff_id = $row['our_staff_id'];

    $our_staff_name = $row['our_staff_name'];

    $our_staff_designation = $row['our_staff_designation'];

    $our_staff_image = $row['our_staff_image'];


}

  if (isset($_POST['update_our_staff'])) {
    
    $our_staff_name = mysqli_real_escape_string($connection, $_POST['our_staff_name']);
    $our_staff_designation = mysqli_real_escape_string($connection, $_POST['our_staff_designation']);
    
    $our_staff_image = $_FILES['our_staff_image']['name'];
    $our_staff_image_temp = $_FILES['our_staff_image']['tmp_name'];
    

    move_uploaded_file($our_staff_image_temp, "../admin_images/our_staff/$our_staff_image");

    if (empty($our_staff_image)) {
      
      $query = "SELECT * FROM about_us_our_staff WHERE our_staff_id = $the_our_staff_id ";
      $select_image = mysqli_query($connection, $query);

      while ($row = mysqli_fetch_array($select_image)) {
        
        $our_staff_image = $row['our_staff_image'];
      }
    }

    $query = "UPDATE about_us_our_staff SET ";
    $query.= "our_staff_name = '{$our_staff_name}', ";
    $query.= "our_staff_designation = '{$our_staff_designation}', ";
    $query.= "our_staff_image = '{$our_staff_image}' ";

    $query.= "WHERE our_staff_id = {$the_our_staff_id} ";

    $update_our_staff = mysqli_query($connection,$query);

    if (! $update_our_staff) {
      die("QUERY FAILED" . mysqli_error($connection));
    } else {
      echo "<center><h4 style='color:green;font-weight:600;'>staff Info Has Been Updated!</h4></center>";
    }
    
  }
    

 ?>

                          



                          <form class="col-md-12 col-sm-12" method="post" action="" enctype="multipart/form-data">
                              <div class="form-group col-md-6">
                                 <label>Name</label>
                                 <input type="text" class="form-control" name="our_staff_name" value="<?php echo $our_staff_name; ?>">
                              </div> 
                               <div class="form-group col-md-6">
                                 <label>Designation</label>
                                 <input type="text" class="form-control" name="our_staff_designation" value="<?php echo $our_staff_designation; ?>">
                              </div> 

                              <div class="col-md-12">
                                <center>
                                  <img src="../admin_images/our_staff/<?php echo $our_staff_image; ?>" >
                                </center>
                              </div>                      

                              <div class="form-group col-md-12">
                                 <label>Picture upload</label>
                                 <input type="file" class="form-control" name="our_staff_image">
                                 
                              </div>
                            
                              <br>
                              <div class="form-group col-md-12 text-center">
                                 <input type="submit" name="update_our_staff" value="update" class="btn btn-success">
                                 <br>
                                 <br>
                              </div>
                           </form>




                         </div>
                        </div>
                  </div>





<!-- Upload our_board Image End -->





               </div>
               
            </section>
            <!-- /.content -->