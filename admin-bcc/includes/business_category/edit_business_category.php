            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-pencil" aria-hidden="true"></i>
               </div>
               <div class="header-title">
                  <h1>Edit Category</h1>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">







<!-- Upload category Image -->






                  <div class="col-md-12">

<?php 

  if (isset($_GET['c_id'])) {

      $the_category_id = $_GET['c_id'];
    }

    $query = "SELECT * FROM business_category WHERE category_id= $the_category_id "; //query for showing post.
    $select_category_info = mysqli_query($connection, $query);

    while ($row = mysqli_fetch_assoc($select_category_info)) {

    $category_id = $row['category_id'];

    $category_title = $row['category_title'];

    $category_image = $row['category_image'];


}

  if (isset($_POST['update_category'])) {
    
    $category_title = mysqli_real_escape_string($connection, $_POST['category_title']);

    $category_image = $_FILES['category_image']['name'];
    $category_image_temp = $_FILES['category_image']['tmp_name'];
    

    move_uploaded_file($category_image_temp, "../admin_images/business_category/$category_image");

    if (empty($category_image)) {
      
      $query = "SELECT * FROM business_category WHERE category_id = $the_category_id ";
      $select_image = mysqli_query($connection, $query);

      while ($row = mysqli_fetch_array($select_image)) {
        
        $category_image = $row['category_image'];
      }
    }
    
    $query = "UPDATE business_category SET ";
    $query.= "category_title = '{$category_title}', ";
    $query.= "category_image = '{$category_image}' ";

    $query.= "WHERE category_id = {$the_category_id} ";

    $update_category = mysqli_query($connection,$query);

    if (! $update_category) {
      die("QUERY FAILED" . mysqli_error($connection));
    } else {
      echo "<center><h4 style='color:green;font-weight:600;'>Category Has Been Updated!</h4></center>";
    }
    
  }
    

 ?>                        


                          <form class="col-md-12 col-sm-12" method="post" action="" enctype="multipart/form-data">
                              <div class="col-md-6 col-md-offset-3">
                                <center>
                                  <img src="../admin_images/business_category/<?php echo $category_image; ?>" class="img-responsive" width="50%">
                                </center>
                              </div>

                              <div class="form-group col-md-6 col-md-offset-3">
                                 <label>Category Title</label>
                                 <input type="text" class="form-control" name="category_title" value="<?php echo $category_title; ?>">
                              </div>

                              <div class="form-group col-md-6 col-md-offset-3">
                                <label>Image</label> <span style="color: red">(Size : 350*200 Pixel)</span>
                                <input type="file" name="category_image" class="form-control">
                              </div>                       

                              <div class="form-group col-md-12 text-center">
                                 <input type="submit" name="update_category" value="Update" class="btn btn-success">
                                 <br>
                                 <br>
                                 <a href="business_category_case.php" class="btn btn-danger">View All Category</a>
                              </div>

                           </form>
                  </div>





<!-- Upload category Image End -->






               </div>
               
            </section>
            <!-- /.content -->