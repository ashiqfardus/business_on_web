            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-plus" aria-hidden="true"></i>
               </div>
               <div class="header-title">
                  <h1>Add Category</h1>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">







<!-- Upload category Image -->






                  <div class="col-md-12">

<?php 


if (isset($_POST['add_category'])) {

   $category_title = mysqli_real_escape_string($connection,$_POST['category_title']);
   
   $category_image = $_FILES['category_image']['name'];
   $category_image = time().$category_image;
   $category_image_temp = $_FILES['category_image']['tmp_name'];

   move_uploaded_file($category_image_temp, "../admin_images/business_category/$category_image");

   $query = "INSERT INTO business_category(category_title, category_image) ";

   $query .="VALUES('{$category_title}', '{$category_image}') ";

   $create_category_query = mysqli_query($connection, $query);

   if (!$create_category_query) {
      die("Query FAILED" . mysqli_error($connection));
   } else {
      echo "<script> window.location='business_category_case.php'; </script>";
   }
    
}


 ?>



                     <form class="col-md-10 col-md-offset-1 col-sm-12" method="post" action="" enctype="multipart/form-data">
                              <div class="form-group col-md-6">
                                 <label>Category Title</label>
                                 <input type="text" class="form-control" name="category_title" required>
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Image</label> <span style="color: red">(Size : 350*200 Pixel)</span>
                                 <input type="file" name="category_image" class="form-control" required>
                              </div>


                              <div class="form-group col-md-12 text-center">
                                 <input type="submit" name="add_category" value="Add New Category" class="btn btn-success">
                                 <br>
                                 <br>
                              </div>
                           </form>
                  </div>





<!-- Upload category Image End -->







<!-- All category Table -->

                  <div class="col-md-12">
                     <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                             <center> <a class="btn btn-add "> 
                              <i class="fa fa-list"></i> All Category</a> </center> 
                           </div>
                        </div>
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                                 <thead>
                                    <tr class="info">


                                       <th>ID</th>
                                       <th>Title</th>
                                       <th>Image</th>
                                       <th>Date of Creation</th>
                                       <th>Action</th>
                                       
                                    </tr>
                                 </thead>
                                 <tbody>


<?php

$query = "SELECT * FROM business_category ORDER BY category_id LIMIT 50 OFFSET 1";
$select_category_info = mysqli_query($connection, $query);

while ($row = mysqli_fetch_assoc($select_category_info)) {
   
   $category_id = $row['category_id'];
   $category_title = $row['category_title'];
   $category_image = $row['category_image'];
   $category_date = $row['category_date'];


   echo "<tr>";

   echo "<td> $category_id </td>";

   echo "<td> $category_title </td>";

   echo "<td> <center> <img width='25%' src='../admin_images/business_category/$category_image'> </center> </td>";

   echo "<td> $category_date </td>";

   echo "<td>
               <a style='text-decoration:none; color:green;font-size: 30px;' href='business_category_case.php?source=edit_business_category&c_id={$category_id}'><i class='fa fa-pencil-square' aria-hidden='true'></i></a>
               <a onclick='return ask_for_delete()' style='text-decoration:none; color:red;font-size: 30px;' href='business_category_case.php?delete={$category_id}'><i class='fa fa-trash' aria-hidden='true'></i></a>
         </td>";

   echo "</tr>";
}

 ?>


                                    
                                 </tbody>
                              </table>


 <?php 

    if (isset($_GET['delete'])) {
        $the_category_id = $_GET['delete'];
        $query = "SELECT * FROM business_category WHERE category_id = $the_category_id";
        $result = mysqli_query($connection, $query);

        while ($row = mysqli_fetch_assoc($result)) {
          $category_id = $row['category_id'];
          $category_image = $row['category_image'];
    
        $file = "../admin_images/business_category/$category_image";
            if (!unlink($file))
            {
            echo ("Error deleting $file");
            }
            else
            {
            echo ("Deleted $file");
            }

        $query = "DELETE FROM business_category WHERE category_id = $the_category_id ";
        $delete_query = mysqli_query($connection, $query);
        
        echo "<script> window.location='business_category_case.php'; </script>";
    }
}

 ?>






                           </div>
                        </div>
                     </div>
                  </div>

<!-- All category Table End -->






               </div>
               
            </section>
            <!-- /.content -->