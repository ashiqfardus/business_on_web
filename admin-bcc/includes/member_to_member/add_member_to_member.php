<section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-plus"></i>
               </div>
               <div class="header-title">
                  <h1>Add Member To Member Benifits</h1>
                  <!-- <small></small> -->
                 <!--  <br> -->
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="member_to_member_case.php"> 
                              <i class="fa fa-list" aria-hidden="true"></i> All Member To Member Benifits</a>  
                           </div>
                        </div>
                        <div class="panel-body">




<?php 

if (isset($_POST['add_member_to_member'])) {
   
   $member_to_member_category_id = mysqli_real_escape_string($connection, $_POST['member_to_member_category_id']);

   $member_to_member_info = mysqli_real_escape_string($connection, $_POST['member_to_member_info']);

   $member_to_member_contact = mysqli_real_escape_string($connection, $_POST['member_to_member_contact']);



   $query = "INSERT INTO member_to_member(member_to_member_category_id, member_to_member_info, member_to_member_contact)";
   $query .= "VALUES('{$member_to_member_category_id}', '{$member_to_member_info}', '{$member_to_member_contact}')";

   $create_member_to_member_query = mysqli_query($connection, $query);

   if (!$create_member_to_member_query) {
      die("Query FAILED" . mysqli_error($connection));
   } else {
      echo "<script> window.location='member_to_member_case.php'; </script>";
   }

}

 ?>





                           <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                              <div class="form-group col-md-5">
                                 <label>Select Member</label>
                                 <select class="form-control" id="sel1" name="member_to_member_category_id">
<?php 

$query = "SELECT * FROM business_organization";
$select_info = mysqli_query($connection, $query);

while($row = mysqli_fetch_assoc($select_info)){

$b_o_id = $row['b_o_id'];
$b_o_name = $row['b_o_name'];

?>


                              <option value="<?php echo $b_o_id; ?>"><?php echo $b_o_name; ?></option> 
<?php } ?>
                                  </select>
                              </div>

                              <div class="form-group col-md-7">
                                 <label>Contact No.</label>
                                 <input type="text" name="member_to_member_contact" class="form-control" required>
                              </div>
                              
                              <div class="form-group col-md-12">
                                 <label>Benifits/Discount Info</label>
                                 <textarea id="editor1" class="form-control" rows="3" col="50" name="member_to_member_info" required></textarea>
                              </div>
                              

                                <br>
                             
                               <div class="form-group col-md-12">
                                 <center><input type="submit" name="add_member_to_member" class="btn btn-success" value="Add"></center>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>