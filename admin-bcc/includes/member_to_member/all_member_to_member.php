            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-list" aria-hidden="true"></i>
               </div>
               <div class="header-title">
                  <h1>All Member To Member Benifits</h1>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="member_to_member_case.php?source=add_member_to_member"> 
                              <i class="fa fa-plus"></i> Add Member to Member Benifits</a>  
                           </div>
                        </div>
                        <div class="panel-body">

                           <div class="table-responsive">
                              <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                                 <thead>
                                    <tr class="info">
                                       <th>ID</th>
                                       <th>Member Name</th>
                                       <th>Offered Discount/Benifit</th>
                                       <th>Contact No.</th>
                                       <th>Date</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>



<?php

$query = "SELECT * FROM member_to_member";
$select_member_to_member_info = mysqli_query($connection, $query);

while ($row = mysqli_fetch_assoc($select_member_to_member_info)) {
   
   $member_to_member_id = $row['member_to_member_id'];
   $member_to_member_category_id = $row['member_to_member_category_id'];
   $member_to_member_info = $row['member_to_member_info'];
   $member_to_member_contact = $row['member_to_member_contact'];
   $date = $row['date'];



   echo "<tr>";

   echo "<td> $member_to_member_id </td>";

    $query = "SELECT * FROM business_organization WHERE b_o_id= {$member_to_member_category_id} "; //edit categories id.
    $select_categories_id = mysqli_query($connection, $query);

    while ($row = mysqli_fetch_assoc($select_categories_id)) {
    $b_o_id = $row['b_o_id'];
    $b_o_name = $row['b_o_name'];

    echo "<td>{$b_o_name}</td>";
    }

   echo "<td> $member_to_member_info </td>";

   echo "<td> $member_to_member_contact </td>";

    echo "<td> $date </td>";

   echo "<td>
               <a style='text-decoration:none; color:green;font-size: 30px;' href='member_to_member_case.php?source=edit_member_to_member&m_id={$member_to_member_id}'><i class='fa fa-pencil-square' aria-hidden='true'></i></a>
               <a onclick='return ask_for_delete()' style='text-decoration:none; color:red;font-size: 30px;' href='member_to_member_case.php?delete={$member_to_member_id}'><i class='fa fa-trash' aria-hidden='true'></i></a>
         </td>";

   echo "</tr>";
}

 ?>


                                    
                                 </tbody>
                              </table>




<?php 

    if (isset($_GET['delete'])) {
        $the_member_to_member_id = $_GET['delete'];
        $query = "DELETE FROM member_to_member WHERE member_to_member_id = $the_member_to_member_id ";
        $delete_query = mysqli_query($connection, $query);
        
        echo "<script> window.location.href='member_to_member_case.php'; </script>";
    }


 ?>





                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </section>
            <!-- /.content -->