<section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-pencil"></i>
               </div>
               <div class="header-title">
                  <h1>Edit Member To Member Benifits</h1>
                  <!-- <small></small> -->
                 <!--  <br> -->
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonlist"> 
                              <a class="btn btn-add " href="member_to_member_case.php"> 
                              <i class="fa fa-calendar"></i> All Member To Member Benifits</a>  
                           </div>
                        </div>
                        <div class="panel-body">



<?php 

if (isset($_GET['m_id'])) {
   $the_member_to_member_id = $_GET['m_id'];
}

$query = "SELECT * FROM member_to_member WHERE member_to_member_id =$the_member_to_member_id";

$select_member_to_member_info = mysqli_query($connection, $query);

while ($row = mysqli_fetch_assoc($select_member_to_member_info)) {

   $member_to_member_id = $row['member_to_member_id'];

   $member_to_member_category_id = $row['member_to_member_category_id'];

   $member_to_member_info = $row['member_to_member_info'];

   $member_to_member_contact = $row['member_to_member_contact'];


}


if (isset($_POST['update_member_to_member'])) {
   
   $member_to_member_category_id = mysqli_real_escape_string($connection, $_POST['member_to_member_category_id']);

   $member_to_member_info = mysqli_real_escape_string($connection, $_POST['member_to_member_info']);
   $member_to_member_info = str_ireplace('\r\n', '', $member_to_member_info);
   $member_to_member_contact = mysqli_real_escape_string($connection, $_POST['member_to_member_contact']);


   $query = " UPDATE member_to_member SET ";

   $query .= " member_to_member_category_id = '{$member_to_member_category_id}', ";

   $query .= " member_to_member_info = '{$member_to_member_info}', ";

   $query .= " member_to_member_contact = '{$member_to_member_contact}' ";



   $query .= " WHERE member_to_member_id = {$the_member_to_member_id} ";

   $update_member_to_member = mysqli_query($connection, $query);

   if (! $update_member_to_member) {
       die("QUERY FAILED" . mysqli_error($connection));
   }
   else {
      echo "<center><h4 style='color:green;font-weight:600;'>Your Info Has Been Updated!</h4></center>";
    }

}


 ?>



                           <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                              <div class="form-group col-md-5">
                                 <label>Select Member</label>
                                 <select class="form-control" id="sel1" name="member_to_member_category_id">




<?php 


         $query = "SELECT * FROM business_organization WHERE b_o_id= {$member_to_member_category_id} "; //edit categories id.
         $select_categories_id = mysqli_query($connection, $query);

         while ($row = mysqli_fetch_assoc($select_categories_id)) {
         $b_o_id = $row['b_o_id'];
         $b_o_name = $row['b_o_name'];

         echo "<option value='$b_o_id'>{$b_o_name}</option>";
         }

          ?>

            
         <?php             

         $query = "SELECT * FROM business_organization ";
         $select_categories = mysqli_query($connection, $query);

         while ($row = mysqli_fetch_assoc($select_categories)) {
         $b_o_id = $row['b_o_id'];
         $b_o_name = $row['b_o_name'];

         echo "<option value='$b_o_id'>{$b_o_name}</option>";


         }

         ?>




                                  </select>
                              </div>

                              <div class="form-group col-md-7">
                                 <label>Contact No.</label>
                                 <input type="text" name="member_to_member_contact" class="form-control" value="<?php echo $member_to_member_contact; ?>">
                              </div>
                              
                              <div class="form-group col-md-12">
                                 <label>Benifits/Discount Info</label>
                                 <textarea id="editor1" class="form-control" rows="3" col="50" name="member_to_member_info"><?php echo $member_to_member_info; ?></textarea>
                              </div>
                              

                                <br>
                             
                               <div class="form-group col-md-12">
                                 <center><input type="submit" name="update_member_to_member" class="btn btn-success" value="Update"></center>
                              </div>
                           </form>




                        </div>
                     </div>
                  </div>
               </div>
            </section>