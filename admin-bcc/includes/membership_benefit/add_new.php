<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-plus"></i>
    </div>
    <div class="header-title">
        <h1>Add Membership Benefit</h1>
        <!-- <small></small> -->
        <!--  <br> -->
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Form controls -->
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="membership_benefit_case.php">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> All</a>
                    </div>
                </div>
                <div class="panel-body">


                    <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group col-md-6">
                            <label>Directory name</label>
                            <select class="custom-select form-control" name="directory_name" required>
                                <option value="">Select Directory Name</option>
                                <?php
                                $query=mysqli_query($connection,"SELECT * FROM business_directory_details");
                                while ($row=mysqli_fetch_array($query)) {
                                    $directory_name = $row['bd_title'];
                                    $directory_id = $row['bd_id'];

                                    ?>
                                    <option value="<?php echo $directory_id;?>"><?php echo $directory_name;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Discount</label>
                            <input type="text" class="form-control" name="discount" placeholder="Example: 30%" required>
                        </div>

                        <br>

                        <div class="form-group col-md-12">
                            <center><input type="submit" name="submit" class="btn btn-success" value="submit"></center>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</section>

<?php
if (isset($_POST['submit']))
{

    $directory_id=$_POST['directory_name'];
    $discount=$_POST['discount'];


        $query1=mysqli_query($connection,"SELECT * FROM member_discount where bd_id='$directory_id'");
        if (mysqli_num_rows($query1)>0)
        {
            echo "<center><h4 style='color:darkred;font-weight:600;'>This directory is already listed.</h4></center>";
        }
        else
        {
            $insert="INSERT INTO member_discount(bd_id,discount) VALUES ('$directory_id','$discount')";
            if (mysqli_query($connection,$insert))
            {
                echo "<center><h4 style='color:green;font-weight:600;'>Discount added for the directory.</h4></center>";
            }
        }




}
?>