<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
    </div>
    <div class="header-title">
        <h1>All </h1>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="membership_benefit_case.php?source=add_new">
                            <i class="fa fa-plus"></i> Add New</a>
                    </div>
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr class="info">


                                <th>ID</th>
                                <th>Title</th>
                                <th>Discount</th>
                                <th>Image</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $b_query=mysqli_query($connection,"SELECT * FROM member_discount");
                            while ($row=mysqli_fetch_array($b_query))
                            {
                                $id=$row['id'];
                                $bd_id=$row['bd_id'];
                                $discount=$row['discount'];
                                $search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_id='$bd_id'");
                                while ($res=mysqli_fetch_array($search_query)) {
                                    $bd_title = $res['bd_title'];

                                    $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($img = mysqli_fetch_array($image_query)) {
                                        $image = $img['bdi_image'];

                                        echo "<tr>";
                                        echo "<td> $id </td>";
                                        echo "<td> $bd_title </td>";
                                        echo "<td> $discount </td>";
                                        echo "<td><div style='text-align: center;'> <img width='120px;' height='100px' src='../directory/image/$image' > </div> </td>";
                                        echo "<td>
                                                    <a href='membership_benefit_case.php?source=edit&edit_id={$id}' class='btn btn-info btn-sm'>Edit</a>
                                                    <a onclick='return ask_for_delete()' href='membership_benefit_case.php?delete_id={$id}' class='btn btn-danger btn-sm'>Delete</a>
                                                  </td>";
                                        echo "</tr>";

                                    }
                                }


                            }
                             ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($_GET['delete_id']))
    {
        $delete_id=$_GET['delete_id'];
            $details_delete="DELETE FROM member_discount where id='$delete_id'";
            mysqli_query($connection,$details_delete);
            echo "<script> window.location='membership_benefit_case.php'; </script>";


    }?>

</section>
<!-- /.content -->