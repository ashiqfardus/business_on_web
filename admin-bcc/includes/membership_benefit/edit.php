<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-plus"></i>
    </div>
    <div class="header-title">
        <h1>Edit</h1>
        <!-- <small></small> -->
        <!--  <br> -->
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Form controls -->
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="membership_benefit_case.php">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> All Directory</a>
                    </div>
                </div>
                <div class="panel-body">


                    <?php
                    if (isset($_GET['edit_id']))
                    {
                        $bd_ad_id=$_GET['edit_id'];
                        $query=mysqli_query($connection,"SELECT * FROM member_discount where id='$bd_ad_id'");

                        while ($result=mysqli_fetch_array($query))
                        {
                            $bd_id=$result['bd_id'];
                            $discount=$result['discount'];
                        }
                    }
                    ?>

                    <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group col-md-6">
                            <label>Directory name</label>
                            <select class="custom-select form-control" name="directory_name" required>
                                <?php
                                $q=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_id='$bd_id'");
                                while ($r=mysqli_fetch_array($q))
                                {
                                    $bd_title=$r['bd_title'];
                                }
                                ?>

                                <option value="<?php  echo $bd_id;?>"><?php echo $bd_title;?></option>
                                <?php
                                $query=mysqli_query($connection,"SELECT * FROM business_directory_details");
                                while ($row=mysqli_fetch_array($query)) {
                                    $directory_name = $row['bd_title'];
                                    $directory_id = $row['bd_id'];

                                    ?>
                                    <option value="<?php echo $directory_id;?>"><?php echo $directory_name;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Discount</label>
                            <input type="text" class="form-control" name="discount" value="<?php echo $discount;?>">
                        </div>

                        <br>

                        <div class="form-group col-md-12">
                            <center><input type="submit" name="submit" class="btn btn-success" value="submit"></center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
if (isset($_POST['submit']))
{
    $id=$_GET['edit_id'];
    $directory_name=$_POST['directory_name'];
    $discount=$_POST['discount'];
    $update_query="UPDATE member_discount SET bd_id='$directory_name',
                                                        discount='$discount'
                                                        where id='$id'";
    if (mysqli_query($connection,$update_query))
    {
        echo "Success";
    }
}
?>