<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
    </div>
    <div class="header-title">
        <h1>All AD </h1>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="business_directory_top_left_ad_case.php?source=add_new">
                            <i class="fa fa-plus"></i> Add New AD</a>
                    </div>
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr class="info">


                                <th>ID</th>
                                <th>Title</th>
                                <th>Url</th>
                                <th>Image</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $ad_search_query=mysqli_query($connection,"SELECT * FROM business_directory_ad where bd_ad_category='top_left_ad'");
                            while ($result=mysqli_fetch_array($ad_search_query)) {
                                $bd_ad_id=$result['bd_ad_id'];
                                $bd_ad_title=$result['bd_ad_title'];
                                $bd_ad_image=$result['bd_ad_image'];
                                $bd_ad_url=$result['bd_ad_url'];
                                $bd_ad_category=$result['bd_ad_category'];
                                $bd_ad_date=$result['bd_ad_date'];

                                            echo "<tr>";
                                            echo "<td> $bd_ad_id </td>";
                                            echo "<td> $bd_ad_title </td>";
                                            echo "<td> $bd_ad_url </td>";

                                            echo "<td> <div style='text-align: center;'> <img width='auto' height='100px' src='../image/ad_image/$bd_ad_image' > </div> </td>";
                                            echo "<td> $bd_ad_category </td>";
                                            echo "<td> $bd_ad_date </td>";
                                            echo "<td>
                                                    <a href='business_directory_top_left_ad_case.php?source=edit&edit_id={$bd_ad_id}' class='btn btn-info btn-sm'>Edit</a>
                                                    <a onclick='return ask_for_delete()' href='business_directory_top_left_ad_case.php?delete_id={$bd_ad_id}' class='btn btn-danger btn-sm'>Delete</a>
                                                  </td>";
                                            echo "</tr>";

                                        } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($_GET['delete_id']))
    {
        $delete_id=$_GET['delete_id'];
        $img= "SELECT * FROM business_directory_ad WHERE bd_ad_id = '$delete_id'";
        $res=mysqli_query($connection,$img);
        while ($data=mysqli_fetch_array($res))
        {
            $image_details=$data['bd_ad_image'];
            $file = "../image/ad_image/$image_details";
            if (!unlink($file))
            {
                echo ("Error deleting $file");
            }
            else
            {
                echo ("Deleted $file");
            }

            $details_delete="DELETE FROM business_directory_ad where bd_ad_id='$delete_id'";
            mysqli_query($connection,$details_delete);
            echo "<script> window.location='business_directory_top_left_ad_case.php'; </script>";

        }


    }?>

</section>
<!-- /.content -->