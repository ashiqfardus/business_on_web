<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-plus"></i>
    </div>
    <div class="header-title">
        <h1>Add Single Page AD</h1>
        <!-- <small></small> -->
        <!--  <br> -->
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Form controls -->
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="business_directory_single_page_ad_case.php">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> All AD</a>
                    </div>
                </div>
                <div class="panel-body">


                    <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group col-md-6">
                            <label>AD Title</label>
                            <input type="text" class="form-control" name="ad_title" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>URL</label>
                            <input type="text" class="form-control" name="ad_url" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Image</label> <span style="color: red;">(Size 650*400 Pixel)</span>
                            <input type="file" name="ad_image" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Category</label>
                            <input type="text" class="form-control" name="ad_category" value="single_page_ad" readonly="readonly">
                        </div>
                        <br>
                        <div class="form-group col-md-12">
                            <center><input type="submit" name="submit" class="btn btn-success" value="Submit"></center>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</section>

<?php
if (isset($_POST['submit']))
{
    $ad_title=$_POST['ad_title'];
    $ad_url=$_POST['ad_url'];
    $ad_image=$_FILES['ad_image']['name'];
    $image_tmp=$_FILES['ad_image']['tmp_name'];
    $ad_category=$_POST['ad_category'];

    move_uploaded_file($image_tmp,"../image/ad_image/$ad_image");

    $insert_query="INSERT INTO business_directory_ad(bd_ad_title,bd_ad_image,bd_ad_url,bd_ad_category)
                        values ('$ad_title','$ad_image','$ad_url','$ad_category')";
    if (mysqli_query($connection,$insert_query))
    {
        echo "Success";
    }
}
?>