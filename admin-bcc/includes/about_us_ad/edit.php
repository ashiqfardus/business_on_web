<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-plus"></i>
    </div>
    <div class="header-title">
        <h1>Edit AD</h1>
        <!-- <small></small> -->
        <!--  <br> -->
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Form controls -->
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="about_us_ad_case.php">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> All Directory</a>
                    </div>
                </div>
                <div class="panel-body">


                    <?php
                    if (isset($_GET['edit_id']))
                    {
                        $bd_ad_id=$_GET['edit_id'];
                        $query=mysqli_query($connection,"SELECT * FROM business_directory_ad where bd_ad_id='$bd_ad_id'");

                        while ($result=mysqli_fetch_array($query))
                        {
                            $bd_ad_id=$result['bd_ad_id'];
                            $bd_ad_title=$result['bd_ad_title'];
                            $bd_ad_image=$result['bd_ad_image'];
                            $bd_ad_url=$result['bd_ad_url'];
                            $bd_ad_category=$result['bd_ad_category'];
                            $bd_ad_date=$result['bd_ad_date'];
                        }
                    }
                    ?>



                    <form class="col-md-12 col-sm-12" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group col-md-6">
                            <label>AD Title</label>
                            <input type="text" class="form-control" name="ad_title" required value="<?php echo $bd_ad_title ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>URL</label>
                            <input type="text" class="form-control" name="ad_url" required value="<?php echo $bd_ad_url ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Image</label> <span style="color: red;">(Size 650*400 Pixel)</span>
                            <input type="file" name="ad_image" class="form-control">
                            <img src="../image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="" width="100">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Category</label>
                            <select class="custom-select form-control" name="ad_category">
                                <option value="<?php echo $bd_ad_category;?>"><?php if($bd_ad_category=='join_now_ad_left'){echo 'Left';} else{echo 'Right';};?></option>
                                <option value="about_us_ad_left">Left</option>
                                <option value="about_us_ad_right">Right</option>
                            </select>
                        </div>
                        <br>
                        <div class="form-group col-md-12">
                            <center><input type="submit" name="submit" class="btn btn-success" value="Submit"></center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
if (isset($_POST['submit']))
{
    $ad_title=$_POST['ad_title'];
    $ad_url=$_POST['ad_url'];
    $ad_image=$_FILES['ad_image']['name'];
    $image_tmp=$_FILES['ad_image']['tmp_name'];
    $ad_category=$_POST['ad_category'];

    if (empty($ad_image))
    {
        $ad_image=$bd_ad_image;
    }
    move_uploaded_file($image_tmp,"../image/ad_image/$ad_image");
    $update_query="UPDATE business_directory_ad SET bd_ad_title='$ad_title',
                                                        bd_ad_url='$ad_url',
                                                        bd_ad_image='$ad_image',
                                                        bd_ad_category='$ad_category' where bd_ad_id='$bd_ad_id'";
    if (mysqli_query($connection,$update_query))
    {
        echo "Success";
    }
}
?>