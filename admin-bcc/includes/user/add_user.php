<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-plus"></i>
    </div>
    <div class="header-title">
        <h1>Add User</h1>
        <!-- <small></small> -->
        <!--  <br> -->
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- Form controls -->
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="user_case.php">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> All User</a>
                    </div>
                </div>
                <div class="panel-body">




                    <?php

                    if (isset($_POST['submit'])) {
                        $last_name=$_POST['last_name'];
                        $given_name=$_POST['given_name'];
                        $date_of_birth=$_POST['date_of_birth'];
                        $email=$_POST['email'];
                        $user_role=$_POST['user_role'];
                        $password=$_POST['password'];

                        //Email check if exists or not
                        $emailcheck=mysqli_query($connection,"Select email from user_details WHERE email='$email'");
                        $echeck=mysqli_num_rows($emailcheck);
                        if ($echeck!=0)
                        {
                            echo "Email already exists";
                        }
                        else
                        {
                            //Id no check if exists or not
                            $lastnamechk=mysqli_query($connection,"Select last_name from user_details WHERE last_name='$last_name'");
                            $id_check=mysqli_num_rows($lastnamechk);
                            if ($id_check!=0)
                            {
                                echo "Last name already exists";
                            }
                            else
                            {
                                //Password length check
                                if (strlen($password)>9 || strlen($password)<6)
                                {
                                    echo 'Password length must be between 6-9 character';
                                }
                                else
                                {
                                    $password=sha1($password);
                                    $query="INSERT INTO user_details(last_name,given_name,date_of_birth,email,password,user_role) 
                                             VALUES ('$last_name','$given_name','$date_of_birth','$email','$password','$user_role')";
                                    if (mysqli_query($connection,$query))
                                    {
                                        echo "Registration complete";
                                        echo "<script> window.location='user_case.php'; </script>";
                                    }
                                }

                            }

                        }


                    }
                    ?>
                    <form class="col-md-12 col-sm-12" action="" method="post">
                        <div class="form-group col-md-6">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="last_name" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Given Name</label>
                            <input type="text" class="form-control" name="given_name" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Date Of Birth</label>
                            <input type="date" name="date_of_birth" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Select User Role</label>
                            <select class="custom-select form-control" name="user_role">
                                <option selected>Select User Role</option>
                                <option value="regular">Regular</option>
                                <option value="gold">Gold</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Create Password</label>
                            <input type="password" class="form-control" name="password" required>
                        </div>

                        <br>

                        <div class="form-group col-md-12">
                            <center><input type="submit" name="submit" class="btn btn-success" value="Submit"></center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
