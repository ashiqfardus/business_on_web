<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="header-icon">
        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
    </div>
    <div class="header-title">
        <h1>All User </h1>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="btn-group" id="buttonlist">
                        <a class="btn btn-add " href="user_case.php?source=add_new_user">
                            <i class="fa fa-plus"></i> Add User</a>
                    </div>
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr class="info">


                                <th>ID</th>
                                <th>Given Name</th>
                                <th>Email</th>
                                <th>User Role</th>
                                <th>Active Status</th>
                                <th>Date Of Birth</th>
<!--                                <th>Action</th>-->

                            </tr>
                            </thead>
                            <tbody>


                            <?php

                            $query = "SELECT * FROM user_details ";
                            $select_news_info = mysqli_query($connection, $query);

                            while ($row = mysqli_fetch_assoc($select_news_info)) {

                                $user_id = $row['user_id'];
                                $given_name = $row['given_name'];
                                $email = $row['email'];
                                $user_role = $row['user_role'];
                                $active_status = $row['active_status'];
                                $date_of_birth = $row['date_of_birth'];


                                echo "<tr>";

                                echo "<td> $user_id </td>";

                                echo "<td> $given_name </td>";

                                echo "<td> $email </td>";

                                echo "<td> $user_role </td>";

                                if ($active_status==1)
                                {
                                    echo "<td>    
                                            <div style='text-align: center;'>
                                                <a class='btn btn-success' href='user_case.php?deactivate={$user_id}'>Deactivate</a>
                                            </div> 
                                          </td>";
                                }
                                else
                                {
                                    echo "<td>    
                                            <div style='text-align: center;'>
                                                <a class='btn btn-danger' href='user_case.php?activate={$user_id}'>Activate</a>
                                            </div> 
                                          </td>";
                                }

                                echo "<td> $date_of_birth </td>";

//                                echo "<td>
//                                       <a style='text-decoration:none; color:green;font-size: 30px;' href='news_case.php?source=edit_news&n_id={$user_id}'><i class='fa fa-pencil-square' aria-hidden='true'></i></a>
//                                       <a onclick='return ask_for_delete()' style='text-decoration:none; color:red;font-size: 30px;' href='user_case.php?delete={$user_id}'><i class='fa fa-trash' aria-hidden='true'></i></a>
//                                 </td>";

                                echo "</tr>";
                            }

                            ?>



                            </tbody>
                        </table>




                        <?php
                        if (isset($_GET['delete'])) {
                            $the_user_id = $_GET['delete'];
                            $query = "DELETE FROM user_details WHERE user_id = $the_user_id ";
                            $delete_query = mysqli_query($connection, $query);

                            echo "<script> window.location='user_case.php'; </script>";
                            }

                        ?>

                        <?php
                        if (isset($_GET['activate'])) {
                            $the_user_id = $_GET['activate'];
                            $query = "UPDATE user_details SET active_status='1' where user_id='$the_user_id'";
                            $activate_query = mysqli_query($connection, $query);

                            echo "<script> window.location='user_case.php'; </script>";
                        }
                        ?>

                        <?php
                        if (isset($_GET['deactivate'])) {
                            $the_user_id = $_GET['deactivate'];
                            $query = "UPDATE user_details SET active_status='0' where user_id='$the_user_id'";
                            $deactivate_query = mysqli_query($connection, $query);

                            echo "<script> window.location='user_case.php'; </script>";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->