<?php include('includes/header.php'); ?>


    <body class="hold-transition sidebar-mini">

    <!-- Site wrapper -->
<div class="wrapper">
<?php include('includes/header_navigation.php'); ?>
    <!-- =============================================== -->
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar -->
        <?php include('includes/sidebar_navigation.php'); ?>
        <!-- /.sidebar -->
    </aside>
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->


        <?php

        if (isset($_GET['source'])) {
            $source = $_GET['source'];
        }else{
            $source='';
        }

        switch($source ) {


            case 'add_new_directory':
                include "includes/business_directory/add_business_directory.php";
                break;

            case 'edit_directory':
                include "includes/business_directory/edit_business_directory.php";
                break;

            case 'view_all_directory':
                include "includes/business_directory/all_business_directory.php";
                break;

            default:
                include "includes/business_directory/all_business_directory.php";
                break;

        }

        ?>



        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php include('includes/footer.php'); ?>