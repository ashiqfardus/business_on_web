<?php include('includes/header.php'); ?>


<body class="hold-transition sidebar-mini">

<!-- Site wrapper -->
<div class="wrapper">
    <?php include('includes/header_navigation.php'); ?>
    <!-- =============================================== -->
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar -->
        <?php include('includes/sidebar_navigation.php'); ?>
        <!-- /.sidebar -->
    </aside>
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->


        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="header-icon">
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </div>
            <div class="header-title">
                <h1>Messages</h1>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- All category Table -->

                <div class="col-md-12">
                    <div class="panel panel-bd lobidisable">
                        <div class="panel-heading">
                            <div class="btn-group" id="buttonlist">
                                <center> <a class="btn btn-add ">
                                        <i class="fa fa-list"></i> All Category</a> </center>
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="table-responsive">
                                <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                                    <thead>
                                    <tr class="info">


                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Subject</th>
                                        <th>Details</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>


                                    <?php

                                    $query = "SELECT * FROM messages ORDER BY id";
                                    $select_category_info = mysqli_query($connection, $query);

                                    while ($row = mysqli_fetch_assoc($select_category_info)) {

                                        $id = $row['id'];
                                        $name = $row['name'];
                                        $email= $row['email'];
                                        $subject= $row['subject'];
                                        $details= $row['details'];


                                        echo "<tr>";

                                        echo "<td> $id; </td>";

                                        echo "<td> $name; </td>";
                                        echo "<td> $email; </td>";


                                        echo "<td> $subject </td>";
                                        echo "<td> $details </td>";

                                        echo "<td>
               <a onclick='return ask_for_delete()' style='text-decoration:none; color:red;font-size: 30px;' href='messages.php?delete={$id}'><i class='fa fa-trash' aria-hidden='true'></i></a>
         </td>";

                                        echo "</tr>";
                                    }

                                    ?>



                                    </tbody>
                                </table>


                                <?php

                                if (isset($_GET['delete'])) {
                                    $the_category_id = $_GET['delete'];
                                        $query = "DELETE FROM messages WHERE id = $the_category_id ";
                                        $delete_query = mysqli_query($connection, $query);

                                        echo "<script> window.location='messages.php'; </script>";

                                }

                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- All category Table End -->






            </div>

        </section>
        <!-- /.content -->


        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<?php include('includes/footer.php'); ?>