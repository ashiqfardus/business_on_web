<?php include "config/header.php" ?>

<body class="header_sticky">
<!-- Preloader -->
<!--    <section class="loading-overlay">-->
<!--        <div class="Loading-Page">-->
<!--            <h2 class="loader">Loading</h2>-->
<!--        </div>-->
<!--    </section> -->

<!-- Boxed -->
<div class="boxed">



    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    ?>
    <?php
    if (!isset($_SESSION))
    {
        session_start();
    }
    ?>


    <style>

        .listing-grid .flat-product .featured-product .rate-product {
            padding: 20px 12px 102px 6px;
        }
        .listing-grid .flat-product {
            padding: 5px;
            margin-bottom: 30px;
            height: 10em;
            overflow: hidden;
        }
        .flat-product .rate-product .flat-button:before {
            background: rgba(107, 107, 107, 0.28);
        }
        .widget-form .flat-button {
            color: #777;
            border: 1px solid #f2f2f2;
            padding: 15px 0px 13px 0px;
            background: #fff;
            box-shadow: 1px 2px 5px 0px rgba(0, 0, 0, 0.1);
        }
        .card-header
        {
            padding: 0.25rem 0.45rem;
            margin-bottom: 0;
            background-color: #8a171a !important;
            color: #ffffff;
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
            height: 3em;
            overflow: hidden;
        }
        .bg-dark
        {
            font-family: sans-serif;
            font-size: 100%;
            font-weight: inherit;
            font-style: inherit;
            vertical-align: baseline;
            margin: 0;
            line-height: 1.3;
            border: 0;
            outline: 0;
            background: transparent;
        }
    </style>

    <section class="page__header">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h1 class="text-center">Terms & Conditions</h1>
                </div>
                <div class="col-md-3"></div>

            </div>
        </div>
    </section>
    <section class="aboutSection">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-lg-3">
                    <div class=" widget widget-form style2" style="text-align: center;">

                        <?php
                        $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='about_us_ad_left' limit 22");
                        while ($result=mysqli_fetch_array($sql))
                        {
                            $bd_ad_id=$result['bd_ad_id'];
                            $bd_ad_title=$result['bd_ad_title'];
                            $bd_ad_image=$result['bd_ad_image'];
                            $bd_ad_url=$result['bd_ad_url'];
                            $bd_ad_category=$result['bd_ad_category'];
                            $bd_ad_date=$result['bd_ad_date'];
                            ?>
                            <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 10em; margin-top:5px;"></a>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="about-details">


                        <?php
                        $query = "SELECT * FROM terms ORDER By id LIMIT 1";
                        $select_info = mysqli_query($connection, $query);

                        $row = mysqli_fetch_assoc($select_info);

                        $details = $row['details'];

                        echo $details;

                        ?>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3">
                    <div class=" widget widget-form style2" style="text-align: center;">

                        <?php
                        $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='about_us_ad_right' limit 22");
                        while ($result=mysqli_fetch_array($sql))
                        {
                            $bd_ad_id=$result['bd_ad_id'];
                            $bd_ad_title=$result['bd_ad_title'];
                            $bd_ad_image=$result['bd_ad_image'];
                            $bd_ad_url=$result['bd_ad_url'];
                            $bd_ad_category=$result['bd_ad_category'];
                            $bd_ad_date=$result['bd_ad_date'];
                            ?>
                            <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 10em; margin-top:5px;"></a>
                            <?php
                        }
                        ?>
                    </div>
                </div>



            </div>
        </div>
    </section>


    <?php include "config/footer.php" ?>


</body>
</html>