<?php include "config/header.php"; ?>

<body class="header_sticky">
<!-- Preloader -->
<!--    <section class="loading-overlay">-->
<!--        <div class="Loading-Page">-->
<!--            <h2 class="loader">Loading</h2>-->
<!--        </div>-->
<!--    </section>-->

<!-- Boxed -->
<div class="boxed">

    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    if (!isset($_SESSION))
    {
        session_start();
    }
    ?>

    <section class="main-content page-listing-grid">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class=" clearfix">
                        <div class="float-left clearfix">

                            <form novalidate="" class="filter-form clearfix form-inline" id="filter-form" method="post" action="index.php">
                                <p class="book-notes">
                                    <input type="text" placeholder="What are you looking for?" name="question" required="" style="height: 34px;">
                                </p>
                                <p class="book-notes" style="    margin-left: 25px; margin-right: 25px; margin-bottom: 14px;">
                                    IN
                                </p>

                                <p class="book-form-address icon">
                                    <input type="text" placeholder="Postcode/Suburb/State" name="address" required="" style="height: 34px;">
                                </p>

                                <button style="height: 34px; margin-left: 50px; margin-bottom: 19px; padding: 10px; width: 145px;" class="" value="search" type="submit" name="search">Search <i class="ion-ios-search-strong"></i></button>

                            </form>
                        </div>
                        <div class="float-right">
                            <div class="flat-sort">

                                <?php
                                if (isset($_GET['id'])) {
                                    $category_id = $_GET['id'];
                                }
                                ?>
                                <a href="listing_list_directory.php?view=<?php echo $category_id;?>" class="course-list-view"><i class="fa fa-list"></i></a>
                                <a href="listing_grid_directory.php?id=<?php echo $category_id;?>" class="course-grid-view active"><i class="fa fa-th"></i></a>
                            </div>
                        </div>
                    </div>
                    <hr>


                    <div class="row">
                        <div class="col-md-2" style="">
                            <h5 class="text-center">Categories</h5>
                            <a href="listing-grid.php"><p><b>All Directory</b></p></a>
                            <hr>

                            <?php
                            //Category search
                            $c_sql=mysqli_query($connection, "SELECT * FROM business_directory_category order by bdc_title");
                            while ($c_res=mysqli_fetch_array($c_sql))
                            {
                                $cat=$c_res['bdc_title'];
                                $cat_id=$c_res['bdc_id'];
                                ?>

                                <li><a href="listing_grid_directory.php?id=<?php echo $cat_id;?>"><?php echo $cat; ?></a></li>
                                <?php
                            }

                            ?>
                        </div>

                        <div class="col-md-10">
                            <H5 CLASS="text-center" >A</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'A%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'A%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center" STYLE="margin-top: 10px ">B</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'B%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'B%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">C</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'C%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'C%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">D</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'D%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'D%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">E</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'E%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'E%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">F</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'F%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'F%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">G</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'G%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'G%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">H</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'H%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'H%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">I</H5>
                            <div class="row  text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'I%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'I%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">J</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'J%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'J%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">K</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'K%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'K%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">L</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'L%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'L%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">M</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'M%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'M%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">N</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'N%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'N%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">O</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'O%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'O%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">P</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'P%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'P%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">Q</H5>
                            <div class="row text-center  listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'Q%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'Q%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">R</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'R%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'R%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">S</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'S%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'S%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">T</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'T%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'T%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">U</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'U%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'U%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">V</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'V%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'V%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">W</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'W%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'W%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">X</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'X%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'X%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">Y</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'Y%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'Y%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                            <H5 CLASS="text-center">Z</H5>
                            <div class="row text-center listing-grid">
                                <?php
                                if (isset($_GET['id']))
                                {
                                    $category_id=$_GET['id'];
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published' and bd_category='$category_id'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;


                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_title like 'Z%' AND bd_category='$category_id'
                                      AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                else
                                {
                                    $results_per_page = 18;
                                    // find out the number of results stored in database
                                    $sql="SELECT * FROM business_directory_details
                                      where bd_status='published'";
                                    $result = mysqli_query($connection, $sql);
                                    $number_of_results = mysqli_num_rows($result);
                                    // determine number of total pages available
                                    $number_of_pages = ceil($number_of_results/$results_per_page);

                                    // determine which page number visitor is currently on
                                    if (!isset($_GET['page'])) {
                                        $page = 1;
                                    } else {
                                        $page = $_GET['page'];
                                    }
                                    // determine the sql LIMIT starting number for the results on the displaying page
                                    $this_page_first_result = ($page-1)*$results_per_page;
                                    $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                      where  bd_title like 'Z%' AND bd_status='published' ORDER BY bd_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");
                                }
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    //$map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];


                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                    while ($image=mysqli_fetch_array($image_query))
                                    {
                                        $image_name=$image['bdi_image'];

                                        ?>
                                        <div class="col-md-2">
                                            <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                                <img class="card-img-top" style="height: 70px;" src="image/<?php echo $image_name; ?>">
                                                <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }} ?>
                            </div>

                        </div>
                    </div>

                    <div class="blog-pagination style2 text-center">
                        <ul class="flat-pagination clearfix">
                            <?php
                            for ($i=1;$i<=$number_of_pages;$i++) {
                                echo '<li class="active"><a href="listing-grid.php?page=' . $i . '">'.$i.'</a></li>';
                            }
                            ?>
                            <?php
                            if ($page==$number_of_pages || $number_of_pages==0)
                            {

                            }
                            else {
                                $page_final=$page+1;
                                echo '<li class="next">
                                            <a href="listing-grid.php?page='.$page_final.'">Next</a>
                                        </li>';
                            }
                            ?>


                        </ul><!-- /.flat-pagination -->
                    </div><!-- /.blog-pagination -->
                </div><!-- /.col-lg-9 -->
                <div class="col-lg-4">
                    <div class="sidebar">

                        <div class="widget widget-map" style="text-align: center;">
                            <?php
                            $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='all_directory_ad' limit 18");
                            while ($result=mysqli_fetch_array($sql))
                            {
                                $bd_ad_id=$result['bd_ad_id'];
                                $bd_ad_title=$result['bd_ad_title'];
                                $bd_ad_image=$result['bd_ad_image'];
                                $bd_ad_url=$result['bd_ad_url'];
                                $bd_ad_category=$result['bd_ad_category'];
                                $bd_ad_date=$result['bd_ad_date'];
                                ?>
                                <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                                <?php
                            }
                            ?>
                        </div>
                    </div><!-- /.sidebar -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    ?>
    <?php include "config/footer.php" ?>
</body>
</html>