
<?php session_start(); ?>

<?php include('../config/database.php');?>



<!DOCTYPE html>

<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin Sign In</title>

        <!-- Favicon and touch icons -->
        <!-- <link rel="shortcut icon" href="assets/dist/img/ico/favicon.png" type="image/x-icon"> -->
        <!-- Bootstrap -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- Bootstrap rtl -->
        <!--<link href="assets/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>-->
        <!-- Pe-icon-7-stroke -->
        <link href="assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css"/>
        <!-- style css -->
        <link href="assets/dist/css/stylecrm.css" rel="stylesheet" type="text/css"/>
        <!-- Theme style rtl -->
        <!--<link href="assets/dist/css/stylecrm-rtl.css" rel="stylesheet" type="text/css"/>-->
    </head>
    <body>
        <!-- Content Wrapper -->
        <div class="login-wrapper">
           
            <div class="container-center" style="margin-top: 7%;">
            <div class="login-area">
                <div class="panel panel-bd panel-custom">
                    <div class="panel-heading">
                        <div class="view-header">
                            <div class="header-icon">
                                <i class="pe-7s-unlock"></i>
                            </div>
                            <div class="header-title">
                                <h3 style="font-weight: 500; color: #fff;">Sign In</h3>
                                <small><strong style="font-weight: 500; color: #fff;">Chamber of Commerce</strong></small>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="" method="post">
                            <div class="form-group">
                                <label class="control-label" for="username">Username <span style="color: red;">*</span></label>
                                <input type="text" placeholder="example@gmail.com" title="Please enter you username" value="" name="admin_username" id="username" class="form-control" required>
                                <span class="help-block small">Your unique username</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Password <span style="color: red;">*</span></label>
                                <input type="password" title="Please enter your password" placeholder="******" value="" name="admin_password" id="password" class="form-control" required>
                                <span class="help-block small">Your strong password</span>
                            </div>

                             <center>
                            <div>
                           
                                <button class="btn btn-add" type="submit" name="signin">Sign In</button>
                                
                                
                                
                                
                            </div>
                            </center>
                        </form>

<?php
if(isset($_POST['signin'])){

$email = mysqli_real_escape_string($connection,$_POST['admin_username']);

$pass = mysqli_real_escape_string($connection, sha1($_POST['admin_password']));



$sel_user = "select * from admin_info where admin_username='$email' AND admin_password='$pass'";

$run_user = mysqli_query($connection, $sel_user);

$check_user = mysqli_num_rows($run_user);

if($check_user>0){

$_SESSION['admin_username']=$email;

echo "<script> window.location='../admin-bcc/index.php'; </script>";

}

else {

echo "<script>alert('Username or Password is not correct, try again!')</script>";

}

}
?>


                        </div>
                        </div>
                </div>
                
            </div>
        </div>
        <div class="col-md-12">
                    <center><strong>Copyright &copy;2017 <a href="http://techsolutionsglobal.com.au/" target="_blank">TechSolutions Global</a>.</strong> All rights reserved.</center>
                </div>
                
        <!-- /.content-wrapper -->
        <!-- jQuery -->
        <script src="assets/plugins/jQuery/jquery-1.12.4.min.js" type="text/javascript"></script>
        <!-- bootstrap js -->
        <script src="assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>

<!-- Developed By Arifur Rahman -->
</html>