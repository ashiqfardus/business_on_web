<?php include "config/header.php" ?>

<body class="header_sticky">
<!-- Preloader -->
<!--    <section class="loading-overlay">-->
<!--        <div class="Loading-Page">-->
<!--            <h2 class="loader">Loading</h2>-->
<!--        </div>-->
<!--    </section> -->

<!-- Boxed -->
<div class="boxed">



    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    ?>
    <?php
    if (!isset($_SESSION))
    {
        session_start();
    }
    ?>


<style>

.listing-grid .flat-product .featured-product .rate-product {
    padding: 20px 12px 102px 6px;
}
.listing-grid .flat-product {
    padding: 5px;
    margin-bottom: 30px;
    height: 10em;
    overflow: hidden;
}
.flat-product .rate-product .flat-button:before {
    background: rgba(107, 107, 107, 0.28);
}
.widget-form .flat-button {
    color: #777;
    border: 1px solid #f2f2f2;
    padding: 15px 0px 13px 0px;
    background: #fff;
    box-shadow: 1px 2px 5px 0px rgba(0, 0, 0, 0.1);
}
.card-header
{
    padding: 0.25rem 0.45rem;
    margin-bottom: 0;
    background-color: #8a171a !important;
    color: #ffffff;
    border-bottom: 1px solid rgba(0, 0, 0, 0.125);
    height: 3em;
    overflow: hidden;
}
.bg-dark
{
font-family: sans-serif;
    font-size: 100%;
    font-weight: inherit;
    font-style: inherit;
    vertical-align: baseline;
    margin: 0;
    line-height: 1.3;
    border: 0;
    outline: 0;
    background: transparent;
}
</style>


    <?php //include "config/menu.php" ?>



    <div class="container-fluid">
        <div class="wrap-form">
            <?php include "config/slider.php" ?>
            <?php include "config/home-search.php" ?>


        </div>
    </div>



    <?php include "config/footer.php" ?>


</body>
</html>