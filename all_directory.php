<?php include "config/header.php"; ?>

<body class="header_sticky">
<!-- Preloader -->
<!--    <section class="loading-overlay">-->
<!--        <div class="Loading-Page">-->
<!--            <h2 class="loader">Loading</h2>-->
<!--        </div>-->
<!--    </section>-->

<!-- Boxed -->
<div class="boxed">

    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    if (!isset($_SESSION))
    {
        session_start();
    }
    ?>

    <section class="main-content page-listing-grid">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="clearfix">
                        <div class="float-left clearfix">

                            <form novalidate="" class="filter-form clearfix form-inline" id="filter-form" method="post" action="index.php">
                                <p class="book-notes">
                                    <input type="text" placeholder="What are you looking for?" name="question" required="" style="height: 34px;">
                                </p>
                                <p class="book-notes" style="    margin-left: 25px; margin-right: 25px; margin-bottom: 14px;">
                                    IN
                                </p>

                                <p class="book-form-address icon">
                                    <input type="text" placeholder="Postcode/Suburb/State" name="address" required="" style="height: 34px;">
                                </p>

                                <button style="height: 34px; margin-left: 50px; margin-bottom: 19px; padding: 10px; width: 145px;" class="" value="search" type="submit" name="search">Search <i class="ion-ios-search-strong"></i></button>

                            </form>
                        </div>

                        <div class="float-right">
                            <div class="flat-sort">
                                <a href="all_directory_list.php" class="course-list-view"><i class="fa fa-list"></i></a>
                                <a href="all_directory.php" class="course-grid-view active"><i class="fa fa-th"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <!--                        <div class="col-md-2">-->
                        <!--                                <h5 class="text-center">Categories</h5>-->
                        <!--                            <a href="listing-grid.php"><p><b>Go Back</b></p></a>-->
                        <!--                            <hr>-->
                        <!--                                --><?php
                        //                                //Category search
                        //                                $c_sql=mysqli_query($connection, "SELECT * FROM business_directory_category order by bdc_title");
                        //                                while ($c_res=mysqli_fetch_array($c_sql))
                        //                                {
                        //                                    $cat=$c_res['bdc_title'];
                        //                                    $cat_id=$c_res['bdc_id'];
                        //                                    ?>
                        <!---->
                        <!--                                    <li><a href="all_cat_directory.php?id=--><?php //echo $cat_id;?><!--">--><?php //echo $cat; ?><!--</a></li>-->
                        <!--                                    --><?php
                        //                                }
                        //
                        //                                ?>
                        <!--                        </div>-->
                        <div class="col-md-10">


                            <h5 class="text-center">A</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'A%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">B</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'B%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">C</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'C%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">D</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'D%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">E</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'E%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">F</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'F%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">G</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'G%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">H</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'H%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">I</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'I%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">J</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'J%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">K</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'K%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">L</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'L%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">M</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'M%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">N</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'N%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">O</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'O%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">P</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'P%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">Q</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'Q%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">R</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'R%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">S</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'S%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">T</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'T%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">U</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'U%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">V</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'V%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">W</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'W%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">X</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'X%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">Y</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'Y%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                            <h5 class="text-center">Z</h5>
                            <div class="row listing-grid">

                                <?php
                                $results_per_page = 18;
                                // find out the number of results stored in database
                                $sql="SELECT * FROM business_directory_category";
                                $result = mysqli_query($connection, $sql);
                                $number_of_results = mysqli_num_rows($result);
                                // determine number of total pages available
                                $number_of_pages = ceil($number_of_results/$results_per_page);

                                // determine which page number visitor is currently on
                                if (!isset($_GET['page'])) {
                                    $page = 1;
                                } else {
                                    $page = $_GET['page'];
                                }
                                // determine the sql LIMIT starting number for the results on the displaying page
                                $this_page_first_result = ($page-1)*$results_per_page;
                                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_title like 'Z%' ORDER BY bdc_title ASC
                                                                                        LIMIT $this_page_first_result,$results_per_page");

                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bdc_id=$result['bdc_id'];
                                    $title=$result['bdc_title'];
                                    $image=$result['bdc_image'];


                                    ?>
                                    <div class="col-md-2">
                                        <div class="card" style="width: 6.7rem; margin-top: 15px;">
                                            <img class="card-img-top" style="height: 70px;" src="image/cat_image/<?php echo $image; ?>">
                                            <div class="card-header bg-dark" style="height: 4em; font-family: sans-serif; line-height: 1.1; background-color: #8a171a !important;">
                                                <a href="all_cat_directory.php?id=<?php echo $bdc_id?>" style="font-size:10px; color:#ffffff"><?php echo $title; ?></a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
                            </div>

                        </div>
                    </div>




                    <div class="blog-pagination style2 text-center">
                        <ul class="flat-pagination clearfix">
                            <?php
                            for ($i=1;$i<=$number_of_pages;$i++) {
                                echo '<li class="active"><a href="listing-grid.php?page=' . $i . '">'.$i.'</a></li>';
                            }
                            ?>
                            <?php
                            if ($page==$number_of_pages || $number_of_pages==0)
                            {

                            }
                            else {
                                $page_final=$page+1;
                                echo '<li class="next">
                                            <a href="listing-grid.php?page='.$page_final.'">Next</a>
                                        </li>';
                            }
                            ?>


                        </ul><!-- /.flat-pagination -->
                    </div><!-- /.blog-pagination -->
                </div><!-- /.col-lg-9 -->
                <div class="col-lg-4">
                    <div class="sidebar">

                        <div class="widget widget-map" style="text-align: center;">
                            <?php
                            $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='all_directory_ad' limit 18");
                            while ($result=mysqli_fetch_array($sql))
                            {
                                $bd_ad_id=$result['bd_ad_id'];
                                $bd_ad_title=$result['bd_ad_title'];
                                $bd_ad_image=$result['bd_ad_image'];
                                $bd_ad_url=$result['bd_ad_url'];
                                $bd_ad_category=$result['bd_ad_category'];
                                $bd_ad_date=$result['bd_ad_date'];
                                ?>
                                <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                                <?php
                            }
                            ?>
                        </div>
                    </div><!-- /.sidebar -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    ?>
    <?php include "config/footer.php" ?>
</body>
</html>