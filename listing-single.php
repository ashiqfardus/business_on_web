<?php include "config/header.php" ?>

<body class="header_sticky">
<!-- Preloader -->
<!--<section class="loading-overlay">-->
<!--    <div class="Loading-Page">-->
<!--        <h2 class="loader">Loading</h2>-->
<!--    </div>-->
<!--</section>-->

<!-- Boxed -->
<div class="boxed">

    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    ?>
    <?php
    if (!isset($_SESSION))
    {
        session_start();
    }

    ?>


    <!-- Page title -->
<!--    <div class="page-title style2">-->
<!--        <div class="flat-maps">-->
<!--            <div class="maps2" style="width: 100%; height: 268px; "></div> -->
<!--        </div>-->
<!--        -->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-md-12">  -->
<!--                    <div class="wrap-pagetitle">-->
<!--                        <div class="flat-pagetitle">-->
<!--                            <div class="page-title-heading">-->
<!--                                <h1 class="title">Apricot West Britford Arch</h1>-->
<!--                            </div><!-- /.page-title-captions -->
<!--                            <div class="breadcrumbs style2">-->
<!--                                <ul>-->
<!--                                    <li><i class="ion-ios-location-outline"></i>The Bronx / New York City</li>-->
<!--                                </ul>                   -->
<!--                            </div><!-- /.breadcrumbs -->
<!--                        </div>            -->
<!--                    </div>-->
<!--                </div><!-- /.col-md-12 -->
<!--            </div><!-- /.row -->
<!--        </div><!-- /.container -->
<!--    </div>-->

        <!-- /.page-title -->



    <!-- Blog posts -->
    <section class="main-content page-listing">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="listing-wrap">
                        <div class="tf-gallery">
                            <div>
                                <ul class="nav nav-tabs text-center" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#gallery" role="tab" aria-controls="home" aria-selected="true">Gallery</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#video" role="tab" aria-controls="profile" aria-selected="false">Video</a>
                                    </li>
                                </ul>
                            </div>

                                <?php
if (isset($_GET['view']))
    {
        $view_id=$_GET['view'];
        $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_id='$view_id' ");
                                while ($result=mysqli_fetch_array($business_search_query))
                                {
                                    $bd_id=$result['bd_id'];
                                    $title=$result['bd_title'];
                                    $postcode=$result['bd_postcode_suburb_state'];
                                    $facebook=$result['bd_facebook'];
                                    $twitter=$result['bd_twitter'];
                                    $linkedin=$result['bd_linkedin'];
                                    $youtube=$result['bd_youtube'];
                                    $android_app=$result['bd_android_app'];
                                    $ios_app=$result['bd_ios_app'];
                                    $map=$result['bd_map'];
                                    $contact_address=$result['bd_contact_address'];
                                    $contact_telephone=$result['bd_contact_telephone'];
                                    $contact_mobile=$result['bd_contact_mobile'];
                                    $contact_email=$result['bd_contact_email'];
                                    $contact_website=$result['bd_contact_website'];
                                    $about_us=$result['bd_about_us'];
                                    $services=$result['bd_services'];
                                    $status=$result['bd_status'];
                                    ?>



                                        <!-- Start WOWSlider.com BODY section -->

                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="gallery" role="tabpanel" aria-labelledby="home-tab" style="margin-top: 20px;">
                                            <div id="wowslider-container1">

                                                <div class="ws_images">
                                    <?php
                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where  bdi_bd_id='$bd_id'");
                                    while ($image=mysqli_fetch_array($image_query)) {
                                        $image_name = $image['bdi_image'];
                                        ?>
                                                    <ul>
                                                        <li><img src="image/<?php echo $image_name; ?>" alt="national-parliament" title="<?php echo $title; ?>" id="wows1_0"/></li>
                                                    </ul>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                                <div class="ws_bullets">
                                                    <div>
                                    <?php
                                    //image for business
                                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                    where  bdi_bd_id='$bd_id'");
                                    while ($image=mysqli_fetch_array($image_query)) {
                                        echo $image_name = $image['bdi_image'];
                                        ?>
                                        <a href="#" title="<?php echo $title; ?>"><span><img src="image/<?php echo $image_name; ?>" alt="national-parliament"/>1</span></a>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="ws_script" style="position:absolute;left:-99%"><a href="#"></a></div>
                                                <div class="ws_shadow"></div>
                                            </div>
                                            <script type="text/javascript" src="engine1/wowslider.js"></script>
                                            <script type="text/javascript" src="engine1/script.js"></script>
                                            <!-- End WOWSlider.com BODY section -->
                                        </div>
                                        <!--Video Slider-->
                                        <div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="profile-tab">
                                                <?php
                                            //image for business
                                            $video_query=mysqli_query($connection,"SELECT * FROM business_directory_video
                                                                            where  bdv_bd_id='$bd_id'");
                                            while ($video=mysqli_fetch_array($video_query)) {
                                                $video_name = $video['bdv_video'];

                                                ?>

                                                <iframe style="margin-top: 15px;" width="750" height="480"
                                                        src="https://www.youtube.com/embed/<?php echo $video_name; ?>?rel=0" frameborder="0"
                                                        allow="autoplay; encrypted-media" allowfullscreen></iframe>

                                                <?php
                                            }
                                                ?>
                                        </div>
                                    </div>
                                        <?php

                                    }
    }

 ?>


                        </div><!-- /.tf-gallery -->
                        <div class="content-listing">
                            <div class="text">
                                <h3 class="title-listing"><?php echo $title?></h3>
                                <p><strong>About Us:</strong></p>
                                <p><?php echo $about_us;?></p>
                                <br>
                                <p><strong>Services:</strong></p>
                                <p><?php echo $services;?></p>
                            </div>
                        </div>
                    </div><!-- /.listing-wrap -->
                </div><!-- /.col-lg-9 -->
                <div class="col-lg-4">
                    <div class="sidebar">
                            <div class="widget widget-contact">
<!--                                <h5 class="widget-title">Map</h5>-->
                                <div style="width: 100%; margin-top: 42px;">
                                    <?php
                                        echo $map;
                                    ?>
                                </div>
                                <br/>
                            </div>
                        <div class="widget widget-contact">
                            <h5 class="widget-title">Contact Us</h5>
                            <ul>
                                <li class="adress"><?php echo $contact_address?></li>
                                <a href="tel:<?php echo $contact_telephone;?>"><li class="phone"><?php echo $contact_telephone?></li></a>
                                <a href="tel:<?php echo $contact_mobile;?>"><li class="mobile"><?php echo $contact_mobile?></li></a>
                                <a href="mailto:<?php echo $contact_email;?>"><li class="email"><?php echo $contact_email?></li></a>
                                <a href="<?php echo $contact_website;?>"><li class="web"><u><?php echo $contact_website?></u></li></a>

                            </ul>
                           <div class="social-links">
                                <a href="<?php echo $facebook?>"><i class="fa fa-facebook"></i></a>
                                <a href="<?php echo $twitter?>"><i class="fa fa-twitter"></i></a>
                                <a href="<?php echo $linkedin?>"><i class="fa fa-linkedin"></i></a>
                                <a href="<?php echo $youtube ?>"><i class="fa fa-youtube"></i></a>

                            </div>
                        </div>
                        <div class="widget widget-map" style="text-align: center;">
                            <?php
                            $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='single_page_ad' limit 12");
                            while ($result=mysqli_fetch_array($sql))
                            {
                                $bd_ad_id=$result['bd_ad_id'];
                                $bd_ad_title=$result['bd_ad_title'];
                                $bd_ad_image=$result['bd_ad_image'];
                                $bd_ad_url=$result['bd_ad_url'];
                                $bd_ad_category=$result['bd_ad_category'];
                                $bd_ad_date=$result['bd_ad_date'];
                                ?>
                                <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                                <?php
                            }
                            ?>
                        </div>
                        
                    </div><!-- /.sidebar -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>


<?php include "config/footer.php" ?>
<script type="text/javascript">
    $('.carousel').carousel()
    </script>

</body>
</html>
