<?php include "config/header.php" ?>

<body class="header_sticky">
<!-- Preloader -->
<!--    <section class="loading-overlay">-->
<!--        <div class="Loading-Page">-->
<!--            <h2 class="loader">Loading</h2>-->
<!--        </div>-->
<!--    </section> -->

<!-- Boxed -->
<div class="boxed">

    <?php

    include 'config/logged_in_user.php';
    include 'config/menu.php';
    if (!isset($_SESSION))
    {
        session_start();
    }

    ?>

    <?php

    if (isset($_SESSION['email'])) {
        $email = $_SESSION['email'];
        $sql = mysqli_query($connection, "SELECT * FROM user_details where email='$email'");
        while ($row = mysqli_fetch_array($sql)) {
            $user_id = $row['user_id'];
            $user_role = $row['user_role'];
            $given_name = $row['given_name'];
            $date_of_birth = $row['date_of_birth'];
        }

        ?>

        <!-- Page title -->
        <div class="page-title parallax parallax1">
            <div class="section-overlay">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title-heading">
                            <h1 class="title">Listing</h1>
                        </div><!-- /.page-title-captions -->
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="index.html">Home</a></li>
                                <li> -</li>
                                <li>Listing</li>
                            </ul>
                        </div><!-- /.breadcrumbs -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.page-title -->

              <!-- Blog posts -->
        <section class="main-content page-listing-grid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="flat-select clearfix">
                            <div class="float-left width50 clearfix">
                                <div class="one-three showing">
                                    <p><span>16</span> Found Listings</p>
                                </div>
                                <div class="one-three more-filter">
                                    <ul class="unstyled">
                                        <li class="current"><a href="#" class="title">More Fillter <i
                                                    class="fa fa-angle-right"></i></a>
                                            <ul class="unstyled">
                                                <li class="en">
                                                    <input type="checkbox" id="wifi" name="category">
                                                    <label for="wifi">Wifi</label>
                                                </li>
                                                <li class="en">
                                                    <input type="checkbox" id="smoking" name="category">
                                                    <label for="smoking">Smoking allowed</label>
                                                </li>
                                                <li class="en">
                                                    <input type="checkbox" id="onl" name="category">
                                                    <label for="onl">Online Reservation</label>
                                                </li>
                                                <li class="en">
                                                    <input type="checkbox" id="parking" name="category"
                                                           checked="checked">
                                                    <label for="parking">Parking street</label>
                                                </li>
                                                <li class="en">
                                                    <input type="checkbox" id="event" name="category">
                                                    <label for="event">Events</label>
                                                </li>
                                                <li class="en">
                                                    <input type="checkbox" id="in" name="category" checked="checked">
                                                    <label for="in">Elevator in building</label>
                                                </li>
                                                <li class="en">
                                                    <input type="checkbox" id="card" name="category">
                                                    <label for="card">Credit Card Payment</label>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="one-three sortby">
                                    <ul class="unstyled">
                                        <li class="current"><a href="#" class="title">Sort by: Random <i
                                                    class="fa fa-angle-right"></i></a>
                                            <ul class="unstyled">
                                                <li class="en"><a href="#" title=""><i class="fa fa-caret-right"></i>Open
                                                        Now</a></li>
                                                <li class="en"><a href="#" title=""><i class="fa fa-caret-right"></i>Most
                                                        reviewed</a></li>
                                                <li class="en"><a href="#" title=""><i class="fa fa-caret-right"></i>Top
                                                        rated</a></li>
                                                <li class="en"><a href="#" title=""><i class="fa fa-caret-right"></i>Random</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="float-right">
                                <div class="flat-sort">
                                    <a href="listing-list.php" class="course-list-view active"><i
                                            class="fa fa-list"></i></a>
                                    <a href="listing-grid.php" class="course-grid-view "><i class="fa fa-th"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="content-tab listing-user">
                            <div class="content-inner active listing-list">
                                <?php
                                if (isset($_POST['search'])) {
                                     $question = $_POST['question'];
                                     $address = $_POST['address'];

                                    $search_query = mysqli_query($connection, "SELECT * FROM business_directory_details WHERE 
                                                              bd_title LIKE '%$question%' 
                                                              and bd_postcode_suburb_state like '%$address%' 
                                                              and bd_user_id='$user_id'");
                                    while ($search_result = mysqli_fetch_array($search_query)) {
                                         $bd_id = $search_result['bd_id'];
                                         $title = $search_result['bd_title'];
                                         $contact_address = $search_result['bd_contact_address'];
                                         $status = $search_result['bd_status'];


                                        //image for business
                                        $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                          where 	bdi_user_id='$user_id' and bdi_bd_id='$bd_id'");
                                        while ($image = mysqli_fetch_array($image_query)) {
                                             $image_name = $image['bdi_image'];
                                            ?>

                                            <div class="flat-product clearfix">
                                                <div class="featured-product">
                                                    <img src="image/<?php echo "$image_name"; ?>" alt="image" style="width: 290px; height: 210px; border-radius: 4px;">
                                                </div>
                                                <div class="rate-product">
                                                    <div class="link-review clearfix">
                                                        <div class="button-product float-left">
                                                            <?php
                                                            if ($status=="Published")
                                                            { ?>
                                                                <button type="button" class="flat-button bg-green" onclick="location.href='#'"><?php echo $status;?></button>
                                                            <?php }
                                                            else{
                                                                ?>
                                                                <button type="button" class="flat-button" onclick="location.href='#'"><?php echo $status;?></button>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="info-product">
                                                        <h6 class="title"><a href="listing-single.php?view=<?php echo $bd_id?>"><?php echo $title?></a></h6>
                                                        <p><?php echo $contact_address;?></p>
                                                    </div>
                                                </div>
                                                <ul class="wrap-button float-right">
                                                    <li>
                                                        <a href="listing-single.php?view=<?php echo $bd_id?>"><button type="button" class="button" onclick="location.href='#'"><i class="ion-eye"></i><span>View</span></button></a>
                                                    </li>
                                                    <li>
                                                        <a href="edit_directory.php?edit=<?php echo $bd_id?>"><button type="button" class="button" onclick="location.href='#'"><i class="ion-edit"></i><span>Edit</span></button></a>
                                                    </li>
                                                    <li>
                                                        <a onclick='return ask_for_delete()' href="page-user.php?delete=<?php echo $bd_id ?>"><button type="button" class="button"><i class="ion-trash-a"></i><span>Delete</span></button></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?php
                                        }
                                    }
                                } ?>

                            </div>
                        </div>
                    </div><!-- /.col-lg-9 -->
                    <div class="col-lg-3">
                        <div class="sidebar">
                            <div class=" widget widget-form style2">
                                <h5 class="widget-title">
                                    Search Box
                                </h5>
                                <form novalidate="" class="filter-form clearfix" id="filter-form" method="post"
                                      action="listing_search.php">
                                    <p class="book-notes">
                                        <input type="text" placeholder="What are you looking for?" name="question"
                                               required="">
                                    </p>
                                    <p class="book-form-address icon">
                                        <input type="text" placeholder="Postcode/Suburb/State" name="address"
                                               required="">
                                    </p>
                                    <p class="form-submit text-center">
                                        <button class="flat-button" value="search" type="submit" name="search">Search <i
                                                class="ion-ios-search-strong"></i></button>
                                    </p>
                                </form>
                            </div>
                            <div class="widget widget-map" style="text-align: center;">
                                <?php
                                $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='bottom_right_ad' limit 18");
                                while ($result=mysqli_fetch_array($sql))
                                {
                                    $bd_ad_id=$result['bd_ad_id'];
                                    $bd_ad_title=$result['bd_ad_title'];
                                    $bd_ad_image=$result['bd_ad_image'];
                                    $bd_ad_url=$result['bd_ad_url'];
                                    $bd_ad_category=$result['bd_ad_category'];
                                    $bd_ad_date=$result['bd_ad_date'];
                                    ?>
                                    <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: 120px; width: 130px;  margin-top:5px; "></a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div><!-- /.sidebar -->
                    </div><!-- /.col-md-3 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section>

        <section class="flat-row v1 bg-theme">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="title-section">
                            <h1 class="title">Newsletter</h1>
                            <div class="sub-title">
                                Subscribe now and receive weekly newsletter with
                            </div>
                        </div>
                        <form id="subscribe-form" class="flat-mailchimp" method="post" action="#" data-mailchimp="true">
                            <div class="field clearfix" id="subscribe-content">
                                <p class="wrap-input-email">
                                    <input type="text" tabindex="2" id="subscribe-email" name="subscribe-email"
                                           placeholder="Your Email Here">
                                </p>
                                <p class="wrap-btn">
                                    <button type="button" id="subscribe-button" class=" subscribe-submit effect-button"
                                            title="Subscribe now">SUBSCRIBE
                                    </button>
                                </p>
                            </div>
                            <div id="subscribe-msg"></div>
                        </form>
                    </div>
                    <div class="col-lg-2">
                        <div class="flat-counter text-center">
                            <div class="content-counter">
                                <div class="icon-count">
                                    <i class="ion-waterdrop"></i>
                                </div>
                                <div class="name-count">Listing</div>
                                <div class="numb-count" data-to="1897" data-speed="2000" data-waypoint-active="yes">
                                    1897
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="flat-counter text-center">
                            <div class="content-counter">
                                <div class="icon-count">
                                    <i class="ion-pricetags"></i>
                                </div>
                                <div class="name-count">Categories</div>
                                <div class="numb-count" data-to="967" data-speed="2000" data-waypoint-active="yes">967
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="flat-counter text-center">
                            <div class="content-counter">
                                <div class="icon-count">
                                    <i class="ion-ios-people"></i>
                                </div>
                                <div class="name-count">Users</div>
                                <div class="numb-count" data-to="1101" data-speed="2000" data-waypoint-active="yes">
                                    1101
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
    }
    ?>


    <?php include "config/footer.php" ?>

    <?php

    if (!isset($_SESSION['email']))
    { echo "
    <script type='text/javascript'>
$(window).load(function()
{
    $('#popup_login').modal('show');
});
</script>"; }

    ?>


</body>
</html>