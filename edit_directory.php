<?php
/**
 * Created by PhpStorm.
 * User: i7
 * Date: 8/26/2018
 * Time: 10:34 AM
 */
include "config/header.php" ?>

<body class="header_sticky">
    <!-- Preloader -->
<!--    <section class="loading-overlay">-->
<!--        <div class="Loading-Page">-->
<!--            <h2 class="loader">Loading</h2>-->
<!--        </div>-->
<!--    </section> -->

    <!-- Boxed -->
    <div class="boxed">

<?php

include 'config/logged_in_user.php';
include 'config/menu.php';
?>
    <?php

    if (!isset($_SESSION))
    {
    session_start();
    }

?>

<?php

if (isset($_SESSION['email']))
    {
    $email=$_SESSION['email'];
    $sql=mysqli_query($connection,"SELECT * FROM user_details where email='$email'");
    while ($row=mysqli_fetch_array($sql))
    {
        $user_id=$row['user_id'];
        $user_role=$row['user_role'];
    }
 ?>

    <section class="flat-row page-addlisting">
        <div class="container">
            <div class="add-filter">
                <div class="row">
                    <div class="col-md-12 wrap-accadion">
                        <?php
                        if (isset($_GET['edit']))
                        {
                            $bd_id=$_GET['edit'];
                            $query=mysqli_query($connection,"SELECT * FROM business_directory_details where bd_id='$bd_id'");
                            $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image where bdi_bd_id='$bd_id'");
                            $video_query=mysqli_query($connection,"SELECT * FROM business_directory_video where bdv_bd_id='$bd_id'");

                            while ($result=mysqli_fetch_array($query))
                            {
                                $title=$result['bd_title'];
                                $postcode=$result['bd_postcode_suburb_state'];
                                $facebook=$result['bd_facebook'];
                                $twitter=$result['bd_twitter'];
                                $linkedin=$result['bd_linkedin'];
                                $android_app=$result['bd_android_app'];
                                $ios_app=$result['bd_ios_app'];
                                $map=$result['bd_map'];
                                $youtube=$result['bd_youtube'];
                                $contact_address=$result['bd_contact_address'];
                                $contact_telephone=$result['bd_contact_telephone'];
                                $contact_mobile=$result['bd_contact_mobile'];
                                $contact_email=$result['bd_contact_email'];
                                $contact_website=$result['bd_contact_website'];
                                $about_us=$result['bd_about_us'];
                                $services=$result['bd_services'];
                                $status=$result['bd_status'];
                                $category=$result['bd_category'];
                            }
                            while ($image=mysqli_fetch_array($image_query))
                            {
                                $image_name=$image['bdi_image'];
                            }
                            while ($video_result=mysqli_fetch_array($video_query))
                            {

                                $video_name=$video_result['bdv_video'];
                            }
                        }
                        ?>

                        <form method="post" action="edit_directory.php?edit=<?php echo $bd_id ?>" class="filter-form form-addlist" enctype="multipart/form-data">
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Business Title</label>
                                    <input type="text" class="form-control" name="title" id="title" value="<?php echo $title?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Postcode,Suburb,State</label>
                                    <input type="text" name="postcode" id="title" value="<?php echo $postcode?>">
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Image</label>
                                    <input type="file" name="image" ">
                                    <img src="image/<?php echo "$image_name"; ?>" alt="" width="200">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Video</label>
                                    <input type="text" name="video" id="title" value="<?php echo $video_name?>">
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Facebook</label>
                                    <input type="text" name="facebook" id="title" value="<?php echo $facebook?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Twitter</label>
                                    <input type="text" name="twitter" id="title" value="<?php echo $twitter ?>">
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Linkedin</label>
                                    <input type="text" name="linkedin" id="title" value="<?php echo $linkedin ?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Youtube</label>
                                    <input type="text" name="youtube" id="title" value="<?php echo $youtube?>">
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Android App</label>
                                    <input type="text" name="android_app" id="title" value="<?php echo $android_app?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">IOS App</label>
                                    <input type="text" name="ios_app" id="title" value="<?php echo $ios_app ?>">
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Map</label>
                                    <input type="text" name="map" id="title" value="<?php echo $map;?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Address</label>
                                    <input type="text" name="address" id="title" value="<?php echo $contact_address ?>">
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Telephone</label>
                                    <input type="text" name="telephone" id="title" value="<?php echo $contact_telephone ?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Mobile</label>
                                    <input type="text" name="mobile" id="title" value="<?php echo $contact_mobile ?>">
                                </div>
                            </div>
                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Email</label>
                                    <input type="text" name="email" id="title" value="<?php echo $contact_email ?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="nhan">Website</label>
                                    <input type="text" name="website" id="title" value="<?php echo $contact_website ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="nhan">About Us</label>
                                <textarea cols="4" rows="4" name="about_us"><?php echo $about_us?> </textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="nhan">Services</label>
                                <textarea cols="4" rows="4" name="services"><?php echo $services?></textarea>
                            </div>

                            <div class="col-md-12 row">
                                <div class="form-group col-md-6">
                                    <label class="nhan">Status</label>
                                    <select name="status">
                                        <option value="<?php echo $status ?>"><?php echo $status ?></option>
                                        <option value="Published">Publish</option>
                                        <option value="Draft">Draft</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="nhan">Category</label>
                                    <select name="category" >
                                        <option value="<?php echo $category?>"><?php $q=mysqli_query($connection,"SELECT * FROM business_directory_category where bdc_id='$category'");
                                            $r=mysqli_fetch_array($q);
                                            echo $category_title=$r['bdc_title'];
                                            ?></option>
                                        <?php
                                        $q=mysqli_query($connection,"SELECT * FROM business_directory_category");
                                        while($res=mysqli_fetch_array($q)) {
                                            $bdc_id = $res['bdc_id'];
                                            $bdc_title = $res['bdc_title'];
                                            ?>

                                            <option value="<?php echo $bdc_id ?>"><?php echo $bdc_title;?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="button-addlisting">
                                <button type="submit" name="submit" class="flat-button"">Submit</button>
                            </div>
                        </form>

                        <!--From action-->
                        <?php
                        if (isset($_POST['submit']))
                        {
                            $image=$_FILES['image']['name'];
                            $image_tmp=$_FILES['image']['tmp_name'];
                            $video=$_POST['video'];
                            $title=$_POST['title'];
                            $postcode=$_POST['postcode'];
                            $facebook=$_POST['facebook'];
                            $twitter=$_POST['twitter'];
                            $linkedin=$_POST['linkedin'];
                            $youtube=$_POST['youtube'];
                            $android_app=$_POST['android_app'];
                            $ios_app=$_POST['ios_app'];
                            $map=$_POST['map'];
                            $address=$_POST['address'];
                            $telephone=$_POST['telephone'];
                            $mobile=$_POST['mobile'];
                            $email=$_POST['email'];
                            $website=$_POST['website'];
                            $about_us=$_POST['about_us'];
                            $services=$_POST['services'];
                            $d_status=$_POST['status'];
                            $categoryf=$_POST['category'];

                            $insert_query="UPDATE business_directory_details SET 
                                          bd_title='$title', bd_postcode_suburb_state='$postcode',
                                          bd_facebook='$facebook', bd_twitter='$twitter',
                                          bd_linkedin='$linkedin', bd_youtube='$youtube',
                                          bd_android_app='$android_app',  bd_ios_app='$ios_app',
                                          bd_map='$map',bd_contact_address='$address',
                                          bd_contact_telephone='$telephone',bd_contact_mobile='$mobile',
                                          bd_contact_email='$email',bd_contact_website='$website',
                                          bd_about_us='$about_us',bd_services='$services',bd_status='$d_status', bd_category='$categoryf' where bd_id='$bd_id' ";
                            if (mysqli_query($connection,$insert_query))
                            {
                                        move_uploaded_file($image_tmp,"image/$image");
                                        if (empty($image))
                                        {
                                            $image=$image_name;
                                        }
                                        $image_insert_query="UPDATE business_directory_image SET 
                                                             bdi_image='$image' where bdi_bd_id='$bd_id'";
                                        if (mysqli_query($connection,$image_insert_query))
                                        {
                                            $video_insert_query="UPDATE business_directory_video SET 
                                                             bdv_video='$video' where bdv_bd_id='$bd_id'";
                                            if (mysqli_query($connection,$video_insert_query))
                                            {
                                                echo "Success";
                                            }
                                        }
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div class="button-addlisting">
                <a href="page-user.php" class="flat-button"">View All</a>
            </div>

        </div>

    </section>


<?php } ?>



<?php include "config/footer.php" ?>


</body>
</html>