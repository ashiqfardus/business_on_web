<?php include "config/header.php" ?>

<body class="header_sticky">   

    <!-- Boxed -->
    <div class="boxed">

        <?php

        include 'config/logged_in_user.php';
        include 'config/menu.php';
        if (!isset($_SESSION))
        {
            session_start();
        }
        if (isset($_SESSION['email']))
        {
            $email=$_SESSION['email'];
            $sql=mysqli_query($connection,"SELECT * FROM user_details where email='$email'");
            while ($row=mysqli_fetch_array($sql))
            {
                $user_id=$row['user_id'];
                $user_role=$row['user_role'];
                $given_name=$row['given_name'];
                $date_of_birth=$row['date_of_birth'];
            }
            if (isset($_GET['lid']))
            {
                $cat_id=$_GET['lid'];

                ?>
                <section class="main-content page-listing-grid">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class=" clearfix">
                                    <div class="float-left clearfix">

                                        <form novalidate="" class="filter-form clearfix form-inline" id="filter-form" method="post" action="index.php">
                                            <p class="book-notes">
                                                <input type="text" placeholder="What are you looking for?" name="question" required="" style="height: 34px;">
                                            </p>
                                            <p class="book-notes" style="    margin-left: 25px; margin-right: 25px; margin-bottom: 14px;">
                                                IN
                                            </p>

                                            <p class="book-form-address icon">
                                                <input type="text" placeholder="Postcode/Suburb/State" name="address" required="" style="height: 34px;">
                                            </p>

                                            <button style="height: 34px; margin-left: 50px; margin-bottom: 19px; padding: 10px; width: 145px;" class="" value="search" type="submit" name="search">Search <i class="ion-ios-search-strong"></i></button>

                                        </form>
                                    </div>


                                    <div class="float-right">
                                        <div class="flat-sort">
                                            <a href="listing-list.php?lid=<?php echo $cat_id;?>" class="course-list-view active"><i class="fa fa-list"></i></a>
                                            <a href="listing_grid_directory.php?id=<?php echo $cat_id?>" class="course-grid-view "><i class="fa fa-th"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>A</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'A%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>B</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'B%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>C</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'C%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>D</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'D%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>E</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'E%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>F</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'F%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>G</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'G%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>H</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'H%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>I</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'I%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>J</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'J%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>K</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'K%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>L</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'L%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>M</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'M%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>N</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'N%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>O</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'O%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>P</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'P%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>Q</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'Q%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>R</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'R%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>S</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'S%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>T</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'T%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>U</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'U%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>V</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'V%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>W</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'W%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>X</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'X%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>Y</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'Y%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <div class="col-md-4">
                                                <h6 class="text-center"><b>Z</b></h6>
                                            </div>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_user_id='$user_id' AND bd_title LIKE 'Z%' AND bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_user_id='$user_id' and bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <div class="col-md-4">
                                                        <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>
                                                    </div>

                                                <?php } }?>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- /.col-lg-9 -->
                            <div class="col-lg-4">
                                <div class="sidebar">

                                    <div class="widget widget-map" style="text-align: center;">
                                        <?php
                                        $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='all_directory_ad' limit 18");
                                        while ($result=mysqli_fetch_array($sql))
                                        {
                                            $bd_ad_id=$result['bd_ad_id'];
                                            $bd_ad_title=$result['bd_ad_title'];
                                            $bd_ad_image=$result['bd_ad_image'];
                                            $bd_ad_url=$result['bd_ad_url'];
                                            $bd_ad_category=$result['bd_ad_category'];
                                            $bd_ad_date=$result['bd_ad_date'];
                                            ?>
                                            <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div><!-- /.sidebar -->
                            </div><!-- /.col-md-3 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>

                <?php
            }

            else
            {
                ?>

                <section class="main-content page-listing-grid">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class=" clearfix">
                                    <div class="float-left clearfix">

                                        <form novalidate="" class="filter-form clearfix form-inline" id="filter-form" method="post" action="index.php">
                                            <p class="book-notes">
                                                <input type="text" placeholder="What are you looking for?" name="question" required="" style="height: 34px;">
                                            </p>
                                            <p class="book-notes" style="    margin-left: 25px; margin-right: 25px; margin-bottom: 14px;">
                                                IN
                                            </p>

                                            <p class="book-form-address icon">
                                                <input type="text" placeholder="Postcode/Suburb/State" name="address" required="" style="height: 34px;">
                                            </p>

                                            <button style="height: 34px; margin-left: 50px; margin-bottom: 19px; padding: 10px; width: 145px;" class="" value="search" type="submit" name="search">Search <i class="ion-ios-search-strong"></i></button>

                                        </form>
                                    </div>


                                    <div class="float-right">
                                        <div class="flat-sort">
                                            <a href="listing-list.php" class="course-list-view active"><i class="fa fa-list"></i></a>
                                            <a href="listing-grid.php" class="course-grid-view "><i class="fa fa-th"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>A</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'A%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                    $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>B</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'B%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>C</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'C%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>D</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'D%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>E</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'E%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>F</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'F%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>G</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'G%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>H</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'H%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>I</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'I%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>J</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'J%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>K</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'K%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>L</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'L%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>M</b></h6>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'M%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>N</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'N%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>O</b></h6>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'O%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>P</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'P%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>Q</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'Q%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>R</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'R%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>S</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'S%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>T</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'T%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>

                                                        <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>U</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'U%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>V</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'V%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>W</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'W%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>X</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'X%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>

                                                        <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>Y</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'Y%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                                <h6 class="text-center"><b>Z</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'Z%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bdc_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];

                                                $image_name=$result['bdc_image'];

                                                    ?>

                                                        <a class="text-center" href="listing_list_directory.php?view=<?php echo $bdc_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php }?>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- /.col-lg-9 -->
                            <div class="col-lg-4">
                                <div class="sidebar">

                                    <div class="widget widget-map" style="text-align: center;">
                                        <?php
                                        $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='all_directory_ad' limit 18");
                                        while ($result=mysqli_fetch_array($sql))
                                        {
                                            $bd_ad_id=$result['bd_ad_id'];
                                            $bd_ad_title=$result['bd_ad_title'];
                                            $bd_ad_image=$result['bd_ad_image'];
                                            $bd_ad_url=$result['bd_ad_url'];
                                            $bd_ad_category=$result['bd_ad_category'];
                                            $bd_ad_date=$result['bd_ad_date'];
                                            ?>
                                            <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div><!-- /.sidebar -->
                            </div><!-- /.col-md-3 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>


                <?php
            }

        }
        elseif (!isset($_SESSION['email']))
        {
            if (isset($_GET['id']))
            {
                $cat_id=$_GET['id'];
                ?>
                <section class="main-content page-listing-grid">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class=" clearfix">

                                    <div class="float-left clearfix">

                                        <form novalidate="" class="filter-form clearfix form-inline" id="filter-form" method="post" action="index.php">
                                            <p class="book-notes">
                                                <input type="text" placeholder="What are you looking for?" name="question" required="" style="height: 34px;">
                                            </p>
                                            <p class="book-notes" style="    margin-left: 25px; margin-right: 25px; margin-bottom: 14px;">
                                                IN
                                            </p>

                                            <p class="book-form-address icon">
                                                <input type="text" placeholder="Postcode/Suburb/State" name="address" required="" style="height: 34px;">
                                            </p>

                                            <button style="height: 34px; margin-left: 50px; margin-bottom: 19px; padding: 10px; width: 145px;" class="" value="search" type="submit" name="search">Search <i class="ion-ios-search-strong"></i></button>

                                        </form>
                                    </div>

                                    <div class="float-right">
                                        <div class="flat-sort">
                                            <a href="listing-list.php?lid=<?php echo $cat_id?>" class="course-list-view active"><i class="fa fa-list"></i></a>
                                            <a href="listing-grid.php?lid=<?php echo $cat_id?>" class="course-grid-view "><i class="fa fa-th"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>A</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'A%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>B</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'B%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>C</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'C%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>D</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'D%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>E</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'E%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>F</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'F%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>G</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'G%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>H</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'H%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>I</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'I%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>J</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'J%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>K</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'K%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>L</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'L%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <h6 class="text-center"><b>M</b></h6>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'M%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>N</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'N%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <h6 class="text-center"><b>O</b></h6>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'O%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>P</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'P%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where  bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>Q</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where  bd_title LIKE 'Q%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>R</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where  bd_title LIKE 'R%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <h6 class="text-center"><b>S</b></h6>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where  bd_title LIKE 'S%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where  bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>T</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'T%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>U</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'U%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>V</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'V%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>W</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'W%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>X</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'X%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>


                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>Y</b></h6>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where  bd_title LIKE 'Y%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where  bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>
                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php } }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>Z</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                                        where bd_title LIKE 'Z%' and bd_category='$cat_id'
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bd_id'];
                                                $title=$result['bd_title'];
                                                $postcode=$result['bd_postcode_suburb_state'];
                                                $facebook=$result['bd_facebook'];
                                                $twitter=$result['bd_twitter'];
                                                $linkedin=$result['bd_linkedin'];
                                                $android_app=$result['bd_android_app'];
                                                $ios_app=$result['bd_ios_app'];
                                                $map=$result['bd_map'];
                                                $contact_address=$result['bd_contact_address'];
                                                $contact_telephone=$result['bd_contact_telephone'];
                                                $contact_mobile=$result['bd_contact_mobile'];
                                                $contact_email=$result['bd_contact_email'];
                                                $contact_website=$result['bd_contact_website'];
                                                $about_us=$result['bd_about_us'];
                                                $services=$result['bd_services'];
                                                $status=$result['bd_status'];


                                                //image for business
                                                $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image 
                                                                          where bdi_bd_id='$bd_id' LIMIT 1");
                                                while ($image=mysqli_fetch_array($image_query))
                                                {
                                                    $image_name=$image['bdi_image'];

                                                    ?>

                                                    <a class="text-center" href="listing-single.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php } }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="sidebar">

                                    <div class="widget widget-map" style="text-align: center;">
                                        <?php
                                        $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='all_directory_ad' limit 18");
                                        while ($result=mysqli_fetch_array($sql))
                                        {
                                            $bd_ad_id=$result['bd_ad_id'];
                                            $bd_ad_title=$result['bd_ad_title'];
                                            $bd_ad_image=$result['bd_ad_image'];
                                            $bd_ad_url=$result['bd_ad_url'];
                                            $bd_ad_category=$result['bd_ad_category'];
                                            $bd_ad_date=$result['bd_ad_date'];
                                            ?>
                                            <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div><!-- /.sidebar -->
                            </div>
                        </div>
                    </div>
                </section>
                <?php
            }

            else
            {

                ?>


                <section class="main-content page-listing-grid">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class=" clearfix">

                                    <div class="float-left clearfix">

                                        <form novalidate="" class="filter-form clearfix form-inline" id="filter-form" method="post" action="index.php">
                                            <p class="book-notes">
                                                <input type="text" placeholder="What are you looking for?" name="question" required="" style="height: 34px;">
                                            </p>
                                            <p class="book-notes" style="    margin-left: 25px; margin-right: 25px; margin-bottom: 14px;">
                                                IN
                                            </p>

                                            <p class="book-form-address icon">
                                                <input type="text" placeholder="Postcode/Suburb/State" name="address" required="" style="height: 34px;">
                                            </p>

                                            <button style="height: 34px; margin-left: 50px; margin-bottom: 19px; padding: 10px; width: 145px;" class="" value="search" type="submit" name="search">Search <i class="ion-ios-search-strong"></i></button>

                                        </form>
                                    </div>

                                    <div class="float-right">
                                        <div class="flat-sort">
                                            <a href="listing-list.php" class="course-list-view active"><i class="fa fa-list"></i></a>
                                            <a href="listing-grid.php" class="course-grid-view "><i class="fa fa-th"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>A</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'A%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>B</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'B%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>C</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'C%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php } ?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>D</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'D%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>E</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'E%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>F</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'F%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>G</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'G%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>H</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'H%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>I</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'I%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>J</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'J%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>K</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'K%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>L</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'L%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <h6 class="text-center"><b>M</b></h6>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'M%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>N</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'N%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <h6 class="text-center"><b>O</b></h6>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'O%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>P</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'P%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>Q</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'Q%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>R</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'R%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">
                                            <h6 class="text-center"><b>S</b></h6>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'S%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>T</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'T%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>U</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'U%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>V</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'V%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>W</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'W%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>X</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'X%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>Y</b></h6>

                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'Y%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                            <?php }?>
                                        </div>
                                        <div class="content-inner active listing-list">

                                            <h6 class="text-center"><b>Z</b></h6>


                                            <?php

                                            $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_category
                                                                                        where bdc_title LIKE 'Z%' 
                                                                                       ");
                                            while ($result=mysqli_fetch_array($business_search_query))
                                            {
                                                $bd_id=$result['bdc_id'];
                                                $title=$result['bdc_title'];
                                                ?>
                                                    <a class="text-center" href="listing_list_directory.php?view=<?php echo $bd_id?>"><p class="text-center"><?php echo $title;?></p></a>

                                                <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="sidebar">

                                    <div class="widget widget-map" style="text-align: center;">
                                        <?php
                                        $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='all_directory_ad' limit 18");
                                        while ($result=mysqli_fetch_array($sql))
                                        {
                                            $bd_ad_id=$result['bd_ad_id'];
                                            $bd_ad_title=$result['bd_ad_title'];
                                            $bd_ad_image=$result['bd_ad_image'];
                                            $bd_ad_url=$result['bd_ad_url'];
                                            $bd_ad_category=$result['bd_ad_category'];
                                            $bd_ad_date=$result['bd_ad_date'];
                                            ?>
                                            <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div><!-- /.sidebar -->
                            </div>
                        </div>
                    </div>
                </section>

               <?php
            }
        }
        ?>



<?php include "config/footer.php" ?>




</body>
</html>