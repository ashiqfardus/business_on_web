<?php
/**
 * Created by PhpStorm.
 * User: i7
 * Date: 8/16/2018
 * Time: 3:11 PM
 */
if (!isset($_SESSION))
{
    session_start();
}

if (isset($_SESSION['email']))
{
    $email=$_SESSION['email'];
    $sql=mysqli_query($connection,"SELECT * FROM user_details where email='$email'");
        while ($row=mysqli_fetch_array($sql))
        {
            $user_id=$row['user_id'];
            $user_role=$row['user_role'];
            $given_name=$row['given_name'];
            $date_of_birth=$row['date_of_birth'];
        }

?>


    <!-- Header -->            
    <header id="header" class="header clearfix">
        <div class="container">
            <div class="row">                 
                <div class="col-lg-4">
                    <div id="logo" class="float-left" style="margin: 5px 0 0 0; " >
                        <a href="index.php" rel="home">
                            <img src="images/logo.png" alt="image" class="directory_logo" style="max-width: 80%; !important;">
                        </a>
                    </div><!-- /.logo -->
                    <div class="btn-menu">
                        <span></span>
                    </div><!-- //mobile menu button -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-8">
                    <div class="nav-wrap">                            
                        <nav id="mainnav" class="mainnav float-left">
                            <ul class="menu"> 
                                <li class="home">
                                    <a href="index.php">Home</a>
                                </li>
                                <li><a href="about_us.php">About Us</a></li>
                                <li><a href="services.php">Services</a></li>
                                <li><a href="contact.php">Contact</a></li>

                                <li><a href="#">Category</a>
                                    <ul class="submenu"> 
                                        <li><a href="listing-grid.php">My Category</a>
                                        </li>
                                        <li><a href="all_directory.php">All Category</a>
                                        </li>
                                    </ul>
                                </li>





<!--                                                     -->
                            </ul><!-- /.menu -->
                        </nav><!-- /.mainnav -->  

                         <nav id="mainnav" class="mainnav float-right">
                            <ul class="menu"> 

                                <li><a style="border-radius: 50px;padding: 22px 15px;border: 2px solid #e8280b;" href="#">Profile</a>
                                    <ul class="submenu"> 
                                        
                                        <li style="color: #fff; background-color: #8a171a; padding: .3em 1.7em; line-height: 1.7em;">
                                        	<p style="font-size: 1em; text-transform: uppercase; font-weight: 700;"><?php echo $given_name; ?></p>
                                        	<p style="border-left: 1px solid #ffffff; padding-left: 0.3em; font-size: .8em;"> <?php echo $email; ?> </p>
                                        </li>
                                        
                                        <li><a href="page-addlisting.php">Add Directory</a>
                                        </li>
                                        <li><a href="page-user.php">DashBoard</a>
                                        </li>
                                        <li><a href="config/logout.php">Logout</a>
                                        </li>
                                        
                                        
                                    </ul><!-- /.submenu -->
                                </li>                        
                            </ul><!-- /.menu -->
                        </nav><!-- /.mainnav -->  

                    </div><!-- /.nav-wrap -->
                </div><!-- /.col-lg-8 -->                                    
            </div><!-- /.row -->
        </div>
    </header><!-- /.header -->
<?php

    }
?>

