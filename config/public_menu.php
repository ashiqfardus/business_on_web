<!-- Header -->
    <header id="header" class="header clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div id="logo" class="logo float-left">
                        <a href="../index.php" rel="home">
                            <img src="images/logo.png" alt="image">
                        </a>
                    </div><!-- /.logo -->
                    <div class="btn-menu">
                        <span></span>
                    </div><!-- //mobile menu button -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-8">
                    <div class="nav-wrap">
                        <nav id="mainnav" class="mainnav float-left">
                            <ul class="menu">
                                <li class="home">
                                    <a href="index.php">Home</a>
                                </li>
                                <li><a href="listing-grid.html">Listing</a>
                                    <ul class="submenu">
                                        <li><a href="listing-grid.php">Listing Grid</a>
                                        </li>
                                        <li><a href="listing-list.php">Listing List</a>
                                        </li>
                                        <li><a href="listing-full.php">Listing Full Width</a>
                                        </li>
                                        <li><a href="listing-map1.php">Listing Map V1</a>
                                        </li>
                                        <li><a href="listing-map2.php">Listing Map V2</a>
                                        </li>
                                        <li><a href="listing-single.php">Listing Single</a>
                                        </li>
                                    </ul><!-- /.submenu -->
                                </li>
                                <li><a href="blog.php">Blog</a>
                                    <ul class="submenu">
                                        <li><a href="blog.php">Blog</a>
                                        </li>
                                        <li><a href="blog-single.php">Blog Single</a>
                                        </li>
                                    </ul><!-- /.submenu -->
                                </li>
                                <li><a href="page-about.php">Page</a>
                                    <ul class="submenu">
                                        <li><a href="page-about.php">About Us</a>
                                        </li>
                                        <li><a href="page-services.php">Services</a>
                                        </li>
                                        <li><a href="page-user.php">User</a>
                                        </li>
                                        <li><a href="page-profile.php">User profile</a>
                                        </li>
                                        <li><a href="page-addlisting.php">Add Listing</a>
                                        </li>
                                        <li><a href="page-pricing.php">pricing</a>
                                        </li>
                                        <li><a href="page-contact.php">Contact Us</a>
                                        </li>
                                    </ul><!-- /.submenu -->
                                </li>
                                <li>
                                    <a id="myButton" data-toggle="modal" href="#popup_login"><i class="fa fa-user"></i> Sign in</a>
                                </li>
                                <li>
                                    <a data-toggle="modal" href="#popup_register"><i class="fa fa-user-plus"></i> Register</a>
                                </li>
                            </ul><!-- /.menu -->
                        </nav><!-- /.mainnav -->

                        <div class="button-addlist float-right">
                            <button type="button" class="flat-button" onclick="location.href='page-addlisting.php'">Add Listing</button>
                        </div>
                    </div><!-- /.nav-wrap -->
                </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
        </div>
    </header><!-- /.header -->
