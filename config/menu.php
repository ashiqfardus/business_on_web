<?php
/**
 * Created by PhpStorm.
 * User: i7
 * Date: 8/16/2018
 * Time: 3:12 PM
 */
if (!isset($_SESSION))
{
    session_start();
}

if (!isset($_SESSION['email']))
{
    ?>


    <!-- Header -->
    <header id="header" class="header clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div id="logo" class="float-left" style="margin: 17px 10px 0px -30px; ">
                        <a href="index.php" rel="home">
                            <img src="images/logo.png" alt="image" style="max-width: 135%; !important" >
                        </a>
                    </div><!-- /.logo -->
                    <div class="btn-menu">
                        <span></span>
                    </div><!-- //mobile menu button -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-10">
                    <div class="nav-wrap">
                        <nav id="mainnav" class="mainnav float-left">
                            <ul class="menu">
                                <li class="home">
                                    <a href="index.php">Home</a>
                                </li>
                                <li><a href="about_us.php">About Us</a></li>
                                <li><a href="services.php">Services</a></li>
                                <li><a href="contact.php">Contact</a></li>
                                <li><a href="listing-grid.php">Category</a>
                                    <ul class="submenu">
                                        <li><a href="listing-grid.php">All Category</a>
                                        </li>
                                    </ul><!-- /.submenu -->
                                </li>
                                
                                <li>
                                    <a id="myButton" data-toggle="modal" data-target="#popup_login"><i class="fa fa-user"></i> Sign in</a>
                                </li>
                                <li>
                                    <a data-toggle="modal" data-target="#popup_register"><i class="fa fa-user-plus"></i> Register</a>
                                </li>
                            </ul><!-- /.menu -->
                        </nav><!-- /.mainnav -->

                        <div class="button-addlist float-right">
                            <button type="button" class="flat-button" onclick="location.href='page-addlisting.php'">Add Directory</button>
                        </div>
                    </div><!-- /.nav-wrap -->
                </div><!-- /.col-lg-8 -->
            </div><!-- /.row -->
        </div>
    </header><!-- /.header -->

    <?php

}
?>