
<div class="row">

    <div class="col-md-4">

        <div class="flat-formsearch" style="position: relative; top: 0 !important;">
            <div class=" widget widget-form style2" style="text-align: center; margin-top: 30px;">

                <?php
                $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='top_left_ad' limit 4");
                while ($result=mysqli_fetch_array($sql))
                {
                    $bd_ad_id=$result['bd_ad_id'];
                    $bd_ad_title=$result['bd_ad_title'];
                    $bd_ad_image=$result['bd_ad_image'];
                    $bd_ad_url=$result['bd_ad_url'];
                    $bd_ad_category=$result['bd_ad_category'];
                    $bd_ad_date=$result['bd_ad_date'];
                    ?>
                    <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                    <?php
                }
                ?>

            </div>
        </div>
    </div>
    <div class="col-md-4" style="margin-left: -48px; margin-right: 38px; margin-top: 35px;">
        <div id="rev_slider_1078_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic4export" data-source="gallery" style=" margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
            <!-- START REVOLUTION SLIDER 5.3.0.2 auto mode -->
            <div id="rev_slider_1078_1" class="rev_slider fullwidthabanner" style="width: 120%;" data-version="5.3.0.2">
                <div class="slotholder"></div>
                <ul><!-- SLIDE  -->

                    <!-- SLIDE 2 -->
                    <li data-index="rs-3051" data-transition="slideremovedown" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"    data-rotate="0"  data-saveperformance="off"  data-title="Ken Burns" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                        <!-- MAIN IMAGE -->

                        <img src="images/slides/bg.jpg"  alt=""  data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 12 -->
                        <div class="tp-caption title-slide"
                             id="slide-3051-layer-1"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['-128','-128','-128','-50']"
                             data-fontsize="['65','65','45','30']"
                             data-lineheight="['65','65','45','35']"
                             data-fontweight="['600','600','600','600']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"

                             data-type="text"
                             data-responsive_offset="on"

                             data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                             data-textAlign="['center','center','center','center']"
                             data-paddingtop="[10,10,10,10]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingbottom="[0,0,0,0"
                             data-paddingleft="[0,0,0,0]"

                             style="z-index: 16; white-space: nowrap;">Find Anything You Want
                        </div>

                        <!-- LAYER NR. 13 -->
                        <div class="tp-caption sub-title"
                             id="slide-3051-layer-4"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['-32','-32','-32','20']"
                             data-fontsize="['20',20','20','14']"
                             data-lineheight="['35','35','35','20']"
                             data-fontweight="['300','300','300','300']"
                             data-width="['1200','660','660','300']"
                             data-height="none"
                             data-whitespace="nowrap"

                             data-type="text"
                             data-responsive_offset="on"

                             data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                             data-textAlign="['center','center','center','center']"
                             data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"

                             style="z-index: 17; white-space: nowrap;">Find awesome places, bars, restaurants <span>just one click </span> Our website will provide you with reliable <br> addresses best price for the nearest address
                        </div>
                    </li>
                    <li data-index="rs-3051" data-transition="slideremovedown" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"    data-rotate="0"  data-saveperformance="off"  data-title="Ken Burns" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                        <!-- MAIN IMAGE -->

                        <img src="images/slides/2.jpg"  alt=""  data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 12 -->
                        <div class="tp-caption title-slide"
                             id="slide-3051-layer-1"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['-128','-128','-128','-50']"
                             data-fontsize="['65','65','45','30']"
                             data-lineheight="['65','65','45','35']"
                             data-fontweight="['600','600','600','600']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"

                             data-type="text"
                             data-responsive_offset="on"

                             data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                             data-textAlign="['center','center','center','center']"
                             data-paddingtop="[10,10,10,10]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingbottom="[0,0,0,0"
                             data-paddingleft="[0,0,0,0]"

                             style="z-index: 16; white-space: nowrap;">Find Anything You Want
                        </div>

                        <!-- LAYER NR. 13 -->
                        <div class="tp-caption sub-title"
                             id="slide-3051-layer-4"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['-32','-32','-32','20']"
                             data-fontsize="['20',20','20','14']"
                             data-lineheight="['35','35','35','20']"
                             data-fontweight="['300','300','300','300']"
                             data-width="['1200','660','660','300']"
                             data-height="none"
                             data-whitespace="nowrap"

                             data-type="text"
                             data-responsive_offset="on"

                             data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                             data-textAlign="['center','center','center','center']"
                             data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"

                             style="z-index: 17; white-space: nowrap;">Find awesome places, bars, restaurants <span>just one click </span> Our website will provide you with reliable <br> addresses best price for the nearest address
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
    <div class="col-md-4">

        <div class="flat-formsearch" style="position: relative; top: 0 !important;">
            <div class=" widget widget-form style2" style="text-align: center; margin-top: 30px;">

                <?php
                $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='top_left_ad'  limit 4");
                while ($result=mysqli_fetch_array($sql))
                {
                    $bd_ad_id=$result['bd_ad_id'];
                    $bd_ad_title=$result['bd_ad_title'];
                    $bd_ad_image=$result['bd_ad_image'];
                    $bd_ad_url=$result['bd_ad_url'];
                    $bd_ad_category=$result['bd_ad_category'];
                    $bd_ad_date=$result['bd_ad_date'];
                    ?>
                    <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                    <?php
                }
                ?>

            </div>
        </div>
    </div>
</div>

