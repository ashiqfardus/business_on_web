<?php
/**
 * Created by PhpStorm.
 * User: i7
 * Date: 8/16/2018
 * Time: 2:41 PM
 */


function user_registration()
{
    global $connection;

    $result=array(
        'result'=>1,
        'message'=>'undefined error'
    );

    if (isset($_POST['register']))
    {
        $last_name=$_POST['last_name'];
        $given_name=$_POST['given_name'];
        $date_of_birth=$_POST['date_of_birth'];
        $email=$_POST['email'];
        $user_role=$_POST['user_role'];
        $password=$_POST['password'];
        $password2=$_POST['password2'];

        //Email check if exists or not
        $emailcheck=mysqli_query($connection,"Select email from user_details WHERE email='$email'");
        $echeck=mysqli_num_rows($emailcheck);
        if ($echeck!=0)
        {
            $result['message']='Email already exists';
            return $result;
        }

        //Password length check
        if (strlen($password)>30 || strlen($password)<6)
        {
            $result['message']='Password length must be between 6-30 character';
            return $result;
        }

        if ($password!=$password2)
        {
            $result['message']="Password didn't match";
            return $result;
        }

        //$password=sha1($password);
        $query="INSERT INTO user_details(last_name,given_name,date_of_birth,email,password,user_role) 
                VALUES ('$last_name','$given_name','$date_of_birth','$email','$password','$user_role')";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Registration complete.';
            return $result;
        }
    }
}
