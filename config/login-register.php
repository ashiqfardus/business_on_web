      <div class="modal fade flat-popupform" id="popup_login">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body text-center clearfix">


                    <form class="form-login form-listing" action="" method="post">
                            <h3 class="title-formlogin">Log in</h3>
                            <span class="input-login icon-form"><input type="email" placeholder="Enter email" name="email" required="required"><i class="fa fa-user"></i></span>
                            <span class="input-login icon-form"><input type="password" placeholder="Enter Password" name="password" required="required" data-minlength="6"><i class="fa fa-lock"></i></span>
                            <div class="flat-fogot clearfix">
                            </div>
                            <span class="wrap-button">
                                <button type="submit" id="login-button" class=" login-btn effect-button" name="login" title="log in">LOG IN</button>
                            </span>
                        <br>
                        <br>
                        <a href="forgot_pass.php"><p> Forgot Password?</p></a>
                    </form>


<?php


require_once 'login_validation.php';
$result=user_login();

//        Showing messages

if ($result)
{
    if ($result['result']==0)
    {
           echo "<script>window.location.href='';</script>";
    }
    else
    {
    echo "
        <script type='text/javascript'>
            $(window).load(function()
            {
                $('#popup_login').modal('show');
            });
        </script>";
        ?>
        <div class="col-md-12" style=" margin-top:15px;">
            <div class="alert alert-danger" role="alert">
                <strong>Error!</strong> <?php echo $result['message']; ?>
            </div>
        </div>
        <?php
    }
}
?>


            </div>
        </div>
    </div>
</div> 


    <div class="modal fade flat-popupform" id="popup_register">
            <div style="max-width: 800px;" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body text-center clearfix">

                        <form class="form-login form-listing" action="" method="post">
                            <h3 class="title-formlogin">Sign Up</h3>
                            <div class="pull-right col-md-6">
                                 <span class="input-login icon-form"><input type="text" placeholder="Enter Given Name" name="given_name" required="required"><i class="fa fa-user"></i></span>
                                 <span class="input-login icon-form"><input type="text" placeholder="Enter Email" name="email" required="required"><i class="fa fa-envelope"></i></span>
                                 <span class="input-login icon-form"><input type="password" placeholder="Enter Password" name="password" required="required"><i class="fa fa-lock"></i></span>
                                 <span class="input-login icon-form"><input type="password" placeholder="Re-Enter Password" name="password2" required="required"><i class="fa fa-lock"></i></span>
                             </div>
                            <div class="col-md-6">
                            <span class="input-login icon-form"><input type="text" placeholder="Enter Last Name" name="last_name" required="required"><i class="fa fa-user"></i></span>
                            <span class="input-login icon-form"><input type="date" placeholder="Select Birth Date" name="date_of_birth" required="required"><i class="fa fa-calender"></i></span>
                            <span class="input-login icon-form">
                                <select class="input-login icon-form custom-select" name="user_role">
                                    <option selected>Membership Type</option>
                                    <option value="regular">Regular</option>
                                    <option value="gold">Gold</option>
                                </select>
                                <!-- <i class="fa fa-lock"></i> -->
                            </span>
                             </div>
                                <p style="color: #0a0a0b" class="text-left">Note: Regular Users can't upload more than 5 photos. </p>
                                <p style="color: #0a0a0b" class="text-left">Note: Gold Users can upload videos. </p>
                            <div class="wrap-button signup">
                                <button type="submit" id="logup-button"  class=" login-btn effect-button" name="register" title="log in">Sign Up</button>
                            </div>
                        </form>

<!--        Sign-up action-->
<?php
require_once 'registration_validation.php';
$reg_result=user_registration();

//        Showing messages

if ($reg_result)
{
    if ($reg_result['result']==0)
    {
    
    echo "
        <script type='text/javascript'>
            $(window).load(function()
            {
                $('#popup_register').modal('show');
            });
        </script>";
        ?>
        <div class="col-md-12" style=" margin-top:15px;">
            <div class="alert alert-success" role="alert">
                <strong>Success!</strong> <?php echo $reg_result['message']; ?> <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#popup_login" ><b>Login Now</b></a>
            </div>
        </div>
        <?php
        
        
    }
    else
    {
    echo "
        <script type='text/javascript'>
            $(window).load(function()
            {
                $('#popup_register').modal('show');
            });
        </script>";
        ?>
        <div class="col-md-12" style=" margin-top:15px;">
            <div class="alert alert-danger" role="alert">
                <strong>Error!</strong> <?php echo $reg_result['message']; ?>
            </div>
        </div>
        <?php
    }
}
?>

                    </div>
                </div>
            </div>
    </div>