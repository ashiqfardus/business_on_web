 <div class="row" style="margin-top: 270px;">
    <!--Top left ad-->
    <div class="col-md-4">
        <div class="flat-formsearch" style="position: relative;">
            <div class=" widget widget-form style2" style="text-align: center; margin-top: 30px;">

                <?php
                $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='top_left_ad' limit 12 OFFSET 5");
                while ($result=mysqli_fetch_array($sql))
                {
                    $bd_ad_id=$result['bd_ad_id'];
                    $bd_ad_title=$result['bd_ad_title'];
                    $bd_ad_image=$result['bd_ad_image'];
                    $bd_ad_url=$result['bd_ad_url'];
                    $bd_ad_category=$result['bd_ad_category'];
                    $bd_ad_date=$result['bd_ad_date'];
                    ?>
                    <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                    <?php
                }
                ?>

            </div>
        </div>
    </div>

    <div class="col-md-4">

        <div class="row">
            <div class="flat-formsearch" style="position: relative;">
                <div class=" widget widget-form style2">
                    <div class="row">
                        <div class="float-right">
                            <div class="flat-sort" style="    margin-left: 424px; margin-top: 35px;">
                                <a href="listing-list.php" class="course-list-view active"><i class="fa fa-list"></i></a>
                                <a href="listing-grid.php" class="course-grid-view "><i class="fa fa-th"></i></a>
                            </div>
                        </div>
                    </div>
                    <form method="post" action="" class="filter-form clearfix">
                        <div style="background-color: #ffffff; padding-top: 15px; border-radius: 2.2em; background-image: url("https://www.transparenttextures.com/patterns/embossed-paper.png");" class="row">
                        <div class="col">
                            <input type="text" style="font-size: 10px; width: 173px;" class=" flat-button form-control" placeholder="What are you looking for?" name="question" autocomplete="off">
                        </div>

                        <p style="text-align: center; margin-top: 15px; font-size: 10px; margin-left: -5px; margin-right: -5px;"><b>IN</b></p>

                        <div class="col">
                            <input type="text" style="font-size: 10px; width: 165px;" class="flat-button form-control" placeholder="Postcode/Suburb/State" name="address" autocomplete="off">
                        </div>
                        <div class="col">
                            <button class="flat-button form-control" value="search" style="width: 70px; border-radius: 0px; margin-left: -22px;" type="submit" name="search"><i class="ion-ios-search-strong"></i></button>
                        </div>

                </div>
                <hr>
                </form>
            </div>
        </div>

    </div>

    <?php
    if (isset($_POST['search'])) {
        $question = $_POST['question'];
        $address = $_POST['address'];
        ?>
        <div class=" widget widget-form style2" style="margin-bottom-40px;">
            <div class="content-inner active listing-list">
                <!--                    <h1 style="text-align: center;"> <b>Search Results</b></h1>-->


                <?php
                if (!empty($question) || !empty($address))
                {
                    $search_query = mysqli_query($connection, "SELECT * FROM business_directory_details WHERE 
                                                      bd_title LIKE '%$question%' 
                                                      and bd_postcode_suburb_state like '%$address%'");

                    echo "<h1 style=\"text-align: center;\\margin-top:-211px; \"> <b>Search Results</b></h1>";
                    while ($search_result = mysqli_fetch_array($search_query)) {
                        $bd_id = $search_result['bd_id'];
                        $title = $search_result['bd_title'];
                        $contact_address = $search_result['bd_contact_address'];
                        $status = $search_result['bd_status'];


                        //image for business
                        $image_query = mysqli_query($connection, "SELECT * FROM business_directory_image 
                                                                  where bdi_bd_id='$bd_id' limit 1");
                        while ($image = mysqli_fetch_array($image_query)) {
                            $image_name = $image['bdi_image'];
                            ?>

                            <div class="flat-product clearfix" style="margin-top: -19px; ">
                                <div class="featured-product">
                                    <img src="image/<?php echo "$image_name"; ?>" alt="image" style="width: 150px; height: 100px; border-radius: 4px;">
                                </div>
                                <div class="rate-product">
                                    <div class="info-product">
                                        <p class="title"><a style="color: #0f0f0f;" href="listing_common.php?view=<?php echo $bd_id?>"><?php echo $title?></a></p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }


                ?>

            </div>
        </div>
    <?php }
    else
    {
        ?>


        <div class="row" style="margin-top: -21em;">
            <div class="row listing-grid">
                <?php

                $business_search_query=mysqli_query($connection,"SELECT * FROM business_directory_details
                                                                             where bd_status='published' order by bd_time LIMIT 20");
                while ($result=mysqli_fetch_array($business_search_query))
                {
                    $bd_id=$result['bd_id'];
                    $title=$result['bd_title'];
                    $postcode=$result['bd_postcode_suburb_state'];
                    $facebook=$result['bd_facebook'];
                    $twitter=$result['bd_twitter'];
                    $linkedin=$result['bd_linkedin'];
                    $android_app=$result['bd_android_app'];
                    $ios_app=$result['bd_ios_app'];
                    $map=$result['bd_map'];
                    $contact_address=$result['bd_contact_address'];
                    $contact_telephone=$result['bd_contact_telephone'];
                    $contact_mobile=$result['bd_contact_mobile'];
                    $contact_email=$result['bd_contact_email'];
                    $contact_website=$result['bd_contact_website'];
                    $about_us=$result['bd_about_us'];
                    $services=$result['bd_services'];
                    $status=$result['bd_status'];


                    //image for business
                    $image_query=mysqli_query($connection,"SELECT * FROM business_directory_image
                                                                          where  bdi_bd_id='$bd_id' LIMIT 1");
                    while ($image=mysqli_fetch_array($image_query))
                    {
                        $image_name=$image['bdi_image'];

                        ?>
                        <div class="col-md-3" >
                            <div class="card" style="width: 7rem; margin-top: 5px;">
                                <img class="card-img-top" style="width:270px; height: 70px;" src="image/<?php echo $image_name; ?>">
                                <div class="card-header bg-dark">
                                    <a href="listing_common.php?view=<?php echo $bd_id?>" style="font-size:1em; color:white !important; "><?php echo $title; ?></a>
                                </div>
                            </div>
                        </div>

                    <?php }} ?>
            </div>
        </div>
    <?php } ?>


</div>
<div class="col-md-4">
    <div class="flat-formsearch" style="position: relative;">
        <div class=" widget widget-form style2" style="text-align: center; margin-top: 30px;">
            <?php
            $sql=mysqli_query($connection,"SELECT * FROM business_directory_ad 
                                          where bd_ad_category='top_right_ad' limit 12 OFFSET 5");
            while ($result=mysqli_fetch_array($sql))
            {
                $bd_ad_id=$result['bd_ad_id'];
                $bd_ad_title=$result['bd_ad_title'];
                $bd_ad_image=$result['bd_ad_image'];
                $bd_ad_url=$result['bd_ad_url'];
                $bd_ad_category=$result['bd_ad_category'];
                $bd_ad_date=$result['bd_ad_date'];
                ?>
                <a href="<?php echo $bd_ad_url?>"><img src="image/ad_image/<?php echo "$bd_ad_image"; ?>" alt="image" style="height: auto; width: 12em; margin-top:5px;"></a>
                <?php
            }
            ?>
        </div>
    </div>
</div>

</div>







