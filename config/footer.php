    <!-- Footer -->
    <footer class="footer footer-widgets" style="padding: 0px; margin-top: 15px;">

        <div class="container">
            <div class="row" >
                <div class="col-lg-4" style="margin-top: 30px;">
                    <div id="nav_menu-2" class="widget widget_nav_menu">
                        <h5 class="widget-title">Information</h5>
                        <div class="wrap-menufooter clearfix">
                            <ul class="menu one-half">
                                <li class="menu-item"><a href="privacy.php">Privacy Notice</a></li>
                                <li class="menu-item"><a href="condition_of_use.php">Condition Of Use</a></li>
                                <li class="menu-item"><a href="terms.php">Terms & Condition</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" style="margin-top: 30px;">
                    <div id="nav_menu-2" class="widget widget_nav_menu">
                        <h5 class="widget-title">Help & Content</h5>
                        <div class="wrap-menufooter clearfix">
                            <ul class="menu one-half">
                                <li class="menu-item"><a href="#">Photo Upload</a></li>
                                <li class="menu-item"><a href="#">Video Upload</a></li>
                                <li class="menu-item"><a href="#">Content Writing</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4" style="margin-top: 30px;">
                    <div id="nav_menu-2" class="widget widget_nav_menu">
                        <h5 class="widget-title">My Account</h5>
                        <div class="wrap-menufooter clearfix">
                            <ul class="menu one-half">
                                <li class="menu-item"><a href="#">My Information</a></li>
                                <li class="menu-item"><a href="#">Orders</a></li>
                                <li class="menu-item"><a href="#">Address</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.row -->
        </div>
            <div class="container">
                <div class="bottom">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="copyright"> 
                                <p>Copyright © 2018. Developed by <a href="#">Techsolutions Global</a>. All Rights Reserved.
                                </p>
                            </div>                   
                        </div><!-- /.col-md-12 -->
                        <div class="col-lg-6">
                            <div class="social-links float-right">
                                <a href="https://www.facebook.com/baysidechamberofcommerce/"><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
                                <a href="https://plus.google.com/discover"><i class="fa fa-google-plus"></i></a>
                                <a href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
    </footer>




<?php include "login-register.php" ?>
    
    <!-- Go Top -->
    <a class="go-top effect-button">
        <i class="fa fa-angle-up"></i>
    </a>   

    </div>
    
     <!-- Javascript -->
    <script src="javascript/jquery.min.js"></script>
    <script src="javascript/tether.min.js"></script>
    <script src="javascript/bootstrap.min.js"></script> 
    <script src="javascript/jquery.easing.js"></script>    
    <script src="javascript/jquery-waypoints.js"></script> 
    <script src="javascript/jquery-countTo.js"></script>  
    <script src="javascript/owl.carousel.js"></script>
    <script src="javascript/jquery.cookie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>
    <script src="javascript/parallax.js"></script>
    <script src="javascript/bootstrap-slider.min.js"></script>
    <script src="javascript/smoothscroll.js"></script>   

    <script src="javascript/main.js"></script>

    <!-- Revolution Slider -->
    <script src="revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="revolution/js/slider.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->    
    <script src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript">

        function ask_for_delete() {
            var chk = confirm('Are you sure you want to delete?');
            if (chk) {
                return true;
            } else {
                return false;
            }
        }
    </script>
