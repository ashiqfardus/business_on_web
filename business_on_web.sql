-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2018 at 09:01 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `business_on_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `about_us_id` int(11) NOT NULL,
  `about_us_title` varchar(255) NOT NULL,
  `about_us_content` text NOT NULL,
  `about_us_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`about_us_id`, `about_us_title`, `about_us_content`, `about_us_image`) VALUES
(1, 'About Us', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'we.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_governance`
--

CREATE TABLE `about_us_governance` (
  `governance_id` int(11) NOT NULL,
  `governance_title` varchar(255) NOT NULL,
  `governance_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us_governance`
--

INSERT INTO `about_us_governance` (`governance_id`, `governance_title`, `governance_content`) VALUES
(1, 'Governance', '<h3>Board Management</h3><p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_history`
--

CREATE TABLE `about_us_history` (
  `history_id` int(11) NOT NULL,
  `history_title` varchar(255) NOT NULL,
  `history_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us_history`
--

INSERT INTO `about_us_history` (`history_id`, `history_title`, `history_content`) VALUES
(1, 'About Us', '<p>The Bayside Chamber of Commerce consists of a group of professionals &amp; business owners, who are committed to connect, assist and support each other in the interest of improving the Bayside Business community.</p><p>The Bayside area is one of the most interesting and diverse areas to visit in Sydney. Especially our beaches such as Brighton Le Sands,&nbsp; Ramsgate and Dolls Point, Shopping is plentiful&nbsp; such as the big Westfield complex at&nbsp; Eastgarden as well as a choice of many ethnic eating places in Rockdale, Arncliffee, Brighton Le Sands, Eastgardens and Mascot. We are conveniently located a short distance from Sydney domestic and international Airport and the Sydney CBD.</p><p>What we do for our Members:</p><ul>	<li>To meets monthly to advance its work program.</li>	<li>To promote the business interests of members;</li>	<li>To promote and facilitate communication between busineses through our website and regular meeting;</li>	<li>To encourag residents to support local business;</li>	<li>To ensure transport, parking and pedestrian facilities meet the needs of the community and businesses</li>	<li>To promote the benefits of shopping in Bayside Area;</li>	<li>To increase commercial activity for local businesses;</li>	<li>To provide networking, learning and development for members;</li>	<li>To act as a single voice for local businesses to represent their collective views to all levels of government, industry and the community;</li>	<li>Support a vibrant and flourishing business community that contributes positively to the broader local community;</li>	<li>	<p>To represent the interests of the business community to government, local government and statutory authorities</p>	</li></ul>');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_members_forum`
--

CREATE TABLE `about_us_members_forum` (
  `members_forum_id` int(11) NOT NULL,
  `members_forum_name` varchar(255) NOT NULL,
  `members_forum_designation` varchar(255) NOT NULL,
  `members_forum_info` text NOT NULL,
  `members_forum_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us_members_forum`
--

INSERT INTO `about_us_members_forum` (`members_forum_id`, `members_forum_name`, `members_forum_designation`, `members_forum_info`, `members_forum_date`) VALUES
(4, 'Farid Ahmed', 'CEO', 'Gess Australia\r\nRockdale, Australia. ', '2017-10-22 11:16:06'),
(5, 'Faisal', 'Membership ', '', '2017-11-06 02:39:53');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_our_mission`
--

CREATE TABLE `about_us_our_mission` (
  `our_mission_id` int(11) NOT NULL,
  `our_mission_title` varchar(255) NOT NULL,
  `our_mission_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us_our_mission`
--

INSERT INTO `about_us_our_mission` (`our_mission_id`, `our_mission_title`, `our_mission_content`) VALUES
(1, 'Our Mission', '<p>Our mission is to<strong> </strong>promote and meet the needs of business and industry and to create the best community in which to live, work and do business. The mission&quot; of the Bayside Chamber of Commerce is to provide leadership for the advancement of economic vitality and equality of life for the total community.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_our_staff`
--

CREATE TABLE `about_us_our_staff` (
  `our_staff_id` int(11) NOT NULL,
  `our_staff_name` varchar(255) NOT NULL,
  `our_staff_designation` varchar(255) NOT NULL,
  `our_staff_image` text NOT NULL,
  `our_staff_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `about_us_our_staff`
--

INSERT INTO `about_us_our_staff` (`our_staff_id`, `our_staff_name`, `our_staff_designation`, `our_staff_image`, `our_staff_date`) VALUES
(1, 'Farid Ahmed', 'CEO', '1508672479221729_1028077860527_1350_n.jpg', '2017-10-22 11:41:19');

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `admin_id` int(11) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`admin_id`, `admin_username`, `admin_password`) VALUES
(1, 'admin@chamberofcommerce.com', '7c4a8d09ca3762af61e59520943dc26494f8941b'),
(3, 'techsolutions', 'd3d6459872ab6ca5930b88c86afba57021b44910');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `album_id` int(11) NOT NULL,
  `album_title` varchar(255) NOT NULL,
  `album_image` text NOT NULL,
  `album_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`album_id`, `album_title`, `album_image`, `album_date`) VALUES
(1, 'Christmas Party', '1512972059christmas-party.jpg', '2017-12-11 06:00:59');

-- --------------------------------------------------------

--
-- Table structure for table `album_photo`
--

CREATE TABLE `album_photo` (
  `album_photo_id` int(11) NOT NULL,
  `album_photo_c_id` int(11) NOT NULL,
  `album_photo_name` varchar(255) NOT NULL,
  `album_photo_image` text NOT NULL,
  `album_photo_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album_photo`
--

INSERT INTO `album_photo` (`album_photo_id`, `album_photo_c_id`, `album_photo_name`, `album_photo_image`, `album_photo_date`) VALUES
(1, 1, 'Christmas Party Annoucement', '151297281324232162_198533880706221_4171814665627039536_n.jpg', '2017-12-11 06:13:33'),
(2, 1, 'Perticipents', '151297283624775044_198811940678415_1626973651592704269_n.jpg', '2017-12-11 06:13:56'),
(3, 1, 'Board Members', '151297289324296844_198811864011756_1538390141103383509_n.jpg', '2017-12-11 06:14:53'),
(4, 1, 'Christmas Party ', '151297291524796394_198811844011758_3505371604339754559_n.jpg', '2017-12-11 06:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL,
  `banner_image` text NOT NULL,
  `banner_image_title` varchar(255) NOT NULL,
  `banner_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_image`, `banner_image_title`, `banner_date`) VALUES
(1, '1508661497s1.jpg', '1', '2017-10-22 08:38:17'),
(5, '1509929237King Street.jpg', 'rockdale', '2017-11-06 00:45:24'),
(6, '1509929266Rockdale Council.jpg', 'rockdale', '2017-11-06 00:45:52'),
(8, '1510033477Sydney Airport.jpg', 'Sydney Airport', '2017-11-07 05:42:43'),
(9, '1510033716Novotel-Brighton-2400x600.jpg', 'Brighton Le Sands', '2017-11-07 05:46:42'),
(10, '1510035366Port Botany.jpg', 'Port Botany', '2017-11-07 06:14:12'),
(11, '1510036661Sydney Airport1.jpg', 'Sydney Airport', '2017-11-07 06:35:48');

-- --------------------------------------------------------

--
-- Table structure for table `bottom_left_ad`
--

CREATE TABLE `bottom_left_ad` (
  `bottom_left_ad_id` int(11) NOT NULL,
  `bottom_left_ad_image` text NOT NULL,
  `bottom_left_ad_image_title` varchar(255) NOT NULL,
  `bottom_left_ad_link` varchar(255) NOT NULL,
  `bottom_left_ad_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bottom_left_ad`
--

INSERT INTO `bottom_left_ad` (`bottom_left_ad_id`, `bottom_left_ad_image`, `bottom_left_ad_image_title`, `bottom_left_ad_link`, `bottom_left_ad_date`) VALUES
(1, 'ad222.jpg', '1', '#', '2017-11-08 07:55:32'),
(2, 'ad222.jpg', '2', '#', '2017-11-08 07:55:40'),
(3, 'ad222.jpg', '3', '#', '2017-11-08 07:56:16'),
(4, 'ad222.jpg', '4', '#', '2017-11-08 07:56:23'),
(5, 'ad222.jpg', '5', '#', '2017-11-08 07:56:34'),
(6, 'ad222.jpg', '6', '#', '2017-11-08 07:56:47'),
(7, 'ad222.jpg', '7', '#', '2017-11-08 07:56:55'),
(8, 'ad222.jpg', '8', '#', '2017-11-08 07:57:08'),
(9, 'ad222.jpg', '9', '#', '2017-11-08 07:57:18'),
(10, 'ad222.jpg', '10', '#', '2017-11-08 07:57:26'),
(11, 'ad222.jpg', '11', '#', '2017-11-08 07:57:40'),
(12, 'ad222.jpg', '12', '#', '2017-11-08 07:57:56'),
(13, 'ad222.jpg', '13', '#', '2017-11-08 07:58:10'),
(14, 'ad222.jpg', '14', '#', '2017-11-08 07:58:24'),
(15, 'ad222.jpg', '15', '#', '2017-11-08 07:58:56'),
(16, 'ad222.jpg', '16', '#', '2017-11-08 07:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `bottom_right_ad`
--

CREATE TABLE `bottom_right_ad` (
  `bottom_right_ad_id` int(11) NOT NULL,
  `bottom_right_ad_image` text NOT NULL,
  `bottom_right_ad_image_title` varchar(255) NOT NULL,
  `bottom_right_ad_link` varchar(255) NOT NULL,
  `bottom_right_ad_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bottom_right_ad`
--

INSERT INTO `bottom_right_ad` (`bottom_right_ad_id`, `bottom_right_ad_image`, `bottom_right_ad_image_title`, `bottom_right_ad_link`, `bottom_right_ad_date`) VALUES
(1, 'ad222.jpg', '1', '#', '2017-11-08 08:16:32'),
(2, 'ad222.jpg', '2', '#', '2017-11-08 08:16:40'),
(3, 'ad222.jpg', '3', '#', '2017-11-08 08:16:47'),
(4, 'ad222.jpg', '4', '#', '2017-11-08 08:16:55'),
(5, 'ad222.jpg', '5', '#', '2017-11-08 08:17:03'),
(6, 'ad222.jpg', '6', '#', '2017-11-08 08:17:11'),
(7, 'ad222.jpg', '7', '#', '2017-11-08 08:17:20'),
(8, 'ad222.jpg', '8', '#', '2017-11-08 08:17:28'),
(9, 'ad222.jpg', '9', '#', '2017-11-08 08:17:37'),
(10, 'ad222.jpg', '10', '#', '2017-11-08 08:17:47'),
(11, 'ad222.jpg', '11', '#', '2017-11-08 08:17:56'),
(12, 'ad222.jpg', '12', '#', '2017-11-08 08:18:59'),
(13, 'ad222.jpg', '13', '#', '2017-11-08 08:18:48'),
(14, 'ad222.jpg', '14', '#', '2017-11-08 08:18:30'),
(15, 'ad222.jpg', '15', '#', '2017-11-08 08:18:17'),
(16, 'ad222.jpg', '16', '#', '2017-11-08 08:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `business_category`
--

CREATE TABLE `business_category` (
  `category_id` int(11) NOT NULL,
  `category_title` varchar(255) NOT NULL,
  `category_image` text NOT NULL,
  `category_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_category`
--

INSERT INTO `business_category` (`category_id`, `category_title`, `category_image`, `category_date`) VALUES
(3, 'A TO Z', '15088217321.jpg', '2017-10-24 05:38:52'),
(4, 'Hotel & Accomodation', '15088218302.jpg', '2017-11-22 08:57:22'),
(5, 'Priting & Design', '1508821848artist-2.jpg', '2017-11-22 08:58:12'),
(6, 'BANKS', '1508821865banks.jpg', '2017-10-24 05:11:05'),
(7, 'Restaurant and Cafee', 'Restaurant.jpg', '2017-11-22 09:08:33'),
(8, 'Function Centres', 'Function photo.jpg', '2017-11-22 09:08:55'),
(9, 'Migration and Student Admission', 'Student Admissions and Migration.jpg', '2017-11-22 09:57:01'),
(10, 'Travel & Ticketing', 'Travel and Ticketing.jpg', '2017-11-22 10:01:33'),
(11, 'Accounting and Taxation', 'Accounting and Taxation.jpg', '2017-11-22 09:04:49'),
(12, 'Real Estate and Strata', '1511342234Real Estate Logo.jpg', '2017-11-22 09:15:21'),
(13, 'Cleaning Services', 'Cleaning Logo.jpg', '2017-11-23 03:14:55');

-- --------------------------------------------------------

--
-- Table structure for table `business_directory_ad`
--

CREATE TABLE `business_directory_ad` (
  `bd_ad_id` int(11) NOT NULL,
  `bd_ad_title` varchar(255) NOT NULL,
  `bd_ad_image` varchar(255) NOT NULL,
  `bd_ad_url` varchar(255) NOT NULL,
  `bd_ad_category` varchar(255) NOT NULL,
  `bd_ad_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business_directory_ad`
--

INSERT INTO `business_directory_ad` (`bd_ad_id`, `bd_ad_title`, `bd_ad_image`, `bd_ad_url`, `bd_ad_category`, `bd_ad_date`) VALUES
(53, 'Chamber Of Commerce1', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(54, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(55, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(56, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(57, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(58, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(59, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(60, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(61, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(62, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(63, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(64, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(65, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(66, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(67, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(68, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(69, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(70, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(71, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(72, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(73, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(74, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(75, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(76, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(77, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(78, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(79, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(80, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(81, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(82, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(83, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(84, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(85, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(86, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(87, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(88, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(89, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(90, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(91, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(92, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(93, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(94, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(95, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(96, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(97, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(98, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(99, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(100, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(101, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(102, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(103, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(104, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(105, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(106, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(107, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(108, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(109, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(110, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(111, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(112, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(113, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(114, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(115, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(116, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_left_ad', '2018-09-09 06:36:13'),
(117, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(118, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(119, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(120, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(121, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(122, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(123, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(124, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(125, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(126, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(127, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(128, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(129, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(130, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(131, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(132, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'bottom_right_ad', '2018-09-09 06:38:18'),
(133, 'single ad1', 'a.jpg', 'http://techsolutionsbd.com', 'single_page_ad', '2018-09-22 10:11:06'),
(135, 'Top right', 'a.jpg', 'http://techsolutionsbd.com', 'all_directory_ad', '2018-09-22 10:12:19'),
(137, 'Test ad', 'a.jpg', 'http://techsolutionsbd.com', 'all_directory_ad', '2018-09-22 10:18:17'),
(138, 'Top right ad', 'a.jpg', 'http://techsolutionsbd.com', 'all_directory_ad', '2018-09-22 10:18:26'),
(139, 'single ad1', 'a.jpg', 'http://techsolutionsbd.com', 'single_page_ad', '2018-09-22 10:11:06'),
(140, 'single ad1', 'a.jpg', 'http://techsolutionsbd.com', 'single_page_ad', '2018-09-22 10:11:06'),
(141, 'single ad1', 'a.jpg', 'http://techsolutionsbd.com', 'single_page_ad', '2018-09-22 10:11:06'),
(142, 'single ad1', 'a.jpg', 'http://techsolutionsbd.com', 'single_page_ad', '2018-09-22 10:11:06'),
(143, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(144, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(145, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(146, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(148, 'Top right ad', 'a.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad', '2018-10-21 07:16:04'),
(150, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad', '2018-10-21 08:50:56'),
(152, 'Top right ad3', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad', '2018-10-21 08:59:20'),
(159, 'Top right adz', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad', '2018-10-21 09:20:11'),
(160, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', '', '2018-10-21 10:26:37'),
(161, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', '', '2018-10-21 10:27:14'),
(162, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-21 10:28:44'),
(163, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 10:41:46'),
(164, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 10:47:23'),
(165, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-21 10:54:31'),
(166, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:00:10'),
(167, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:02:57'),
(168, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:03:03'),
(169, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:03:10'),
(170, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:06:13'),
(171, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:06:20'),
(172, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:08:17'),
(173, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-21 11:08:27'),
(174, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-21 11:08:39'),
(175, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-21 11:16:17'),
(176, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-21 11:16:23'),
(177, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-21 11:16:31'),
(178, 'dsFdxfffffdffd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-21 11:16:40'),
(179, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-21 11:17:11'),
(180, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:22:56'),
(181, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:02'),
(182, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:08'),
(183, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:15'),
(184, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:22'),
(185, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:29'),
(186, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:36'),
(187, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:58'),
(188, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:24:05'),
(189, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:10'),
(190, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:17'),
(191, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:24'),
(192, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:31'),
(193, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:37'),
(194, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_right', '2018-10-21 11:31:52'),
(195, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_right', '2018-10-21 11:31:59'),
(196, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_right', '2018-10-21 11:32:06'),
(197, 'Test edit', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-21 12:16:44'),
(198, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-21 12:16:50'),
(199, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-21 12:16:59'),
(200, 'Test edit', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-21 12:18:35'),
(201, 'Test edit', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-21 12:18:42'),
(202, 'Test edit', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-21 12:18:54'),
(203, 'Test ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-21 12:19:03'),
(205, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 09:03:59'),
(206, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 09:04:05'),
(208, 'Top right ad 2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 11:49:37'),
(210, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 11:58:11'),
(211, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 11:58:18'),
(212, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 11:58:38'),
(214, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 12:06:20'),
(215, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 12:06:29'),
(216, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 12:07:36'),
(217, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:14:59'),
(218, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:18:54'),
(219, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:19:03'),
(220, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:19:09'),
(221, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:44:40'),
(222, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:44:47'),
(223, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:44:54'),
(224, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:01'),
(225, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:08'),
(226, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:16'),
(227, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:21'),
(228, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:27'),
(229, 'Top right adw', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:34'),
(230, 'w', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:45:43'),
(231, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:45:49'),
(232, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:45:56'),
(233, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:46:02'),
(234, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:46:39'),
(235, 'Chamber Of Commerce1', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(236, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(237, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(238, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(239, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(240, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(241, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(242, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(243, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(244, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(245, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(246, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(247, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(248, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(249, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(250, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(251, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(252, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(253, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(254, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(255, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(256, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(257, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(258, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(259, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(260, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(261, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(262, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(263, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(264, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(265, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:31'),
(266, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_left_ad', '2018-09-09 06:17:49'),
(267, 'Chamber Of Commerce', 'a.jpg', 'http://www.baysidechamberofcommerce.com.au', 'top_right_ad', '2018-09-09 06:29:15'),
(268, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:44:40'),
(269, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:44:47'),
(270, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:44:54'),
(271, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:01'),
(272, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:08'),
(273, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:16'),
(274, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:21'),
(275, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:27'),
(276, 'Top right adw', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:45:34'),
(277, 'w', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:45:43'),
(278, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:45:49'),
(279, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:45:56'),
(280, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:46:02'),
(281, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-23 12:46:39'),
(282, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_left', '2018-10-21 10:28:44'),
(283, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 10:41:46'),
(284, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 10:47:23'),
(285, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-21 10:54:31'),
(286, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:00:10'),
(287, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:02:57'),
(288, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:03:03'),
(289, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:03:10'),
(290, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:06:13'),
(291, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:06:20'),
(292, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-21 11:08:17'),
(293, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-21 11:08:27'),
(294, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-21 11:08:39'),
(295, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-21 11:16:17'),
(296, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-21 11:16:23'),
(297, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-21 11:16:31'),
(298, 'dsFdxfffffdffd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-21 11:16:40'),
(299, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-21 11:17:11'),
(300, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:22:56'),
(301, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:02'),
(302, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:08'),
(303, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:15'),
(304, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:22'),
(305, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:29'),
(306, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:36'),
(307, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:58'),
(308, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:24:05'),
(309, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:10'),
(310, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:17'),
(311, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:24'),
(312, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:31'),
(313, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:37'),
(314, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_right', '2018-10-21 11:31:52'),
(315, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:50:56'),
(316, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:51:04'),
(317, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:51:10'),
(318, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:51:16'),
(319, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:51:23'),
(320, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:51:29'),
(321, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:51:58'),
(322, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:54:00'),
(323, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:54:07'),
(324, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'member_benefit_ad_right', '2018-10-23 12:54:19'),
(325, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:25'),
(326, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:32'),
(327, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(328, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(329, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:32'),
(330, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:25'),
(331, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_right', '2018-10-21 11:31:52'),
(332, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:37'),
(333, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:31'),
(334, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:24'),
(335, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:17'),
(336, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:10'),
(337, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:24:05'),
(338, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:58'),
(339, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:36'),
(340, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:29'),
(341, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:22'),
(342, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:22'),
(343, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:29'),
(344, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:36'),
(345, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:58'),
(346, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:24:05'),
(347, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:10'),
(348, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:17'),
(349, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:24'),
(350, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:31'),
(351, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:37'),
(352, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_right', '2018-10-21 11:31:52'),
(353, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:25'),
(354, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:32'),
(355, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(356, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(357, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:32'),
(358, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:25'),
(359, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:25'),
(360, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:32'),
(361, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(362, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(363, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:32'),
(364, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:25'),
(365, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_right', '2018-10-21 11:31:52'),
(366, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:37'),
(367, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:31'),
(368, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:24'),
(369, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:17'),
(370, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:10'),
(371, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:24:05'),
(372, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:58'),
(373, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:36'),
(374, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:29'),
(375, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:22'),
(376, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:22'),
(377, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:29'),
(378, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:36'),
(379, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:58'),
(380, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:24:05'),
(381, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:10'),
(382, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:17'),
(383, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:24'),
(384, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:24'),
(385, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:17'),
(386, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:10'),
(387, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:24:05'),
(388, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:58'),
(389, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:36'),
(390, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:29'),
(391, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:22'),
(392, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:22'),
(393, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:29'),
(394, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:36'),
(395, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:58'),
(396, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:24:05'),
(397, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:10'),
(398, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:17'),
(399, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:24'),
(400, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:31'),
(401, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:37'),
(402, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_right', '2018-10-21 11:31:52'),
(403, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:25'),
(404, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:32'),
(405, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(406, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(407, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:32'),
(408, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:25'),
(409, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:25'),
(410, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:32'),
(411, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(412, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(413, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:32'),
(414, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_left', '2018-10-23 12:55:25'),
(415, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_right', '2018-10-21 11:31:52'),
(416, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:37'),
(417, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:31'),
(418, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:24'),
(419, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:17'),
(420, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:10'),
(421, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:24:05'),
(422, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:58'),
(423, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:36'),
(424, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:29'),
(425, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:22'),
(426, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-21 11:23:22'),
(427, 'wqssddddddddddddddddddddd', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:29'),
(428, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:36'),
(429, 'Top right ad2', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:23:58'),
(430, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_right', '2018-10-21 11:24:05'),
(431, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:10'),
(432, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:17'),
(433, 'Test edit', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'business_directory_ad_left', '2018-10-21 11:31:24'),
(434, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:58:22'),
(435, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:58:22'),
(436, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(437, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(438, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(439, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(440, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:58:22'),
(441, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:58:22'),
(442, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:58:22'),
(443, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:58:22'),
(444, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(445, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(446, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(447, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:56:05'),
(448, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:58:22'),
(449, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'about_us_ad_right', '2018-10-23 12:58:22'),
(450, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(451, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(452, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(453, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(454, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(455, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(456, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(457, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(458, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(459, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(460, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(461, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(462, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(463, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(464, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(465, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(466, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(467, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(468, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(469, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(470, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(471, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(472, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(473, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(474, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(475, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(476, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(477, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(478, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(479, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(480, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(481, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(482, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(483, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(484, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(485, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(486, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(487, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(488, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(489, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(490, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(491, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(492, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(493, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(494, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(495, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(496, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(497, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(498, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(499, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(500, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_right', '2018-10-23 12:59:44'),
(501, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(502, 'Top right ad2', '1.jpg', 'wwww.techsolutionsbd.com', 'our_board_ad_left', '2018-10-23 12:59:37'),
(503, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(504, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(505, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(506, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(507, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(508, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(509, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(510, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(511, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(512, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35');
INSERT INTO `business_directory_ad` (`bd_ad_id`, `bd_ad_title`, `bd_ad_image`, `bd_ad_url`, `bd_ad_category`, `bd_ad_date`) VALUES
(513, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(514, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(515, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(516, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(517, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(518, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(519, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(520, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(521, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(522, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(523, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(524, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(525, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(526, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(527, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(528, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(529, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(530, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(531, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(532, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(533, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(534, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(535, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(536, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(537, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(538, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(539, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(540, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(541, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(542, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(543, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(544, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(545, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(546, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(547, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(548, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(549, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(550, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(551, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(552, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(553, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(554, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(555, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(556, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(557, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_left', '2018-10-23 13:01:26'),
(558, 'wqssddddddddddddddddddddd', '1.jpg', 'wwww.techsolutionsbd.com', 'join_now_ad_right', '2018-10-23 13:01:35'),
(559, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(560, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(561, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(562, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(563, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(564, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(565, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(566, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(567, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(568, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(569, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(570, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(571, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(572, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(573, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(574, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(575, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(576, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(577, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(578, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(579, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(580, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(581, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(582, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(583, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(584, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(585, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(586, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(587, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(588, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(589, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_right', '2018-10-23 13:03:52'),
(590, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'event_news_ad_left', '2018-10-23 13:03:45'),
(591, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-23 13:05:57'),
(592, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'business_resource_ad_left', '2018-10-23 13:06:02'),
(593, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(594, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(595, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(596, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(597, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(598, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(599, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(600, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(601, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(602, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(603, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(604, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(605, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(606, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(607, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(608, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(609, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(610, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(611, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(612, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(613, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(614, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(615, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(616, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(617, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(618, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(619, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(620, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(621, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(622, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(623, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:06:41'),
(624, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_left', '2018-10-23 13:06:33'),
(625, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'gallery_ad_right', '2018-10-23 13:08:07'),
(626, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(627, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(628, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(629, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(630, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(631, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(632, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(633, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(634, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(635, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(636, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(637, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(638, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(639, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(640, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(641, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(642, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(643, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(644, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(645, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(646, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(647, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(648, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(649, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(650, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(651, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(652, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(653, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(654, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(655, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(656, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(657, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(658, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(659, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(660, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(661, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(662, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(663, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(664, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(665, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(666, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(667, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(668, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(669, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(670, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(671, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(672, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(673, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(674, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:51'),
(675, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(676, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(677, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(678, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:29'),
(679, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_left', '2018-10-23 13:08:35'),
(680, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'contact_ad_case_right', '2018-10-23 13:08:43'),
(681, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'all_directory_ad', '2018-10-29 10:09:12'),
(682, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'all_directory_ad', '2018-10-29 10:09:19'),
(683, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'all_directory_ad', '2018-10-29 10:09:25'),
(684, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'all_directory_ad', '2018-10-29 10:09:31'),
(685, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'all_directory_ad', '2018-10-29 10:09:38'),
(686, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'all_directory_ad', '2018-10-29 10:09:47'),
(687, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'all_directory_ad', '2018-10-29 10:09:59'),
(688, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'all_directory_ad', '2018-10-29 10:10:06'),
(689, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'all_directory_ad', '2018-10-29 10:10:12'),
(691, 'Top right ad', '1.jpg', 'wwww.techsolutionsbd.com', 'service_ad_case_right', '2018-11-19 12:06:10'),
(692, 'Top right ad', 'ad222.jpg', 'wwww.techsolutionsbd.com', 'service_ad_case_left', '2018-11-19 12:16:24'),
(693, 'Top right ad', 'a.jpg', 'wwww.techsolutionsbd.com', 'service_ad_case_left', '2018-11-19 12:16:45'),
(694, 'Top right ad', 'a.jpg', 'wwww.techsolutionsbd.com', 'service_ad_case_right', '2018-11-19 12:16:54');

-- --------------------------------------------------------

--
-- Table structure for table `business_directory_category`
--

CREATE TABLE `business_directory_category` (
  `bdc_id` int(11) NOT NULL,
  `bdc_title` varchar(255) NOT NULL,
  `bdc_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bdc_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business_directory_category`
--

INSERT INTO `business_directory_category` (`bdc_id`, `bdc_title`, `bdc_date`, `bdc_image`) VALUES
(3, 'TechSolutions BD1', '2018-09-26 07:09:14', '2270 - Copy - Copy.jpg'),
(5, 'Solar Panel Installation', '2018-09-26 07:34:11', '2270 - Copy - Copy - Copy.jpg'),
(7, 'Straight 5% Price Beat', '2018-10-28 09:04:13', '2270 - Copy.jpg'),
(8, 'Abacus', '2018-10-30 04:59:06', 'bg.jpg'),
(9, 'Accommodation', '2018-10-30 05:02:42', 'slider2.jpg'),
(10, 'Accountants', '2018-10-30 05:02:54', 'slider1.jpg'),
(11, 'Actors', '2018-10-30 05:03:52', 'slider2.jpg'),
(12, 'Acupressure', '2018-10-30 05:04:26', 'slider2.jpg'),
(13, 'Acupuncture', '2018-10-30 05:04:40', 'slider_2.jpg'),
(14, 'Adelaide', '2018-10-30 05:05:01', 'bg.jpg'),
(15, 'Adimurai', '2018-10-30 05:17:32', 'slider1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `business_directory_details`
--

CREATE TABLE `business_directory_details` (
  `bd_id` int(11) NOT NULL,
  `bd_user_id` int(11) NOT NULL,
  `bd_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bd_title` varchar(255) NOT NULL,
  `bd_category` varchar(255) NOT NULL,
  `bd_postcode_suburb_state` varchar(255) NOT NULL,
  `bd_facebook` varchar(255) NOT NULL,
  `bd_twitter` varchar(255) NOT NULL,
  `bd_linkedin` varchar(255) NOT NULL,
  `bd_youtube` varchar(255) NOT NULL,
  `bd_android_app` varchar(255) NOT NULL,
  `bd_ios_app` varchar(255) NOT NULL,
  `bd_contact_address` varchar(255) NOT NULL,
  `bd_contact_telephone` varchar(255) NOT NULL,
  `bd_contact_mobile` varchar(255) NOT NULL,
  `bd_contact_email` varchar(255) NOT NULL,
  `bd_contact_website` varchar(255) NOT NULL,
  `bd_about_us` text NOT NULL,
  `bd_map` text NOT NULL,
  `bd_services` text NOT NULL,
  `bd_unique_id` varchar(255) NOT NULL,
  `bd_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business_directory_details`
--

INSERT INTO `business_directory_details` (`bd_id`, `bd_user_id`, `bd_time`, `bd_title`, `bd_category`, `bd_postcode_suburb_state`, `bd_facebook`, `bd_twitter`, `bd_linkedin`, `bd_youtube`, `bd_android_app`, `bd_ios_app`, `bd_contact_address`, `bd_contact_telephone`, `bd_contact_mobile`, `bd_contact_email`, `bd_contact_website`, `bd_about_us`, `bd_map`, `bd_services`, `bd_unique_id`, `bd_status`) VALUES
(36, 8, '2018-09-09 05:20:03', 'AOZ Prints and Graphics', '3', '1215', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'Mascot Post Office 1215 Botany Road, Mascot NSW 2020', '02 9317 5263', '0401 086 391', 'info@aozprint.com.au', 'http://www.aozprint.com.au/', '<p>Bar + Grill is a WordPress theme designed to the exacting standards of modern restaurants &amp; &ldquo;local&rdquo; businesses.</p>\r\n', '', 'The theme comes bundled with several premium features, including: Theme Customizer: Unlimited Font & Color Options Design options for for white tablecloth restaurants, small pubs & everything in between The Revolution Slider Drag & Drop Content via Visual Composer Open Table Integration Foursquare Integration Social Media Integration Menu Templates Responsive Layouts (great for mobile devices) WooCommerce Integration (great for Gift Cards & Merchandise) Frontend Content Builder Frontend Theme Customizer AJAX Live Search (see results instantly, lowers your bounce rate!) SEO Supercharged Fast Page Loadtimes The Mythology Framework features pack', '520138', 'Published'),
(37, 8, '2018-09-09 05:38:32', 'GESS AUSTRALIA', '3', '2216 ', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'Rockdale Gardens, Iris Tower Shop 2, 5 Keats Ave Rockdale, NSW-2216', '02 8003 8006, 02 8355 5514, 02 8387 7701 02 8355 5517, 02 8355 5518', ' 0402 22 22 11 www.gessaustralia.com.au ', 'dummy@email.com', 'www.gessaustralia.com.au', '<p>Australia is still a young country and offers many opportunities for further education, employment &amp; Permanent Residency. The Australian education system is of a high standard and tertiary qualifications are recognized world-wide. Job opportunities are many and varied. To gain permanent employment in Australia will be of great benefit to you &amp; your family that provides even greater opportunities and security for the future of your children.</p>\r\n', '', 'GESS Australia will assist you with every step of the visa application process using cost effective way. We focus on providing high-quality service and customer satisfaction. We offer a range of services in relation to Visa Application, Review Tribunal and Citizenship matter.With a variety of offerings to choose from, we\'re sure you\'ll be happy to deal with us. Look around our website and if you have any questions , please feel free to contact us. We hope to see you again! Check back later for new updates to our website. There\'s much more to come! GESS Australia is a trusted firm that helps people with all types overseas student admissions, visa applications to DIBP and other immigration & citizenship related matters.', '9866963', 'Published'),
(38, 8, '2018-09-09 05:41:33', 'Subcontinental Travel', '3', '2216 ', 'http://www.baysidechamberofcommerce.com.au/', 'http://www.baysidechamberofcommerce.com.au/', 'http://www.baysidechamberofcommerce.com.au/', 'http://www.baysidechamberofcommerce.com.au/', 'http://www.baysidechamberofcommerce.com.au/', 'http://www.baysidechamberofcommerce.com.au/', 'Rockdale Gardens, Iris Tower Shop 2, 5 Keats Ave Rockdale, NSW-2216', '02 8003 8006, 02 8355 5514, 02 8387 7701 02 8355 5517, 02 8355 5518', '0402 22 22 11 ', 'dummy@email.com', 'www.subcontinentaltravel.com.au', '<p>Subcontinental Travel offers international travel service to our valued clients with cheaper flights to most countries in the world. Our Firm offers a full range of travel products such as international and domestic Travel Tickets, Hotel and car Booking and Travel insurance etc. We have the expertise of travel staff who can provide all our clients with a personalized, professional service, from initial enquiries through to final arrangements, with the best possible value for money.&nbsp;</p>\r\n', '', 'Our experienced travel service team specialises in providing travel products at discounted prices for passenger. We can also issue ticket from our office regardless of where you travel from, For example, if someone from Bangladesh would like to travel from Bangladesh to Australia, we can issue cheaper ticket for your relatives from Sydney. We deliver a friendly, reliable, quality service with a focus on the individual travellerâ€™s specific needs and expectations.', '2419883', 'Published'),
(39, 8, '2018-09-09 05:49:24', 'Voyager Accounting Services', '3', '2216 ', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'Voyager Accounting Services Room 2; Suite 2 5 Keats Avenue; ROCKDALE NSW 2216', '02 -8355 5514', '0401 359 427', 'quamrulkhan@voyageraccounting.com', 'www.voyageraccounting.com', '<p>Voyager Accounting Services is an accounting, business advisory and consulting firm, offering clients exceptional quality services with maintaining high professionalism. We are dedicated to providing our clients with excellent r professional, personalized services in the wide range of tax, accounting, financial, and business worlds.</p>\r\n', '', 'Tax and accounting done with the utmost knowledge, integrity, trust, passion, and security â€“ thatâ€™s what we are all about. We are also committed to forming close relationship with our clients, enabling us to understand your unique situation and customise the assistance we provide to suit your requirements.', '7888733', 'Published'),
(40, 8, '2018-09-09 05:52:04', 'Red Rose Function Center', '3', '2216', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', '96A Railway St, Rockdale, NSW 2216', 'Aziz: 0424 714 934 Sharif: 0434 197 785', '02 9567 1984', 'demo@demo.com', 'www.redrosefunctioncentre.com.au', '<p>We are a Function Venue of Hut-Bazaar Restaurant, which you can have for your almost any type of parties, starting from personal/ friends &amp; family programs to official conference. We offer a full package including food. Depending on your requirements, we also offer Balloon decorations, Cards, Posters, as well as full wedding stage setup. A dedicated, professional and friendly team of floor staff and function coordinators will ensure your amazing experience for your program in Red Rose Function Centre, giving you an absolute peace of mind. Just beside Red Rose, located our Restaurant, Hut-Bazaar, the famous and authentic place for Bangladeshi and sub-continental cuisine. Being established in 2007, Alhamdu-Lillah, We feel proud of being the first Bangladeshi restaurant in Sydney. Accordingly we ourselves provide the catering in the functions in this venue; so it becomes easier for us to make sure quality food is served. We would like to say- we, Hut-Bazaar, are the first to be a Gold Licence Caterer in the community and maintaining it since several years</p>\r\n', '', ' However, while we mostly offer Bangladeshi, Indian, Nepalese or Pakistani food in our venue, at the same time we have done programs with Lebanese, Thai or Chinese food. So, whatever you have in your mind about food, we are open to discuss! But we are not only doing programs in our venue. If you got your function booked in some other venues but prefer catering from us; we have that options as well. Depending on your requirements, we can offer both Delivery and On Site Service!', '8687304', 'Published'),
(41, 8, '2018-09-09 05:55:12', 'Awesome JK Cleaning Services', '3', '1217', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '0415 409 009 ', 'demo@demo.com', 'www.awesomejkcleaning services.com.au', '', '', 'We provide all types cleaning services.', '8056276', 'Published'),
(42, 8, '2018-09-09 06:10:20', 'Tax Assist Accountant', '3', '2216 ', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'Shop 2/466, Princes Highway, Rockdale, 2216', '02 9056 3396', '02 9056 3396', 'rockdale@taxassistau.com.au ', 'www.taxassistau.com.au/rockdale ', '<p>We use cookies for statistical analysis purposes. For more information please see our privacy policy Rockdale. TaxAssist Accountants Rockdale provides a wide range of accounting services including tax returns, annual accounts, payroll and bookkeeping. TaxAssist Accountants in Rockdale are a part of the largest network of accountants who offer a wealth of experience and expertise to over 3,630 small businesses right across Australia. We help a diverse range of small businesses including start-ups, sole traders, limited companies and landlords who operate in many industries. About us Sayedul Islam MPA, AIPA is a TaxAssist Accountant who offers clients a local, personal and friendly service. Sayedul has many years experience in small and medium businesses bookkeeping, payroll, taxation and BAS processes and requirements, reporting and operations.</p>\r\n', '', 'At TaxAssist Accountants Rockdale, clients benefit from the core accountancy services, plus other additional services which help clients build their business and gain good contacts locally. You will find us on 466 Princes Highway, Rockdale, and we are close to the traffic lights and opposite the Anglicare Shop and T-Bargain. We are a very short walk from Rockdale train station. TaxAssist Accountants Rockdale specialises in the following: An open and friendly approach â€“ we are local to you and happy to meet out of hours too. Up-front pricing â€“ we provide a fixed fee price, which can be payable monthly with no hidden surprises. Straightforward to understand â€“ we explain your business affairs clearly and avoid jargon. Save you money â€“ we ensure that on tax you pay the legal minimum and not a cent more. Perfect for small business â€“ we specialise in small businesses and individual tax payers, so you donâ€™t need a big accountant, just one thatâ€™s right for you. In summary, providing a high quality, fast and reliable service at a competitive price.', '5524290', 'Published'),
(43, 8, '2018-09-17 05:24:12', 'AOZ Prints and Graphics', '3', '2216 ', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.aozprint.com.au/', '<p>Bar + Grill is a WordPress theme designed to the exacting standards of modern restaurants &amp; &ldquo;local&rdquo; businesses.</p>\r\n', '', 'The theme comes bundled with several premium features, including: Theme Customizer: Unlimited Font & Color Options Design options for for white tablecloth restaurants, small pubs & everything in between The Revolution Slider Drag & Drop Content via Visual Composer Open Table Integration Foursquare Integration Social Media Integration Menu Templates Responsive Layouts (great for mobile devices) WooCommerce Integration (great for Gift Cards & Merchandise) Frontend Content Builder Frontend Theme Customizer AJAX Live Search (see results instantly, lowers your bounce rate!) SEO Supercharged Fast Page Loadtimes The Mythology Framework features pack', '981576', 'Published'),
(44, 8, '2018-09-17 05:29:43', 'Voyager Accounting Services', '3', '2216 ', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', '<p>Voyager Accounting Services is an accounting, business advisory and consulting firm, offering clients exceptional quality services with maintaining high professionalism. We are dedicated to providing our clients with excellent r professional, personalized services in the wide range of tax, accounting, financial, and business worlds.</p>\r\n', '', 'Tax and accounting done with the utmost knowledge, integrity, trust, passion, and security â€“ thatâ€™s what we are all about. We are also committed to forming close relationship with our clients, enabling us to understand your unique situation and customise the assistance we provide to suit your requirements.', '8469358', 'Published'),
(45, 8, '2018-09-17 05:32:17', 'We provide all types cleaning services.', '3', '1217', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'http://www.baysidechamberofcommerce.com.au', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.aozprint.com.au/', '<p>We provide all types cleaning services.</p>\r\n', '', 'We provide all types cleaning services.', '8760180', 'Published'),
(46, 8, '2018-09-17 05:34:18', 'Tax Assist Accountant', '3', '1217', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', '<p>We use cookies for statistical analysis purposes. For more information please see our privacy policy Rockdale. TaxAssist Accountants Rockdale provides a wide range of accounting services including tax returns, annual accounts, payroll and bookkeeping. TaxAssist Accountants in Rockdale are a part of the largest network of accountants who offer a wealth of experience and expertise to over 3,630 small businesses right across Australia. We help a diverse range of small businesses including start-ups, sole traders, limited companies and landlords who operate in many industries. About us Sayedul Islam MPA, AIPA is a TaxAssist Accountant who offers clients a local, personal and friendly service. Sayedul has many years experience in small and medium businesses bookkeeping, payroll, taxation and BAS processes and requirements, reporting and operations.</p>\r\n', '', 'At TaxAssist Accountants Rockdale, clients benefit from the core accountancy services, plus other additional services which help clients build their business and gain good contacts locally. You will find us on 466 Princes Highway, Rockdale, and we are close to the traffic lights and opposite the Anglicare Shop and T-Bargain. We are a very short walk from Rockdale train station. TaxAssist Accountants Rockdale specialises in the following: An open and friendly approach â€“ we are local to you and happy to meet out of hours too. Up-front pricing â€“ we provide a fixed fee price, which can be payable monthly with no hidden surprises. Straightforward to understand â€“ we explain your business affairs clearly and avoid jargon. Save you money â€“ we ensure that on tax you pay the legal minimum and not a cent more. Perfect for small business â€“ we specialise in small businesses and individual tax payers, so you donâ€™t need a big accountant, just one thatâ€™s right for you. In summary, providing a high quality, fast and reliable service at a competitive price.', '7438216', 'Published'),
(47, 8, '2018-09-17 05:41:07', 'Red Rose Function Center', '3', '2216', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', '<p>We are a Function Venue of Hut-Bazaar Restaurant, which you can have for your almost any type of parties, starting from personal/ friends &amp; family programs to official conference. We offer a full package including food. Depending on your requirements, we also offer Balloon decorations, Cards, Posters, as well as full wedding stage setup. A dedicated, professional and friendly team of floor staff and function coordinators will ensure your amazing experience for your program in Red Rose Function Centre, giving you an absolute peace of mind. Just beside Red Rose, located our Restaurant, Hut-Bazaar, the famous and authentic place for Bangladeshi and sub-continental cuisine. Being established in 2007, Alhamdu-Lillah, We feel proud of being the first Bangladeshi restaurant in Sydney. Accordingly we ourselves provide the catering in the functions in this venue; so it becomes easier for us to make sure quality food is served. We would like to say- we, Hut-Bazaar, are the first to be a Gold Licence Caterer in the community and maintaining it since several years</p>\r\n', '', 'However, while we mostly offer Bangladeshi, Indian, Nepalese or Pakistani food in our venue, at the same time we have done programs with Lebanese, Thai or Chinese food. So, whatever you have in your mind about food, we are open to discuss! But we are not only doing programs in our venue. If you got your function booked in some other venues but prefer catering from us; we have that options as well. Depending on your requirements, we can offer both Delivery and On Site Service!', '3832140', 'Published'),
(48, 15, '2018-09-20 07:38:22', 'TechSolutions BD', '', '1217', 'http://www.baysidechamberofcommerce.com.au/', 'http://www.baysidechamberofcommerce.com.au/', 'https://www.realcommercial.com.au/for-sale', 'http://www.baysidechamberofcommerce.com.au', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.aozprint.com.au/', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '3618917', 'Published'),
(49, 33, '2018-09-22 11:14:22', 'TechSolutions BD', '3', '1217', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'https://kallyas-template.net/', '<ul>\r\n	<li><a href=\"page-addlisting.php\">Add Directory</a></li>\r\n	<li><a href=\"page-addlisting.php\">Add Directory</a></li>\r\n</ul>\r\n ', '', 'sdddddddddddddddddddZfz', '1795405', 'Published'),
(50, 33, '2018-09-23 09:19:10', 'TechSolutions BD', '3', '1217', 'http://www.baysidechamberofcommerce.com.au', 'https://www.realcommercial.com.au/for-sale', 'fbfbfbfbfbfbfbfbfbfb', 'http://www.baysidechamberofcommerce.com.au', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '6708987', 'Published'),
(51, 33, '2018-09-23 09:48:08', 'TechSolutions BD', '', '1217', '', '', '', '', '', '', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '', 'ts.hosting2017@gmail.com', '', 'sdfffffffffff', '', 'fsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsd', '9539798', 'Published'),
(52, 34, '2018-09-23 10:34:29', 'TechSolutions BD', '', '1217', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au/', 'https://www.realcommercial.com.au/for-sale', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au/', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'https://kallyas-template.net/', 'ts.hosting2wwww017@gmail.com', '', 'ts.hosting2wwww017@gmail.com', '4302253', 'Published'),
(53, 33, '2018-09-23 10:55:04', 'TechSolutions BD', '', '1217', 'http://www.baysidechamberofcommerce.com.au', 'http://www.baysidechamberofcommerce.com.au/', 'https://www.realcommercial.com.au/for-sale', 'http://www.baysidechamberofcommerce.com.au', 'https://www.realcommercial.com.au/for-sale', '', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'https://kallyas-template.net/', 'ewraivnjfdhuigdgbjbvjhhfsdk;vdjk mvhfiudhcjkn fdm,xcjhnxbguyfgvchbdsjkxcyhfdsmcxnbfjgfyhdsgjbjhgujhsjknmnxzhjgxhbmdnkixihfdjbdhnxved', '', 'ewraivnjfdhuigdgbjbvjhhfsdk;vdjk mvhfiudhcjkn fdm,xcjhnxbguyfgvchbdsjkxcyhfdsmcxnbfjgfyhdsgjbjhgujhsjknmnxzhjgxhbmdnkixihfdjbdhnxved', '2690332', 'Published'),
(59, 33, '2018-09-24 09:14:40', 'TechSolutions BD', '', '1217', 'https://www.realcommercial.com.au/for-sale', '', '', '', '', '', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', 'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.', '', 'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.', '7687676', 'Published'),
(60, 33, '2018-09-24 09:15:19', 'TechSolutions BD', '', '1217', 'http://www.baysidechamberofcommerce.com.au/', '', '', '', '', '', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', 'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.', '', 'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.', '3471414', 'Published'),
(61, 34, '2018-09-26 07:44:11', 'TechSolutions BD', '5', '1217', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', '<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n\r\n<pre>\r\ncategory</pre>\r\n', '', 'categorycategorycategorycategorycategorycategorycategorycategorycategorycategorycategory', '6251959', 'Published'),
(62, 33, '2018-09-26 10:54:56', 'TechSolutions BDwerrrrrrrrrrrrrr', '5', '1217', 'http://www.baysidechamberofcommerce.com.au', '', 'https://www.realcommercial.com.au/for-sale', '', '', '', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', 'dfssssssssssssssssssssssssssfgvdcxfgvdc ', '', 'fedsXfcdsxdfsxdxc', '752251', 'Published'),
(63, 35, '2018-11-08 09:04:22', 'TechSolutions BD', '9', '1217', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', 'Hello there reaggggggggggggggggggg', '', 'Is there?gazzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzr', '4808051', 'Published'),
(64, 33, '2018-11-11 05:45:16', 'TechSolutions BD', '6', '1217', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '90.4007625', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '1951112', 'Published'),
(65, 33, '2018-11-11 05:55:24', 'TechSolutions BD', '6', '1217', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '90.4007625', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '4790589', 'Published'),
(66, 33, '2018-11-11 05:58:53', 'TechSolutions BD', '6', '1217', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '90.4007625', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '6217414', 'Published'),
(67, 33, '2018-11-11 05:59:36', 'TechSolutions BD', '6', '1217', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '90.4007625', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '1734809', 'Published'),
(68, 33, '2018-11-11 06:21:48', 'fddgfgfgf', '3', 'fggfgf', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'http://www.baysidechamberofcommerce.com.au', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'https://kallyas-template.net/', 'sdfffgfdgfdfdfgf', '90.4007625', 'fdfdfdfdggffgf', '1422402', 'Published'),
(69, 33, '2018-11-11 09:31:06', 'ashiq fardu', '3', '1217', 'https://www.realcommercial.com.au/for-sale', 'http://www.baysidechamberofcommerce.com.au', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'http://www.techsolutionsbd.com', 'rdaffffgsfdfd', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2171.462199057505!2d90.39792577804862!3d23.749178110727748!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8904c3f18ef%3A0x8a77ea03ab53614d!2sRed+Orchid+Restaurant+%26+Party+Center!5e0!3m2!1sen!2sbd!4v1541928951998\" width=\"400\" height=\"300\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'gfdgdfgfdfd', '4322877', 'Published'),
(70, 48, '2018-11-13 06:02:55', 'TechSolutions BD', '8', '1217', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', 'https://www.realcommercial.com.au/for-sale', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton', '01926677536', '1926677536', 'ts.hosting2017@gmail.com', 'www.techsolutonsbd.com', '<p>sxqwaesd</p>\r\n', 'https://www.realcommercial.com.au/for-sale', 'sddsadeswed', '9586076', 'Published');

-- --------------------------------------------------------

--
-- Table structure for table `business_directory_image`
--

CREATE TABLE `business_directory_image` (
  `id` int(11) NOT NULL,
  `bdi_user_id` int(11) NOT NULL,
  `bdi_bd_id` int(11) NOT NULL,
  `bdi_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business_directory_image`
--

INSERT INTO `business_directory_image` (`id`, `bdi_user_id`, `bdi_bd_id`, `bdi_image`) VALUES
(68, 8, 36, '1.jpg'),
(69, 8, 37, '2.jpg'),
(70, 8, 38, '3.JPG'),
(71, 8, 39, '4.png'),
(72, 8, 40, '5.png'),
(73, 8, 41, '6.jpg'),
(74, 8, 42, '7.jpg'),
(75, 8, 43, '1.jpg'),
(76, 8, 44, '7.jpg'),
(77, 8, 45, '6.jpg'),
(78, 8, 46, '2.jpg'),
(79, 8, 47, '5.png'),
(80, 15, 48, 'Dr. C. J. Hwang.jpg'),
(81, 15, 48, 'Dr. Chowdhury Mofizur Rahman.jpg'),
(82, 15, 48, 'Dr. Golam Mawla Chowdhury.JPG'),
(83, 15, 48, 'Dr. Jamilur Reza Chowdhury.jpg'),
(84, 33, 49, 'Dr. Golam Mawla Chowdhury.JPG'),
(85, 33, 50, 'branding-bangladesh2.jpg'),
(86, 33, 50, 'branding-bangladesh2.jpg'),
(87, 33, 50, 'branding-bangladesh2.jpg'),
(88, 33, 50, 'branding-bangladesh2.jpg'),
(89, 33, 50, 'branding-bangladesh2.jpg'),
(90, 33, 50, 'branding-bangladesh2.jpg'),
(91, 33, 51, 'c1.png'),
(92, 33, 51, 'c2.jpeg'),
(93, 33, 51, 'c3.jpg'),
(94, 33, 51, 'c5.jpg'),
(95, 33, 51, 'c6.jpg'),
(96, 34, 52, 'smartex.png'),
(97, 34, 52, 'techsolu.png'),
(98, 34, 52, 'test.jpg'),
(99, 34, 52, 'ubs.jpg'),
(100, 34, 52, 'unique.png'),
(101, 34, 52, 'upohar.png'),
(102, 34, 52, 'web.jpg'),
(103, 34, 52, 'webbanner_01.jpg'),
(104, 34, 52, 'webbanner_02.jpg'),
(105, 34, 52, 'webbanner_03.jpg'),
(106, 34, 52, 'wejute.png'),
(107, 33, 53, 'blogo.png'),
(108, 33, 53, 'common-email-envelope-mail-glyph-128.png'),
(109, 33, 53, 'daraz.png'),
(110, 33, 53, 'Discount-Vector.jpg'),
(111, 33, 59, '5.jpg'),
(112, 33, 59, 'about1.png'),
(113, 33, 59, 'bg.jpg'),
(114, 33, 60, 'Tech Solutions_Ad_Final.png'),
(115, 33, 60, 'ts-ad.jpg'),
(116, 33, 60, 'ts-ad-2018.png'),
(117, 34, 61, 'admin_logo.png'),
(118, 33, 62, 'explore-logo.png'),
(119, 33, 62, 'explore-logo.png'),
(120, 33, 62, 'explore-logo.png'),
(121, 35, 63, '5.jpg'),
(122, 35, 63, '5.jpg'),
(123, 35, 63, '5.jpg'),
(124, 35, 63, '5.jpg'),
(125, 35, 63, '5.jpg'),
(126, 33, 64, 'c2.jpeg'),
(127, 33, 64, 'c3.jpg'),
(128, 33, 64, 'c6.jpg'),
(129, 33, 65, 'c2.jpeg'),
(130, 33, 65, 'c3.jpg'),
(131, 33, 65, 'c6.jpg'),
(132, 33, 66, 'c2.jpeg'),
(133, 33, 66, 'c3.jpg'),
(134, 33, 66, 'c6.jpg'),
(135, 33, 67, 'c2.jpeg'),
(136, 33, 67, 'c3.jpg'),
(137, 33, 67, 'c6.jpg'),
(138, 33, 68, '30442635_1688573441221367_799216709964660736_n.jpg'),
(139, 33, 69, 'placebg.jpg'),
(140, 33, 69, 'shutterstock_176616410.jpg'),
(141, 33, 69, 'slider-img2.jpg'),
(142, 48, 70, 'bg.jpg'),
(143, 48, 70, 'slider_2.jpg'),
(144, 48, 70, 'slider1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `business_directory_video`
--

CREATE TABLE `business_directory_video` (
  `bdv_id` int(11) NOT NULL,
  `bdv_user_id` int(11) NOT NULL,
  `bdv_bd_id` int(11) NOT NULL,
  `bdv_video` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business_directory_video`
--

INSERT INTO `business_directory_video` (`bdv_id`, `bdv_user_id`, `bdv_bd_id`, `bdv_video`) VALUES
(35, 8, 36, 'http://www.baysidechamberofcommerce.com.au'),
(36, 8, 37, 'http://www.baysidechamberofcommerce.com.au'),
(37, 8, 38, 'http://www.baysidechamberofcommerce.com.au/'),
(38, 8, 39, 'http://www.baysidechamberofcommerce.com.au'),
(39, 8, 40, 'http://www.baysidechamberofcommerce.com.au'),
(40, 8, 41, 'http://www.baysidechamberofcommerce.com.au'),
(41, 8, 42, 'http://www.baysidechamberofcommerce.com.au'),
(42, 8, 43, 'https://www.youtube.com/watch?v=taRTlG4Omr0'),
(43, 8, 44, 'taRTlG4Omr0'),
(44, 8, 45, 'Dhaka'),
(45, 8, 46, 'Dhaka'),
(46, 8, 47, 'https://www.youtube.com/watch?v=taRTlG4Omr0'),
(47, 15, 48, 'Dhaka'),
(48, 33, 49, 'Dhaka'),
(49, 33, 50, 'http://www.baysidechamberofcommerce.com.au'),
(50, 33, 51, 'i0scW5lXAT8'),
(51, 34, 52, 'DO9U_XxN-Kc'),
(52, 33, 53, 'http://www.baysidechamberofcommerce.com.au'),
(53, 33, 54, ''),
(54, 33, 55, ''),
(55, 33, 56, ''),
(56, 33, 57, ''),
(57, 33, 58, ''),
(58, 33, 59, 'Dhaka'),
(59, 33, 60, 'http://www.baysidechamberofcommerce.com.au'),
(60, 34, 61, 'Dhaka'),
(61, 33, 62, 'Dhaka'),
(62, 35, 63, ''),
(63, 33, 64, ''),
(64, 33, 65, ''),
(65, 33, 66, ''),
(66, 33, 69, ''),
(67, 0, 70, 'Dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `business_organization`
--

CREATE TABLE `business_organization` (
  `b_o_id` int(11) NOT NULL,
  `b_o_c_id` varchar(255) NOT NULL,
  `b_o_name` varchar(255) NOT NULL,
  `b_o_short_info` text NOT NULL,
  `b_o_contact` text NOT NULL,
  `b_o_link` text NOT NULL,
  `b_o_image` text NOT NULL,
  `b_o_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_organization`
--

INSERT INTO `business_organization` (`b_o_id`, `b_o_c_id`, `b_o_name`, `b_o_short_info`, `b_o_contact`, `b_o_link`, `b_o_image`, `b_o_date`) VALUES
(1, '9', 'GESS AUSTRALIA', 'Australia is still a young country and offers many opportunities for further education, employment & Permanent Residency.  The Australian education system is of a high standard and tertiary qualifications are recognized world-wide.  Job opportunities are many and varied.  To gain permanent employment in Australia will be of great benefit to you & your family that provides even greater opportunities and security for the future of your children. GESS Australia will assist you with every step of the visa application process using cost effective way. We focus on providing high-quality service and customer satisfaction. We offer a range of services in relation to Visa Application, Review Tribunal and Citizenship matter.With a variety of offerings to choose from, we\\\'re sure you\\\'ll be happy to deal with us. Look around our website and if you have any questions , please feel free to contact us. We hope to see you again! Check back later for new updates to our website. There\\\'s much more to come! GESS Australia is a trusted firm that helps people with all types overseas student admissions, visa applications to DIBP and other immigration & citizenship related matters.', ' Street Address:\r\nRockdale Gardens, \r\nIris Tower \r\nShop 2, 5 Keats Ave\r\nRockdale, NSW-2216\r\n\r\nPhone: 02 8003 8006, 02 8355 5514, 02 8387 7701\r\n              02 8355 5517, 02 8355 5518\r\n\r\nMobile: 0402 22 22 11\r\nwww.gessaustralia.com.au', 'www.gessaustralia.com.au', 'Gess Logo.jpg', '2017-11-22 10:06:49'),
(2, '10', 'Subcontinental Travel', 'Subcontinental Travel offers international travel service to our valued clients with cheaper flights to most countries in the world.  Our Firm offers a full range of travel products such as international and domestic Travel Tickets, Hotel and car Booking and Travel insurance etc.  We have the expertise of travel staff who can provide all our clients with a personalized, professional service, from initial enquiries through to final arrangements, with the best possible value for money.  Our experienced travel service team specialises in providing travel products at discounted prices for passenger. We can also issue ticket from our office regardless of where you travel from, For example, if someone from Bangladesh would like to travel from Bangladesh to Australia, we can issue cheaper ticket for your relatives from Sydney. We deliver a friendly, reliable, quality service with a focus on the individual travellerâ€™s specific needs and expectations.', ' Street Address:\r\nRockdale Gardens, \r\nIris Tower \r\nShop 2, 5 Keats Ave\r\nRockdale, NSW-2216\r\n\r\nPhone: 02 8003 8006, 02 8355 5514, 02 8387 7701\r\n              02 8355 5517, 02 8355 5518\r\n\r\nMobile: 0402 22 22 11\r\nwww.subcontinentaltravel.com.au', 'www.subcontinentaltravel.com.au', 'Travel Logo.jpg', '2017-11-22 10:07:26'),
(3, '11', 'Voyager Accounting Services', 'Voyager Accounting Services is an accounting, business advisory and consulting firm, offering clients exceptional quality services with maintaining high professionalism. We are dedicated to providing our clients with excellent r professional, personalized services in the wide range of tax, accounting, financial, and business worlds. Tax and accounting done with the utmost knowledge, integrity, trust, passion, and security â€“ thatâ€™s what we are all about. We are also committed to forming close relationship with our clients, enabling us to understand your unique situation and customise the assistance we provide to suit your requirements. ', '\r\nVoyager Accounting Services\r\n\r\nRoom 2; Suite 2\r\n5 Keats Avenue; ROCKDALE\r\nNSW 2216\r\nTel: 02 -8355 5514\r\nM : 0401 359 427\r\nEmail : quamrulkhan@voyageraccounting.com or info@voyageraccouting.com', 'www.voyageraccounting.com', '1511179057Voyager.png', '2017-11-20 11:55:44'),
(4, '9', 'Internation Migration and Education Services', 'Introduction:\r\n\r\nInternational Migration & Education Services (IMES) Pty Ltd is a leading education consulting firm working with fame since 2009. We are recognized as a trustworthy partner to help our client. Having qualified education counselor team, IMES has been cooperating professionals and students to achieve their goals. We are dedicated to help onshore and offshore students to enroll different college and universities in Australia. Not only in studentship, but we also have the expertise to advise you in your career as an advantage. By proving such service with the efficiency we have enrolled more than 2900 students in different colleges and university in Australia since its insertion in the year of 2009. We have the client testimony with satisfaction.\r\n\r\n\r\nPreface:\r\n\r\nIMES attributes its success to people who constitute IMES and the ever-growing client base largely because of the referrals and strict adherence to ethical policies. It provides step-by-step guidance to its entire client base, not through standard steps rather defining each clientâ€™s individual requirements with pre and post departure arrangements all less than one roof. The center is dedicated to providing strategic advice and support in a professional way. IMES has the experience as well as the expertise to help you realize your dream and advice the client according to their career.\r\n\r\n\r\nBackground of IMES:\r\n\r\nInternational Migration & Education Services (IMES) was founded on May 24, 2015, by Mr. Soyab Ahmed. Previously, Mr. Soyab worked in student consultancy firm and he joined the firm in 2009. Since then, he has personally enrolled more than 2900 students in different colleges and universities. Since IMES establishes\r\n\r\nA. More than 700+ students have been enrolled in different colleges and universities in Australia.\r\n\r\nB. Migration assistance served more than 100+ clients.\r\n\r\nC. Enrolled Professional year courses are 60+\r\n\r\nD. RPL qualification arranged 150+\r\n\r\nE. Internship arranged 5+\r\n\r\nF. Other services 100+', 'http://imesaustralia.com.au/contactus', 'http://imesaustralia.com.au/', '1511321614IMES Logo.png', '2017-11-22 03:31:41'),
(5, '8', 'Red Rose Function Centre', 'We are a Function Venue of Hut-Bazaar Restaurant, which you can have for your almost any type of parties, starting from personal/ friends & family programs to official conference. We offer a full package including food. Depending on your requirements, we also offer Balloon decorations, Cards, Posters, as well as full wedding stage setup.\r\n\r\nA dedicated, professional and friendly team of floor staff and function coordinators will ensure your amazing experience for your program in Red Rose Function Centre, giving you an absolute peace of mind.\r\n\r\nJust beside Red Rose, located our Restaurant, Hut-Bazaar, the famous and authentic place for Bangladeshi and sub-continental cuisine. Being established in 2007, Alhamdu-Lillah, We feel proud of being the first Bangladeshi restaurant in Sydney.\r\n\r\nAccordingly we ourselves provide the catering in the functions in this venue; so it becomes easier for us to make sure quality food is served. We would like to say- we, Hut-Bazaar, are the first to be a Gold Licence Caterer in the community and maintaining it since several years!\r\n\r\nHowever, while we mostly offer Bangladeshi, Indian, Nepalese or Pakistani food in our venue, at the same time we have done programs with Lebanese, Thai or Chinese food. So, whatever you have in your mind about food, we are open to discuss!\r\n\r\nBut we are not only doing programs in our venue. If you got your function booked in some other venues but prefer catering from us; we have that options as well. Depending on your requirements, we can offer both Delivery and On Site Service!', 'Contact Us At ::\r\n\r\n96A Railway St, Rockdale, NSW 2216.    \r\nAziz: 0424 714 934\r\nSharif: 0434 197 785\r\nOffice: 02 9567 1984\r\n\r\nTo visit the venue and discuss, pls call us first to have an appointment.', 'www.redrosefunctioncentre.com.au', '1511342064Red Rose Logo.png', '2017-11-22 09:12:31'),
(6, '12', 'W Herrmann Real Estate', 'W. Herrmann Real Estate first opened their doors in 1969. From its inception, the directors were determined that the office would provide complete real estate service to the community.As a family run business, emphasis has always been on friendly, efficient service backed by professional expertise and industry experience. ', 'Address: 20 King St, Rockdale NSW 2216, Australia\r\nContact: (02) 9597 3655\r\nOpening Hours:\r\nMon  9:00 am â€“ 5:00 pm\r\nTue   9:00 am â€“ 5:00 pm\r\nWed  9:00 am â€“ 5:00 pm\r\nThu   9:00 am â€“ 5:00 pm\r\nFri    9:00 am â€“ 5:00 pm\r\nSat   Closed\r\nSun  Closed', 'http://www.wherrmann.com.au', 'W hermann logo.jpg', '2017-11-22 09:45:03'),
(7, '5', 'AOZ Prints and Graphics', 'Bar + Grill is a WordPress theme designed to the exacting standards of modern restaurants & â€œlocalâ€ businesses. The theme comes bundled with several premium features, including:\r\n\r\n    Theme Customizer: Unlimited Font & Color Options\r\n    Design options for for white tablecloth restaurants, small pubs & everything in between\r\n    The Revolution Slider\r\n    Drag & Drop Content via Visual Composer\r\n    Open Table Integration\r\n    Foursquare Integration\r\n    Social Media Integration\r\n    Menu Templates\r\n    Responsive Layouts (great for mobile devices)\r\n    WooCommerce Integration (great for Gift Cards & Merchandise)\r\n    Frontend Content Builder\r\n    Frontend Theme Customizer\r\n    AJAX Live Search (see results instantly, lowers your bounce rate!)\r\n    SEO Supercharged\r\n    Fast Page Loadtimes\r\n    The Mythology Framework features pack\r\n', 'AOZ PRINT & GRAPHICS\r\n\r\nWe are conveniently located at Mascot shops, only metres away from Mascot Post Office\r\n\r\n1215 Botany Road, Mascot NSW 2020\r\nTel: 02 9317 5263\r\nFax: 02 9317 5264\r\nMob: 0401 086 391\r\ninfo@aozprint.com.au\r\n\r\nHOURS OF OPERATION\r\nMon-Fri 9am - 6pm | Sat 10am - 4pm\r\nSunday & Public Holidays CLOSED\r\n', 'http://www.aozprint.com.au/', '1511344348AOz Prints.jpg', '2017-11-22 09:50:35'),
(8, '13', 'Awesome JK Cleaning Services', 'We provide all types cleaning services.', 'Contact: 0415 409 009', 'www.awesomejkcleaning services.com.au', '1511407259Cleaning Logo.jpg', '2017-11-23 03:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `business_resource`
--

CREATE TABLE `business_resource` (
  `b_r_id` int(11) NOT NULL,
  `b_r_title` varchar(255) NOT NULL,
  `b_r_content` text NOT NULL,
  `b_r_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_resource`
--

INSERT INTO `business_resource` (`b_r_id`, `b_r_title`, `b_r_content`, `b_r_date`) VALUES
(1, 'Starting a business', '<h2><strong>Are you planning to start a business?</strong></h2><p>For free advice contact business advisor from Bayside Chamber of Commerce 0402 22 22 11 &nbsp; <a href=\"\\\">http://www.baysidechamberofcommerce.com.au</a></p><p>Need an ABN (Australian Business Number)? Contact&nbsp;<a href=\"\\\">www.abr.gov.au</a></p><p>Need a TFN Tax File Number?&nbsp;<a href=\"\\\">www.ato.gov.au</a></p><p>Australian Securities &amp; Investment Commission (ASIC) &ndash; if you wish to form a company 1300 300 630&nbsp;<a href=\"\\\">www.asic.gov.au</a></p>', '2017-11-14 07:32:20'),
(2, 'Running your business', '<p><strong>Below are a few resources to help with running your business in theBayside business areas;</strong></p><p>ABN Lookup Link<br /><a href=\"http://abr.business.gov.au/\" target=\"_blank\">http://abr.business.gov.au/</a></p><p>Australian Consumer Laws<br /><a href=\"http://www.consumerlaw.gov.au/\" target=\"_blank\">http://www.consumerlaw.gov.au</a></p><p>Bayside Council Requirements<br />Ph: 02 9562 1666</p><p>Department of Fair Trading<br /><a href=\"http://www.fairtrading.nsw.gov.au/\" target=\"_blank\">http://www.fairtrading.nsw.gov.au</a></p><p>Employment Obligations<br />FWA/FWO Fair Work Australia (FWA)<br /><a href=\"http://www.fwa.gov.au/\" target=\"_blank\">http://www.fwa.gov.au</a>&nbsp;</p><p>Fair Work Information Statement<br /><a href=\"http://www.fairwork.gov.au/employment/employees/pages/fair-work-information-statement.aspx\">http://www.fairwork.gov.au/employment/employees/pages/fair-work-information-statement.aspx</a></p><p>Work, Health &amp; Safety (WHS) and Workers Compensation<br />(systems and accident notification, hazard identification)<br />Ph: 13 10 50<br /><a href=\"http://www.workcover.nsw.gov.au/\" target=\"_blank\">http://www.workcover.nsw.gov.au</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; .</p><p>Tax Tables links to the ATO&nbsp;<br /><a href=\"http://www.ato.gov.au/businesses/content/33283.htm?headline=Newwithholdingtaxtablesavailablehere&amp;segment=businesses\" target=\"_blank\">http://www.ato.gov.au/businesses/content/33283.htm?headline=Newwithholdingtaxtablesavailablehere&amp;segment=businesses</a></p><p>Small Business Support Line<br /><strong>1800 77 7275</strong><br />Monday - Friday | 8am - 8pm<br /><a href=\"http://www.ausindustry.gov.au/programs/small-business/sbsl/Pages/default.aspx\" target=\"_blank\">http://www.ausindustry.gov.au/programs/small-business/sbsl/Pages/default.aspx</a></p><p>Superannuation Information<br /><a href=\"http://www.ato.gov.au/individuals/pathway.aspx?pc=001/002/064&amp;alias=super\" target=\"_blank\">http://www.ato.gov.au/individuals/pathway.aspx?pc=001/002/064&amp;alias=super</a></p>', '2017-11-14 07:35:08'),
(3, 'Useful links', '<h3>BUSINESS LINKS</h3><p>Australian Government Business:&nbsp;<a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">http://www.business.gov.au/business-topics/Pages/default.aspx</a></p><p>Business Enterprise Centres: <a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">https://www.becaustralia.org.au</a></p><p>Australian Tax Office &ndash; Small Business:&nbsp;<a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">https://www.ato.gov.au/Business</a></p><p>Australian Tax Office &ndash; Super:&nbsp;<a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">https://www.ato.gov.au/super</a></p><p>NSW Business Chamber:&nbsp;<a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">http://www.nswbusinesschamber.com.au</a></p><p>Free information about Fair Work, health and safety compliance and workplace relations issues in Australia.</p><p><a href=\"\\\" target=\"\\&quot;_blank\\&quot;\">http://www.workplaceassured.com.au/</a></p>', '2017-11-14 07:39:58'),
(5, 'NSW Business Chamber', '<p>Bayside Chamber of Commerce has an Alliance Partner Membership with NSW Business Chamber and all&nbsp;our members can choose to take up the free associate membership package on offer worth over $1,000.&nbsp;<a href=\"http://www.nswbusinesschamber.com.au/Home\">http://www.nswbusinesschamber.com.au</a></p><p>Benefits for Regional Chamber Members are:</p><ul>	<li>Unlimited phone calls to Business Hotline 13 26 96 (information, support and advice for all your business questions)</li>	<li>3 phone calls to Workplace Advice Line 13 29 59 (workplace experts to help with industrial relations decisions)</li>	<li>Online access to &quot;Ask Us How&quot; business guides such as sales, marketing, business planning, WHS, people management and finance</li>	<li>One online business health check per year</li>	<li>Discount on networking and educational events at NSWBC</li>	<li>Quarterly Business Magazine and monthly e-news</li>	<li>Annual listing on the online business directory on NSWBC</li>	<li>Limited access to Business Legals Toolkit</li>	<li>Discount on business support tools and products</li>	<li>Free trial of Work Health Safety</li>	<li>Free trial of Workplaceinfo</li></ul>', '2017-11-14 07:36:25');

-- --------------------------------------------------------

--
-- Table structure for table `client_feedback`
--

CREATE TABLE `client_feedback` (
  `client_feedback_id` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_feedback_date` varchar(255) NOT NULL,
  `client_feedback_image` text NOT NULL,
  `client_feedback` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_feedback`
--

INSERT INTO `client_feedback` (`client_feedback_id`, `client_name`, `client_feedback_date`, `client_feedback_image`, `client_feedback`) VALUES
(2, 'Farid AHMED', '2017-11-27', '622EB8FE-9770-4B1B-A87B-6754542E5DEB.jpeg', 'Few years ago i joined Rockdale Chamber of Commerce as a member. I attended most of the meetings and functions organised by Chamber. i served previous Rockdale chamber of Commerce as a Vice President. This year our chamber name has been changed to Bayside Chamber of Commerce in line with council amalgamations. i am current elected Secretary of the chamber. It is a great opportunity and my previlege to represent such a fantastic business organization as being a long time local business person. urge all the business people in the bayside area should join the chamber and make it active.'),
(4, 'Ron ROYSTON OAM', '2017-11-29', '15118750259A631ECF-2A47-4A56-9637-EBDEE336493F.jpeg', 'Life Member, Canterbury District soccer and Football Association(CDSFA)\r\nLife Member, Former Secretary and President, Marrickville Red Davills Football Club.'),
(6, 'Michael NAGI Councillor', '2017-11-29', '151187553542C8660F-0C64-4A15-B032-5DAB37AE5075.jpeg', 'I am Michael NAGI, a current councillor of Bayside Council and former Councillor and Deputy Mayor of Rockdale Council. I am a local Business man and running my business in Arncliffee for many years.I am proud to be part of the Bayside Chamber of commerce that is representing the business people of the bayside communty in thr interst of the their business. I urge the local business people in Bayside area to join the chamber as i did.'),
(7, 'Dr. Anthony PUN OAM', '2017-11-29', '1511875619DE04F7C5-8F08-4398-8805-86BE15ADB13E.jpeg', 'Chairman, Multicultural Communities council of NSW'),
(8, 'Mahabub CHOWDHURY', '2017-11-29', '1511875948EBF194FF-C91A-4740-83EA-87FA7CD34CC9.jpeg', 'My name is Mahabub Chowdhury and i am current president of Bangladesh association of NSW. Our community organization has been serving the people of Bangladesh cummunity more than 32 years in Australia. I am very proud to support Bayside Chamber of Commerce that is running fantastically in the Bayside area and i am very proud to support the chamber.'),
(9, 'Tim CAHILL', '2017-11-29', '1511878025F3C8FA70-F5F8-415A-90B5-695192D16E4F.jpeg', 'Australian National Team Socceroos.'),
(10, 'Armando GARDIMAN', '2017-11-29', '151187920803EBEC5D-87A2-464C-B440-D3C8A373DC12.jpeg', 'Managing Partner, Turner Freeman Lawyer'),
(11, 'William WH SEUNG', '2017-11-29', '151196018452F89466-9F5B-4C0E-A136-4CD67C91786F.jpeg', 'My name is William W.H. Seung and I am currently Chairman of Korean Australian Community Support Inc., Vive Chair of Multicultural Communities Council of NSW, Honorary Member of National Unification Advisory Council Korea. I also served community since 1979  many ways as President of Korean Society of Sydney Australia, President Korean Chamber of Commerce & Industry in Australia, President of Korean Language Schools Association in Australia and Statutory Board Member of Anti-Discrimination Board NSW.\r\n\r\nI am very proud to support Bayside Chamber of Commerce and to encourage all members do their business successfully so that they could make this area is one of the best community in NSW.\r\n\r\nI am very much appreciate that you give me an opportunity to congratulate and support for your Chamber. \"\r\n\r\nWilliam Won-Hong Seung JP\r\n\r\nChairperson, Korean Australian Community Support Inc. \r\nVice Chairperson, Multicultural Communities Council, NSW Inc. \r\nHonorary Member, National Unification Advisory Council Korea \r\nFormer Statutory Board Member, Anti Discrimination Board NSW,\r\nFormer President, Korean Society of Sydney, Australia Inc. (å‰ í˜¸ì£¼ì‹œë“œë‹ˆí•œì¸íšŒ/íšŒìž¥)'),
(12, 'Dr. Thang HA', '2017-11-29', '1511960349B7E250DD-E73C-4632-8F38-A4F4A353C3F6.jpeg', 'President Vietnamese community Australia\r\nDirector, Multicultural communties council of NSW'),
(13, 'John MARCUS', '2017-11-30', '1511960629975EF748-2B2F-46E8-A955-3AC8A0780DA3.jpeg', 'Principal \r\nMARCUS LEGAL'),
(14, 'Nick ILOSKI', '2017-11-30', '1511960973AE56D714-B669-4AD0-9507-F89838DE7E61.png', 'Managing Director\r\nGlad GROUP');

-- --------------------------------------------------------

--
-- Table structure for table `condition_of_use`
--

CREATE TABLE `condition_of_use` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `condition_of_use`
--

INSERT INTO `condition_of_use` (`id`, `title`, `details`) VALUES
(2, 'TechSolutions BD1111aa', '<h3>1914 translation by H. Rackham</h3>\r\n\r\n<p>&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;</p>\r\n\r\n<h3>Section 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC</h3>\r\n\r\n<p>&quot;At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.&quot;</p>\r\n\r\n<h3>1914 translation by H. Rackham</h3>\r\n\r\n<p>&quot;On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.&quot;</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `contact_us_id` int(11) NOT NULL,
  `contact_us_address` text NOT NULL,
  `contact_us_phone` varchar(255) NOT NULL,
  `contact_us_email` varchar(255) NOT NULL,
  `contact_us_website` varchar(255) NOT NULL,
  `contact_us_info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`contact_us_id`, `contact_us_address`, `contact_us_phone`, `contact_us_email`, `contact_us_website`, `contact_us_info`) VALUES
(1, 'Rockdale Gardens Iris Tower, \r\nShop 2, 5 Keats ave, \r\nRockdale, NSW-2216 \r\n\r\nAustralia Postal Address: \r\nPO Box 140, Rockdale, NSW-2216  ', 'Office: 02-8003 8006, 02-83555514, 02-83555517, 02-83555518 Mobile: 0402 22 22 11', 'secretary@baysidechamberofcommerce.com.au', 'http://chamber.techsolutions-global.com/', 'We are about 2 minutes walk from Rockdale railway station and 2 minutes walk from the bus stop at the corner of Princes HWY and Bay Street. ');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_type` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `event_summary` text NOT NULL,
  `event_details` text NOT NULL,
  `event_image` text NOT NULL,
  `event_location` varchar(255) NOT NULL,
  `event_creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_title`, `event_type`, `start_date`, `end_date`, `event_summary`, `event_details`, `event_image`, `event_location`, `event_creation_date`) VALUES
(2, 'Test2 22', 'Regular 22', '2017-10-25', '2017-10-28', 'Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2 Test2  22', '<p>Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp;Test2&nbsp; 22</p>', 'event3.jpg', 'Barisal 22', '2017-10-03 09:14:23');

-- --------------------------------------------------------

--
-- Table structure for table `interview`
--

CREATE TABLE `interview` (
  `in_id` int(11) NOT NULL,
  `in_title` varchar(255) NOT NULL,
  `in_person` varchar(255) NOT NULL,
  `in_details` text NOT NULL,
  `in_image` text NOT NULL,
  `in_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interview`
--

INSERT INTO `interview` (`in_id`, `in_title`, `in_person`, `in_details`, `in_image`, `in_date`) VALUES
(3, 'Interview Demo 1', 'One Specific Person Name', '<p><strong>Question 1: Why Do You Want To Work For Us?</strong><br /><br />It&rsquo;s rare for an interview not to include this question.<br />The good news is that it&rsquo;s an easy one to prepare for.<br />Most companies want to recruit people who are enthusiastic about the company and its products. They don&rsquo;t want people on the team who &ldquo;ended up there by accident&rdquo;. So this is your chance to show why working for the company is important to you and why you think you will fit in.<br /><br /><strong>Question 2: What interests you about this job?</strong><br /><br />When you&#39;re asked what interests you about the position you are interviewing for, the best way to respond is to describe the qualifications listed in the job posting, then connect them to your skills and experience. That way, the employer will see that you know about the job you&#39;re interviewing for (not everyone does) and that you have the qualifications necessary to do the job.&nbsp;<br /><br /><strong>Question 3: What do you know about Our Company?</strong><br /><br />A typical job interview question, asked to find out how much company research you have conducted, is &quot;What do you know about this company?&quot;&nbsp;<br />Prepare in advance, and in a word, research, so, you can provide relevant and current information about your prospective employer to the interviewer. Start by researching the company online. Review the &quot;About Us&quot; section of the company web site. Google the company, read blogs that mention it, and check.<br /><br /><strong>Question 4: What challenges are you looking for in this position?</strong><br /><br />A typical interview question to determine what you are looking for your in next job, and whether you would be a good fit for the position being hired for, is &quot;What challenges are you looking for in a position?&quot;&nbsp;<br /><br /><strong>Question 5: Who was your best boss and who was the worst?</strong><br /><br />I&#39;ve learned from each boss I&#39;ve had. From the good ones I learnt what to do, from the challenging ones - what not to do.&nbsp;<br />Early in my career, I had a mentor who helped me a great deal, we still stay in touch. I&#39;ve honestly learned something from each boss I&#39;ve had.&nbsp;<br /><br /><br /><strong>Question 6: What have you been doing since your last job?</strong><br /><br />If you have an employment gap on your resume, the interviewer will probably ask you what you have been doing while you were out of work.&nbsp;<br />The best way to answer this question is to be honest, but do have an answer prepared. You will want to let the interviewer know that you were busy and active, regardless of whether you were out of work by choice, or otherwise.&nbsp;<br /><br /><strong>Question 7: Why did you choose this particular career path?</strong><br /><br />Sometimes in interviews, you will be asked questions that lend themselves to be answered vaguely or with lengthy explanations. Take this opportunity to direct your answer in a way that connects you with the position and company, be succinct and support your answer with appropriate specific examples.<br /><br /><strong>Question 8: What are your aspirations beyond this job?</strong><br /><br />Again, don&#39;t fall into the trap of specifying job titles. Stick to a natural progression you see as plausible. How should this job grow for the good of the organization? Then turn your attention once again to the job at hand. If you seem too interested in what lies beyond this job, the interviewer will fear that you won&#39;t stick around for long.<br /><br /><strong>Question 9: Why do you think this industry would sustain your interest in the long haul?</strong><br /><br />What expectations or projects do you have for the business that would enable you to grow without necessarily advancing? What excites you about the business? What proof can you offer that your interest has already come from a deep curiosity-perhaps going back at least a few years-rather than a current whim you&#39;ll outgrow?<br /><br /><strong>Question 10: Tell me about yourself?</strong><br /><br />This is not an invitation to ramble on. If the context isn&#39;t clear, you need to know more about the question before giving an answer. In such a situation, you could ask, &quot;Is there a particular aspect of my background that you would like more information on?&quot; This will enable the interviewer to help you find the appropriate focus and avoid discussing irrelevancies.</p>', '1509342076information-interview.png', '2017-10-30 08:20:21'),
(4, 'Interview Demo 2', 'One Specific Person Name', '<p><strong>Question 1: Why Do You Want To Work For Us?</strong><br /><br />It&rsquo;s rare for an interview not to include this question.<br />The good news is that it&rsquo;s an easy one to prepare for.<br />Most companies want to recruit people who are enthusiastic about the company and its products. They don&rsquo;t want people on the team who &ldquo;ended up there by accident&rdquo;. So this is your chance to show why working for the company is important to you and why you think you will fit in.<br /><br /><strong>Question 2: What interests you about this job?</strong><br /><br />When you&#39;re asked what interests you about the position you are interviewing for, the best way to respond is to describe the qualifications listed in the job posting, then connect them to your skills and experience. That way, the employer will see that you know about the job you&#39;re interviewing for (not everyone does) and that you have the qualifications necessary to do the job.&nbsp;<br /><br /><strong>Question 3: What do you know about Our Company?</strong><br /><br />A typical job interview question, asked to find out how much company research you have conducted, is &quot;What do you know about this company?&quot;&nbsp;<br />Prepare in advance, and in a word, research, so, you can provide relevant and current information about your prospective employer to the interviewer. Start by researching the company online. Review the &quot;About Us&quot; section of the company web site. Google the company, read blogs that mention it, and check.<br /><br /><strong>Question 4: What challenges are you looking for in this position?</strong><br /><br />A typical interview question to determine what you are looking for your in next job, and whether you would be a good fit for the position being hired for, is &quot;What challenges are you looking for in a position?&quot;&nbsp;<br /><br /><strong>Question 5: Who was your best boss and who was the worst?</strong><br /><br />I&#39;ve learned from each boss I&#39;ve had. From the good ones I learnt what to do, from the challenging ones - what not to do.&nbsp;<br />Early in my career, I had a mentor who helped me a great deal, we still stay in touch. I&#39;ve honestly learned something from each boss I&#39;ve had.&nbsp;<br /><br /><br /><strong>Question 6: What have you been doing since your last job?</strong><br /><br />If you have an employment gap on your resume, the interviewer will probably ask you what you have been doing while you were out of work.&nbsp;<br />The best way to answer this question is to be honest, but do have an answer prepared. You will want to let the interviewer know that you were busy and active, regardless of whether you were out of work by choice, or otherwise.&nbsp;<br /><br /><strong>Question 7: Why did you choose this particular career path?</strong><br /><br />Sometimes in interviews, you will be asked questions that lend themselves to be answered vaguely or with lengthy explanations. Take this opportunity to direct your answer in a way that connects you with the position and company, be succinct and support your answer with appropriate specific examples.<br /><br /><strong>Question 8: What are your aspirations beyond this job?</strong><br /><br />Again, don&#39;t fall into the trap of specifying job titles. Stick to a natural progression you see as plausible. How should this job grow for the good of the organization? Then turn your attention once again to the job at hand. If you seem too interested in what lies beyond this job, the interviewer will fear that you won&#39;t stick around for long.<br /><br /><strong>Question 9: Why do you think this industry would sustain your interest in the long haul?</strong><br /><br />What expectations or projects do you have for the business that would enable you to grow without necessarily advancing? What excites you about the business? What proof can you offer that your interest has already come from a deep curiosity-perhaps going back at least a few years-rather than a current whim you&#39;ll outgrow?<br /><br /><strong>Question 10: Tell me about yourself?</strong><br /><br />This is not an invitation to ramble on. If the context isn&#39;t clear, you need to know more about the question before giving an answer. In such a situation, you could ask, &quot;Is there a particular aspect of my background that you would like more information on?&quot; This will enable the interviewer to help you find the appropriate focus and avoid discussing irrelevancies.</p>', '1509351831iStock_64702199_MEDIUM-778x518.jpg', '2017-10-30 08:22:40'),
(5, 'Interview Demo 3', 'One Specific Person Name', '<p><strong>Question 1: Why Do You Want To Work For Us?</strong><br />\r\n<br />\r\nIt&rsquo;s rare for an interview not to include this question.<br />\r\nThe good news is that it&rsquo;s an easy one to prepare for.<br />\r\nMost companies want to recruit people who are enthusiastic about the company and its products. They don&rsquo;t want people on the team who &ldquo;ended up there by accident&rdquo;. So this is your chance to show why working for the company is important to you and why you think you will fit in.<br />\r\n<br />\r\n<strong>Question 2: What interests you about this job?</strong><br />\r\n<br />\r\nWhen you&#39;re asked what interests you about the position you are interviewing for, the best way to respond is to describe the qualifications listed in the job posting, then connect them to your skills and experience. That way, the employer will see that you know about the job you&#39;re interviewing for (not everyone does) and that you have the qualifications necessary to do the job.&nbsp;<br />\r\n<br />\r\n<strong>Question 3: What do you know about Our Company?</strong><br />\r\n<br />\r\nA typical job interview question, asked to find out how much company research you have conducted, is &quot;What do you know about this company?&quot;&nbsp;<br />\r\nPrepare in advance, and in a word, research, so, you can provide relevant and current information about your prospective employer to the interviewer. Start by researching the company online. Review the &quot;About Us&quot; section of the company web site. Google the company, read blogs that mention it, and check.<br />\r\n<br />\r\n<strong>Question 4: What challenges are you looking for in this position?</strong><br />\r\n<br />\r\nA typical interview question to determine what you are looking for your in next job, and whether you would be a good fit for the position being hired for, is &quot;What challenges are you looking for in a position?&quot;&nbsp;<br />\r\n<br />\r\n<strong>Question 5: Who was your best boss and who was the worst?</strong><br />\r\n<br />\r\nI&#39;ve learned from each boss I&#39;ve had. From the good ones I learnt what to do, from the challenging ones - what not to do.&nbsp;<br />\r\nEarly in my career, I had a mentor who helped me a great deal, we still stay in touch. I&#39;ve honestly learned something from each boss I&#39;ve had.&nbsp;<br />\r\n<br />\r\n<br />\r\n<strong>Question 6: What have you been doing since your last job?</strong><br />\r\n<br />\r\nIf you have an employment gap on your resume, the interviewer will probably ask you what you have been doing while you were out of work.&nbsp;<br />\r\nThe best way to answer this question is to be honest, but do have an answer prepared. You will want to let the interviewer know that you were busy and active, regardless of whether you were out of work by choice, or otherwise.&nbsp;<br />\r\n<br />\r\n<strong>Question 7: Why did you choose this particular career path?</strong><br />\r\n<br />\r\nSometimes in interviews, you will be asked questions that lend themselves to be answered vaguely or with lengthy explanations. Take this opportunity to direct your answer in a way that connects you with the position and company, be succinct and support your answer with appropriate specific examples.<br />\r\n<br />\r\n<strong>Question 8: What are your aspirations beyond this job?</strong><br />\r\n<br />\r\nAgain, don&#39;t fall into the trap of specifying job titles. Stick to a natural progression you see as plausible. How should this job grow for the good of the organization? Then turn your attention once again to the job at hand. If you seem too interested in what lies beyond this job, the interviewer will fear that you won&#39;t stick around for long.<br />\r\n<br />\r\n<strong>Question 9: Why do you think this industry would sustain your interest in the long haul?</strong><br />\r\n<br />\r\nWhat expectations or projects do you have for the business that would enable you to grow without necessarily advancing? What excites you about the business? What proof can you offer that your interest has already come from a deep curiosity-perhaps going back at least a few years-rather than a current whim you&#39;ll outgrow?<br />\r\n<br />\r\n<strong>Question 10: Tell me about yourself?</strong><br />\r\n<br />\r\nThis is not an invitation to ramble on. If the context isn&#39;t clear, you need to know more about the question before giving an answer. In such a situation, you could ask, &quot;Is there a particular aspect of my background that you would like more information on?&quot; This will enable the interviewer to help you find the appropriate focus and avoid discussing irrelevancies.</p>\r\n', '1509351865Credible-Coaching.jpg', '2017-10-30 08:22:31');

-- --------------------------------------------------------

--
-- Table structure for table `join_now_content`
--

CREATE TABLE `join_now_content` (
  `join_now_content_id` int(11) NOT NULL,
  `join_now_title` varchar(255) NOT NULL,
  `join_now_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `join_now_content`
--

INSERT INTO `join_now_content` (`join_now_content_id`, `join_now_title`, `join_now_content`) VALUES
(1, 'Why join the Bayside Chamber of Commerce?', '<ol>	<li><strong>Business&nbsp;Promotion.</strong><br />	Chamber membership offers you many opportunities to promote your business including our monthly newsletter, social media, event attendance and sponsorship, online advertising and SO much more. &nbsp;</li>	<li><strong>Networking Opportunities.</strong><br />	Connect to the Bayside&#39;s foremost business network of a number of members working together to improve the business and economic prosperity of the region. Meet local business owners and decision-makers, and develop valuable new business relationships.</li>	<li><strong>Advocacy opportunities.</strong><br />	The Chamber serves as a strong voice in both local and state government in association with NSW Business Chamber, collectively representing members on issues that impact businesses. &nbsp;The Chamber focuses on policy areas that are critical to the economic vitality of the Bayside area. <strong>&nbsp;</strong></li>	<li><strong>Community</strong>.Heighten your knowledge of how the community is moving ahead by reading the Chamber Business News and our website blogs. Business &amp; Community workshop allow you to connect and network with members from the community!</li>	<li><strong>Referrals</strong><br />	&nbsp;As the Bayside chamber of Commerce, we advocate for the success and advancement of all member businesses; therefore, it is our policy to primarily provide referrals to members. &nbsp;Your involvement in Chamber membership demonstrates&nbsp; to other businesses and to potential customers that you are committed to supporting the local economy. &nbsp;</li>	<li><strong>Members-Only Discounts.</strong><br />	Members can take advantage of many cost-saving promotional opportunities and discounts offered by other members.</li>	<li><strong>Seminar and Workshop.</strong><br />	Our Chamber offers a wide range of professional development and informational seminars in association with Local Council and NSW chamber of Commerce throughout the year that is beneficial to our members. The programs are designed to help your business needs. &nbsp;</li>	<li><strong>Build Credibility and Growth.</strong>&nbsp;<br />	Demonstrate your commitment to the community by becoming a part of the vision and strategies of the most influential business organization in the Bayside area. . As a member of Bayside Chamber of commerce, you have the opportunity to join various committees in your Local Council so you may have a voice, make a difference and make connections.Your investment in the Chamber helps us to promote and strengthen Bayside business community. When the Chamber assists Businesses that are moving here or expanding their businesses, it means more jobs and more customers for you.</li></ol><p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9. Leadership Opportunities.</strong><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Members determine the goals and objectives of the Chamber by participating meeting that support their business and professional goals. These involvements enable members to take active leadership roles in the Chamber and in the business</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; community.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `join_now_info`
--

CREATE TABLE `join_now_info` (
  `join_now_info_id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `m_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `abn_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `business_address` text NOT NULL,
  `postal_address` text NOT NULL,
  `office_number` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `join_now_info`
--

INSERT INTO `join_now_info` (`join_now_info_id`, `f_name`, `m_name`, `l_name`, `business_name`, `abn_number`, `email`, `business_address`, `postal_address`, `office_number`, `mobile_number`, `website`, `profession`, `join_date`) VALUES
(1, 'Arifur', 'Rahman', 'Raju', 'TechSolutions', '112233', 'ariftechsolutions@gmail.com', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton, Dhaka', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton, Dhaka', '01926677534', '01715799134', 'http://www.techsolutionsbd.com', 'Programmer', '2017-10-23 07:56:07'),
(3, 'Tanvir ', 'Ahmed', 'Shaikat', 'TechSolutions', '110022', 'ta.shaikat@gmail.com', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton, Dhaka.', '404, Golam Rasul Plaza, 1st Floor, A-4, Dilu Road, New Eskaton, Dhaka.', '01911111', '01811111', 'http://www.techsolutionsbd.com', 'Programmer', '2017-10-23 11:51:18'),
(4, 'Nuruzzaman ', '', 'Lucky', 'Stark Mobile', '009988', 'nuruzzamanlucky@gmail.com', 'Panthopath, Dhaka.', 'Shyamoli, Dhaka.', '0191111111', '017155555', 'http://lucky.com', 'Designer', '2017-10-25 11:59:14'),
(5, 'Nuruzzaman ', 'Ahmed', 'Lucky', 'Stark Mobile', '11223344', 'lucky@gmail.com', 'Rockdale Gardens Iris Tower, Shop 2, 5 Keats ave, ', 'Rockdale Gardens Iris Tower, Shop 2, 5 Keats ave, 2', '112200', '112299', 'http://lucky.com', 'Designer', '2017-11-09 12:00:41'),
(6, 'Nuruzzaman ', 'Ahmed', 'Lucky', 'Stark Mobile', '11223344', 'lucky@gmail.com', 'Rockdale Gardens Iris Tower, Shop 2, 5 Keats ave, ', 'Rockdale Gardens Iris Tower, Shop 2, 5 Keats ave, 2', '112200', '112299', 'http://lucky.com', 'Designer', '2017-11-09 12:02:48'),
(7, 'Farid', '', 'Ahmed', 'Gess', '17449700286', 'farid_farida@hotmail.com', '2 keats ave ', 'As above', '', '0402222211', '', '', '2017-11-12 10:51:39');

-- --------------------------------------------------------

--
-- Table structure for table `members_benifit`
--

CREATE TABLE `members_benifit` (
  `members_benifit_id` int(11) NOT NULL,
  `members_benifit_title` varchar(255) NOT NULL,
  `members_benifit_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `members_benifit`
--

INSERT INTO `members_benifit` (`members_benifit_id`, `members_benifit_title`, `members_benifit_content`) VALUES
(1, 'Membership & Benefits', '<h3><strong>Why Join the Bayside Chamber of Commerce ? </strong></h3><ul>	<li><strong>Business&nbsp;Promotion.</strong><br />	Chamber membership offers you many opportunities to promote your business including our monthly newsletter, social media, event attendance and sponsorship, online advertising and SO much more. &nbsp;</li>	<li><strong>Networking Opportunities.</strong><br />	Connect to the Bayside&#39;s foremost business network of a number of members working together to improve the business and economic prosperity of the region. Meet local business owners and decision-makers, and develop valuable new business relationships.</li>	<li><strong>Advocacy opportunities.</strong><br />	The Chamber serves as a strong voice in both local and state government in association with NSW Business Chamber, collectively representing members on issues that impact businesses. &nbsp;The Chamber focuses on policy areas that are critical to the economic vitality of the Bayside area. <strong>&nbsp;</strong></li>	<li><strong>Community</strong>.Heighten your knowledge of how the community is moving ahead by reading the Chamber Business News and our website blogs. Business &amp; Community workshop allow you to connect and network with members from the community!</li>	<li><strong>Referrals</strong><br />	&nbsp;As the Bayside chamber of Commerce, we advocate for the success and advancement of all member businesses; therefore, it is our policy to primarily provide referrals to members. &nbsp;Your involvement in Chamber membership demonstrates&nbsp; to other businesses and to potential customers that you are committed to supporting the local economy. &nbsp;</li>	<li><strong>Members-Only Discounts.</strong><br />	Members can take advantage of many cost-saving promotional opportunities and discounts offered by other members.</li>	<li><strong>Seminar and Workshop.</strong><br />	Our Chamber offers a wide range of professional development and informational seminars in association with Local Council and NSW chamber of Commerce throughout the year that is beneficial to our members. The programs are designed to help your business needs. &nbsp;</li>	<li><strong>Build Credibility and Growth.</strong>&nbsp;<br />	Demonstrate your commitment to the community by becoming a part of the vision and strategies of the most influential business organization in the Bayside area. . As a member of Bayside Chamber of commerce, you have the opportunity to join various committees in your Local Council so you may have a voice, make a difference and make connections.Your investment in the Chamber helps us to promote and strengthen Bayside business community. When the Chamber assists Businesses that are moving here or expanding their businesses, it means more jobs and more customers for you.</li>	<li><strong>9. Leadership Opportunities.</strong> Members determine the goals and objectives of the Chamber by participating meeting that support their business and professional goals. These involvements enable members to take active leadership roles in the Chamber and in the busines community.</li></ul><h3><strong>Additional Benefits</strong></h3><ul>	<li><a href=\"http://businesschamber.com.au/Business-Savers-Program\">NSW Business Chamber Benefits</a></li></ul>');

-- --------------------------------------------------------

--
-- Table structure for table `member_discount`
--

CREATE TABLE `member_discount` (
  `id` int(11) NOT NULL,
  `bd_id` int(11) NOT NULL,
  `discount` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member_discount`
--

INSERT INTO `member_discount` (`id`, `bd_id`, `discount`) VALUES
(2, 37, '14'),
(6, 52, '52'),
(7, 41, '15'),
(8, 44, '30'),
(9, 50, '52'),
(10, 60, '12');

-- --------------------------------------------------------

--
-- Table structure for table `member_to_member`
--

CREATE TABLE `member_to_member` (
  `member_to_member_id` int(11) NOT NULL,
  `member_to_member_category_id` varchar(255) NOT NULL,
  `member_to_member_info` text NOT NULL,
  `member_to_member_contact` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_to_member`
--

INSERT INTO `member_to_member` (`member_to_member_id`, `member_to_member_category_id`, `member_to_member_info`, `member_to_member_contact`, `date`) VALUES
(2, '1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '+61 111222333', '2017-10-24 07:04:28'),
(4, '2', '<p>20% Discount for members for business and organisational development strategies and projects,</p>\r\n', '0428104482', '2017-10-24 07:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `subject` text NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `subject`, `details`) VALUES
(2, 'Techsolutions BD', 'ts.hosting2017@gmail.com', 'sddsasfdsfd', '');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_title` varchar(255) NOT NULL,
  `news_author` varchar(255) NOT NULL,
  `news_content` text NOT NULL,
  `news_image` text NOT NULL,
  `news_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_title`, `news_author`, `news_content`, `news_image`, `news_date`) VALUES
(3, 'Campaign.', '', '', '1508925904pexels-photo-341858.jpeg', '2017-10-25'),
(4, 'Seminar', 'Farid AHMED', '', 'konf.jpg', '2017-10-30'),
(5, 'Presentation ', 'Bayside Chamber of Commerce', '', 'VCM_events_live_stream_hero.png', '2017-10-29'),
(6, 'Christmas Party 2017', 'Bayside Chamber of Commerce', '<p>Our Christmas Party will be held at Red Rose Function Centre,96A Railway Street, Rockdale,NSW-2216 at 6.30PM.</p><p>&nbsp;</p>', '1512397176B0FBB119-6A22-43C3-AC64-522459F22ED5.jpeg', '2017-12-05');

-- --------------------------------------------------------

--
-- Table structure for table `our_board`
--

CREATE TABLE `our_board` (
  `member_id` int(11) NOT NULL,
  `member_name` varchar(255) NOT NULL,
  `member_designation` varchar(255) NOT NULL,
  `member_image` text NOT NULL,
  `member_details` text NOT NULL,
  `member_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `our_board`
--

INSERT INTO `our_board` (`member_id`, `member_name`, `member_designation`, `member_image`, `member_details`, `member_date`) VALUES
(16, 'Quamrul Islam KHAN', 'President', 'Quamrul.jpeg', '<p>My Name is Quamrul Islam KHAN and I am a current President of Bayside Chamber of commerce. Firstly I would like to thank all members who supported me to be elected as a President.</p><p>Currently I have been running my own business in the Bayside area as a Tax Consultant with Australian Taxation Office. I have also worked as a Manager Accounts with H&amp;R Blocks and Tax Accountant at Nextzen Accounting Services as a Tax Consultant. In addition to working as a consultant in the Taxation area, I have also worked as an accounting Lecturer in a Vocational educational institute in Sydney.</p><p>I migrated to Australia from Bangladesh as a skilled migrant with my family. Before coming to Australia, I worked in Beximco group as a senior corporate manager, Yearly turnover of the company is equivalent to AUD $10 Billion.</p><p>I have completed Master of Commerce and Master of Business Administration from Bangladesh and after migrated to Australia, I completed Advanced Diploma in Accounting from St. George TAFE.</p><p>&nbsp;</p><p>My professional membership includes the following: IPA Australia: MIPA Membership</p><p>CPA Australia: An Associate Member of CPA Australia</p><p>IFA (Institute of Financial Accountants) , UK : AFA and</p><p>also i am a JP (Justice of the peace).</p><p>As being a business professional many years, I will try my best to help other business professionals in the bayside area in my capacity as you know that we are a not for profit organization and everyone is very busy in their own profession to run the family.</p><p>&nbsp;</p>', '2017-12-06 02:29:50'),
(17, 'Patrick MEDWAY', 'Vice President', 'Patrick.JPG', '', '2017-11-14 07:54:09'),
(18, 'Farid AHMED', 'Secretary', '1510644207Farid AHmed.jpg', '<p>My name is Farid AHMED and I am the owner of GESS Australia- advising clients in the area of Australian Immigration &amp; Citizenship Law and Sub continental Travel- Selling domestic &amp; International Air Tickets and All types of Travel Insurance. I have been working in the Bayside Area for more than a decade.I have been serving Australian communities in the areas of Business, Social, Sports, Cultural and other areas in Australia, in various capacities for more 15 years.&nbsp;</p>\r\n\r\n<p>My aim is to support and assist the diverse business community in the Bayside council area and work closely with business owners regardless of the size of the business. I believe that an active involvement in the leadership of the business activities of the Bayside Business community is one of the key ways for revitalizing businesses in the LGA and the close connection to the people of the community.</p>\r\n\r\n<p>I completed Bachelor &amp; Master Degree, (MBA) from Bangladesh, I came to Australia to undertake another post graduate degree &amp; completed Master of Business Administration (MBA) specializing in information system from Australia. After completing my MBA degree, I studied Professional Doctorate Program. I also completed a few other courses from Australia in the area of &quot; Australian Immigration and Citizenship Law&quot; from University of Technology Sydney (UTS), &quot;Foundation of Tax in Australian Taxation Law&quot; from Taxation Institute of Australia and &quot;International Travel and Ticketing Operations&quot; from AFTA.&nbsp;</p>\r\n\r\n<p>Current &amp; Previous community Involvements:</p>\r\n\r\n<p>1.) &nbsp;Rockdale Chamber of Commerce.</p>\r\n\r\n<p>== Secretary</p>\r\n\r\n<p>== Former Vice President</p>\r\n\r\n<p>2.) &nbsp;Multicultural Communities&rsquo; Council of NSW Inc.</p>\r\n\r\n<p>== Director</p>\r\n\r\n<p>3.) &nbsp;The Ethnic Communities Council of NSW Inc.</p>\r\n\r\n<p>== Former Vice Chair</p>\r\n\r\n<p>== Member of the Management committee</p>\r\n\r\n<p>&nbsp;4.) Marrickville Football Club</p>\r\n\r\n<p>== Member of the Management committee</p>\r\n\r\n<p>5.) Community Welfare First Inc.</p>\r\n\r\n<p>== Former President</p>\r\n\r\n<p>6.) Villawood Immigration Detention Centre</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;(Department of Immigration and Citizenship)</p>\r\n\r\n<p>== Committee member of DIAC&#39;s consultative Group&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; Committee</p>\r\n\r\n<p>7.) Migration Review Tribunal and Refugee&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; Review Tribunal</p>\r\n\r\n<p>==Member of the Community Liaison Meetings Committee</p>\r\n\r\n<p>8.) Department of Families &amp; community Services</p>\r\n\r\n<p>==Member of Multicultural Affairs advisory Group &nbsp;</p>\r\n\r\n<p>9.) Red Cross Australia</p>\r\n\r\n<p>==Member and Contributor</p>\r\n\r\n<p>10.) Amnesty International Australia</p>\r\n\r\n<p>= &nbsp; Member and Contributor</p>\r\n\r\n<p>11) Canterbury and District Soccer&nbsp;Football Association (CDSFA)</p>\r\n\r\n<p>&nbsp;= &nbsp; Former Grading Officer</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>My involvement in various community activities in numerous roles has given me a unique opportunity to mix with people from different backgrounds and understand their needs and concerns. I have a special interest in the Business community in terms of the Small to Large organizations in the Bayside Area and wish to work together to share our concerns &amp; other issues.</p>\r\n\r\n<p>With a wealth of local business experience, knowledge and community involvement, I have much to contribute to the members of the Bayside business community. &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-11-14 07:21:34'),
(19, 'Nazmy SALIB', 'Treasurer', 'Flower.jpg', '', '2017-11-14 07:54:47'),
(20, 'Soyeb AHMED', 'Board Member', 'IMAG0120.jpg', '', '2017-11-18 11:05:13'),
(21, 'Alam SMITH', 'Board Member', 'IMG_4928.JPG', '', '2017-11-15 05:43:13'),
(22, 'Khondoker Jalil ur Rahman KHAN', 'Board Member', 'IMG_4877.JPG', '<p>My Name is Jalilur Rahman KHAN. I am proud to get elected as a&nbsp; Board Member of Bayside Chamber of commerce and also would like to thank all members who supported me to be elected as a Board Member.</p><p>I am running a cleaning business in the Bayside area and as a local business person, I am always passionate to help other business people in the area and also support local community by providing my services.</p><p>I came to Australia with my family as an international student and hence we naturalised in the country. Before coming to Australia, I run clothing business in Garments industry.</p><p>I completed Bachelor of Commerce in Bangladesh before coming to Australia.</p><p>As being a business professional in the area, it is my privilege and great opportunity to help other business professionals in the bayside area in my capacity and I will try my best to do so by involving in the Chamber.&nbsp;&nbsp;&nbsp;</p>', '2017-11-15 05:43:25'),
(23, 'Reg SOARES', 'Board Member', 'Reg 1.jpg', '', '2017-11-28 09:25:14'),
(24, 'Saleh MOSTAFA', 'Board Member', 'F06E6AFB-866F-4A7C-AE3F-FAEB520F98BC.jpeg', '', '2017-11-28 13:44:27'),
(25, 'Abdur RAHIM', 'Board Member', 'Rahim.jpeg', '', '2017-11-29 03:10:37');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `photo_id` int(11) NOT NULL,
  `photo_name` text NOT NULL,
  `photo_title` varchar(255) NOT NULL,
  `photo_upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`photo_id`, `photo_name`, `photo_title`, `photo_upload_date`) VALUES
(1, 'DBEBC1A3-58E2-4E6B-83A8-179F2E584B0A.jpeg', 'Business peesentation 1', '2017-11-15 13:50:25'),
(2, '322C0617-A648-415D-8D59-7182A18D282F.jpeg', 'Business presentation 2 ', '2017-11-15 13:52:36'),
(3, 'D939F0DA-1F71-4A13-9209-25AF986B86C0.jpeg', 'Business presentation 3', '2017-12-05 13:43:44'),
(4, '590683AE-97F4-4AB9-A57F-D03C8FE95372.jpeg', 'Event 4', '2017-11-15 13:53:49'),
(5, '1508843226eid.jpg', 'Inauguration Ceremony', '2017-10-25 06:04:01'),
(6, '74515BA6-7C8C-4834-843A-7C3BE2C8F78A.jpeg', 'We are', '2017-12-05 13:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `privacy`
--

CREATE TABLE `privacy` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacy`
--

INSERT INTO `privacy` (`id`, `title`, `details`) VALUES
(2, 'TechSolutions BD fdesf', '<h3>1914 translation by H. Rackham</h3>\r\n\r\n<p>&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;</p>\r\n\r\n<h3>Section 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC</h3>\r\n\r\n<p>&quot;At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.&quot;</p>\r\n\r\n<h3>1914 translation by H. Rackham</h3>\r\n\r\n<p>&quot;On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.&quot;</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `details`) VALUES
(1, 'services', 'Our mission is to promote and meet the needs of business and industry and to create the best community in which to live, work and do business. The mission\" of the Bayside Chamber of Commerce is to provide leadership for the advancement of economic vitality and equality of life for the total community.');

-- --------------------------------------------------------

--
-- Table structure for table `slogan`
--

CREATE TABLE `slogan` (
  `slogan_id` int(11) NOT NULL,
  `slogan_title` varchar(255) NOT NULL,
  `slogan_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slogan`
--

INSERT INTO `slogan` (`slogan_id`, `slogan_title`, `slogan_date`) VALUES
(1, 'Your Membership renewal is due. Please pay now.', '2017-11-20 14:45:31'),
(2, 'Our Christmas party is on 5th December 2017 at Red Rose Function Centre, Rockdale.', '2017-11-20 14:46:58'),
(3, 'Please Book for your advertisement. First come first serve basis!!!', '2017-11-20 14:48:55'),
(4, 'Our Next Meeting is on 05 December 2017 at Advanced Diversity Services', '2017-11-20 14:47:46');

-- --------------------------------------------------------

--
-- Table structure for table `social_link`
--

CREATE TABLE `social_link` (
  `social_link_id` int(11) NOT NULL,
  `social_link` varchar(255) NOT NULL,
  `social_link_icon` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_link`
--

INSERT INTO `social_link` (`social_link_id`, `social_link`, `social_link_icon`) VALUES
(1, 'Bayside Chamber of Commerce', '<i class=\"fa fa-facebook\" aria-hidden=\"true\" style=\" color: #3a589e;\"></i> '),
(2, 'https://twitter.com/', '<i class=\"fa fa-twitter\" aria-hidden=\"true\" style=\"color: #00aced;\"></i>'),
(3, 'https://www.linkedin.com/', '<i class=\"fa fa-linkedin\" aria-hidden=\"true\" style=\"color: #225982;\"></i>'),
(4, 'https://www.youtube.com/', '<i class=\"fa fa-youtube\" aria-hidden=\"true\" style=\"color: #f00;\"></i> ');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `title`, `details`) VALUES
(2, 'TechSolutions BD2', '<h3>1914 translation by H. Rackham</h3>\r\n\r\n<p>&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;</p>\r\n\r\n<h3>Section 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC</h3>\r\n\r\n<p>&quot;At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.&quot;</p>\r\n\r\n<h3>1914 translation by H. Rackham</h3>\r\n\r\n<p>&quot;On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.&quot;</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `top_left_ad`
--

CREATE TABLE `top_left_ad` (
  `top_left_ad_id` int(11) NOT NULL,
  `top_left_ad_image` text NOT NULL,
  `top_left_ad_image_title` varchar(255) NOT NULL,
  `top_left_ad_link` varchar(255) NOT NULL,
  `top_left_ad_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `top_left_ad`
--

INSERT INTO `top_left_ad` (`top_left_ad_id`, `top_left_ad_image`, `top_left_ad_image_title`, `top_left_ad_link`, `top_left_ad_date`) VALUES
(1, 'ad222.jpg', '1', '#', '2017-11-08 07:51:17'),
(2, 'ad222.jpg', '2', '#', '2017-11-08 07:51:41'),
(3, 'ad222.jpg', '3', '#', '2017-11-08 07:51:49'),
(4, 'ad222.jpg', '4', '#', '2017-11-08 07:51:57'),
(5, 'ad222.jpg', '5', '#', '2017-11-08 07:52:05'),
(6, 'ad222.jpg', '6', '#', '2017-11-08 07:52:15');

-- --------------------------------------------------------

--
-- Table structure for table `top_right_ad`
--

CREATE TABLE `top_right_ad` (
  `top_right_ad_id` int(11) NOT NULL,
  `top_right_ad_image` text NOT NULL,
  `top_right_ad_image_title` varchar(255) NOT NULL,
  `top_right_ad_link` varchar(255) NOT NULL,
  `top_right_ad_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `top_right_ad`
--

INSERT INTO `top_right_ad` (`top_right_ad_id`, `top_right_ad_image`, `top_right_ad_image_title`, `top_right_ad_link`, `top_right_ad_date`) VALUES
(1, 'ad222.jpg', '1', '#', '2017-11-08 07:52:45'),
(2, 'ad222.jpg', '2', '#', '2017-11-08 07:52:53'),
(3, 'ad222.jpg', '3', '#', '2017-11-08 07:54:34'),
(4, 'ad222.jpg', '4', '#', '2017-11-08 07:54:41'),
(5, 'ad222.jpg', '5', '#', '2017-11-08 07:54:49'),
(6, 'ad222.jpg', '6', '#', '2017-11-08 07:54:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `user_id` int(11) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `given_name` varchar(255) NOT NULL,
  `date_of_birth` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `user_role` varchar(255) NOT NULL,
  `date_of_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`user_id`, `last_name`, `given_name`, `date_of_birth`, `email`, `password`, `user_role`, `date_of_creation`, `active_status`) VALUES
(48, 'BD', 'Techsolutions', '2018-11-07', 'ts.hosting2017@gmail.com', 'ashiq113015', 'regular', '2018-11-12 10:20:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `video_id` int(11) NOT NULL,
  `video_title` varchar(255) NOT NULL,
  `youtube_video_id` varchar(255) NOT NULL,
  `video_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`video_id`, `video_title`, `youtube_video_id`, `video_date`) VALUES
(3, 'Test1', 'nFmXyjG2lwA', '2017-10-30 07:40:36'),
(4, 'Test2', 'bfgcFJFE3mE', '2017-10-30 07:40:09'),
(5, 'Test3', 'jO58Zk826Tc', '2017-10-30 07:39:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us_governance`
--
ALTER TABLE `about_us_governance`
  ADD PRIMARY KEY (`governance_id`);

--
-- Indexes for table `about_us_history`
--
ALTER TABLE `about_us_history`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `about_us_members_forum`
--
ALTER TABLE `about_us_members_forum`
  ADD PRIMARY KEY (`members_forum_id`);

--
-- Indexes for table `about_us_our_mission`
--
ALTER TABLE `about_us_our_mission`
  ADD PRIMARY KEY (`our_mission_id`);

--
-- Indexes for table `about_us_our_staff`
--
ALTER TABLE `about_us_our_staff`
  ADD PRIMARY KEY (`our_staff_id`);

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`album_id`);

--
-- Indexes for table `album_photo`
--
ALTER TABLE `album_photo`
  ADD PRIMARY KEY (`album_photo_id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `bottom_left_ad`
--
ALTER TABLE `bottom_left_ad`
  ADD PRIMARY KEY (`bottom_left_ad_id`);

--
-- Indexes for table `bottom_right_ad`
--
ALTER TABLE `bottom_right_ad`
  ADD PRIMARY KEY (`bottom_right_ad_id`);

--
-- Indexes for table `business_category`
--
ALTER TABLE `business_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `business_directory_ad`
--
ALTER TABLE `business_directory_ad`
  ADD PRIMARY KEY (`bd_ad_id`);

--
-- Indexes for table `business_directory_category`
--
ALTER TABLE `business_directory_category`
  ADD PRIMARY KEY (`bdc_id`);

--
-- Indexes for table `business_directory_details`
--
ALTER TABLE `business_directory_details`
  ADD PRIMARY KEY (`bd_id`);

--
-- Indexes for table `business_directory_image`
--
ALTER TABLE `business_directory_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_directory_video`
--
ALTER TABLE `business_directory_video`
  ADD PRIMARY KEY (`bdv_id`);

--
-- Indexes for table `business_organization`
--
ALTER TABLE `business_organization`
  ADD PRIMARY KEY (`b_o_id`);

--
-- Indexes for table `business_resource`
--
ALTER TABLE `business_resource`
  ADD PRIMARY KEY (`b_r_id`);

--
-- Indexes for table `client_feedback`
--
ALTER TABLE `client_feedback`
  ADD PRIMARY KEY (`client_feedback_id`);

--
-- Indexes for table `condition_of_use`
--
ALTER TABLE `condition_of_use`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`contact_us_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `interview`
--
ALTER TABLE `interview`
  ADD PRIMARY KEY (`in_id`);

--
-- Indexes for table `join_now_content`
--
ALTER TABLE `join_now_content`
  ADD PRIMARY KEY (`join_now_content_id`);

--
-- Indexes for table `join_now_info`
--
ALTER TABLE `join_now_info`
  ADD PRIMARY KEY (`join_now_info_id`);

--
-- Indexes for table `members_benifit`
--
ALTER TABLE `members_benifit`
  ADD PRIMARY KEY (`members_benifit_id`);

--
-- Indexes for table `member_discount`
--
ALTER TABLE `member_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_to_member`
--
ALTER TABLE `member_to_member`
  ADD PRIMARY KEY (`member_to_member_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `our_board`
--
ALTER TABLE `our_board`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `privacy`
--
ALTER TABLE `privacy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slogan`
--
ALTER TABLE `slogan`
  ADD PRIMARY KEY (`slogan_id`);

--
-- Indexes for table `social_link`
--
ALTER TABLE `social_link`
  ADD PRIMARY KEY (`social_link_id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `top_left_ad`
--
ALTER TABLE `top_left_ad`
  ADD PRIMARY KEY (`top_left_ad_id`);

--
-- Indexes for table `top_right_ad`
--
ALTER TABLE `top_right_ad`
  ADD PRIMARY KEY (`top_right_ad_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us_governance`
--
ALTER TABLE `about_us_governance`
  MODIFY `governance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `about_us_history`
--
ALTER TABLE `about_us_history`
  MODIFY `history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `about_us_members_forum`
--
ALTER TABLE `about_us_members_forum`
  MODIFY `members_forum_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `about_us_our_mission`
--
ALTER TABLE `about_us_our_mission`
  MODIFY `our_mission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `about_us_our_staff`
--
ALTER TABLE `about_us_our_staff`
  MODIFY `our_staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `album_photo`
--
ALTER TABLE `album_photo`
  MODIFY `album_photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `bottom_left_ad`
--
ALTER TABLE `bottom_left_ad`
  MODIFY `bottom_left_ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `bottom_right_ad`
--
ALTER TABLE `bottom_right_ad`
  MODIFY `bottom_right_ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `business_category`
--
ALTER TABLE `business_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `business_directory_ad`
--
ALTER TABLE `business_directory_ad`
  MODIFY `bd_ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=695;
--
-- AUTO_INCREMENT for table `business_directory_category`
--
ALTER TABLE `business_directory_category`
  MODIFY `bdc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `business_directory_details`
--
ALTER TABLE `business_directory_details`
  MODIFY `bd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `business_directory_image`
--
ALTER TABLE `business_directory_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `business_directory_video`
--
ALTER TABLE `business_directory_video`
  MODIFY `bdv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `business_organization`
--
ALTER TABLE `business_organization`
  MODIFY `b_o_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `business_resource`
--
ALTER TABLE `business_resource`
  MODIFY `b_r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `client_feedback`
--
ALTER TABLE `client_feedback`
  MODIFY `client_feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `condition_of_use`
--
ALTER TABLE `condition_of_use`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `contact_us_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `interview`
--
ALTER TABLE `interview`
  MODIFY `in_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `join_now_content`
--
ALTER TABLE `join_now_content`
  MODIFY `join_now_content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `join_now_info`
--
ALTER TABLE `join_now_info`
  MODIFY `join_now_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `members_benifit`
--
ALTER TABLE `members_benifit`
  MODIFY `members_benifit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member_discount`
--
ALTER TABLE `member_discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `member_to_member`
--
ALTER TABLE `member_to_member`
  MODIFY `member_to_member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `our_board`
--
ALTER TABLE `our_board`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `privacy`
--
ALTER TABLE `privacy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slogan`
--
ALTER TABLE `slogan`
  MODIFY `slogan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `social_link`
--
ALTER TABLE `social_link`
  MODIFY `social_link_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `top_left_ad`
--
ALTER TABLE `top_left_ad`
  MODIFY `top_left_ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `top_right_ad`
--
ALTER TABLE `top_right_ad`
  MODIFY `top_right_ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
